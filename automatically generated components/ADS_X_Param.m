classdef ADS_X_Param < ADSnodeless
    % ADS_X_Param matlab representation for the ADS X_Param component
    % X-Parameters Extraction Analysis
    % X_Param [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Output X-Parameters in GMDIF file (s---b) 
        GmdifEnabled
        % GMDIF file name (s---s) 
        GmdifFile
        % HB analysis name (s---s) 
        HB_Analysis
        % User defined freq SweepVar/SweepPlan (s---s) 
        FreqSweep
        % User defined non-freq SweepVar/SweepPlan (s---s) 
        UserSweep
        % Enable X_Param extraction (s---b) 
        ExtractionEnabled
        % Max order for XParam extraction (s---i) 
        XParamMaxOrder
        % Version of file to be output (s---s) 
        XFileVersion
        % Use `Restart' only after a failed extraction point (s---b) 
        AutoRestart
        % Single loads - eliminate independent vars and adjust X-parameter terms (s---b) 
        AdjustTerms
    end
    methods
        function obj = set.GmdifEnabled(obj,val)
            obj = setParameter(obj,'GmdifEnabled',val,0,'boolean');
        end
        function res = get.GmdifEnabled(obj)
            res = getParameter(obj,'GmdifEnabled');
        end
        function obj = set.GmdifFile(obj,val)
            obj = setParameter(obj,'GmdifFile',val,0,'string');
        end
        function res = get.GmdifFile(obj)
            res = getParameter(obj,'GmdifFile');
        end
        function obj = set.HB_Analysis(obj,val)
            obj = setParameter(obj,'HB_Analysis',val,0,'string');
        end
        function res = get.HB_Analysis(obj)
            res = getParameter(obj,'HB_Analysis');
        end
        function obj = set.FreqSweep(obj,val)
            obj = setParameter(obj,'FreqSweep',val,0,'string');
        end
        function res = get.FreqSweep(obj)
            res = getParameter(obj,'FreqSweep');
        end
        function obj = set.UserSweep(obj,val)
            obj = setParameter(obj,'UserSweep',val,0,'string');
        end
        function res = get.UserSweep(obj)
            res = getParameter(obj,'UserSweep');
        end
        function obj = set.ExtractionEnabled(obj,val)
            obj = setParameter(obj,'ExtractionEnabled',val,0,'boolean');
        end
        function res = get.ExtractionEnabled(obj)
            res = getParameter(obj,'ExtractionEnabled');
        end
        function obj = set.XParamMaxOrder(obj,val)
            obj = setParameter(obj,'XParamMaxOrder',val,0,'integer');
        end
        function res = get.XParamMaxOrder(obj)
            res = getParameter(obj,'XParamMaxOrder');
        end
        function obj = set.XFileVersion(obj,val)
            obj = setParameter(obj,'XFileVersion',val,0,'string');
        end
        function res = get.XFileVersion(obj)
            res = getParameter(obj,'XFileVersion');
        end
        function obj = set.AutoRestart(obj,val)
            obj = setParameter(obj,'AutoRestart',val,0,'boolean');
        end
        function res = get.AutoRestart(obj)
            res = getParameter(obj,'AutoRestart');
        end
        function obj = set.AdjustTerms(obj,val)
            obj = setParameter(obj,'AdjustTerms',val,0,'boolean');
        end
        function res = get.AdjustTerms(obj)
            res = getParameter(obj,'AdjustTerms');
        end
    end
end

classdef ADS_GaAs_Model < ADSmodel
    % ADS_GaAs_Model matlab representation for the ADS GaAs_Model component
    % GaAs MESFET Transistor model
    % model ModelName GaAs ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % N type GaAs MESFET (s---b) 
        NFET
        % P type GaAs MESFET (s---b) 
        PFET
        % 1=CQ 2=CC 3=Statz 4=Materka 5=Tajima 6=symbolic 7=TOM 8=Modified Materka 9=TriQuint Materka (s---i) 
        Idsmod
        % 0=none 1=linear 2=junction 3=Statz Charge 4=symbolic 5=Statz Cap (s---i) 
        Gscap
        % 0=none 1=linear 2=diode (s---i) 
        Gsfwd
        % 0=none 1=linear 2=diode 3=symbolic (s---i) 
        Gsrev
        % 0=none 1=linear 2=junction 3=Statz 4=symbolic (s---i) 
        Gdcap
        % 0=none 1=linear 2=diode (s---i) 
        Gdfwd
        % 0=none 1=linear 2=diode 3=symbolic (s---i) 
        Gdrev
        % Pinch-off voltage (smorr) Unit: V
        Vto
        % Transconductance parameter (smorr) Unit: A/V^2
        Beta
        % Channel length modulation parameter (smorr) Unit: V^-1
        Lambda
        % Model 2: Ids coefficient constant (smorr) Unit: A
        A0
        % Model 2: Ids linear coefficient (smorr) Unit: S
        A1
        % Model 2: Ids quadratic coefficient (smorr) Unit: A/V^2
        A2
        % Model 2: Ids cubic coefficient (smorr) Unit: A/V^3
        A3
        % Model 2: Pinch off change with Vds (smorr) 
        Beta2
        % Saturation voltage: tanh parameter (alpha = gamma2) (smorr) Unit: V
        Alpha
        % Saturation voltage: tanh parameter (alpha = gamma2) (smorr) Unit: V
        Gamma
        % Model 2: DC conductance at Vgs=0 (smorr) Unit: Ohms
        Rds0
        % Model 2: Vds at `rds0' meaured bias (smorr) Unit: V
        Vdsdc
        % Model 2: Vds at `Ai' measured bias (smorr) Unit: V
        Vds0
        % Model 3: Doping tail extending parameter (smorr) Unit: V^-1
        B
        % Model 4: Idss value (smorr) Unit: A
        Idss
        % Model 5: `a' coefficient (smorr) 
        Ta
        % Model 5: `b' coefficient (smorr) 
        Tb
        % Model 5: `m' coefficient (smorr) 
        Tm
        % Model 5: drain current saturation voltage (smorr) Unit: V
        Vdss
        % Model 7: exponential factor in drain current (smorr) 
        Q
        % Model 7: drain current thermal factor (smorr) Unit: 1/W
        Tqdelta
        % Model 7: pinch-off change with vds (smorr) 
        Tqgamma
        % parameter for critical field for mobility degradation (smorr) 
        Ucrit
        % exponential parameter for advanced curtice model (smorr) 
        Vgexp
        % describes the effective pinch-off combined with vds (smorr) 
        Gamds
        % linear region slope of vgs=0 drain characteristic (smorr) Unit: S
        Sl
        % dependence on vgs of drain slope in linear region (smorr) Unit: V^-1
        Kg
        % saturation region drain slope characteristic at vgs=0 (smorr) Unit: S
        Ss
        % exponent defining dependence of saturation current (smorr) 
        Ee
        % parameter describing dependence on gate voltage (smorr) Unit: V^-1
        Ke
        % parameter (smorr) Unit: V^-1
        Kd
        %  (smorr) 
        Ss0
        %  (smorr) 
        Ss1
        % Linear temperature coefficient for RG 1/degC (smorr) Unit: 1/deg C
        Trg1
        % Linear temperature coefficient for RS 1/degC (smorr) Unit: 1/deg C
        Trs1
        % Linear temperature coefficient for RD 1/degC (smorr) Unit: 1/deg C
        Trd1
        % Linear temperature coefficient for Beta (smorr) 
        Betatce
        % Linear temperature coefficient for pinchoff voltage (smorr) Unit: 1/deg C
        Vtotc
        % gate finger width for (in meters). (smorr) Unit: m
        Ugw
        % number of gate fingers. (sm-ri) 
        Ngf
        % Model 6: expression ids(_v1, _v2) {Vds, Vgc} (smo-r) Unit: A
        Ids
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Gate resistance (smorr) Unit: Ohms
        Rg
        % Drain inductance (smorr) Unit: H
        Ld
        % Source inductance (smorr) Unit: H
        Ls
        % Gate inductance (smorr) Unit: H
        Lg
        % Drain source capacitance (smorr) Unit: F
        Cds
        % R for frequency dependent output conductance (smorr) Unit: Ohms
        Rc
        % C for frequency dependent output conductance (smorr) Unit: F
        Crf
        % Gate-source capacitance (smorr) Unit: F
        Cgs
        % Gate-drain capacitance (smorr) Unit: F
        Cgd
        % expression qgs(_v1, _v2) {Vgsc, Vgdc} (smo-r) Unit: C
        Qgs
        % expression qgd(_v1, _v2) {Vgdc, Vgsc} (smo-r) Unit: C
        Qgd
        % Band gap (smorr) Unit: eV
        Eg
        % Forward-bias depletion capacitance threshold (smorr) 
        Fc
        % Gate fwd saturation current (is = js) (smorr) Unit: A
        Is
        % Emission coefficient (smorr) 
        N
        % Saturation current temperature exponent (xti=pt) (smorr) 
        Xti
        % Explosion current (smorr) Unit: A
        Imax
        % Explosion current (smorr) Unit: A
        Imelt
        % Gate Source forward bias resistance (smorr) Unit: Ohms
        Rf
        % Vg(s,d)c includes voltage across Rg(s,d) (s---b) 
        Vgr
        % Gate Source resistance (rin = rgs) (smorr) Unit: Ohms
        Rgs
        % Gate Source resistance (rin = rgs) (smorr) Unit: Ohms
        Ris
        % Gate Drain resistance (smorr) Unit: Ohms
        Rgd
        % Gate capacitance pinch-off transition width (smorr) Unit: V
        Delta
        % Capacitance saturation voltage parameter (smorr) Unit: V
        Delta1
        % P-N junction diode grading coefficient (smorr) 
        M
        % expression igd(_v1, _v2) {Vgd, Vgs} (smo-r) Unit: A
        Igd
        % expression igs(_v1, _v2) {Vgs, Vgd} (smo-r) Unit: A
        Igs
        % Gate-Drain breakdown resistance (smorr) Unit: Ohms
        R1
        % Vbr change due to Ids (smorr) Unit: Ohms
        R2
        % Breakdown voltage (smorr) Unit: V
        Vbr
        % Gate rev saturation current (smorr) Unit: A
        Ir
        % Breakdown junction potential. (smorr) Unit: V
        Vjr
        % Use 2nd order Bessel polynomial to model tau effect in transient. (s---b) 
        Taumdl
        % Backgate voltage = bgf(freq) * Vds (smorc) 
        Bgf
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise above ambient (smorr) Unit: deg C
        Trise
        % Ids control voltage = vcf(freq) * Vg(s,d)c (smorc) 
        Vcf
        % Internal time delay (smorr) Unit: s
        Tau
        % Time delay proportionality constant for Vds (smorr) Unit: s/V
        A5
        % Gate junction potential (vbi = pb) (smorr) Unit: V
        Pb
        % Ids temperature coefficient (smorr) Unit: A/deg C
        Idstc
        % Maximum junction voltage before capacitance limiting (smorr) 
        Vmax
        % Temperature coefficient for triquint junction capacitance (smorr) 
        Tqm
        % Flicker-noise corner frequency (smorr) Unit: Hz
        Fnc
        % gate noise coefficient (smorr) 
        R
        % drain noise coefficient (smorr) 
        P
        % gate-drain noise correlation coefficient (smorr) 
        C
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Secured model parameters (s---b) 
        Secured
        % Statz model uses transcapacitor (YES,NO) (s---b) 
        Transcap
        % Scalable portion of threshold voltage. (smorr) Unit: V
        Vtosc
        % Gate metal resistance (smorr) Unit: Ohms
        Rgmet
        % Model 7: AC pinch-off change with vds (smorr) 
        TqgammaAc
        % R for frequency dependent output conductance (smorr) Unit: Ohms
        Rdb
        % C for frequency dependent output captance (smorr) Unit: F
        Cbs
        % Schottky current exponent multiplier (smorr) Unit: V^-1
        Alfg
        % Breakdown exponent fitting factor (smorr) Unit: V^-1
        Alpvb
        % Fitting factor in Igd current exponent (smorr) Unit: V^-1
        AlfdA
        % Fitting factor in Igd current exponent (smorr) Unit: V^-2
        AlfdB
        % Gate-source leakage resistance (smorr) Unit: Ohms
        RLgs
        % Gate-drain leakage resistance (smorr) Unit: Ohms
        RLgd
        % Gate junction forward bias (warning) (s--rr) Unit: V
        wVgfwd
        % Gate-source reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgs
        % Gate-drain reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgd
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
    end
    methods
        function obj = set.NFET(obj,val)
            obj = setParameter(obj,'NFET',val,0,'boolean');
        end
        function res = get.NFET(obj)
            res = getParameter(obj,'NFET');
        end
        function obj = set.PFET(obj,val)
            obj = setParameter(obj,'PFET',val,0,'boolean');
        end
        function res = get.PFET(obj)
            res = getParameter(obj,'PFET');
        end
        function obj = set.Idsmod(obj,val)
            obj = setParameter(obj,'Idsmod',val,0,'integer');
        end
        function res = get.Idsmod(obj)
            res = getParameter(obj,'Idsmod');
        end
        function obj = set.Gscap(obj,val)
            obj = setParameter(obj,'Gscap',val,0,'integer');
        end
        function res = get.Gscap(obj)
            res = getParameter(obj,'Gscap');
        end
        function obj = set.Gsfwd(obj,val)
            obj = setParameter(obj,'Gsfwd',val,0,'integer');
        end
        function res = get.Gsfwd(obj)
            res = getParameter(obj,'Gsfwd');
        end
        function obj = set.Gsrev(obj,val)
            obj = setParameter(obj,'Gsrev',val,0,'integer');
        end
        function res = get.Gsrev(obj)
            res = getParameter(obj,'Gsrev');
        end
        function obj = set.Gdcap(obj,val)
            obj = setParameter(obj,'Gdcap',val,0,'integer');
        end
        function res = get.Gdcap(obj)
            res = getParameter(obj,'Gdcap');
        end
        function obj = set.Gdfwd(obj,val)
            obj = setParameter(obj,'Gdfwd',val,0,'integer');
        end
        function res = get.Gdfwd(obj)
            res = getParameter(obj,'Gdfwd');
        end
        function obj = set.Gdrev(obj,val)
            obj = setParameter(obj,'Gdrev',val,0,'integer');
        end
        function res = get.Gdrev(obj)
            res = getParameter(obj,'Gdrev');
        end
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.Beta(obj,val)
            obj = setParameter(obj,'Beta',val,0,'real');
        end
        function res = get.Beta(obj)
            res = getParameter(obj,'Beta');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.A0(obj,val)
            obj = setParameter(obj,'A0',val,0,'real');
        end
        function res = get.A0(obj)
            res = getParameter(obj,'A0');
        end
        function obj = set.A1(obj,val)
            obj = setParameter(obj,'A1',val,0,'real');
        end
        function res = get.A1(obj)
            res = getParameter(obj,'A1');
        end
        function obj = set.A2(obj,val)
            obj = setParameter(obj,'A2',val,0,'real');
        end
        function res = get.A2(obj)
            res = getParameter(obj,'A2');
        end
        function obj = set.A3(obj,val)
            obj = setParameter(obj,'A3',val,0,'real');
        end
        function res = get.A3(obj)
            res = getParameter(obj,'A3');
        end
        function obj = set.Beta2(obj,val)
            obj = setParameter(obj,'Beta2',val,0,'real');
        end
        function res = get.Beta2(obj)
            res = getParameter(obj,'Beta2');
        end
        function obj = set.Alpha(obj,val)
            obj = setParameter(obj,'Alpha',val,0,'real');
        end
        function res = get.Alpha(obj)
            res = getParameter(obj,'Alpha');
        end
        function obj = set.Gamma(obj,val)
            obj = setParameter(obj,'Gamma',val,0,'real');
        end
        function res = get.Gamma(obj)
            res = getParameter(obj,'Gamma');
        end
        function obj = set.Rds0(obj,val)
            obj = setParameter(obj,'Rds0',val,0,'real');
        end
        function res = get.Rds0(obj)
            res = getParameter(obj,'Rds0');
        end
        function obj = set.Vdsdc(obj,val)
            obj = setParameter(obj,'Vdsdc',val,0,'real');
        end
        function res = get.Vdsdc(obj)
            res = getParameter(obj,'Vdsdc');
        end
        function obj = set.Vds0(obj,val)
            obj = setParameter(obj,'Vds0',val,0,'real');
        end
        function res = get.Vds0(obj)
            res = getParameter(obj,'Vds0');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.Idss(obj,val)
            obj = setParameter(obj,'Idss',val,0,'real');
        end
        function res = get.Idss(obj)
            res = getParameter(obj,'Idss');
        end
        function obj = set.Ta(obj,val)
            obj = setParameter(obj,'Ta',val,0,'real');
        end
        function res = get.Ta(obj)
            res = getParameter(obj,'Ta');
        end
        function obj = set.Tb(obj,val)
            obj = setParameter(obj,'Tb',val,0,'real');
        end
        function res = get.Tb(obj)
            res = getParameter(obj,'Tb');
        end
        function obj = set.Tm(obj,val)
            obj = setParameter(obj,'Tm',val,0,'real');
        end
        function res = get.Tm(obj)
            res = getParameter(obj,'Tm');
        end
        function obj = set.Vdss(obj,val)
            obj = setParameter(obj,'Vdss',val,0,'real');
        end
        function res = get.Vdss(obj)
            res = getParameter(obj,'Vdss');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.Tqdelta(obj,val)
            obj = setParameter(obj,'Tqdelta',val,0,'real');
        end
        function res = get.Tqdelta(obj)
            res = getParameter(obj,'Tqdelta');
        end
        function obj = set.Tqgamma(obj,val)
            obj = setParameter(obj,'Tqgamma',val,0,'real');
        end
        function res = get.Tqgamma(obj)
            res = getParameter(obj,'Tqgamma');
        end
        function obj = set.Ucrit(obj,val)
            obj = setParameter(obj,'Ucrit',val,0,'real');
        end
        function res = get.Ucrit(obj)
            res = getParameter(obj,'Ucrit');
        end
        function obj = set.Vgexp(obj,val)
            obj = setParameter(obj,'Vgexp',val,0,'real');
        end
        function res = get.Vgexp(obj)
            res = getParameter(obj,'Vgexp');
        end
        function obj = set.Gamds(obj,val)
            obj = setParameter(obj,'Gamds',val,0,'real');
        end
        function res = get.Gamds(obj)
            res = getParameter(obj,'Gamds');
        end
        function obj = set.Sl(obj,val)
            obj = setParameter(obj,'Sl',val,0,'real');
        end
        function res = get.Sl(obj)
            res = getParameter(obj,'Sl');
        end
        function obj = set.Kg(obj,val)
            obj = setParameter(obj,'Kg',val,0,'real');
        end
        function res = get.Kg(obj)
            res = getParameter(obj,'Kg');
        end
        function obj = set.Ss(obj,val)
            obj = setParameter(obj,'Ss',val,0,'real');
        end
        function res = get.Ss(obj)
            res = getParameter(obj,'Ss');
        end
        function obj = set.Ee(obj,val)
            obj = setParameter(obj,'Ee',val,0,'real');
        end
        function res = get.Ee(obj)
            res = getParameter(obj,'Ee');
        end
        function obj = set.Ke(obj,val)
            obj = setParameter(obj,'Ke',val,0,'real');
        end
        function res = get.Ke(obj)
            res = getParameter(obj,'Ke');
        end
        function obj = set.Kd(obj,val)
            obj = setParameter(obj,'Kd',val,0,'real');
        end
        function res = get.Kd(obj)
            res = getParameter(obj,'Kd');
        end
        function obj = set.Ss0(obj,val)
            obj = setParameter(obj,'Ss0',val,0,'real');
        end
        function res = get.Ss0(obj)
            res = getParameter(obj,'Ss0');
        end
        function obj = set.Ss1(obj,val)
            obj = setParameter(obj,'Ss1',val,0,'real');
        end
        function res = get.Ss1(obj)
            res = getParameter(obj,'Ss1');
        end
        function obj = set.Trg1(obj,val)
            obj = setParameter(obj,'Trg1',val,0,'real');
        end
        function res = get.Trg1(obj)
            res = getParameter(obj,'Trg1');
        end
        function obj = set.Trs1(obj,val)
            obj = setParameter(obj,'Trs1',val,0,'real');
        end
        function res = get.Trs1(obj)
            res = getParameter(obj,'Trs1');
        end
        function obj = set.Trd1(obj,val)
            obj = setParameter(obj,'Trd1',val,0,'real');
        end
        function res = get.Trd1(obj)
            res = getParameter(obj,'Trd1');
        end
        function obj = set.Betatce(obj,val)
            obj = setParameter(obj,'Betatce',val,0,'real');
        end
        function res = get.Betatce(obj)
            res = getParameter(obj,'Betatce');
        end
        function obj = set.Vtotc(obj,val)
            obj = setParameter(obj,'Vtotc',val,0,'real');
        end
        function res = get.Vtotc(obj)
            res = getParameter(obj,'Vtotc');
        end
        function obj = set.Ugw(obj,val)
            obj = setParameter(obj,'Ugw',val,0,'real');
        end
        function res = get.Ugw(obj)
            res = getParameter(obj,'Ugw');
        end
        function obj = set.Ngf(obj,val)
            obj = setParameter(obj,'Ngf',val,0,'integer');
        end
        function res = get.Ngf(obj)
            res = getParameter(obj,'Ngf');
        end
        function obj = set.Ids(obj,val)
            obj = setParameter(obj,'Ids',val,0,'real');
        end
        function res = get.Ids(obj)
            res = getParameter(obj,'Ids');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Ld(obj,val)
            obj = setParameter(obj,'Ld',val,0,'real');
        end
        function res = get.Ld(obj)
            res = getParameter(obj,'Ld');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Lg(obj,val)
            obj = setParameter(obj,'Lg',val,0,'real');
        end
        function res = get.Lg(obj)
            res = getParameter(obj,'Lg');
        end
        function obj = set.Cds(obj,val)
            obj = setParameter(obj,'Cds',val,0,'real');
        end
        function res = get.Cds(obj)
            res = getParameter(obj,'Cds');
        end
        function obj = set.Rc(obj,val)
            obj = setParameter(obj,'Rc',val,0,'real');
        end
        function res = get.Rc(obj)
            res = getParameter(obj,'Rc');
        end
        function obj = set.Crf(obj,val)
            obj = setParameter(obj,'Crf',val,0,'real');
        end
        function res = get.Crf(obj)
            res = getParameter(obj,'Crf');
        end
        function obj = set.Cgs(obj,val)
            obj = setParameter(obj,'Cgs',val,0,'real');
        end
        function res = get.Cgs(obj)
            res = getParameter(obj,'Cgs');
        end
        function obj = set.Cgd(obj,val)
            obj = setParameter(obj,'Cgd',val,0,'real');
        end
        function res = get.Cgd(obj)
            res = getParameter(obj,'Cgd');
        end
        function obj = set.Qgs(obj,val)
            obj = setParameter(obj,'Qgs',val,0,'real');
        end
        function res = get.Qgs(obj)
            res = getParameter(obj,'Qgs');
        end
        function obj = set.Qgd(obj,val)
            obj = setParameter(obj,'Qgd',val,0,'real');
        end
        function res = get.Qgd(obj)
            res = getParameter(obj,'Qgd');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Rf(obj,val)
            obj = setParameter(obj,'Rf',val,0,'real');
        end
        function res = get.Rf(obj)
            res = getParameter(obj,'Rf');
        end
        function obj = set.Vgr(obj,val)
            obj = setParameter(obj,'Vgr',val,0,'boolean');
        end
        function res = get.Vgr(obj)
            res = getParameter(obj,'Vgr');
        end
        function obj = set.Rgs(obj,val)
            obj = setParameter(obj,'Rgs',val,0,'real');
        end
        function res = get.Rgs(obj)
            res = getParameter(obj,'Rgs');
        end
        function obj = set.Ris(obj,val)
            obj = setParameter(obj,'Ris',val,0,'real');
        end
        function res = get.Ris(obj)
            res = getParameter(obj,'Ris');
        end
        function obj = set.Rgd(obj,val)
            obj = setParameter(obj,'Rgd',val,0,'real');
        end
        function res = get.Rgd(obj)
            res = getParameter(obj,'Rgd');
        end
        function obj = set.Delta(obj,val)
            obj = setParameter(obj,'Delta',val,0,'real');
        end
        function res = get.Delta(obj)
            res = getParameter(obj,'Delta');
        end
        function obj = set.Delta1(obj,val)
            obj = setParameter(obj,'Delta1',val,0,'real');
        end
        function res = get.Delta1(obj)
            res = getParameter(obj,'Delta1');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Igd(obj,val)
            obj = setParameter(obj,'Igd',val,0,'real');
        end
        function res = get.Igd(obj)
            res = getParameter(obj,'Igd');
        end
        function obj = set.Igs(obj,val)
            obj = setParameter(obj,'Igs',val,0,'real');
        end
        function res = get.Igs(obj)
            res = getParameter(obj,'Igs');
        end
        function obj = set.R1(obj,val)
            obj = setParameter(obj,'R1',val,0,'real');
        end
        function res = get.R1(obj)
            res = getParameter(obj,'R1');
        end
        function obj = set.R2(obj,val)
            obj = setParameter(obj,'R2',val,0,'real');
        end
        function res = get.R2(obj)
            res = getParameter(obj,'R2');
        end
        function obj = set.Vbr(obj,val)
            obj = setParameter(obj,'Vbr',val,0,'real');
        end
        function res = get.Vbr(obj)
            res = getParameter(obj,'Vbr');
        end
        function obj = set.Ir(obj,val)
            obj = setParameter(obj,'Ir',val,0,'real');
        end
        function res = get.Ir(obj)
            res = getParameter(obj,'Ir');
        end
        function obj = set.Vjr(obj,val)
            obj = setParameter(obj,'Vjr',val,0,'real');
        end
        function res = get.Vjr(obj)
            res = getParameter(obj,'Vjr');
        end
        function obj = set.Taumdl(obj,val)
            obj = setParameter(obj,'Taumdl',val,0,'boolean');
        end
        function res = get.Taumdl(obj)
            res = getParameter(obj,'Taumdl');
        end
        function obj = set.Bgf(obj,val)
            obj = setParameter(obj,'Bgf',val,0,'complex');
        end
        function res = get.Bgf(obj)
            res = getParameter(obj,'Bgf');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Vcf(obj,val)
            obj = setParameter(obj,'Vcf',val,0,'complex');
        end
        function res = get.Vcf(obj)
            res = getParameter(obj,'Vcf');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.A5(obj,val)
            obj = setParameter(obj,'A5',val,0,'real');
        end
        function res = get.A5(obj)
            res = getParameter(obj,'A5');
        end
        function obj = set.Pb(obj,val)
            obj = setParameter(obj,'Pb',val,0,'real');
        end
        function res = get.Pb(obj)
            res = getParameter(obj,'Pb');
        end
        function obj = set.Idstc(obj,val)
            obj = setParameter(obj,'Idstc',val,0,'real');
        end
        function res = get.Idstc(obj)
            res = getParameter(obj,'Idstc');
        end
        function obj = set.Vmax(obj,val)
            obj = setParameter(obj,'Vmax',val,0,'real');
        end
        function res = get.Vmax(obj)
            res = getParameter(obj,'Vmax');
        end
        function obj = set.Tqm(obj,val)
            obj = setParameter(obj,'Tqm',val,0,'real');
        end
        function res = get.Tqm(obj)
            res = getParameter(obj,'Tqm');
        end
        function obj = set.Fnc(obj,val)
            obj = setParameter(obj,'Fnc',val,0,'real');
        end
        function res = get.Fnc(obj)
            res = getParameter(obj,'Fnc');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.P(obj,val)
            obj = setParameter(obj,'P',val,0,'real');
        end
        function res = get.P(obj)
            res = getParameter(obj,'P');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.Transcap(obj,val)
            obj = setParameter(obj,'Transcap',val,0,'boolean');
        end
        function res = get.Transcap(obj)
            res = getParameter(obj,'Transcap');
        end
        function obj = set.Vtosc(obj,val)
            obj = setParameter(obj,'Vtosc',val,0,'real');
        end
        function res = get.Vtosc(obj)
            res = getParameter(obj,'Vtosc');
        end
        function obj = set.Rgmet(obj,val)
            obj = setParameter(obj,'Rgmet',val,0,'real');
        end
        function res = get.Rgmet(obj)
            res = getParameter(obj,'Rgmet');
        end
        function obj = set.TqgammaAc(obj,val)
            obj = setParameter(obj,'TqgammaAc',val,0,'real');
        end
        function res = get.TqgammaAc(obj)
            res = getParameter(obj,'TqgammaAc');
        end
        function obj = set.Rdb(obj,val)
            obj = setParameter(obj,'Rdb',val,0,'real');
        end
        function res = get.Rdb(obj)
            res = getParameter(obj,'Rdb');
        end
        function obj = set.Cbs(obj,val)
            obj = setParameter(obj,'Cbs',val,0,'real');
        end
        function res = get.Cbs(obj)
            res = getParameter(obj,'Cbs');
        end
        function obj = set.Alfg(obj,val)
            obj = setParameter(obj,'Alfg',val,0,'real');
        end
        function res = get.Alfg(obj)
            res = getParameter(obj,'Alfg');
        end
        function obj = set.Alpvb(obj,val)
            obj = setParameter(obj,'Alpvb',val,0,'real');
        end
        function res = get.Alpvb(obj)
            res = getParameter(obj,'Alpvb');
        end
        function obj = set.AlfdA(obj,val)
            obj = setParameter(obj,'AlfdA',val,0,'real');
        end
        function res = get.AlfdA(obj)
            res = getParameter(obj,'AlfdA');
        end
        function obj = set.AlfdB(obj,val)
            obj = setParameter(obj,'AlfdB',val,0,'real');
        end
        function res = get.AlfdB(obj)
            res = getParameter(obj,'AlfdB');
        end
        function obj = set.RLgs(obj,val)
            obj = setParameter(obj,'RLgs',val,0,'real');
        end
        function res = get.RLgs(obj)
            res = getParameter(obj,'RLgs');
        end
        function obj = set.RLgd(obj,val)
            obj = setParameter(obj,'RLgd',val,0,'real');
        end
        function res = get.RLgd(obj)
            res = getParameter(obj,'RLgd');
        end
        function obj = set.wVgfwd(obj,val)
            obj = setParameter(obj,'wVgfwd',val,0,'real');
        end
        function res = get.wVgfwd(obj)
            res = getParameter(obj,'wVgfwd');
        end
        function obj = set.wBvgs(obj,val)
            obj = setParameter(obj,'wBvgs',val,0,'real');
        end
        function res = get.wBvgs(obj)
            res = getParameter(obj,'wBvgs');
        end
        function obj = set.wBvgd(obj,val)
            obj = setParameter(obj,'wBvgd',val,0,'real');
        end
        function res = get.wBvgd(obj)
            res = getParameter(obj,'wBvgd');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
    end
end

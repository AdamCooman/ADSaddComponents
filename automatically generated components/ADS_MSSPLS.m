classdef ADS_MSSPLS < ADScomponent
    % ADS_MSSPLS matlab representation for the ADS MSSPLS component
    % Microstrip Spiral Inductor (Square Side Feed)
    % MSSPLS [:Name] outer inner ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Gap (smorr) Unit: m
        S
        % Outer diameter (smorr) Unit: m
        OD
        % Number of Turns (smorr) 
        N
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.OD(obj,val)
            obj = setParameter(obj,'OD',val,0,'real');
        end
        function res = get.OD(obj)
            res = getParameter(obj,'OD');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
    end
end

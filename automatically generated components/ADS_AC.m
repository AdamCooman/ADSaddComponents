classdef ADS_AC < ADSnodeless
    % ADS_AC matlab representation for the ADS AC component
    % Small-Signal AC Analysis
    % AC [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of variable or parameter to be swept (sm-rs) 
        SweepVar
        % Stim instance/path name for sweep values (repeatable) (sm-rd) 
        SweepPlan
        % Frequency value if it's not being swept (smorr) Unit: Hz
        Freq
        % Levels of subcircuits to output (sm-ri) 
        NestLevel
        % Degree of annotation (sm-ri) 
        StatusLevel
        % Variable or parameter to output (repeatable) (sm-rs) 
        OutVar
        % Calculate noise parameters (sm-rb) 
        CalcNoise
        % Bandwidth for noise analysis (smorr) Unit: Hz
        BandwidthForNoise
        % Temperature for linear noise analysis (smorr) Unit: deg C
        TempForNoise
        % Use finite differences for sensitivities (s---b) 
        IncludePortNoise
        % Use finite differences for sensitivities (s---b) 
        UseFiniteDiff
        % Enable AC frequency conversion (s---b) 
        FreqConversion
        % Output top-level pin currents and voltages (s---b) 
        OutputBudgetIV
        % Nodename to compute noise voltage (repeatable) (s---s) 
        NoiseNode
        % Sort Noise Contribution by: Value/1, Name/2  (default: 0/NoOutput) (s---i) 
        SortNoise
        % Noise Contribution Threshold (s---r) 
        NoiseThresh
        % Levels of DC Operating Point Data to output (s---i) 
        DevOpPtLevel
        % OutputPlan Name (s---s) 
        OutputPlan
        % Write W-element Files (s---b) 
        WriteWFiles
        % Write Zc and Gamma of TMLs to dataset (s---b) 
        WriteZcGamma
        % Save DC solution to dataset if it is available (s---i) 
        SaveDCToDataset
    end
    methods
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.NestLevel(obj,val)
            obj = setParameter(obj,'NestLevel',val,0,'integer');
        end
        function res = get.NestLevel(obj)
            res = getParameter(obj,'NestLevel');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.OutVar(obj,val)
            obj = setParameter(obj,'OutVar',val,0,'string');
        end
        function res = get.OutVar(obj)
            res = getParameter(obj,'OutVar');
        end
        function obj = set.CalcNoise(obj,val)
            obj = setParameter(obj,'CalcNoise',val,0,'boolean');
        end
        function res = get.CalcNoise(obj)
            res = getParameter(obj,'CalcNoise');
        end
        function obj = set.BandwidthForNoise(obj,val)
            obj = setParameter(obj,'BandwidthForNoise',val,0,'real');
        end
        function res = get.BandwidthForNoise(obj)
            res = getParameter(obj,'BandwidthForNoise');
        end
        function obj = set.TempForNoise(obj,val)
            obj = setParameter(obj,'TempForNoise',val,0,'real');
        end
        function res = get.TempForNoise(obj)
            res = getParameter(obj,'TempForNoise');
        end
        function obj = set.IncludePortNoise(obj,val)
            obj = setParameter(obj,'IncludePortNoise',val,0,'boolean');
        end
        function res = get.IncludePortNoise(obj)
            res = getParameter(obj,'IncludePortNoise');
        end
        function obj = set.UseFiniteDiff(obj,val)
            obj = setParameter(obj,'UseFiniteDiff',val,0,'boolean');
        end
        function res = get.UseFiniteDiff(obj)
            res = getParameter(obj,'UseFiniteDiff');
        end
        function obj = set.FreqConversion(obj,val)
            obj = setParameter(obj,'FreqConversion',val,0,'boolean');
        end
        function res = get.FreqConversion(obj)
            res = getParameter(obj,'FreqConversion');
        end
        function obj = set.OutputBudgetIV(obj,val)
            obj = setParameter(obj,'OutputBudgetIV',val,0,'boolean');
        end
        function res = get.OutputBudgetIV(obj)
            res = getParameter(obj,'OutputBudgetIV');
        end
        function obj = set.NoiseNode(obj,val)
            obj = setParameter(obj,'NoiseNode',val,0,'string');
        end
        function res = get.NoiseNode(obj)
            res = getParameter(obj,'NoiseNode');
        end
        function obj = set.SortNoise(obj,val)
            obj = setParameter(obj,'SortNoise',val,0,'integer');
        end
        function res = get.SortNoise(obj)
            res = getParameter(obj,'SortNoise');
        end
        function obj = set.NoiseThresh(obj,val)
            obj = setParameter(obj,'NoiseThresh',val,0,'real');
        end
        function res = get.NoiseThresh(obj)
            res = getParameter(obj,'NoiseThresh');
        end
        function obj = set.DevOpPtLevel(obj,val)
            obj = setParameter(obj,'DevOpPtLevel',val,0,'integer');
        end
        function res = get.DevOpPtLevel(obj)
            res = getParameter(obj,'DevOpPtLevel');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,0,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.WriteWFiles(obj,val)
            obj = setParameter(obj,'WriteWFiles',val,0,'boolean');
        end
        function res = get.WriteWFiles(obj)
            res = getParameter(obj,'WriteWFiles');
        end
        function obj = set.WriteZcGamma(obj,val)
            obj = setParameter(obj,'WriteZcGamma',val,0,'boolean');
        end
        function res = get.WriteZcGamma(obj)
            res = getParameter(obj,'WriteZcGamma');
        end
        function obj = set.SaveDCToDataset(obj,val)
            obj = setParameter(obj,'SaveDCToDataset',val,0,'integer');
        end
        function res = get.SaveDCToDataset(obj)
            res = getParameter(obj,'SaveDCToDataset');
        end
    end
end

classdef ADS_TFC_MDS < ADScomponent
    % ADS_TFC_MDS matlab representation for the ADS TFC_MDS component
    % Thin Film Capacitor
    % TFC_MDS [:Name] upper lower ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Top Conductor Width (smorr) Unit: m
        Wt
        % Film Thickness (smorr) Unit: m
        T
        % Parallel Layer Length (smorr) Unit: m
        L
        % Film Dielectric Constant (smorr) 
        Er
        % Dielectric Loss Tangent (smorr) 
        TanD
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Wt(obj,val)
            obj = setParameter(obj,'Wt',val,0,'real');
        end
        function res = get.Wt(obj)
            res = getParameter(obj,'Wt');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
    end
end

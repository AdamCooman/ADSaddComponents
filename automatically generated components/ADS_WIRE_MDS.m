classdef ADS_WIRE_MDS < ADScomponent
    % ADS_WIRE_MDS matlab representation for the ADS WIRE_MDS component
    % Wire over optional substrate
    % WIRE_MDS [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (sm--s) 
        Subst
        % Wire radius (smorr) Unit: m
        R
        % Height above ground (smorr) Unit: m
        B
        % Length (smorr) Unit: m
        L
        % Wire conductivity per meter (smorr) Unit: S/m
        Cond
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
    end
end

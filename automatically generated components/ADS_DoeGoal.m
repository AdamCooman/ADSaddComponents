classdef ADS_DoeGoal < ADSnodeless
    % ADS_DoeGoal matlab representation for the ADS DoeGoal component
    % Goals for Design of Experiments
    % DoeGoal [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % spec expression (s---s) 
        Expr
        % analysis name (s---s) 
        SimInstanceName
        % min acceptable spec value (smo-r) 
        Min
        % max acceptable spec value (smo-r) 
        Max
        % weighting used in error function calculation (smo-r) 
        Weight
        % send spec value to dataset (s---b) 
        Save
        % name of range variable (s---s) 
        RangeVar
        % minimum acceptable value for range variable (s---r) 
        RangeMin
        % maximum acceptable value for range variable (s---r) 
        RangeMax
        % Spec limit line (s---s) 
        SpecLimitLine
        % name of goal range variable (s----) 
        IndepVar
        % only resolve at optim node (s---b) 
        ResolveAtOptim
    end
    methods
        function obj = set.Expr(obj,val)
            obj = setParameter(obj,'Expr',val,0,'string');
        end
        function res = get.Expr(obj)
            res = getParameter(obj,'Expr');
        end
        function obj = set.SimInstanceName(obj,val)
            obj = setParameter(obj,'SimInstanceName',val,0,'string');
        end
        function res = get.SimInstanceName(obj)
            res = getParameter(obj,'SimInstanceName');
        end
        function obj = set.Min(obj,val)
            obj = setParameter(obj,'Min',val,0,'real');
        end
        function res = get.Min(obj)
            res = getParameter(obj,'Min');
        end
        function obj = set.Max(obj,val)
            obj = setParameter(obj,'Max',val,0,'real');
        end
        function res = get.Max(obj)
            res = getParameter(obj,'Max');
        end
        function obj = set.Weight(obj,val)
            obj = setParameter(obj,'Weight',val,0,'real');
        end
        function res = get.Weight(obj)
            res = getParameter(obj,'Weight');
        end
        function obj = set.Save(obj,val)
            obj = setParameter(obj,'Save',val,0,'boolean');
        end
        function res = get.Save(obj)
            res = getParameter(obj,'Save');
        end
        function obj = set.RangeVar(obj,val)
            obj = setParameter(obj,'RangeVar',val,1,'string');
        end
        function res = get.RangeVar(obj)
            res = getParameter(obj,'RangeVar');
        end
        function obj = set.RangeMin(obj,val)
            obj = setParameter(obj,'RangeMin',val,1,'real');
        end
        function res = get.RangeMin(obj)
            res = getParameter(obj,'RangeMin');
        end
        function obj = set.RangeMax(obj,val)
            obj = setParameter(obj,'RangeMax',val,1,'real');
        end
        function res = get.RangeMax(obj)
            res = getParameter(obj,'RangeMax');
        end
        function obj = set.SpecLimitLine(obj,val)
            obj = setParameter(obj,'SpecLimitLine',val,1,'string');
        end
        function res = get.SpecLimitLine(obj)
            res = getParameter(obj,'SpecLimitLine');
        end
        function obj = set.IndepVar(obj,val)
            obj = setParameter(obj,'IndepVar',val,1,'string');
        end
        function res = get.IndepVar(obj)
            res = getParameter(obj,'IndepVar');
        end
        function obj = set.ResolveAtOptim(obj,val)
            obj = setParameter(obj,'ResolveAtOptim',val,0,'boolean');
        end
        function res = get.ResolveAtOptim(obj)
            res = getParameter(obj,'ResolveAtOptim');
        end
    end
end

classdef ADS_RCLIN < ADScomponent
    % ADS_RCLIN matlab representation for the ADS RCLIN component
    % Distributed R-C Network
    % RCLIN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Series resistance per meter (Smorr) Unit: ohms
        R
        % Shunt Capacitance per meter (Smorr) Unit: f
        C
        % Length (Smorr) Unit: m
        L
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

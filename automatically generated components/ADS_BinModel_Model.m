classdef ADS_BinModel_Model < ADSmodel
    % ADS_BinModel_Model matlab representation for the ADS BinModel_Model component
    % Generic binning model model
    % model ModelName BinModel ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Parameter Name (s---s) 
        Param
        % Model Name (s---s) 
        Model
        % Minimum inclusive value (s---r) 
        Min
        % Maximum exclusive (inclusive if Max = Min) value (s---r) 
        Max
        % Relative tolerance if Min = Max (s---r) 
        RelTol
        % Absolute tolerance if Min = Max (s---r) 
        AbsTol
        % Global Model Name (s---s) 
        GlobalModel
    end
    methods
        function obj = set.Param(obj,val)
            obj = setParameter(obj,'Param',val,1,'string');
        end
        function res = get.Param(obj)
            res = getParameter(obj,'Param');
        end
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,1,'string');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
        function obj = set.Min(obj,val)
            obj = setParameter(obj,'Min',val,2,'real');
        end
        function res = get.Min(obj)
            res = getParameter(obj,'Min');
        end
        function obj = set.Max(obj,val)
            obj = setParameter(obj,'Max',val,2,'real');
        end
        function res = get.Max(obj)
            res = getParameter(obj,'Max');
        end
        function obj = set.RelTol(obj,val)
            obj = setParameter(obj,'RelTol',val,0,'real');
        end
        function res = get.RelTol(obj)
            res = getParameter(obj,'RelTol');
        end
        function obj = set.AbsTol(obj,val)
            obj = setParameter(obj,'AbsTol',val,0,'real');
        end
        function res = get.AbsTol(obj)
            res = getParameter(obj,'AbsTol');
        end
        function obj = set.GlobalModel(obj,val)
            obj = setParameter(obj,'GlobalModel',val,1,'string');
        end
        function res = get.GlobalModel(obj)
            res = getParameter(obj,'GlobalModel');
        end
    end
end

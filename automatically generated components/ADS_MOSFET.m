classdef ADS_MOSFET < ADScomponent
    % ADS_MOSFET matlab representation for the ADS MOSFET component
    % Metal Oxide Semiconductor Transistor
    % ModelName [:Name] drain gate source bulk
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Channel length (smorr) Unit: m
        Length
        % Channel width (smorr) Unit: m
        Width
        % Area of the drain diffusion (smorr) Unit: m^2
        Ad
        % Area of the source diffusion (smorr) Unit: m^2
        As
        % Perimeter of the drain junction (smorr) Unit: m
        Pd
        % Perimeter of the source junction (smorr) Unit: m
        Ps
        % Number of squares of the drain diffusion (smorr) 
        Nrd
        % Number of squares of the source difussion (smorr) 
        Nrs
        % Non-quasi static model selector (s--ri) 
        Nqsmod
        % AC non-quasi static model selector (sm-ri) 
        Acnqsmod
        % Source/Drain sharing selector for ACM=3 (s--ri) 
        Geo
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s--ri) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s--ri) 
        Mode
        % Noise generation on/off (s--rb) 
        Noise
        % Number of devices in parallel (smorr) 
        M
        % LOD stress effect model selector (s--ri) 
        Stimod
        % Distance between OD edge to poly of one side. 1 (smorr) Unit: m
        Sa1
        % Distance between OD edge to poly of one side. 2 (smorr) Unit: m
        Sa2
        % Distance between OD edge to poly of one side. 3 (smorr) Unit: m
        Sa3
        % Distance between OD edge to poly of one side. 4 (smorr) Unit: m
        Sa4
        % Distance between OD edge to poly of one side. 5 (smorr) Unit: m
        Sa5
        % Distance between OD edge to poly of one side. 6 (smorr) Unit: m
        Sa6
        % Distance between OD edge to poly of one side. 7 (smorr) Unit: m
        Sa7
        % Distance between OD edge to poly of one side. 8 (smorr) Unit: m
        Sa8
        % Distance between OD edge to poly of one side. 9 (smorr) Unit: m
        Sa9
        % Distance between OD edge to poly of one side. 10 (smorr) Unit: m
        Sa10
        % Distance between OD edge to poly of the other side. 1 (smorr) Unit: m
        Sb1
        % Distance between OD edge to poly of the other side. 2 (smorr) Unit: m
        Sb2
        % Distance between OD edge to poly of the other side. 3 (smorr) Unit: m
        Sb3
        % Distance between OD edge to poly of the other side. 4 (smorr) Unit: m
        Sb4
        % Distance between OD edge to poly of the other side. 5 (smorr) Unit: m
        Sb5
        % Distance between OD edge to poly of the other side. 6 (smorr) Unit: m
        Sb6
        % Distance between OD edge to poly of the other side. 7 (smorr) Unit: m
        Sb7
        % Distance between OD edge to poly of the other side. 8 (smorr) Unit: m
        Sb8
        % Distance between OD edge to poly of the other side. 9 (smorr) Unit: m
        Sb9
        % Distance between OD edge to poly of the other side. 10 (smorr) Unit: m
        Sb10
        % Width of Sa1/Sb1 (smorr) Unit: m
        Sw1
        % Width of Sa2/Sb2 (smorr) Unit: m
        Sw2
        % Width of Sa3/Sb3 (smorr) Unit: m
        Sw3
        % Width of Sa4/Sb4 (smorr) Unit: m
        Sw4
        % Width of Sa5/Sb5 (smorr) Unit: m
        Sw5
        % Width of Sa6/Sb6 (smorr) Unit: m
        Sw6
        % Width of Sa7/Sb7 (smorr) Unit: m
        Sw7
        % Width of Sa8/Sb8 (smorr) Unit: m
        Sw8
        % Width of Sa9/Sb9 (smorr) Unit: m
        Sw9
        % Width of Sa10/Sb10 (smorr) Unit: m
        Sw10
        % Alias for Sa1 (smorr) Unit: m
        Sa
        % Alias for Sb1 (smorr) Unit: m
        Sb
        % Shift in body bias coefficent K1 (smorr) Unit: V^(1/2)
        Delk1
        % Shift in zero-bias threshold voltage Vth0 (smorr) Unit: V
        Delvt0
        % Mobility multiplier (smorr) 
        Mulu0
        % Shift in subthreshold swing factor Nfactor (smorr) 
        Delnfct
        % Additional source resistance due to contact resistance (smorr) Unit: Ohms
        Rsc
        % Additional drain resistance due to contact resistance (smorr) Unit: Ohms
        Rdc
        % Small signal drain source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vbs to Ids transconductance (---rr) Unit: S
        Gmbs
        % Small signal bulk drain conductance (---rr) Unit: S
        Gbd
        % Small signal bulk source conductance (---rr) Unit: S
        Gbs
        % Small signal Vbd to Ids transconductance (---rr) Unit: S
        Gmr
        % Small signal Vbs to Isd transconductance (---rr) Unit: S
        Gmbsr
        % Small signal bulk drain junction capacitance (---rr) Unit: F
        Capbd
        % Small signal bulk source junction capacitance (---rr) Unit: F
        Capbs
        % Small signal gate source Meyer capacitance (---rr) Unit: F
        Cgsm
        % Small signal gate bulk Meyer capacitance (---rr) Unit: F
        Cgbm
        % Small signal gate drain Meyer capacitance (---rr) Unit: F
        Cgdm
        % Small signal gate capacitance (---rr) Unit: F
        Cggb
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgdb
        % Small signal gate source capacitance (---rr) Unit: F
        Cgsb
        % Small signal drain gate capacitance (---rr) Unit: F
        Cdgb
        % Small signal drain capacitance (---rr) Unit: F
        Cddb
        % Small signal drain source capacitance (---rr) Unit: F
        Cdsb
        % Small signal bulk gate capacitance (---rr) Unit: F
        Cbgb
        % Small signal bulk drain capacitance (---rr) Unit: F
        Cbdb
        % Small signal bulk source capacitance (---rr) Unit: F
        Cbsb
        % Small signal source gate capacitance (---rr) Unit: F
        Csgb
        % Small signal source drain capacitance (---rr) Unit: F
        Csdb
        % Small signal source capacitance (---rr) Unit: F
        Cssb
        % Gate charge (---rr) Unit: C
        Qg
        % Drain charge (---rr) Unit: C
        Qd
        % Bulk charge (---rr) Unit: C
        Qb
        % NMOS type MOSFET (---rb) 
        NMOS
        % PMOS type MOSFET (---rb) 
        PMOS
    end
    methods
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Ad(obj,val)
            obj = setParameter(obj,'Ad',val,0,'real');
        end
        function res = get.Ad(obj)
            res = getParameter(obj,'Ad');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.Pd(obj,val)
            obj = setParameter(obj,'Pd',val,0,'real');
        end
        function res = get.Pd(obj)
            res = getParameter(obj,'Pd');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Nrd(obj,val)
            obj = setParameter(obj,'Nrd',val,0,'real');
        end
        function res = get.Nrd(obj)
            res = getParameter(obj,'Nrd');
        end
        function obj = set.Nrs(obj,val)
            obj = setParameter(obj,'Nrs',val,0,'real');
        end
        function res = get.Nrs(obj)
            res = getParameter(obj,'Nrs');
        end
        function obj = set.Nqsmod(obj,val)
            obj = setParameter(obj,'Nqsmod',val,0,'integer');
        end
        function res = get.Nqsmod(obj)
            res = getParameter(obj,'Nqsmod');
        end
        function obj = set.Acnqsmod(obj,val)
            obj = setParameter(obj,'Acnqsmod',val,0,'integer');
        end
        function res = get.Acnqsmod(obj)
            res = getParameter(obj,'Acnqsmod');
        end
        function obj = set.Geo(obj,val)
            obj = setParameter(obj,'Geo',val,0,'integer');
        end
        function res = get.Geo(obj)
            res = getParameter(obj,'Geo');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Stimod(obj,val)
            obj = setParameter(obj,'Stimod',val,0,'integer');
        end
        function res = get.Stimod(obj)
            res = getParameter(obj,'Stimod');
        end
        function obj = set.Sa1(obj,val)
            obj = setParameter(obj,'Sa1',val,0,'real');
        end
        function res = get.Sa1(obj)
            res = getParameter(obj,'Sa1');
        end
        function obj = set.Sa2(obj,val)
            obj = setParameter(obj,'Sa2',val,0,'real');
        end
        function res = get.Sa2(obj)
            res = getParameter(obj,'Sa2');
        end
        function obj = set.Sa3(obj,val)
            obj = setParameter(obj,'Sa3',val,0,'real');
        end
        function res = get.Sa3(obj)
            res = getParameter(obj,'Sa3');
        end
        function obj = set.Sa4(obj,val)
            obj = setParameter(obj,'Sa4',val,0,'real');
        end
        function res = get.Sa4(obj)
            res = getParameter(obj,'Sa4');
        end
        function obj = set.Sa5(obj,val)
            obj = setParameter(obj,'Sa5',val,0,'real');
        end
        function res = get.Sa5(obj)
            res = getParameter(obj,'Sa5');
        end
        function obj = set.Sa6(obj,val)
            obj = setParameter(obj,'Sa6',val,0,'real');
        end
        function res = get.Sa6(obj)
            res = getParameter(obj,'Sa6');
        end
        function obj = set.Sa7(obj,val)
            obj = setParameter(obj,'Sa7',val,0,'real');
        end
        function res = get.Sa7(obj)
            res = getParameter(obj,'Sa7');
        end
        function obj = set.Sa8(obj,val)
            obj = setParameter(obj,'Sa8',val,0,'real');
        end
        function res = get.Sa8(obj)
            res = getParameter(obj,'Sa8');
        end
        function obj = set.Sa9(obj,val)
            obj = setParameter(obj,'Sa9',val,0,'real');
        end
        function res = get.Sa9(obj)
            res = getParameter(obj,'Sa9');
        end
        function obj = set.Sa10(obj,val)
            obj = setParameter(obj,'Sa10',val,0,'real');
        end
        function res = get.Sa10(obj)
            res = getParameter(obj,'Sa10');
        end
        function obj = set.Sb1(obj,val)
            obj = setParameter(obj,'Sb1',val,0,'real');
        end
        function res = get.Sb1(obj)
            res = getParameter(obj,'Sb1');
        end
        function obj = set.Sb2(obj,val)
            obj = setParameter(obj,'Sb2',val,0,'real');
        end
        function res = get.Sb2(obj)
            res = getParameter(obj,'Sb2');
        end
        function obj = set.Sb3(obj,val)
            obj = setParameter(obj,'Sb3',val,0,'real');
        end
        function res = get.Sb3(obj)
            res = getParameter(obj,'Sb3');
        end
        function obj = set.Sb4(obj,val)
            obj = setParameter(obj,'Sb4',val,0,'real');
        end
        function res = get.Sb4(obj)
            res = getParameter(obj,'Sb4');
        end
        function obj = set.Sb5(obj,val)
            obj = setParameter(obj,'Sb5',val,0,'real');
        end
        function res = get.Sb5(obj)
            res = getParameter(obj,'Sb5');
        end
        function obj = set.Sb6(obj,val)
            obj = setParameter(obj,'Sb6',val,0,'real');
        end
        function res = get.Sb6(obj)
            res = getParameter(obj,'Sb6');
        end
        function obj = set.Sb7(obj,val)
            obj = setParameter(obj,'Sb7',val,0,'real');
        end
        function res = get.Sb7(obj)
            res = getParameter(obj,'Sb7');
        end
        function obj = set.Sb8(obj,val)
            obj = setParameter(obj,'Sb8',val,0,'real');
        end
        function res = get.Sb8(obj)
            res = getParameter(obj,'Sb8');
        end
        function obj = set.Sb9(obj,val)
            obj = setParameter(obj,'Sb9',val,0,'real');
        end
        function res = get.Sb9(obj)
            res = getParameter(obj,'Sb9');
        end
        function obj = set.Sb10(obj,val)
            obj = setParameter(obj,'Sb10',val,0,'real');
        end
        function res = get.Sb10(obj)
            res = getParameter(obj,'Sb10');
        end
        function obj = set.Sw1(obj,val)
            obj = setParameter(obj,'Sw1',val,0,'real');
        end
        function res = get.Sw1(obj)
            res = getParameter(obj,'Sw1');
        end
        function obj = set.Sw2(obj,val)
            obj = setParameter(obj,'Sw2',val,0,'real');
        end
        function res = get.Sw2(obj)
            res = getParameter(obj,'Sw2');
        end
        function obj = set.Sw3(obj,val)
            obj = setParameter(obj,'Sw3',val,0,'real');
        end
        function res = get.Sw3(obj)
            res = getParameter(obj,'Sw3');
        end
        function obj = set.Sw4(obj,val)
            obj = setParameter(obj,'Sw4',val,0,'real');
        end
        function res = get.Sw4(obj)
            res = getParameter(obj,'Sw4');
        end
        function obj = set.Sw5(obj,val)
            obj = setParameter(obj,'Sw5',val,0,'real');
        end
        function res = get.Sw5(obj)
            res = getParameter(obj,'Sw5');
        end
        function obj = set.Sw6(obj,val)
            obj = setParameter(obj,'Sw6',val,0,'real');
        end
        function res = get.Sw6(obj)
            res = getParameter(obj,'Sw6');
        end
        function obj = set.Sw7(obj,val)
            obj = setParameter(obj,'Sw7',val,0,'real');
        end
        function res = get.Sw7(obj)
            res = getParameter(obj,'Sw7');
        end
        function obj = set.Sw8(obj,val)
            obj = setParameter(obj,'Sw8',val,0,'real');
        end
        function res = get.Sw8(obj)
            res = getParameter(obj,'Sw8');
        end
        function obj = set.Sw9(obj,val)
            obj = setParameter(obj,'Sw9',val,0,'real');
        end
        function res = get.Sw9(obj)
            res = getParameter(obj,'Sw9');
        end
        function obj = set.Sw10(obj,val)
            obj = setParameter(obj,'Sw10',val,0,'real');
        end
        function res = get.Sw10(obj)
            res = getParameter(obj,'Sw10');
        end
        function obj = set.Sa(obj,val)
            obj = setParameter(obj,'Sa',val,0,'real');
        end
        function res = get.Sa(obj)
            res = getParameter(obj,'Sa');
        end
        function obj = set.Sb(obj,val)
            obj = setParameter(obj,'Sb',val,0,'real');
        end
        function res = get.Sb(obj)
            res = getParameter(obj,'Sb');
        end
        function obj = set.Delk1(obj,val)
            obj = setParameter(obj,'Delk1',val,0,'real');
        end
        function res = get.Delk1(obj)
            res = getParameter(obj,'Delk1');
        end
        function obj = set.Delvt0(obj,val)
            obj = setParameter(obj,'Delvt0',val,0,'real');
        end
        function res = get.Delvt0(obj)
            res = getParameter(obj,'Delvt0');
        end
        function obj = set.Mulu0(obj,val)
            obj = setParameter(obj,'Mulu0',val,0,'real');
        end
        function res = get.Mulu0(obj)
            res = getParameter(obj,'Mulu0');
        end
        function obj = set.Delnfct(obj,val)
            obj = setParameter(obj,'Delnfct',val,0,'real');
        end
        function res = get.Delnfct(obj)
            res = getParameter(obj,'Delnfct');
        end
        function obj = set.Rsc(obj,val)
            obj = setParameter(obj,'Rsc',val,0,'real');
        end
        function res = get.Rsc(obj)
            res = getParameter(obj,'Rsc');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmbs(obj,val)
            obj = setParameter(obj,'Gmbs',val,0,'real');
        end
        function res = get.Gmbs(obj)
            res = getParameter(obj,'Gmbs');
        end
        function obj = set.Gbd(obj,val)
            obj = setParameter(obj,'Gbd',val,0,'real');
        end
        function res = get.Gbd(obj)
            res = getParameter(obj,'Gbd');
        end
        function obj = set.Gbs(obj,val)
            obj = setParameter(obj,'Gbs',val,0,'real');
        end
        function res = get.Gbs(obj)
            res = getParameter(obj,'Gbs');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Gmbsr(obj,val)
            obj = setParameter(obj,'Gmbsr',val,0,'real');
        end
        function res = get.Gmbsr(obj)
            res = getParameter(obj,'Gmbsr');
        end
        function obj = set.Capbd(obj,val)
            obj = setParameter(obj,'Capbd',val,0,'real');
        end
        function res = get.Capbd(obj)
            res = getParameter(obj,'Capbd');
        end
        function obj = set.Capbs(obj,val)
            obj = setParameter(obj,'Capbs',val,0,'real');
        end
        function res = get.Capbs(obj)
            res = getParameter(obj,'Capbs');
        end
        function obj = set.Cgsm(obj,val)
            obj = setParameter(obj,'Cgsm',val,0,'real');
        end
        function res = get.Cgsm(obj)
            res = getParameter(obj,'Cgsm');
        end
        function obj = set.Cgbm(obj,val)
            obj = setParameter(obj,'Cgbm',val,0,'real');
        end
        function res = get.Cgbm(obj)
            res = getParameter(obj,'Cgbm');
        end
        function obj = set.Cgdm(obj,val)
            obj = setParameter(obj,'Cgdm',val,0,'real');
        end
        function res = get.Cgdm(obj)
            res = getParameter(obj,'Cgdm');
        end
        function obj = set.Cggb(obj,val)
            obj = setParameter(obj,'Cggb',val,0,'real');
        end
        function res = get.Cggb(obj)
            res = getParameter(obj,'Cggb');
        end
        function obj = set.Cgdb(obj,val)
            obj = setParameter(obj,'Cgdb',val,0,'real');
        end
        function res = get.Cgdb(obj)
            res = getParameter(obj,'Cgdb');
        end
        function obj = set.Cgsb(obj,val)
            obj = setParameter(obj,'Cgsb',val,0,'real');
        end
        function res = get.Cgsb(obj)
            res = getParameter(obj,'Cgsb');
        end
        function obj = set.Cdgb(obj,val)
            obj = setParameter(obj,'Cdgb',val,0,'real');
        end
        function res = get.Cdgb(obj)
            res = getParameter(obj,'Cdgb');
        end
        function obj = set.Cddb(obj,val)
            obj = setParameter(obj,'Cddb',val,0,'real');
        end
        function res = get.Cddb(obj)
            res = getParameter(obj,'Cddb');
        end
        function obj = set.Cdsb(obj,val)
            obj = setParameter(obj,'Cdsb',val,0,'real');
        end
        function res = get.Cdsb(obj)
            res = getParameter(obj,'Cdsb');
        end
        function obj = set.Cbgb(obj,val)
            obj = setParameter(obj,'Cbgb',val,0,'real');
        end
        function res = get.Cbgb(obj)
            res = getParameter(obj,'Cbgb');
        end
        function obj = set.Cbdb(obj,val)
            obj = setParameter(obj,'Cbdb',val,0,'real');
        end
        function res = get.Cbdb(obj)
            res = getParameter(obj,'Cbdb');
        end
        function obj = set.Cbsb(obj,val)
            obj = setParameter(obj,'Cbsb',val,0,'real');
        end
        function res = get.Cbsb(obj)
            res = getParameter(obj,'Cbsb');
        end
        function obj = set.Csgb(obj,val)
            obj = setParameter(obj,'Csgb',val,0,'real');
        end
        function res = get.Csgb(obj)
            res = getParameter(obj,'Csgb');
        end
        function obj = set.Csdb(obj,val)
            obj = setParameter(obj,'Csdb',val,0,'real');
        end
        function res = get.Csdb(obj)
            res = getParameter(obj,'Csdb');
        end
        function obj = set.Cssb(obj,val)
            obj = setParameter(obj,'Cssb',val,0,'real');
        end
        function res = get.Cssb(obj)
            res = getParameter(obj,'Cssb');
        end
        function obj = set.Qg(obj,val)
            obj = setParameter(obj,'Qg',val,0,'real');
        end
        function res = get.Qg(obj)
            res = getParameter(obj,'Qg');
        end
        function obj = set.Qd(obj,val)
            obj = setParameter(obj,'Qd',val,0,'real');
        end
        function res = get.Qd(obj)
            res = getParameter(obj,'Qd');
        end
        function obj = set.Qb(obj,val)
            obj = setParameter(obj,'Qb',val,0,'real');
        end
        function res = get.Qb(obj)
            res = getParameter(obj,'Qb');
        end
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
    end
end

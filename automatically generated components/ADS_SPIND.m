classdef ADS_SPIND < ADScomponent
    % ADS_SPIND matlab representation for the ADS SPIND component
    % Microstrip Round Spiral Inductor
    % SPIND [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Inner Diameter (centerline to centerline) (Smorr) Unit: m
        Di
        % Outer Diameter (centerline to centerline) (Smorr) Unit: m
        Do
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Spacing (Smorr) Unit: m
        S
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.Di(obj,val)
            obj = setParameter(obj,'Di',val,0,'real');
        end
        function res = get.Di(obj)
            res = getParameter(obj,'Di');
        end
        function obj = set.Do(obj,val)
            obj = setParameter(obj,'Do',val,0,'real');
        end
        function res = get.Do(obj)
            res = getParameter(obj,'Do');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

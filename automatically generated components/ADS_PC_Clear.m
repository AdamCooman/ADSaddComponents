classdef ADS_PC_Clear < ADScomponent
    % ADS_PC_Clear matlab representation for the ADS PC_Clear component
    % User-defined model
    % PC_Clear [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (Sm-ri) Unit: unknown
        Layer
        % User device parameter (Smorr) Unit: unknown
        DiamClear
        % User device parameter (Smorr) Unit: unknown
        DiamPad
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'integer');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.DiamClear(obj,val)
            obj = setParameter(obj,'DiamClear',val,0,'real');
        end
        function res = get.DiamClear(obj)
            res = getParameter(obj,'DiamClear');
        end
        function obj = set.DiamPad(obj,val)
            obj = setParameter(obj,'DiamPad',val,0,'real');
        end
        function res = get.DiamPad(obj)
            res = getParameter(obj,'DiamPad');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

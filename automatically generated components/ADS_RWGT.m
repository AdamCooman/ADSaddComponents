classdef ADS_RWGT < ADScomponent
    % ADS_RWGT matlab representation for the ADS RWGT component
    % Rectangular waveguide termination
    % RWGT [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Width (wide dimension of inside cross-section) (Smorr) Unit: m
        A
        % Height (narrow dimension of inside cross-section) (Smorr) Unit: m
        B
        % Dielectric constant (Smorr) Unit: unknown
        Er
        % Metal resistivity (relative to copper) (Smorr) Unit: unknown
        Rho
        % Dielectric loss tangent (smorr) Unit: unknown
        TanD
        % Permeability (smorr) Unit: unknown
        Mur
        % Permeability (smorr) Unit: unknown
        TanM
        % Dielectric conductivity (smorr) Unit: unknown
        Sigma
        % Physical temperature (smorr) Unit: C
        Temp
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) Unit: unknown
        DielectricLossModel
        % Frequency at which Er and TanD are measured (smorr) Unit: hz
        FreqForEpsrTand
        % Low roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        LowFreqForTand
        % High roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        HighFreqForTand
    end
    methods
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'real');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.TanM(obj,val)
            obj = setParameter(obj,'TanM',val,0,'real');
        end
        function res = get.TanM(obj)
            res = getParameter(obj,'TanM');
        end
        function obj = set.Sigma(obj,val)
            obj = setParameter(obj,'Sigma',val,0,'real');
        end
        function res = get.Sigma(obj)
            res = getParameter(obj,'Sigma');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.FreqForEpsrTand(obj,val)
            obj = setParameter(obj,'FreqForEpsrTand',val,0,'real');
        end
        function res = get.FreqForEpsrTand(obj)
            res = getParameter(obj,'FreqForEpsrTand');
        end
        function obj = set.LowFreqForTand(obj,val)
            obj = setParameter(obj,'LowFreqForTand',val,0,'real');
        end
        function res = get.LowFreqForTand(obj)
            res = getParameter(obj,'LowFreqForTand');
        end
        function obj = set.HighFreqForTand(obj,val)
            obj = setParameter(obj,'HighFreqForTand',val,0,'real');
        end
        function res = get.HighFreqForTand(obj)
            res = getParameter(obj,'HighFreqForTand');
        end
    end
end

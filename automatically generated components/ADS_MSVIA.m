classdef ADS_MSVIA < ADScomponent
    % ADS_MSVIA matlab representation for the ADS MSVIA component
    % Microstrip Via, Shunt
    % MSVIA [:Name] top ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Outside Diameter (smorr) Unit: m
        OD
        % Conductor Width (smorr) Unit: m
        W
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.OD(obj,val)
            obj = setParameter(obj,'OD',val,0,'real');
        end
        function res = get.OD(obj)
            res = getParameter(obj,'OD');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
    end
end

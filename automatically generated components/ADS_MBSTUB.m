classdef ADS_MBSTUB < ADScomponent
    % ADS_MBSTUB matlab representation for the ADS MBSTUB component
    % Microstrip Butterfly Stub
    % MBSTUB [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of Feed Line (Smorr) Unit: m
        W
        % Outer Radius of Circular Sector (Smorr) Unit: m
        Ro
        % Angle subtended by Circular Sector (Smorr) Unit: deg
        Angle
        % Insertion depth of Circular Sector in Feed Line (Smorr) Unit: m
        D
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Ro(obj,val)
            obj = setParameter(obj,'Ro',val,0,'real');
        end
        function res = get.Ro(obj)
            res = getParameter(obj,'Ro');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

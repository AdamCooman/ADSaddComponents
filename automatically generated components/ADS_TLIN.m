classdef ADS_TLIN < ADScomponent
    % ADS_TLIN matlab representation for the ADS TLIN component
    % Ideal 2-Terminal Transmission Line
    % TLIN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Characteristic Impedance (Smorr) Unit: ohms
        Z
        % Electrical Length (Smorr) Unit: deg
        E
        % Reference Frequency for Electrical Length (Smorr) Unit: hz
        F
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.E(obj,val)
            obj = setParameter(obj,'E',val,0,'real');
        end
        function res = get.E(obj)
            res = getParameter(obj,'E');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
    end
end

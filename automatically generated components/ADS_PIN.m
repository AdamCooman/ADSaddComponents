classdef ADS_PIN < ADScomponent
    % ADS_PIN matlab representation for the ADS PIN component
    % PIN Diode, Chip Model
    % PIN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Junction capacitance (Smorr) Unit: f
        Cj
        % Junction resistance (Smorr) Unit: ohm
        Rj
        % Diode series resistance (Smorr) Unit: ohm
        Rs
        % Bond wire inductance (Smorr) Unit: h
        Ls
        % By-pass capacitance (Smorr) Unit: f
        Cb
        % Capacitance of gap across which diode is connected (Smorr) Unit: f
        Cg
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.Cj(obj,val)
            obj = setParameter(obj,'Cj',val,0,'real');
        end
        function res = get.Cj(obj)
            res = getParameter(obj,'Cj');
        end
        function obj = set.Rj(obj,val)
            obj = setParameter(obj,'Rj',val,0,'real');
        end
        function res = get.Rj(obj)
            res = getParameter(obj,'Rj');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Cb(obj,val)
            obj = setParameter(obj,'Cb',val,0,'real');
        end
        function res = get.Cb(obj)
            res = getParameter(obj,'Cb');
        end
        function obj = set.Cg(obj,val)
            obj = setParameter(obj,'Cg',val,0,'real');
        end
        function res = get.Cg(obj)
            res = getParameter(obj,'Cg');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

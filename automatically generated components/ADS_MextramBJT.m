classdef ADS_MextramBJT < ADScomponent
    % ADS_MextramBJT matlab representation for the ADS MextramBJT component
    % Mextram Bipolar Junction Transistor
    % ModelName [:Name] collector base emitter ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Junction area (smorr) Unit: m^2
        Area
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s---i) 
        Region
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Difference of the device temperature to the ambient temperature (smorr) Unit: deg
        Dta
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small Signal Transconductance gx (---rr) Unit: S
        Gx
        % Small Signal Transconductance gy (---rr) Unit: S
        Gy
        % Small Signal Transconductance gz (---rr) Unit: S
        Gz
        % Small Signal Transconductance grcvx (---rr) Unit: S
        Grcvx
        % Small Signal Transconductance grcvy (---rr) Unit: S
        Grcvy
        % Small Signal Transconductance grcvz (---rr) Unit: S
        Grcvz
        % Small Signal Base Emmiter capacitance cbex (---rr) Unit: F
        Cbex
        % Small Signal Base Emmiter capacitance cbey (---rr) Unit: F
        Cbey
        % Small Signal Base Emmiter capacitance cbez (---rr) Unit: F
        Cbez
        % Small Signal conductance gpi (---rr) Unit: S
        Gpi
        % Small Signal conductance gmux (---rr) Unit: S
        Gmux
        % Small Signal conductance gmuy (---rr) Unit: S
        Gmuy
        % Small Signal conductance gmuz (---rr) Unit: S
        Gmuz
        % Small Signal Base Emmiter capacitance cbcx (---rr) Unit: F
        Cbcx
        % Small Signal Base Emmiter capacitance cbcy (---rr) Unit: F
        Cbcy
        % Small Signal Base Emmiter capacitance cbcz (---rr) Unit: F
        Cbcz
        % Small Signal Transconductance gbv (---rr) Unit: S
        Gbv
        % Small Signal Transconductance grbvx (---rr) Unit: S
        Grbvx
        % Small Signal Transconductance grbvy (---rr) Unit: S
        Grbvy
        % Small Signal Transconductance grbvz (---rr) Unit: S
        Grbvz
        % Small Signal Base Emmiter capacitance cb1b2 (---rr) Unit: F
        Cb1b2
        % Small Signal Base Emmiter capacitance cb1b2x (---rr) Unit: F
        Cb1b2x
        % Small Signal Base Emmiter capacitance sgpi (---rr) Unit: F
        Sgpi
        % Small Signal conductance scte (---rr) Unit: S
        Scte
        % Small Signal Transconductance gpnp (---rr) Unit: S
        Gpnp
        % Small Signal conductance xgpnp (---rr) Unit: S
        Xgpnp
        % Small Signal conductance gsub (---rr) Unit: S
        Gsub
        % Small Signal capacitance cts (---rr) Unit: F
        Cts
        % Small Signal conductance gmuex (---rr) Unit: S
        Gmuex
        % Small Signal capacitance cbcex (---rr) Unit: F
        Cbcex
        % Small Signal conductance xgmuex (---rr) Unit: S
        Xgmuex
        % Small Signal capacitance xcbcex (---rr) Unit: F
        Xcbcex
        % Small Signal conductance ge (---rr) Unit: S
        Ge
        % Small Signal conductance gcc (---rr) Unit: S
        Gcc
        % Small Signal conductance gbc (---rr) Unit: S
        Gbc
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Dta(obj,val)
            obj = setParameter(obj,'Dta',val,0,'real');
        end
        function res = get.Dta(obj)
            res = getParameter(obj,'Dta');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gx(obj,val)
            obj = setParameter(obj,'Gx',val,0,'real');
        end
        function res = get.Gx(obj)
            res = getParameter(obj,'Gx');
        end
        function obj = set.Gy(obj,val)
            obj = setParameter(obj,'Gy',val,0,'real');
        end
        function res = get.Gy(obj)
            res = getParameter(obj,'Gy');
        end
        function obj = set.Gz(obj,val)
            obj = setParameter(obj,'Gz',val,0,'real');
        end
        function res = get.Gz(obj)
            res = getParameter(obj,'Gz');
        end
        function obj = set.Grcvx(obj,val)
            obj = setParameter(obj,'Grcvx',val,0,'real');
        end
        function res = get.Grcvx(obj)
            res = getParameter(obj,'Grcvx');
        end
        function obj = set.Grcvy(obj,val)
            obj = setParameter(obj,'Grcvy',val,0,'real');
        end
        function res = get.Grcvy(obj)
            res = getParameter(obj,'Grcvy');
        end
        function obj = set.Grcvz(obj,val)
            obj = setParameter(obj,'Grcvz',val,0,'real');
        end
        function res = get.Grcvz(obj)
            res = getParameter(obj,'Grcvz');
        end
        function obj = set.Cbex(obj,val)
            obj = setParameter(obj,'Cbex',val,0,'real');
        end
        function res = get.Cbex(obj)
            res = getParameter(obj,'Cbex');
        end
        function obj = set.Cbey(obj,val)
            obj = setParameter(obj,'Cbey',val,0,'real');
        end
        function res = get.Cbey(obj)
            res = getParameter(obj,'Cbey');
        end
        function obj = set.Cbez(obj,val)
            obj = setParameter(obj,'Cbez',val,0,'real');
        end
        function res = get.Cbez(obj)
            res = getParameter(obj,'Cbez');
        end
        function obj = set.Gpi(obj,val)
            obj = setParameter(obj,'Gpi',val,0,'real');
        end
        function res = get.Gpi(obj)
            res = getParameter(obj,'Gpi');
        end
        function obj = set.Gmux(obj,val)
            obj = setParameter(obj,'Gmux',val,0,'real');
        end
        function res = get.Gmux(obj)
            res = getParameter(obj,'Gmux');
        end
        function obj = set.Gmuy(obj,val)
            obj = setParameter(obj,'Gmuy',val,0,'real');
        end
        function res = get.Gmuy(obj)
            res = getParameter(obj,'Gmuy');
        end
        function obj = set.Gmuz(obj,val)
            obj = setParameter(obj,'Gmuz',val,0,'real');
        end
        function res = get.Gmuz(obj)
            res = getParameter(obj,'Gmuz');
        end
        function obj = set.Cbcx(obj,val)
            obj = setParameter(obj,'Cbcx',val,0,'real');
        end
        function res = get.Cbcx(obj)
            res = getParameter(obj,'Cbcx');
        end
        function obj = set.Cbcy(obj,val)
            obj = setParameter(obj,'Cbcy',val,0,'real');
        end
        function res = get.Cbcy(obj)
            res = getParameter(obj,'Cbcy');
        end
        function obj = set.Cbcz(obj,val)
            obj = setParameter(obj,'Cbcz',val,0,'real');
        end
        function res = get.Cbcz(obj)
            res = getParameter(obj,'Cbcz');
        end
        function obj = set.Gbv(obj,val)
            obj = setParameter(obj,'Gbv',val,0,'real');
        end
        function res = get.Gbv(obj)
            res = getParameter(obj,'Gbv');
        end
        function obj = set.Grbvx(obj,val)
            obj = setParameter(obj,'Grbvx',val,0,'real');
        end
        function res = get.Grbvx(obj)
            res = getParameter(obj,'Grbvx');
        end
        function obj = set.Grbvy(obj,val)
            obj = setParameter(obj,'Grbvy',val,0,'real');
        end
        function res = get.Grbvy(obj)
            res = getParameter(obj,'Grbvy');
        end
        function obj = set.Grbvz(obj,val)
            obj = setParameter(obj,'Grbvz',val,0,'real');
        end
        function res = get.Grbvz(obj)
            res = getParameter(obj,'Grbvz');
        end
        function obj = set.Cb1b2(obj,val)
            obj = setParameter(obj,'Cb1b2',val,0,'real');
        end
        function res = get.Cb1b2(obj)
            res = getParameter(obj,'Cb1b2');
        end
        function obj = set.Cb1b2x(obj,val)
            obj = setParameter(obj,'Cb1b2x',val,0,'real');
        end
        function res = get.Cb1b2x(obj)
            res = getParameter(obj,'Cb1b2x');
        end
        function obj = set.Sgpi(obj,val)
            obj = setParameter(obj,'Sgpi',val,0,'real');
        end
        function res = get.Sgpi(obj)
            res = getParameter(obj,'Sgpi');
        end
        function obj = set.Scte(obj,val)
            obj = setParameter(obj,'Scte',val,0,'real');
        end
        function res = get.Scte(obj)
            res = getParameter(obj,'Scte');
        end
        function obj = set.Gpnp(obj,val)
            obj = setParameter(obj,'Gpnp',val,0,'real');
        end
        function res = get.Gpnp(obj)
            res = getParameter(obj,'Gpnp');
        end
        function obj = set.Xgpnp(obj,val)
            obj = setParameter(obj,'Xgpnp',val,0,'real');
        end
        function res = get.Xgpnp(obj)
            res = getParameter(obj,'Xgpnp');
        end
        function obj = set.Gsub(obj,val)
            obj = setParameter(obj,'Gsub',val,0,'real');
        end
        function res = get.Gsub(obj)
            res = getParameter(obj,'Gsub');
        end
        function obj = set.Cts(obj,val)
            obj = setParameter(obj,'Cts',val,0,'real');
        end
        function res = get.Cts(obj)
            res = getParameter(obj,'Cts');
        end
        function obj = set.Gmuex(obj,val)
            obj = setParameter(obj,'Gmuex',val,0,'real');
        end
        function res = get.Gmuex(obj)
            res = getParameter(obj,'Gmuex');
        end
        function obj = set.Cbcex(obj,val)
            obj = setParameter(obj,'Cbcex',val,0,'real');
        end
        function res = get.Cbcex(obj)
            res = getParameter(obj,'Cbcex');
        end
        function obj = set.Xgmuex(obj,val)
            obj = setParameter(obj,'Xgmuex',val,0,'real');
        end
        function res = get.Xgmuex(obj)
            res = getParameter(obj,'Xgmuex');
        end
        function obj = set.Xcbcex(obj,val)
            obj = setParameter(obj,'Xcbcex',val,0,'real');
        end
        function res = get.Xcbcex(obj)
            res = getParameter(obj,'Xcbcex');
        end
        function obj = set.Ge(obj,val)
            obj = setParameter(obj,'Ge',val,0,'real');
        end
        function res = get.Ge(obj)
            res = getParameter(obj,'Ge');
        end
        function obj = set.Gcc(obj,val)
            obj = setParameter(obj,'Gcc',val,0,'real');
        end
        function res = get.Gcc(obj)
            res = getParameter(obj,'Gcc');
        end
        function obj = set.Gbc(obj,val)
            obj = setParameter(obj,'Gbc',val,0,'real');
        end
        function res = get.Gbc(obj)
            res = getParameter(obj,'Gbc');
        end
    end
end

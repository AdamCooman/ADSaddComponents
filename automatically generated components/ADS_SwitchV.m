classdef ADS_SwitchV < ADScomponent
    % ADS_SwitchV matlab representation for the ADS SwitchV component
    % Voltage controlled switch
    % SwitchV [:Name] p n vcp vcn
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Resistance at voltage 1 (smorr) Unit: Ohms
        R1
        % Voltage 1 (smorr) Unit: V
        V1
        % Resistance at voltage 2 (smorr) Unit: Ohms
        R2
        % Voltage 2 (smorr) Unit: V
        V2
    end
    methods
        function obj = set.R1(obj,val)
            obj = setParameter(obj,'R1',val,0,'real');
        end
        function res = get.R1(obj)
            res = getParameter(obj,'R1');
        end
        function obj = set.V1(obj,val)
            obj = setParameter(obj,'V1',val,0,'real');
        end
        function res = get.V1(obj)
            res = getParameter(obj,'V1');
        end
        function obj = set.R2(obj,val)
            obj = setParameter(obj,'R2',val,0,'real');
        end
        function res = get.R2(obj)
            res = getParameter(obj,'R2');
        end
        function obj = set.V2(obj,val)
            obj = setParameter(obj,'V2',val,0,'real');
        end
        function res = get.V2(obj)
            res = getParameter(obj,'V2');
        end
    end
end

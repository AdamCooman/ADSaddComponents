classdef ADS_AIRIND1 < ADScomponent
    % ADS_AIRIND1 matlab representation for the ADS AIRIND1 component
    % Aircore Inductor (Wire Diameter)
    % AIRIND1 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of turns (Smorr) Unit: unknown
        N
        % Diameter of form (Smorr) Unit: m
        D
        % Length of form (Smorr) Unit: m
        L
        % Wire diameter (Smorr) Unit: m
        WD
        % Conductor resistivity (rel. to copper) (Smorr) Unit: unknown
        Rho
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.WD(obj,val)
            obj = setParameter(obj,'WD',val,0,'real');
        end
        function res = get.WD(obj)
            res = getParameter(obj,'WD');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

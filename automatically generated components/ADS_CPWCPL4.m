classdef ADS_CPWCPL4 < ADScomponent
    % ADS_CPWCPL4 matlab representation for the ADS CPWCPL4 component
    % Coplanar waveguide coupler (4 center conductors)
    % CPWCPL4 [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Width of outer center conductors (Smorr) Unit: m
        W
        % Gap (spacing) between outer center conductors and ground planes (Smorr) Unit: m
        G
        % Gap between outer and inner center conductors (Smorr) Unit: m
        S
        % Width of inner center conductors (Smorr) Unit: m
        Wi
        % Gap between inner center conductors (Smorr) Unit: m
        Si
        % Center conductor length (Smorr) Unit: m
        L
        % Coplanar waveguide substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Wi(obj,val)
            obj = setParameter(obj,'Wi',val,0,'real');
        end
        function res = get.Wi(obj)
            res = getParameter(obj,'Wi');
        end
        function obj = set.Si(obj,val)
            obj = setParameter(obj,'Si',val,0,'real');
        end
        function res = get.Si(obj)
            res = getParameter(obj,'Si');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_C_Model_Model < ADSmodel
    % ADS_C_Model_Model matlab representation for the ADS C_Model_Model component
    % Linear Two Terminal Capacitor with Model model
    % model ModelName C_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Capacitance (smorr) Unit: F
        C
        % Capacitance per area (smorr) Unit: F/m^2
        Cj
        % Capacitance per periphery (smorr) Unit: F/m
        Cjsw
        % Default Length (smorr) Unit: m
        Length
        % Default Width (smorr) Unit: m
        Width
        % Length and width narrowing due to etching (smorr) Unit: m
        Narrow
        % Nominal temperature of resistor in degrees Celsius (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Breakdown voltage (warning) (s--rr) Unit: V
        wBV
        % Capacitance scaling factor (smorr) 
        Scale
        % Nonlinear capacitor polynomial coeffs, syntax: Coeffs=list(c1,c2,...) (smorr) 
        Coeffs
        % Relative dielectric constant (smorr) 
        Di
        % Insulator thickness (smorr) Unit: m
        Thick
        % Shrink Factor for Length and Width (smorr) 
        Shrink
        % Default capacitance value (smorr) Unit: F
        Capdef
        % Maximum operating voltage (TSMC SOA warning) (smorr) Unit: V
        Bv_max
    end
    methods
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Cj(obj,val)
            obj = setParameter(obj,'Cj',val,0,'real');
        end
        function res = get.Cj(obj)
            res = getParameter(obj,'Cj');
        end
        function obj = set.Cjsw(obj,val)
            obj = setParameter(obj,'Cjsw',val,0,'real');
        end
        function res = get.Cjsw(obj)
            res = getParameter(obj,'Cjsw');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Narrow(obj,val)
            obj = setParameter(obj,'Narrow',val,0,'real');
        end
        function res = get.Narrow(obj)
            res = getParameter(obj,'Narrow');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.wBV(obj,val)
            obj = setParameter(obj,'wBV',val,0,'real');
        end
        function res = get.wBV(obj)
            res = getParameter(obj,'wBV');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Coeffs(obj,val)
            obj = setParameter(obj,'Coeffs',val,0,'real');
        end
        function res = get.Coeffs(obj)
            res = getParameter(obj,'Coeffs');
        end
        function obj = set.Di(obj,val)
            obj = setParameter(obj,'Di',val,0,'real');
        end
        function res = get.Di(obj)
            res = getParameter(obj,'Di');
        end
        function obj = set.Thick(obj,val)
            obj = setParameter(obj,'Thick',val,0,'real');
        end
        function res = get.Thick(obj)
            res = getParameter(obj,'Thick');
        end
        function obj = set.Shrink(obj,val)
            obj = setParameter(obj,'Shrink',val,0,'real');
        end
        function res = get.Shrink(obj)
            res = getParameter(obj,'Shrink');
        end
        function obj = set.Capdef(obj,val)
            obj = setParameter(obj,'Capdef',val,0,'real');
        end
        function res = get.Capdef(obj)
            res = getParameter(obj,'Capdef');
        end
        function obj = set.Bv_max(obj,val)
            obj = setParameter(obj,'Bv_max',val,0,'real');
        end
        function res = get.Bv_max(obj)
            res = getParameter(obj,'Bv_max');
        end
    end
end

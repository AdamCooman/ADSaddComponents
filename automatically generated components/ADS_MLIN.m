classdef ADS_MLIN < ADScomponent
    % ADS_MLIN matlab representation for the ADS MLIN component
    % Microstrip Line
    % MLIN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Line Width (Smorr) Unit: m
        W
        % Line Length (Smorr) Unit: m
        L
        % Distance from near edge of Microstrip Transmission Line to 1st sidewall (smorr) Unit: m
        Wall1
        % Distance from near edge of Microstrip Transmission Line to 2nd sidewall (smorr) Unit: m
        Wall2
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Wall1(obj,val)
            obj = setParameter(obj,'Wall1',val,0,'real');
        end
        function res = get.Wall1(obj)
            res = getParameter(obj,'Wall1');
        end
        function obj = set.Wall2(obj,val)
            obj = setParameter(obj,'Wall2',val,0,'real');
        end
        function res = get.Wall2(obj)
            res = getParameter(obj,'Wall2');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_AntLoad < ADScomponent
    % ADS_AntLoad matlab representation for the ADS AntLoad component
    % Antenna Load
    % AntLoad [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Antenna Type (S--ri) 
        AntType
        % end length (Smorr) Unit: meter
        Length
        % length divided by radius (Smorr) 
        RatioLR
        % Non-causal function impulse response order (sm-ri) Unit: (unknown units)
        ImpNoncausalLength
        % Convolution mode (sm-ri) Unit: (unknown units)
        ImpMode
        % Maximum Frequency (smorr) Unit: (unknown units)
        ImpMaxFreq
        % Sample Frequency (smorr) Unit: (unknown units)
        ImpDeltaFreq
        % maximum allowed impulse response order (sm-ri) Unit: (unknown units)
        ImpMaxPts
        % Smoothing Window (sm-ri) Unit: (unknown units)
        ImpWindow
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpRelTrunc
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpAbsTrunc
    end
    methods
        function obj = set.AntType(obj,val)
            obj = setParameter(obj,'AntType',val,0,'integer');
        end
        function res = get.AntType(obj)
            res = getParameter(obj,'AntType');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.RatioLR(obj,val)
            obj = setParameter(obj,'RatioLR',val,0,'real');
        end
        function res = get.RatioLR(obj)
            res = getParameter(obj,'RatioLR');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxPts(obj,val)
            obj = setParameter(obj,'ImpMaxPts',val,0,'integer');
        end
        function res = get.ImpMaxPts(obj)
            res = getParameter(obj,'ImpMaxPts');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTrunc(obj,val)
            obj = setParameter(obj,'ImpRelTrunc',val,0,'real');
        end
        function res = get.ImpRelTrunc(obj)
            res = getParameter(obj,'ImpRelTrunc');
        end
        function obj = set.ImpAbsTrunc(obj,val)
            obj = setParameter(obj,'ImpAbsTrunc',val,0,'real');
        end
        function res = get.ImpAbsTrunc(obj)
            res = getParameter(obj,'ImpAbsTrunc');
        end
    end
end

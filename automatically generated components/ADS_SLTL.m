classdef ADS_SLTL < ADScomponent
    % ADS_SLTL matlab representation for the ADS SLTL component
    % Stripline transmission line
    % SLTL [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of stripline (Smorr) Unit: m
        W
        % Length of stripline (Smorr) Unit: m
        L
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

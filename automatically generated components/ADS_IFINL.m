classdef ADS_IFINL < ADScomponent
    % ADS_IFINL matlab representation for the ADS IFINL component
    % Insulated Finline
    % IFINL [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of gap (Smorr) Unit: m
        D
        % Length of finline (Smorr) Unit: m
        L
        % Finline substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

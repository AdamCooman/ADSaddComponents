classdef ADS_SSLANGE < ADScomponent
    % ADS_SSLANGE matlab representation for the ADS SSLANGE component
    % Suspended Substrate Lange Coupler
    % SSLANGE [:Name] thru1 cpl2 cpl3 thru4 ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Interdigital trace width (smorr) 
        Wf
        % Spacing (smorr) Unit: m
        S
        % Length (smorr) Unit: m
        L
        % Number of fingers (smorr) 
        N
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Wf(obj,val)
            obj = setParameter(obj,'Wf',val,0,'real');
        end
        function res = get.Wf(obj)
            res = getParameter(obj,'Wf');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
    end
end

classdef ADS_Unalter < ADSnodeless
    % ADS_Unalter matlab representation for the ADS Unalter component
    % Undo a previous alter
    % Unalter [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Alter instance/path name to reset (s---s) 
        AlterName
        % Degree of annotation (s---i) 
        StatusLevel
    end
    methods
        function obj = set.AlterName(obj,val)
            obj = setParameter(obj,'AlterName',val,0,'string');
        end
        function res = get.AlterName(obj)
            res = getParameter(obj,'AlterName');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
    end
end

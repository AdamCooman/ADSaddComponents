classdef ADS_MSCTL < ADScomponent
    % ADS_MSCTL matlab representation for the ADS MSCTL component
    % Coupled microstrip transmission line
    % MSCTL [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Substrate label (Sm-rs) Unit: unknown
        Subst
        % Width of coupled microstrip line (Smorr) Unit: m
        W
        % Space of coupled microstrip line (Smorr) Unit: m
        S
        % Length of coupled microstrip line (Smorr) Unit: m
        L
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
    end
end

classdef ADS_Termination < ADScomponent
    % ADS_Termination matlab representation for the ADS Termination component
    % Termination model for Channel Simulation.
    % Termination [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % A resistive load. (s---r) 
        Load
    end
    methods
        function obj = set.Load(obj,val)
            obj = setParameter(obj,'Load',val,0,'real');
        end
        function res = get.Load(obj)
            res = getParameter(obj,'Load');
        end
    end
end

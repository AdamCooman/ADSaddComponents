classdef ADS_TLINP < ADScomponent
    % ADS_TLINP matlab representation for the ADS TLINP component
    % 2-Terminal Physical Transmission Line
    % TLINP [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Characteristic Impedance (Smorr) Unit: ohms
        Z
        % Physical Length (Smorr) Unit: m
        L
        % Effective Dielectric Constant (Smorr) Unit: unknown
        K
        % Attenuation (Smorr) Unit: DB
        A
        % Frequency for Scaling attenuation (Smorr) Unit: hz
        F
        % Dielectric loss tangent value (Smorr) Unit: unknown
        TanD
        % Relative Permeability (Smorr) Unit: unknown
        Mur
        % Magnetic Loss Tangent (Smorr) Unit: unknown
        TanM
        % Dielectric Conductivity value (Smorr) Unit: siemens
        Sigma
        % Physical Temperature (smorr) Unit: C
        Temp
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) Unit: unknown
        DielectricLossModel
        % Frequency at which Er and TanD are measured (smorr) Unit: hz
        FreqForEpsrTand
        % Low roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        LowFreqForTand
        % High roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        HighFreqForTand
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.K(obj,val)
            obj = setParameter(obj,'K',val,0,'real');
        end
        function res = get.K(obj)
            res = getParameter(obj,'K');
        end
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'real');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.TanM(obj,val)
            obj = setParameter(obj,'TanM',val,0,'real');
        end
        function res = get.TanM(obj)
            res = getParameter(obj,'TanM');
        end
        function obj = set.Sigma(obj,val)
            obj = setParameter(obj,'Sigma',val,0,'real');
        end
        function res = get.Sigma(obj)
            res = getParameter(obj,'Sigma');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.FreqForEpsrTand(obj,val)
            obj = setParameter(obj,'FreqForEpsrTand',val,0,'real');
        end
        function res = get.FreqForEpsrTand(obj)
            res = getParameter(obj,'FreqForEpsrTand');
        end
        function obj = set.LowFreqForTand(obj,val)
            obj = setParameter(obj,'LowFreqForTand',val,0,'real');
        end
        function res = get.LowFreqForTand(obj)
            res = getParameter(obj,'LowFreqForTand');
        end
        function obj = set.HighFreqForTand(obj,val)
            obj = setParameter(obj,'HighFreqForTand',val,0,'real');
        end
        function res = get.HighFreqForTand(obj)
            res = getParameter(obj,'HighFreqForTand');
        end
    end
end

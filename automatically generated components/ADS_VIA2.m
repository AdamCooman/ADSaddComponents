classdef ADS_VIA2 < ADScomponent
    % ADS_VIA2 matlab representation for the ADS VIA2 component
    % Cylindrical Via Hole in Microstrip
    % VIA2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Hole Diameter (Smorr) Unit: m
        D
        % Substrate Thickness (Smorr) Unit: m
        H
        % Metal Thickness (Smorr) Unit: m
        T
        % Metal bulk resistivity (relative to Gold) (Smorr) Unit: unknown
        Rho
        % Width of Via pad (assumed square) (Smorr) Unit: m
        W
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

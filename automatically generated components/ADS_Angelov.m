classdef ADS_Angelov < ADScomponent
    % ADS_Angelov matlab representation for the ADS Angelov component
    % Angelov HEMT/MESFET Model
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Channel width (smorr) Unit: m
        W
        % Ambient device temperature (smorr) Unit: deg C
        Temp
        % Temperature offset of the device with respect to Temp (smorr) Unit: K
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal drain source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal gate source conductance (---rr) Unit: S
        Ggs
        % Small signal gate drain conductance (---rr) Unit: S
        Ggd
        % Small signal gate source capacitance (---rr) Unit: F
        Cgs
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgd
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Ggs(obj,val)
            obj = setParameter(obj,'Ggs',val,0,'real');
        end
        function res = get.Ggs(obj)
            res = getParameter(obj,'Ggs');
        end
        function obj = set.Ggd(obj,val)
            obj = setParameter(obj,'Ggd',val,0,'real');
        end
        function res = get.Ggd(obj)
            res = getParameter(obj,'Ggd');
        end
        function obj = set.Cgs(obj,val)
            obj = setParameter(obj,'Cgs',val,0,'real');
        end
        function res = get.Cgs(obj)
            res = getParameter(obj,'Cgs');
        end
        function obj = set.Cgd(obj,val)
            obj = setParameter(obj,'Cgd',val,0,'real');
        end
        function res = get.Cgd(obj)
            res = getParameter(obj,'Cgd');
        end
    end
end

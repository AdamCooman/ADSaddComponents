classdef ADS_Xport < ADScomponent
    % ADS_Xport matlab representation for the ADS Xport component
    % X-parameter Behavioral Model
    % Xport [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % X parameter data file name (sm-rs) 
        File
        % Type of the X-parameter data file: "dataset"/"gmdif" (sm-rs) 
        Type
        % Interpolation mode: "linear"/"spline"/"cubic" (sm-rs) 
        InterpMode
        % Interpolation domain: "ri"/"ma"/ (sm-rs) 
        InterpDom
        % Extrapolation mode: "constant"/"interpMode" (sm-rs) 
        ExtrapMode
        % Perform interpolation with Volterra normalization (sm-rb) 
        EnableNormalization
        % UserDefined independent variable names (sm-rs) 
        UserVar
        % UserDefined independent variable values (sm-rs) 
        UserValue
        % Frequency of model ith fundamental (smorr) Unit: Hz
        ModelFundamentalFreq
        % Small-signal absolute tolerance (smorr) 
        SMminAbs
        % Small-signal relative tolerance (smorr) 
        SMminRel
        % Small-signal max order (sm-ri) 
        SMmaxOrder
        % Model complexity reduction level (0 to 5): 0 - full model -> 5 - max reduction (sm-ri) 
        ModelReductionLevel
        % Flag to save reduced model in a dataset: yes/no (s--rb) 
        SaveReducedModel
        % X-parameter step response data file name (sm-rs) 
        StepResponseFile
    end
    methods
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.InterpMode(obj,val)
            obj = setParameter(obj,'InterpMode',val,0,'string');
        end
        function res = get.InterpMode(obj)
            res = getParameter(obj,'InterpMode');
        end
        function obj = set.InterpDom(obj,val)
            obj = setParameter(obj,'InterpDom',val,0,'string');
        end
        function res = get.InterpDom(obj)
            res = getParameter(obj,'InterpDom');
        end
        function obj = set.ExtrapMode(obj,val)
            obj = setParameter(obj,'ExtrapMode',val,0,'string');
        end
        function res = get.ExtrapMode(obj)
            res = getParameter(obj,'ExtrapMode');
        end
        function obj = set.EnableNormalization(obj,val)
            obj = setParameter(obj,'EnableNormalization',val,0,'boolean');
        end
        function res = get.EnableNormalization(obj)
            res = getParameter(obj,'EnableNormalization');
        end
        function obj = set.UserVar(obj,val)
            obj = setParameter(obj,'UserVar',val,0,'string');
        end
        function res = get.UserVar(obj)
            res = getParameter(obj,'UserVar');
        end
        function obj = set.UserValue(obj,val)
            obj = setParameter(obj,'UserValue',val,0,'string');
        end
        function res = get.UserValue(obj)
            res = getParameter(obj,'UserValue');
        end
        function obj = set.ModelFundamentalFreq(obj,val)
            obj = setParameter(obj,'ModelFundamentalFreq',val,1,'real');
        end
        function res = get.ModelFundamentalFreq(obj)
            res = getParameter(obj,'ModelFundamentalFreq');
        end
        function obj = set.SMminAbs(obj,val)
            obj = setParameter(obj,'SMminAbs',val,0,'real');
        end
        function res = get.SMminAbs(obj)
            res = getParameter(obj,'SMminAbs');
        end
        function obj = set.SMminRel(obj,val)
            obj = setParameter(obj,'SMminRel',val,0,'real');
        end
        function res = get.SMminRel(obj)
            res = getParameter(obj,'SMminRel');
        end
        function obj = set.SMmaxOrder(obj,val)
            obj = setParameter(obj,'SMmaxOrder',val,0,'integer');
        end
        function res = get.SMmaxOrder(obj)
            res = getParameter(obj,'SMmaxOrder');
        end
        function obj = set.ModelReductionLevel(obj,val)
            obj = setParameter(obj,'ModelReductionLevel',val,0,'integer');
        end
        function res = get.ModelReductionLevel(obj)
            res = getParameter(obj,'ModelReductionLevel');
        end
        function obj = set.SaveReducedModel(obj,val)
            obj = setParameter(obj,'SaveReducedModel',val,0,'boolean');
        end
        function res = get.SaveReducedModel(obj)
            res = getParameter(obj,'SaveReducedModel');
        end
        function obj = set.StepResponseFile(obj,val)
            obj = setParameter(obj,'StepResponseFile',val,0,'string');
        end
        function res = get.StepResponseFile(obj)
            res = getParameter(obj,'StepResponseFile');
        end
    end
end

classdef ADS_MICAPo4 < ADScomponent
    % ADS_MICAPo4 matlab representation for the ADS MICAPo4 component
    % Microstrip Interdigital Capacitor (Grounded 2-port) with Odd or Even Number of Fingers
    % MICAPo4 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Finger Width (Smorr) Unit: m
        W
        % Gap between Fingers (Smorr) Unit: m
        G
        % Gap at End of Fingers (Smorr) Unit: m
        Ge
        % Length of Overlapped region (Smorr) Unit: m
        L
        % Number of Fingers (an integer) (Sm-ri) Unit: unknown
        Nd
        % Width of the Interconnect (Smorr) Unit: m
        Wt
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.Ge(obj,val)
            obj = setParameter(obj,'Ge',val,0,'real');
        end
        function res = get.Ge(obj)
            res = getParameter(obj,'Ge');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Nd(obj,val)
            obj = setParameter(obj,'Nd',val,0,'integer');
        end
        function res = get.Nd(obj)
            res = getParameter(obj,'Nd');
        end
        function obj = set.Wt(obj,val)
            obj = setParameter(obj,'Wt',val,0,'real');
        end
        function res = get.Wt(obj)
            res = getParameter(obj,'Wt');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

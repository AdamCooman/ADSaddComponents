classdef ADS_AMIxtlkRx < ADScomponent
    % ADS_AMIxtlkRx matlab representation for the ADS AMIxtlkRx component
    % AMI cross-talk receiver (IBIS) model for Channel Simulation.
    % AMIxtlkRx [:Name] p n
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Channel Index (s--ri) 
        ChannelIndex
        % IBIS file name (sm-rs) 
        IbisFile
        % IC identifier (sm-rs) 
        ComponentName
        % Pin number of an IC (the non-inverting pin number for a differential buffer) (sm-rs) 
        PinName
        % Inverting pin number for a differential buffer (sm-rs) 
        InvPinName
        % Ibis file model name (sm-rs) 
        ModelName
        % Flag to use DataTypeSelector and ignore individual settings (yes/no) (s--rb) 
        SetAllData
        % Global flag for the data type to use: 1-typ, 2-min, 3-max, 4-fast, 5-slow (sm-ri) 
        DataTypeSelector
        % Flag to use the package parasitics (s--rb) 
        UsePkg
        % Data type to use for R_pkg: 1-typ, 2-min, 3-max (sm-ri) 
        RpkgType
        % Data type to use for L_pkg under the [Package] keyword: 1-typ, 2-min, 3-max (sm-ri) 
        LpkgType
        % Data type to use for C_pkg under the [Package] keyword: 1-typ, 2-min, 3-max (sm-ri) 
        CpkgType
        % Delay data type for differential buffers: 1-tdelay_typ, 2-tdelay_min, 3-tdelay_max (sm-ri) 
        DiffTimeDelayType
        % Data type to use for C_comp: 1-typ, 2-min, 3-max (sm-ri) 
        CcompType
        % Data type to use for the transit time ground clamp capacitance: 1-typ, 2-min, 3-max (sm-ri) 
        TTgndType
        % Data type to use for the transit time power clamp capacitance: 1-typ, 2-min, 3-max (sm-ri) 
        TTpowerType
        % Data type to use for Rgnd in a terminator buffer: 1-typ, 2-min, 3-max (sm-ri) 
        RgndType
        % Data type to use for Rpower in a terminator buffer: 1-typ, 2-min, 3-max (sm-ri) 
        RpowerType
        % Data type to use for Rac in a terminator buffer: 1-typ, 2-min, 3-max (sm-ri) 
        RacType
        % Data type to use for Cac in a terminator buffer: 1-typ, 2-min, 3-max (sm-ri) 
        CacType
        % Type of I-V data to use for the pullup device: 1-typ, 2-min, 3-max (sm-ri) 
        PuDataType
        % Type of I-V data to use for the pulldown device: 1-typ, 2-min, 3-max (sm-ri) 
        PdDataType
        % Type of I-V data to use for the power clamp: 1-typ, 2-min, 3-max (sm-ri) 
        PcDataType
        % Type of I-V data to use for the ground clamp: 1-typ, 2-min, 3-max (sm-ri) 
        GcDataType
        % Data type to use for the Vt tables: 1-typ, 2-min, 3-max (sm-ri) 
        WaveformType
        % Flag to use the [Ramp] data instead of Vt tables (yes/no) (sm-rb) 
        IgnoreWaveforms
        % Data type to use for [Ramp] transitions: 1-typ, 2-min, 3-max (sm-ri) 
        RampType
        % Flag to use the Driver Schedule if specified (yes/no) (sm-rb) 
        UseDriverSchedule
        % Flag to override the IBIS file setting of [Polarity]: 0-non-inverting, 1-inverting (sm-ri) 
        Polarity
        % Threshold for trigerring a rising event in a non-inverting buffer - range <0.5, 1.0) (smorr) 
        TriggerLevel
        % Pin1 under Series Pin Mapping (sm-rs) 
        SeriesPin1
        % Pin2 under Series Pin Mapping (sm-rs) 
        SeriesPin2
        % Flag for SeriesSwitch (s--rb) 
        SeriesSwitch
        % Data type to use for R_Series: 1-typ, 2-min, 3-max (sm-ri) 
        R_SeriesType
        % Data type to use for L_Series: 1-typ, 2-min, 3-max (sm-ri) 
        L_SeriesType
        % Data type to use for Rl_Series: 1-typ, 2-min, 3-max (sm-ri) 
        Rl_SeriesType
        % Data type to use for C_Series: 1-typ, 2-min, 3-max (sm-ri) 
        C_SeriesType
        % Data type to use for Rc_Series: 1-typ, 2-min, 3-max (sm-ri) 
        Rc_SeriesType
        % Data type to use for Lc_Series: 1-typ, 2-min, 3-max (sm-ri) 
        Lc_SeriesType
        % Data type to use for Series Current: 1-typ, 2-min, 3-max (sm-ri) 
        SeriesCurrentType
        % Data type to use for Series MOSFET: 1-typ, 2-min, 3-max (sm-ri) 
        SeriesMOSFETType
        % Data type to use for Vcc in Series MOSFET: 1-typ, 2-min, 3-max (sm-ri) 
        VccType
        % Data type to use for Vinh in Model Spec: 1-typ, 2-min, 3-max (sm-ri) 
        VinhType
        % Data type to use for Vinl in Model Spec: 1-typ, 2-min, 3-max (sm-ri) 
        VinlType
        % Data type to use for Off_Delay : 1-typ, 2-min, 3-max (sm-ri) 
        OffDelayType
        % disable ramp for one VT table (s--rb) 
        DisableRampWaveformSolution
        % Interpolation mode: "linear"/"spline"/"cubic" (sm-rs) 
        InterpMode
        % Flag to use the user parameter `EnabledByUser' to set the enable status (sm-rb) 
        EnableOverride
        % Flag to set the enable status if the parameter `EnableOverride' is set to `yes' (sm-rb) 
        EnabledByUser
        % Flag to use the IBIS file data and set up DC voltage sources internally (sm-rb) 
        InternalPowerSupply
        % Flag to use an external pin for component ground (sm-rb) 
        FloatingGround
        % Flag to output an external pin for probing the die/pad voltage (sm-rb) 
        DieProbe
    end
    methods
        function obj = set.ChannelIndex(obj,val)
            obj = setParameter(obj,'ChannelIndex',val,0,'integer');
        end
        function res = get.ChannelIndex(obj)
            res = getParameter(obj,'ChannelIndex');
        end
        function obj = set.IbisFile(obj,val)
            obj = setParameter(obj,'IbisFile',val,0,'string');
        end
        function res = get.IbisFile(obj)
            res = getParameter(obj,'IbisFile');
        end
        function obj = set.ComponentName(obj,val)
            obj = setParameter(obj,'ComponentName',val,0,'string');
        end
        function res = get.ComponentName(obj)
            res = getParameter(obj,'ComponentName');
        end
        function obj = set.PinName(obj,val)
            obj = setParameter(obj,'PinName',val,0,'string');
        end
        function res = get.PinName(obj)
            res = getParameter(obj,'PinName');
        end
        function obj = set.InvPinName(obj,val)
            obj = setParameter(obj,'InvPinName',val,0,'string');
        end
        function res = get.InvPinName(obj)
            res = getParameter(obj,'InvPinName');
        end
        function obj = set.ModelName(obj,val)
            obj = setParameter(obj,'ModelName',val,0,'string');
        end
        function res = get.ModelName(obj)
            res = getParameter(obj,'ModelName');
        end
        function obj = set.SetAllData(obj,val)
            obj = setParameter(obj,'SetAllData',val,0,'boolean');
        end
        function res = get.SetAllData(obj)
            res = getParameter(obj,'SetAllData');
        end
        function obj = set.DataTypeSelector(obj,val)
            obj = setParameter(obj,'DataTypeSelector',val,0,'integer');
        end
        function res = get.DataTypeSelector(obj)
            res = getParameter(obj,'DataTypeSelector');
        end
        function obj = set.UsePkg(obj,val)
            obj = setParameter(obj,'UsePkg',val,0,'boolean');
        end
        function res = get.UsePkg(obj)
            res = getParameter(obj,'UsePkg');
        end
        function obj = set.RpkgType(obj,val)
            obj = setParameter(obj,'RpkgType',val,0,'integer');
        end
        function res = get.RpkgType(obj)
            res = getParameter(obj,'RpkgType');
        end
        function obj = set.LpkgType(obj,val)
            obj = setParameter(obj,'LpkgType',val,0,'integer');
        end
        function res = get.LpkgType(obj)
            res = getParameter(obj,'LpkgType');
        end
        function obj = set.CpkgType(obj,val)
            obj = setParameter(obj,'CpkgType',val,0,'integer');
        end
        function res = get.CpkgType(obj)
            res = getParameter(obj,'CpkgType');
        end
        function obj = set.DiffTimeDelayType(obj,val)
            obj = setParameter(obj,'DiffTimeDelayType',val,0,'integer');
        end
        function res = get.DiffTimeDelayType(obj)
            res = getParameter(obj,'DiffTimeDelayType');
        end
        function obj = set.CcompType(obj,val)
            obj = setParameter(obj,'CcompType',val,0,'integer');
        end
        function res = get.CcompType(obj)
            res = getParameter(obj,'CcompType');
        end
        function obj = set.TTgndType(obj,val)
            obj = setParameter(obj,'TTgndType',val,0,'integer');
        end
        function res = get.TTgndType(obj)
            res = getParameter(obj,'TTgndType');
        end
        function obj = set.TTpowerType(obj,val)
            obj = setParameter(obj,'TTpowerType',val,0,'integer');
        end
        function res = get.TTpowerType(obj)
            res = getParameter(obj,'TTpowerType');
        end
        function obj = set.RgndType(obj,val)
            obj = setParameter(obj,'RgndType',val,0,'integer');
        end
        function res = get.RgndType(obj)
            res = getParameter(obj,'RgndType');
        end
        function obj = set.RpowerType(obj,val)
            obj = setParameter(obj,'RpowerType',val,0,'integer');
        end
        function res = get.RpowerType(obj)
            res = getParameter(obj,'RpowerType');
        end
        function obj = set.RacType(obj,val)
            obj = setParameter(obj,'RacType',val,0,'integer');
        end
        function res = get.RacType(obj)
            res = getParameter(obj,'RacType');
        end
        function obj = set.CacType(obj,val)
            obj = setParameter(obj,'CacType',val,0,'integer');
        end
        function res = get.CacType(obj)
            res = getParameter(obj,'CacType');
        end
        function obj = set.PuDataType(obj,val)
            obj = setParameter(obj,'PuDataType',val,0,'integer');
        end
        function res = get.PuDataType(obj)
            res = getParameter(obj,'PuDataType');
        end
        function obj = set.PdDataType(obj,val)
            obj = setParameter(obj,'PdDataType',val,0,'integer');
        end
        function res = get.PdDataType(obj)
            res = getParameter(obj,'PdDataType');
        end
        function obj = set.PcDataType(obj,val)
            obj = setParameter(obj,'PcDataType',val,0,'integer');
        end
        function res = get.PcDataType(obj)
            res = getParameter(obj,'PcDataType');
        end
        function obj = set.GcDataType(obj,val)
            obj = setParameter(obj,'GcDataType',val,0,'integer');
        end
        function res = get.GcDataType(obj)
            res = getParameter(obj,'GcDataType');
        end
        function obj = set.WaveformType(obj,val)
            obj = setParameter(obj,'WaveformType',val,0,'integer');
        end
        function res = get.WaveformType(obj)
            res = getParameter(obj,'WaveformType');
        end
        function obj = set.IgnoreWaveforms(obj,val)
            obj = setParameter(obj,'IgnoreWaveforms',val,0,'boolean');
        end
        function res = get.IgnoreWaveforms(obj)
            res = getParameter(obj,'IgnoreWaveforms');
        end
        function obj = set.RampType(obj,val)
            obj = setParameter(obj,'RampType',val,0,'integer');
        end
        function res = get.RampType(obj)
            res = getParameter(obj,'RampType');
        end
        function obj = set.UseDriverSchedule(obj,val)
            obj = setParameter(obj,'UseDriverSchedule',val,0,'boolean');
        end
        function res = get.UseDriverSchedule(obj)
            res = getParameter(obj,'UseDriverSchedule');
        end
        function obj = set.Polarity(obj,val)
            obj = setParameter(obj,'Polarity',val,0,'integer');
        end
        function res = get.Polarity(obj)
            res = getParameter(obj,'Polarity');
        end
        function obj = set.TriggerLevel(obj,val)
            obj = setParameter(obj,'TriggerLevel',val,0,'real');
        end
        function res = get.TriggerLevel(obj)
            res = getParameter(obj,'TriggerLevel');
        end
        function obj = set.SeriesPin1(obj,val)
            obj = setParameter(obj,'SeriesPin1',val,0,'string');
        end
        function res = get.SeriesPin1(obj)
            res = getParameter(obj,'SeriesPin1');
        end
        function obj = set.SeriesPin2(obj,val)
            obj = setParameter(obj,'SeriesPin2',val,0,'string');
        end
        function res = get.SeriesPin2(obj)
            res = getParameter(obj,'SeriesPin2');
        end
        function obj = set.SeriesSwitch(obj,val)
            obj = setParameter(obj,'SeriesSwitch',val,0,'boolean');
        end
        function res = get.SeriesSwitch(obj)
            res = getParameter(obj,'SeriesSwitch');
        end
        function obj = set.R_SeriesType(obj,val)
            obj = setParameter(obj,'R_SeriesType',val,0,'integer');
        end
        function res = get.R_SeriesType(obj)
            res = getParameter(obj,'R_SeriesType');
        end
        function obj = set.L_SeriesType(obj,val)
            obj = setParameter(obj,'L_SeriesType',val,0,'integer');
        end
        function res = get.L_SeriesType(obj)
            res = getParameter(obj,'L_SeriesType');
        end
        function obj = set.Rl_SeriesType(obj,val)
            obj = setParameter(obj,'Rl_SeriesType',val,0,'integer');
        end
        function res = get.Rl_SeriesType(obj)
            res = getParameter(obj,'Rl_SeriesType');
        end
        function obj = set.C_SeriesType(obj,val)
            obj = setParameter(obj,'C_SeriesType',val,0,'integer');
        end
        function res = get.C_SeriesType(obj)
            res = getParameter(obj,'C_SeriesType');
        end
        function obj = set.Rc_SeriesType(obj,val)
            obj = setParameter(obj,'Rc_SeriesType',val,0,'integer');
        end
        function res = get.Rc_SeriesType(obj)
            res = getParameter(obj,'Rc_SeriesType');
        end
        function obj = set.Lc_SeriesType(obj,val)
            obj = setParameter(obj,'Lc_SeriesType',val,0,'integer');
        end
        function res = get.Lc_SeriesType(obj)
            res = getParameter(obj,'Lc_SeriesType');
        end
        function obj = set.SeriesCurrentType(obj,val)
            obj = setParameter(obj,'SeriesCurrentType',val,0,'integer');
        end
        function res = get.SeriesCurrentType(obj)
            res = getParameter(obj,'SeriesCurrentType');
        end
        function obj = set.SeriesMOSFETType(obj,val)
            obj = setParameter(obj,'SeriesMOSFETType',val,0,'integer');
        end
        function res = get.SeriesMOSFETType(obj)
            res = getParameter(obj,'SeriesMOSFETType');
        end
        function obj = set.VccType(obj,val)
            obj = setParameter(obj,'VccType',val,0,'integer');
        end
        function res = get.VccType(obj)
            res = getParameter(obj,'VccType');
        end
        function obj = set.VinhType(obj,val)
            obj = setParameter(obj,'VinhType',val,0,'integer');
        end
        function res = get.VinhType(obj)
            res = getParameter(obj,'VinhType');
        end
        function obj = set.VinlType(obj,val)
            obj = setParameter(obj,'VinlType',val,0,'integer');
        end
        function res = get.VinlType(obj)
            res = getParameter(obj,'VinlType');
        end
        function obj = set.OffDelayType(obj,val)
            obj = setParameter(obj,'OffDelayType',val,0,'integer');
        end
        function res = get.OffDelayType(obj)
            res = getParameter(obj,'OffDelayType');
        end
        function obj = set.DisableRampWaveformSolution(obj,val)
            obj = setParameter(obj,'DisableRampWaveformSolution',val,0,'boolean');
        end
        function res = get.DisableRampWaveformSolution(obj)
            res = getParameter(obj,'DisableRampWaveformSolution');
        end
        function obj = set.InterpMode(obj,val)
            obj = setParameter(obj,'InterpMode',val,0,'string');
        end
        function res = get.InterpMode(obj)
            res = getParameter(obj,'InterpMode');
        end
        function obj = set.EnableOverride(obj,val)
            obj = setParameter(obj,'EnableOverride',val,0,'boolean');
        end
        function res = get.EnableOverride(obj)
            res = getParameter(obj,'EnableOverride');
        end
        function obj = set.EnabledByUser(obj,val)
            obj = setParameter(obj,'EnabledByUser',val,0,'boolean');
        end
        function res = get.EnabledByUser(obj)
            res = getParameter(obj,'EnabledByUser');
        end
        function obj = set.InternalPowerSupply(obj,val)
            obj = setParameter(obj,'InternalPowerSupply',val,0,'boolean');
        end
        function res = get.InternalPowerSupply(obj)
            res = getParameter(obj,'InternalPowerSupply');
        end
        function obj = set.FloatingGround(obj,val)
            obj = setParameter(obj,'FloatingGround',val,0,'boolean');
        end
        function res = get.FloatingGround(obj)
            res = getParameter(obj,'FloatingGround');
        end
        function obj = set.DieProbe(obj,val)
            obj = setParameter(obj,'DieProbe',val,0,'boolean');
        end
        function res = get.DieProbe(obj)
            res = getParameter(obj,'DieProbe');
        end
    end
end

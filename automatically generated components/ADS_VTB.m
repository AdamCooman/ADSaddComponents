classdef ADS_VTB < ADScomponent
    % ADS_VTB matlab representation for the ADS VTB component
    % VTB component
    % VTB [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Systemvue workspace filename (s---s) 
        VTB_Workspace
        % Systemvue log file filename (s---s) 
        VTB_LogFile
        % Analysis name (s---s) 
        VTB_AnalysisName
        % Sampling rate (s---r) 
        VTB_TimeStep
        % Sampling rate (s---r) 
        VTB_TimeStop
        % Systemvue port name (s---s) 
        VTB_PortName
        % Systemvue param name vector (s---s) 
        VTB_ParamName
        % Systemvue param value vector (smo-s) 
        VTB_ParamValue
        % Port impedance (s--rc) Unit: Ohms
        VTB_PortZ
        % Max envelope time for 1-port devices. (s---r) 
        VTB_HardStopTime
    end
    methods
        function obj = set.VTB_Workspace(obj,val)
            obj = setParameter(obj,'VTB_Workspace',val,0,'string');
        end
        function res = get.VTB_Workspace(obj)
            res = getParameter(obj,'VTB_Workspace');
        end
        function obj = set.VTB_LogFile(obj,val)
            obj = setParameter(obj,'VTB_LogFile',val,0,'string');
        end
        function res = get.VTB_LogFile(obj)
            res = getParameter(obj,'VTB_LogFile');
        end
        function obj = set.VTB_AnalysisName(obj,val)
            obj = setParameter(obj,'VTB_AnalysisName',val,0,'string');
        end
        function res = get.VTB_AnalysisName(obj)
            res = getParameter(obj,'VTB_AnalysisName');
        end
        function obj = set.VTB_TimeStep(obj,val)
            obj = setParameter(obj,'VTB_TimeStep',val,0,'real');
        end
        function res = get.VTB_TimeStep(obj)
            res = getParameter(obj,'VTB_TimeStep');
        end
        function obj = set.VTB_TimeStop(obj,val)
            obj = setParameter(obj,'VTB_TimeStop',val,0,'real');
        end
        function res = get.VTB_TimeStop(obj)
            res = getParameter(obj,'VTB_TimeStop');
        end
        function obj = set.VTB_PortName(obj,val)
            obj = setParameter(obj,'VTB_PortName',val,1,'string');
        end
        function res = get.VTB_PortName(obj)
            res = getParameter(obj,'VTB_PortName');
        end
        function obj = set.VTB_ParamName(obj,val)
            obj = setParameter(obj,'VTB_ParamName',val,1,'string');
        end
        function res = get.VTB_ParamName(obj)
            res = getParameter(obj,'VTB_ParamName');
        end
        function obj = set.VTB_ParamValue(obj,val)
            obj = setParameter(obj,'VTB_ParamValue',val,1,'string');
        end
        function res = get.VTB_ParamValue(obj)
            res = getParameter(obj,'VTB_ParamValue');
        end
        function obj = set.VTB_PortZ(obj,val)
            obj = setParameter(obj,'VTB_PortZ',val,1,'complex');
        end
        function res = get.VTB_PortZ(obj)
            res = getParameter(obj,'VTB_PortZ');
        end
        function obj = set.VTB_HardStopTime(obj,val)
            obj = setParameter(obj,'VTB_HardStopTime',val,0,'real');
        end
        function res = get.VTB_HardStopTime(obj)
            res = getParameter(obj,'VTB_HardStopTime');
        end
    end
end

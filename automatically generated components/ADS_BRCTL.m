classdef ADS_BRCTL < ADScomponent
    % ADS_BRCTL matlab representation for the ADS BRCTL component
    % Broadside coupled line
    % BRCTL [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Width of broadside coupled line (Smorr) Unit: m
        W
        % Ground plane spacing of broadside coupled line (Smorr) Unit: m
        B
        % Length of broadside coupled line (Smorr) Unit: m
        L
        % Dielectric constant of broadside coupled line (Smorr) Unit: 1
        Er
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

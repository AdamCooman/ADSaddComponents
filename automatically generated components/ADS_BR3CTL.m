classdef ADS_BR3CTL < ADScomponent
    % ADS_BR3CTL matlab representation for the ADS BR3CTL component
    % Three Broadside Coupled Transmission Lines
    % BR3CTL [:Name] line1_upper_left line2_upper_left line3_lower_left line1_upper_right line2_upper_right line3_lower_right ...
    properties (Access=protected)
        NumberOfNodes = 6
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width at terminal 1 (smorr) Unit: m
        W1u
        % Line Width at terminal 2 (smorr) Unit: m
        W2u
        % Line Width at terminal 3 (smorr) Unit: m
        W3l
        % Length (smorr) Unit: m
        L
        % Spacing between strips 1 and 2 on upper side of substrate (smorr) Unit: m
        Su
        % Offset of strip 3 toward strip 1 (smorr) Unit: m
        S
        % Dielectric Constant of Top Layer (smorr) 
        Eru
        % Dielectric Constant of Bottom Layer (smorr) 
        Erl
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W1u(obj,val)
            obj = setParameter(obj,'W1u',val,0,'real');
        end
        function res = get.W1u(obj)
            res = getParameter(obj,'W1u');
        end
        function obj = set.W2u(obj,val)
            obj = setParameter(obj,'W2u',val,0,'real');
        end
        function res = get.W2u(obj)
            res = getParameter(obj,'W2u');
        end
        function obj = set.W3l(obj,val)
            obj = setParameter(obj,'W3l',val,0,'real');
        end
        function res = get.W3l(obj)
            res = getParameter(obj,'W3l');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Su(obj,val)
            obj = setParameter(obj,'Su',val,0,'real');
        end
        function res = get.Su(obj)
            res = getParameter(obj,'Su');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Eru(obj,val)
            obj = setParameter(obj,'Eru',val,0,'real');
        end
        function res = get.Eru(obj)
            res = getParameter(obj,'Eru');
        end
        function obj = set.Erl(obj,val)
            obj = setParameter(obj,'Erl',val,0,'real');
        end
        function res = get.Erl(obj)
            res = getParameter(obj,'Erl');
        end
    end
end

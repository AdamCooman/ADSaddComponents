classdef ADS_DC < ADSnodeless
    % ADS_DC matlab representation for the ADS DC component
    % DC Analysis
    % DC [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of variable or parameter to be swept (s---s) 
        SweepVar
        % Stim instance/path name for sweep values (repeatable) (s---d) 
        SweepPlan
        % Max change in node voltage allowed per iteration (s---r) Unit: V
        MaxDeltaV
        % Convergence algorithm selection (s---i) 
        ConvMode
        % Max number of iterations (s---i) 
        MaxIters
        % Levels of subcircuits to output (s---i) 
        NestLevel
        % Degree of annotation (s---i) 
        StatusLevel
        % Variable or parameter to output (repeatable) (s---s) 
        OutVar
        % Print operating point (s---b) 
        PrintOpPoint
        % Do not use previous solution as initial guess (s---i) 
        Restart
        % Use finite differences for sensitivities/derivatives (s---b) 
        UseFiniteDiff
        % Select limiting mode (s---i) 
        LimitingMode
        % Maximum arc-length step (s---r) 
        ArcMaxStep
        % Maximum arc-length step for source-level continuation (s---r) 
        ArcLevelMaxStep
        % Minimum value for parameter during arclength continuation (s---r) 
        ArcMinValue
        % Maximum value for parameter during arclength continuation (s---r) 
        ArcMaxValue
        % Ratio of maximum to given number of steps (s---r) 
        MaxStepRatio
        % Maximum step shrinkage (s---r) 
        MaxShrinkage
        % Output solution at all internal steps when sweeping (s---b) 
        OutputAllSolns
        % Levels of DC Operating Point Data to output (s---i) 
        DevOpPtLevel
        % OutputPlan Name (s---s) 
        OutputPlan
        % Select if the DC voltage solutions for back-annotation should be saved. (s---b) 
        SaveDCVoltageSolution
        % Select if the DC current solutions for back-annotation should be saved. (s---b) 
        SaveDCCurrentSolution
        % Method with which default DC convergence starts (s---i) 
        DefaultConvStart
        % Final Gnode for accepted solution (s---r) 
        GnodeFinal
        % Final Gnode for accepted solution (s---i) 
        GnodeMaxTotalIters
        % Faster, but less robust gnode stepping (s---i) 
        GnodeFast
    end
    methods
        function obj = set.SweepVar(obj,val)
            obj = setParameter(obj,'SweepVar',val,0,'string');
        end
        function res = get.SweepVar(obj)
            res = getParameter(obj,'SweepVar');
        end
        function obj = set.SweepPlan(obj,val)
            obj = setParameter(obj,'SweepPlan',val,1,'string');
        end
        function res = get.SweepPlan(obj)
            res = getParameter(obj,'SweepPlan');
        end
        function obj = set.MaxDeltaV(obj,val)
            obj = setParameter(obj,'MaxDeltaV',val,0,'real');
        end
        function res = get.MaxDeltaV(obj)
            res = getParameter(obj,'MaxDeltaV');
        end
        function obj = set.ConvMode(obj,val)
            obj = setParameter(obj,'ConvMode',val,0,'integer');
        end
        function res = get.ConvMode(obj)
            res = getParameter(obj,'ConvMode');
        end
        function obj = set.MaxIters(obj,val)
            obj = setParameter(obj,'MaxIters',val,0,'integer');
        end
        function res = get.MaxIters(obj)
            res = getParameter(obj,'MaxIters');
        end
        function obj = set.NestLevel(obj,val)
            obj = setParameter(obj,'NestLevel',val,0,'integer');
        end
        function res = get.NestLevel(obj)
            res = getParameter(obj,'NestLevel');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.OutVar(obj,val)
            obj = setParameter(obj,'OutVar',val,0,'string');
        end
        function res = get.OutVar(obj)
            res = getParameter(obj,'OutVar');
        end
        function obj = set.PrintOpPoint(obj,val)
            obj = setParameter(obj,'PrintOpPoint',val,0,'boolean');
        end
        function res = get.PrintOpPoint(obj)
            res = getParameter(obj,'PrintOpPoint');
        end
        function obj = set.Restart(obj,val)
            obj = setParameter(obj,'Restart',val,0,'integer');
        end
        function res = get.Restart(obj)
            res = getParameter(obj,'Restart');
        end
        function obj = set.UseFiniteDiff(obj,val)
            obj = setParameter(obj,'UseFiniteDiff',val,0,'boolean');
        end
        function res = get.UseFiniteDiff(obj)
            res = getParameter(obj,'UseFiniteDiff');
        end
        function obj = set.LimitingMode(obj,val)
            obj = setParameter(obj,'LimitingMode',val,0,'integer');
        end
        function res = get.LimitingMode(obj)
            res = getParameter(obj,'LimitingMode');
        end
        function obj = set.ArcMaxStep(obj,val)
            obj = setParameter(obj,'ArcMaxStep',val,0,'real');
        end
        function res = get.ArcMaxStep(obj)
            res = getParameter(obj,'ArcMaxStep');
        end
        function obj = set.ArcLevelMaxStep(obj,val)
            obj = setParameter(obj,'ArcLevelMaxStep',val,0,'real');
        end
        function res = get.ArcLevelMaxStep(obj)
            res = getParameter(obj,'ArcLevelMaxStep');
        end
        function obj = set.ArcMinValue(obj,val)
            obj = setParameter(obj,'ArcMinValue',val,0,'real');
        end
        function res = get.ArcMinValue(obj)
            res = getParameter(obj,'ArcMinValue');
        end
        function obj = set.ArcMaxValue(obj,val)
            obj = setParameter(obj,'ArcMaxValue',val,0,'real');
        end
        function res = get.ArcMaxValue(obj)
            res = getParameter(obj,'ArcMaxValue');
        end
        function obj = set.MaxStepRatio(obj,val)
            obj = setParameter(obj,'MaxStepRatio',val,0,'real');
        end
        function res = get.MaxStepRatio(obj)
            res = getParameter(obj,'MaxStepRatio');
        end
        function obj = set.MaxShrinkage(obj,val)
            obj = setParameter(obj,'MaxShrinkage',val,0,'real');
        end
        function res = get.MaxShrinkage(obj)
            res = getParameter(obj,'MaxShrinkage');
        end
        function obj = set.OutputAllSolns(obj,val)
            obj = setParameter(obj,'OutputAllSolns',val,0,'boolean');
        end
        function res = get.OutputAllSolns(obj)
            res = getParameter(obj,'OutputAllSolns');
        end
        function obj = set.DevOpPtLevel(obj,val)
            obj = setParameter(obj,'DevOpPtLevel',val,0,'integer');
        end
        function res = get.DevOpPtLevel(obj)
            res = getParameter(obj,'DevOpPtLevel');
        end
        function obj = set.OutputPlan(obj,val)
            obj = setParameter(obj,'OutputPlan',val,0,'string');
        end
        function res = get.OutputPlan(obj)
            res = getParameter(obj,'OutputPlan');
        end
        function obj = set.SaveDCVoltageSolution(obj,val)
            obj = setParameter(obj,'SaveDCVoltageSolution',val,0,'boolean');
        end
        function res = get.SaveDCVoltageSolution(obj)
            res = getParameter(obj,'SaveDCVoltageSolution');
        end
        function obj = set.SaveDCCurrentSolution(obj,val)
            obj = setParameter(obj,'SaveDCCurrentSolution',val,0,'boolean');
        end
        function res = get.SaveDCCurrentSolution(obj)
            res = getParameter(obj,'SaveDCCurrentSolution');
        end
        function obj = set.DefaultConvStart(obj,val)
            obj = setParameter(obj,'DefaultConvStart',val,0,'integer');
        end
        function res = get.DefaultConvStart(obj)
            res = getParameter(obj,'DefaultConvStart');
        end
        function obj = set.GnodeFinal(obj,val)
            obj = setParameter(obj,'GnodeFinal',val,0,'real');
        end
        function res = get.GnodeFinal(obj)
            res = getParameter(obj,'GnodeFinal');
        end
        function obj = set.GnodeMaxTotalIters(obj,val)
            obj = setParameter(obj,'GnodeMaxTotalIters',val,0,'integer');
        end
        function res = get.GnodeMaxTotalIters(obj)
            res = getParameter(obj,'GnodeMaxTotalIters');
        end
        function obj = set.GnodeFast(obj,val)
            obj = setParameter(obj,'GnodeFast',val,0,'integer');
        end
        function res = get.GnodeFast(obj)
            res = getParameter(obj,'GnodeFast');
        end
    end
end

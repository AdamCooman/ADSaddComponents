classdef ADS_HP_MOSFET_Model < ADSmodel
    % ADS_HP_MOSFET_Model matlab representation for the ADS HP_MOSFET_Model component
    % This device model has been renamed to ADS_MOSFET model
    % model ModelName HP_MOSFET ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % data file name. (sm--s) 
        File
        % library file name. (sm--s) 
        Library
        % library item name. (sm--s) 
        Item
        % Source parasitic resistance (smorr) Unit: Ohms
        Rs
        % Gate parasitic resistance (smorr) Unit: Ohms
        Rg
        % Drain parasitic resistance (smorr) Unit: Ohms
        Rd
        % Source parasitic inductance (smorr) Unit: H
        Ls
        % Gate parasitic inductance (smorr) Unit: H
        Lg
        % Drain parasitic inductance (smorr) Unit: H
        Ld
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
    end
    methods
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
        function obj = set.Library(obj,val)
            obj = setParameter(obj,'Library',val,0,'string');
        end
        function res = get.Library(obj)
            res = getParameter(obj,'Library');
        end
        function obj = set.Item(obj,val)
            obj = setParameter(obj,'Item',val,0,'string');
        end
        function res = get.Item(obj)
            res = getParameter(obj,'Item');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Lg(obj,val)
            obj = setParameter(obj,'Lg',val,0,'real');
        end
        function res = get.Lg(obj)
            res = getParameter(obj,'Lg');
        end
        function obj = set.Ld(obj,val)
            obj = setParameter(obj,'Ld',val,0,'real');
        end
        function res = get.Ld(obj)
            res = getParameter(obj,'Ld');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
    end
end

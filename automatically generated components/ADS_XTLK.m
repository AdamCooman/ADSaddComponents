classdef ADS_XTLK < ADScomponent
    % ADS_XTLK matlab representation for the ADS XTLK component
    % XTLK model for Channel Simulation.
    % XTLK [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % The cross talk model (s--rs) 
        Model
    end
    methods
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,0,'string');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
    end
end

classdef ADS_RIBBON_MDS < ADScomponent
    % ADS_RIBBON_MDS matlab representation for the ADS RIBBON_MDS component
    % Ribbon over optional substrate
    % RIBBON_MDS [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (sm--s) 
        Subst
        % Width of ribbon (smorr) Unit: m
        W
        % Length (smorr) Unit: m
        L
        % Height above ground (smorr) Unit: m
        B
        % Thickness of ribbon (smorr) Unit: m
        T
        % Conductivity per meter of ribbon (smorr) Unit: S/m
        Cond
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
    end
end

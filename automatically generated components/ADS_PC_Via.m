classdef ADS_PC_Via < ADScomponent
    % ADS_PC_Via matlab representation for the ADS PC_Via component
    % User-defined model
    % PC_Via [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Sm-ri) Unit: unknown
        Layer1
        % User device parameter (Sm-ri) Unit: unknown
        Layer2
        % User device parameter (Smorr) Unit: unknown
        DiamVia
        % User device parameter (Smorr) Unit: unknown
        T
        % User device parameter (Smorr) Unit: unknown
        S
        % User device parameter (Smorr) Unit: unknown
        Cond
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Layer1(obj,val)
            obj = setParameter(obj,'Layer1',val,0,'integer');
        end
        function res = get.Layer1(obj)
            res = getParameter(obj,'Layer1');
        end
        function obj = set.Layer2(obj,val)
            obj = setParameter(obj,'Layer2',val,0,'integer');
        end
        function res = get.Layer2(obj)
            res = getParameter(obj,'Layer2');
        end
        function obj = set.DiamVia(obj,val)
            obj = setParameter(obj,'DiamVia',val,0,'real');
        end
        function res = get.DiamVia(obj)
            res = getParameter(obj,'DiamVia');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

classdef ADS_NoiseCorr2Port < ADSnodeless
    % ADS_NoiseCorr2Port matlab representation for the ADS NoiseCorr2Port component
    % Noise correlation between two sources
    % NoiseCorr2Port [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Noise correlation coefficient between sources (smorc) 
        CorrCoeff
        % Correlated source one (s---d) 
        Source1
        % Correlated source 2 (s---d) 
        Source2
    end
    methods
        function obj = set.CorrCoeff(obj,val)
            obj = setParameter(obj,'CorrCoeff',val,0,'complex');
        end
        function res = get.CorrCoeff(obj)
            res = getParameter(obj,'CorrCoeff');
        end
        function obj = set.Source1(obj,val)
            obj = setParameter(obj,'Source1',val,0,'string');
        end
        function res = get.Source1(obj)
            res = getParameter(obj,'Source1');
        end
        function obj = set.Source2(obj,val)
            obj = setParameter(obj,'Source2',val,0,'string');
        end
        function res = get.Source2(obj)
            res = getParameter(obj,'Source2');
        end
    end
end

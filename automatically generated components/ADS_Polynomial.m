classdef ADS_Polynomial < ADScomponent
    % ADS_Polynomial matlab representation for the ADS Polynomial component
    % General Polynomial Filter
    % Polynomial [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Passband edge frequency (smorr) Unit: Hz
        FreqPass
        % Passband attenuation at edge frequency (smorr) Unit: dB
        dbPass
        % Passband or stopband ripple (smorr) Unit: dB
        Ripple
        % Stopband edge frequency (smorr) Unit: Hz
        FreqStop
        % Stopband edge attenuation (smorr) Unit: dB
        dbStop
        % Lower band edge frequency (smorr) Unit: Hz
        FreqLow
        % Upper band edge frequency (smorr) Unit: Hz
        FreqHigh
        % Center frequency (smorr) Unit: Hz
        FreqCenter
        % Passband width of filter (smorr) Unit: Hz
        BW_Pass
        % Stopband width of filter (smorr) Unit: Hz
        BW_Stop
        % Filter order (sm-ri) 
        Order
        % Input impedance of filter (smorr) Unit: Ohms
        Z1
        % Output impedance of filter (smorr) Unit: Ohms
        Z2
        % Insertion loss of filter (smorr) Unit: dB
        InsertionLoss
        % Unloaded Q of filter elements (smorr) 
        Q
        % Filter stopband impedance type (open or short) (sm-rs) 
        StopType
        % phase equalization type (sm-ri) 
        PhaseEQ
        % Lower edge frequency for 2nd passband (smorr) Unit: Hz
        BPF2_FreqLow
        % Higher edge frequency for 2nd passband (smorr) Unit: Hz
        BPF2_FreqHigh
        % Midband rejection level in dB for 2nd passband (smorr) Unit: dB
        BPF2_Rej
        % Filter maximum rejection level in dB (smorr) Unit: dB
        MaxRej
        % Filter type: 'hp', 'lp', 'bs', or 'bp' (S--rs) 
        FilterType
        % Relative group delay at passband edge frequency (smorr) 
        GD_Pass
        % Numerator polynomial of the filter (smorr) Unit: Hz
        Numerator
        % Denominator polynomial of the filter (smorr) Unit: Hz
        Denominator
        % Poles of the filter (smorc) Unit: Hz
        Poles
        % Zeros of the filter (smorc) Unit: Hz
        Zeros
        % Gain factor of filter (smorr) 
        GainFactor
        % Temperature (smorr) 
        Temp
        % Non-causal function impulse response order (sm-ri) Unit: (unknown units)
        ImpNoncausalLength
        % Convolution mode (sm-ri) Unit: (unknown units)
        ImpMode
        % Maximum Frequency (smorr) Unit: (unknown units)
        ImpMaxFreq
        % Sample Frequency (smorr) Unit: (unknown units)
        ImpDeltaFreq
        % maximum allowed impulse response order (sm-ri) Unit: (unknown units)
        ImpMaxPts
        % Smoothing Window (sm-ri) Unit: (unknown units)
        ImpWindow
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpRelTrunc
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpAbsTrunc
    end
    methods
        function obj = set.FreqPass(obj,val)
            obj = setParameter(obj,'FreqPass',val,0,'real');
        end
        function res = get.FreqPass(obj)
            res = getParameter(obj,'FreqPass');
        end
        function obj = set.dbPass(obj,val)
            obj = setParameter(obj,'dbPass',val,0,'real');
        end
        function res = get.dbPass(obj)
            res = getParameter(obj,'dbPass');
        end
        function obj = set.Ripple(obj,val)
            obj = setParameter(obj,'Ripple',val,0,'real');
        end
        function res = get.Ripple(obj)
            res = getParameter(obj,'Ripple');
        end
        function obj = set.FreqStop(obj,val)
            obj = setParameter(obj,'FreqStop',val,0,'real');
        end
        function res = get.FreqStop(obj)
            res = getParameter(obj,'FreqStop');
        end
        function obj = set.dbStop(obj,val)
            obj = setParameter(obj,'dbStop',val,0,'real');
        end
        function res = get.dbStop(obj)
            res = getParameter(obj,'dbStop');
        end
        function obj = set.FreqLow(obj,val)
            obj = setParameter(obj,'FreqLow',val,0,'real');
        end
        function res = get.FreqLow(obj)
            res = getParameter(obj,'FreqLow');
        end
        function obj = set.FreqHigh(obj,val)
            obj = setParameter(obj,'FreqHigh',val,0,'real');
        end
        function res = get.FreqHigh(obj)
            res = getParameter(obj,'FreqHigh');
        end
        function obj = set.FreqCenter(obj,val)
            obj = setParameter(obj,'FreqCenter',val,0,'real');
        end
        function res = get.FreqCenter(obj)
            res = getParameter(obj,'FreqCenter');
        end
        function obj = set.BW_Pass(obj,val)
            obj = setParameter(obj,'BW_Pass',val,0,'real');
        end
        function res = get.BW_Pass(obj)
            res = getParameter(obj,'BW_Pass');
        end
        function obj = set.BW_Stop(obj,val)
            obj = setParameter(obj,'BW_Stop',val,0,'real');
        end
        function res = get.BW_Stop(obj)
            res = getParameter(obj,'BW_Stop');
        end
        function obj = set.Order(obj,val)
            obj = setParameter(obj,'Order',val,0,'integer');
        end
        function res = get.Order(obj)
            res = getParameter(obj,'Order');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.InsertionLoss(obj,val)
            obj = setParameter(obj,'InsertionLoss',val,0,'real');
        end
        function res = get.InsertionLoss(obj)
            res = getParameter(obj,'InsertionLoss');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.StopType(obj,val)
            obj = setParameter(obj,'StopType',val,0,'string');
        end
        function res = get.StopType(obj)
            res = getParameter(obj,'StopType');
        end
        function obj = set.PhaseEQ(obj,val)
            obj = setParameter(obj,'PhaseEQ',val,0,'integer');
        end
        function res = get.PhaseEQ(obj)
            res = getParameter(obj,'PhaseEQ');
        end
        function obj = set.BPF2_FreqLow(obj,val)
            obj = setParameter(obj,'BPF2_FreqLow',val,0,'real');
        end
        function res = get.BPF2_FreqLow(obj)
            res = getParameter(obj,'BPF2_FreqLow');
        end
        function obj = set.BPF2_FreqHigh(obj,val)
            obj = setParameter(obj,'BPF2_FreqHigh',val,0,'real');
        end
        function res = get.BPF2_FreqHigh(obj)
            res = getParameter(obj,'BPF2_FreqHigh');
        end
        function obj = set.BPF2_Rej(obj,val)
            obj = setParameter(obj,'BPF2_Rej',val,0,'real');
        end
        function res = get.BPF2_Rej(obj)
            res = getParameter(obj,'BPF2_Rej');
        end
        function obj = set.MaxRej(obj,val)
            obj = setParameter(obj,'MaxRej',val,0,'real');
        end
        function res = get.MaxRej(obj)
            res = getParameter(obj,'MaxRej');
        end
        function obj = set.FilterType(obj,val)
            obj = setParameter(obj,'FilterType',val,0,'string');
        end
        function res = get.FilterType(obj)
            res = getParameter(obj,'FilterType');
        end
        function obj = set.GD_Pass(obj,val)
            obj = setParameter(obj,'GD_Pass',val,0,'real');
        end
        function res = get.GD_Pass(obj)
            res = getParameter(obj,'GD_Pass');
        end
        function obj = set.Numerator(obj,val)
            obj = setParameter(obj,'Numerator',val,0,'real');
        end
        function res = get.Numerator(obj)
            res = getParameter(obj,'Numerator');
        end
        function obj = set.Denominator(obj,val)
            obj = setParameter(obj,'Denominator',val,0,'real');
        end
        function res = get.Denominator(obj)
            res = getParameter(obj,'Denominator');
        end
        function obj = set.Poles(obj,val)
            obj = setParameter(obj,'Poles',val,0,'complex');
        end
        function res = get.Poles(obj)
            res = getParameter(obj,'Poles');
        end
        function obj = set.Zeros(obj,val)
            obj = setParameter(obj,'Zeros',val,0,'complex');
        end
        function res = get.Zeros(obj)
            res = getParameter(obj,'Zeros');
        end
        function obj = set.GainFactor(obj,val)
            obj = setParameter(obj,'GainFactor',val,0,'real');
        end
        function res = get.GainFactor(obj)
            res = getParameter(obj,'GainFactor');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxPts(obj,val)
            obj = setParameter(obj,'ImpMaxPts',val,0,'integer');
        end
        function res = get.ImpMaxPts(obj)
            res = getParameter(obj,'ImpMaxPts');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTrunc(obj,val)
            obj = setParameter(obj,'ImpRelTrunc',val,0,'real');
        end
        function res = get.ImpRelTrunc(obj)
            res = getParameter(obj,'ImpRelTrunc');
        end
        function obj = set.ImpAbsTrunc(obj,val)
            obj = setParameter(obj,'ImpAbsTrunc',val,0,'real');
        end
        function res = get.ImpAbsTrunc(obj)
            res = getParameter(obj,'ImpAbsTrunc');
        end
    end
end

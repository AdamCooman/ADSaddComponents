classdef ADS_Y_Port < ADScomponent
    % ADS_Y_Port matlab representation for the ADS Y_Port component
    % User Defined Linear Y Parameter N Port
    % Y_Port [:Name] p1 n1 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Y Parameter (smorc) Unit: S
        Y
        % Port is reciprocal (s---b) 
        Recip
        % Temperature in degrees Celsius (smorr) Unit: deg C
        Temp
        % SYM array pointer to all Y parameters (smo-c) 
        All
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
        % Domain mode [0,1,2] => [normal freq, S, Z] (s---i) 
        Domain
        % Time step for Z domain (smo-r) Unit: s
        TimeStep
        % Additional delay for S and Z domain modes (smo-r) Unit: s
        Delay
        % Frequency scaling for S, Z domain functions (smo-r) Unit: Hz
        FreqScale
        % Frequency shift for S, Z domain functions (smo-r) Unit: Hz
        FreqShift
        % Common denominator polynomial for S or Z domain (smo-c) 
        Denom
        % Minimum frequency of data file (smo-r) Unit: Hz
        MinFileFreq
        % Maximum frequency of data file (smo-r) Unit: Hz
        MaxFileFreq
        % Enforce passivity (s---b) 
        EnforcePassivity
        % Envelope fitting bandwidth override (Hz) (smo-r) Unit: Hz
        EnvFitBwHz
    end
    methods
        function obj = set.Y(obj,val)
            obj = setParameter(obj,'Y',val,2,'complex');
        end
        function res = get.Y(obj)
            res = getParameter(obj,'Y');
        end
        function obj = set.Recip(obj,val)
            obj = setParameter(obj,'Recip',val,0,'boolean');
        end
        function res = get.Recip(obj)
            res = getParameter(obj,'Recip');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.All(obj,val)
            obj = setParameter(obj,'All',val,0,'complex');
        end
        function res = get.All(obj)
            res = getParameter(obj,'All');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
        function obj = set.Domain(obj,val)
            obj = setParameter(obj,'Domain',val,0,'integer');
        end
        function res = get.Domain(obj)
            res = getParameter(obj,'Domain');
        end
        function obj = set.TimeStep(obj,val)
            obj = setParameter(obj,'TimeStep',val,0,'real');
        end
        function res = get.TimeStep(obj)
            res = getParameter(obj,'TimeStep');
        end
        function obj = set.Delay(obj,val)
            obj = setParameter(obj,'Delay',val,2,'real');
        end
        function res = get.Delay(obj)
            res = getParameter(obj,'Delay');
        end
        function obj = set.FreqScale(obj,val)
            obj = setParameter(obj,'FreqScale',val,0,'real');
        end
        function res = get.FreqScale(obj)
            res = getParameter(obj,'FreqScale');
        end
        function obj = set.FreqShift(obj,val)
            obj = setParameter(obj,'FreqShift',val,0,'real');
        end
        function res = get.FreqShift(obj)
            res = getParameter(obj,'FreqShift');
        end
        function obj = set.Denom(obj,val)
            obj = setParameter(obj,'Denom',val,0,'complex');
        end
        function res = get.Denom(obj)
            res = getParameter(obj,'Denom');
        end
        function obj = set.MinFileFreq(obj,val)
            obj = setParameter(obj,'MinFileFreq',val,0,'real');
        end
        function res = get.MinFileFreq(obj)
            res = getParameter(obj,'MinFileFreq');
        end
        function obj = set.MaxFileFreq(obj,val)
            obj = setParameter(obj,'MaxFileFreq',val,0,'real');
        end
        function res = get.MaxFileFreq(obj)
            res = getParameter(obj,'MaxFileFreq');
        end
        function obj = set.EnforcePassivity(obj,val)
            obj = setParameter(obj,'EnforcePassivity',val,0,'boolean');
        end
        function res = get.EnforcePassivity(obj)
            res = getParameter(obj,'EnforcePassivity');
        end
        function obj = set.EnvFitBwHz(obj,val)
            obj = setParameter(obj,'EnvFitBwHz',val,0,'real');
        end
        function res = get.EnvFitBwHz(obj)
            res = getParameter(obj,'EnvFitBwHz');
        end
    end
end

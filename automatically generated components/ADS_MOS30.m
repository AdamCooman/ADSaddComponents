classdef ADS_MOS30 < ADScomponent
    % ADS_MOS30 matlab representation for the ADS MOS30 component
    % Mos Model 30 Version 30.02
    % ModelName [:Name] drain gate source ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Number of devices in parallel (smorr) 
        Mult
        % Device Temperature (degrees Celsius) (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % NMOS type MM30 (s---b) 
        nmos
        % PMOS type MM30 (s---b) 
        pmos
        % Drain-Sorce current at operating point (s---r) Unit: A/m^2
        Ids
        % Dissipated power at operating point (s---r) Unit: W
        power
        % Small signal Vs to Ids transconductance (---rr) Unit: S
        gds_s
        % Small signal Vd to Ids transconductance (---rr) Unit: S
        gds_d
        % Small signal Vb to Ids transconductance (---rr) Unit: S
        gds_b
        % Small signal Vg to Ids transconductance (---rr) Unit: S
        gds_g
        % Small signal drain-source capacitance cds_s (---rr) Unit: F
        cds_s
        % Small signal drain-source capacitance cds_d (---rr) Unit: F
        cds_d
        % Small signal drain-source capacitance cds_g (---rr) Unit: F
        cds_g
        % Small signal drain-source capacitance cds_b (---rr) Unit: F
        cds_b
        % Small signal gate-source capacitance cgs_s (---rr) Unit: F
        cgs_s
        % Small signal gate-source capacitance cgs_d (---rr) Unit: F
        cgs_d
        % Small signal gate-source capacitance cgs_g (---rr) Unit: F
        cgs_g
        % Small signal gate-source capacitance cgs_b (---rr) Unit: F
        cgs_b
        % Small signal gate-drain capacitance cgd_s (---rr) Unit: F
        cgd_s
        % Small signal gate-drain capacitance cgd_d (---rr) Unit: F
        cgd_d
        % Small signal gate-drain capacitance cgd_g (---rr) Unit: F
        cgd_g
        % Small signal gate-drain capacitance cgd_b (---rr) Unit: F
        cgd_b
        % Small signal bulk-source capacitance cbs_s (---rr) Unit: F
        cbs_s
        % Small signal bulk-source capacitance cbs_d (---rr) Unit: F
        cbs_d
        % Small signal bulk-source capacitance cbs_g (---rr) Unit: F
        cbs_g
        % Small signal bulk-source capacitance cbs_b (---rr) Unit: F
        cbs_b
        % Small signal bulk-drain capacitance cbd_s (---rr) Unit: F
        cbd_s
        % Small signal bulk-drain capacitance cbd_d (---rr) Unit: F
        cbd_d
        % Small signal bulk-drain capacitance cbd_g (---rr) Unit: F
        cbd_g
        % Small signal bulk-drain capacitance cbd_b (---rr) Unit: F
        cbd_b
        % Qgs gate-source charge (---rr) Unit: C
        Qgs
        % Qgs gate-drain charge (---rr) Unit: C
        Qgd
        % Qbs bulk-source charge (---rr) Unit: C
        Qbs
        % Qbd bulk-drain charge (---rr) Unit: C
        Qbd
        % Qds drain-source charge (---rr) Unit: C
        Qds
    end
    methods
        function obj = set.Mult(obj,val)
            obj = setParameter(obj,'Mult',val,0,'real');
        end
        function res = get.Mult(obj)
            res = getParameter(obj,'Mult');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.nmos(obj,val)
            obj = setParameter(obj,'nmos',val,0,'boolean');
        end
        function res = get.nmos(obj)
            res = getParameter(obj,'nmos');
        end
        function obj = set.pmos(obj,val)
            obj = setParameter(obj,'pmos',val,0,'boolean');
        end
        function res = get.pmos(obj)
            res = getParameter(obj,'pmos');
        end
        function obj = set.Ids(obj,val)
            obj = setParameter(obj,'Ids',val,0,'real');
        end
        function res = get.Ids(obj)
            res = getParameter(obj,'Ids');
        end
        function obj = set.power(obj,val)
            obj = setParameter(obj,'power',val,0,'real');
        end
        function res = get.power(obj)
            res = getParameter(obj,'power');
        end
        function obj = set.gds_s(obj,val)
            obj = setParameter(obj,'gds_s',val,0,'real');
        end
        function res = get.gds_s(obj)
            res = getParameter(obj,'gds_s');
        end
        function obj = set.gds_d(obj,val)
            obj = setParameter(obj,'gds_d',val,0,'real');
        end
        function res = get.gds_d(obj)
            res = getParameter(obj,'gds_d');
        end
        function obj = set.gds_b(obj,val)
            obj = setParameter(obj,'gds_b',val,0,'real');
        end
        function res = get.gds_b(obj)
            res = getParameter(obj,'gds_b');
        end
        function obj = set.gds_g(obj,val)
            obj = setParameter(obj,'gds_g',val,0,'real');
        end
        function res = get.gds_g(obj)
            res = getParameter(obj,'gds_g');
        end
        function obj = set.cds_s(obj,val)
            obj = setParameter(obj,'cds_s',val,0,'real');
        end
        function res = get.cds_s(obj)
            res = getParameter(obj,'cds_s');
        end
        function obj = set.cds_d(obj,val)
            obj = setParameter(obj,'cds_d',val,0,'real');
        end
        function res = get.cds_d(obj)
            res = getParameter(obj,'cds_d');
        end
        function obj = set.cds_g(obj,val)
            obj = setParameter(obj,'cds_g',val,0,'real');
        end
        function res = get.cds_g(obj)
            res = getParameter(obj,'cds_g');
        end
        function obj = set.cds_b(obj,val)
            obj = setParameter(obj,'cds_b',val,0,'real');
        end
        function res = get.cds_b(obj)
            res = getParameter(obj,'cds_b');
        end
        function obj = set.cgs_s(obj,val)
            obj = setParameter(obj,'cgs_s',val,0,'real');
        end
        function res = get.cgs_s(obj)
            res = getParameter(obj,'cgs_s');
        end
        function obj = set.cgs_d(obj,val)
            obj = setParameter(obj,'cgs_d',val,0,'real');
        end
        function res = get.cgs_d(obj)
            res = getParameter(obj,'cgs_d');
        end
        function obj = set.cgs_g(obj,val)
            obj = setParameter(obj,'cgs_g',val,0,'real');
        end
        function res = get.cgs_g(obj)
            res = getParameter(obj,'cgs_g');
        end
        function obj = set.cgs_b(obj,val)
            obj = setParameter(obj,'cgs_b',val,0,'real');
        end
        function res = get.cgs_b(obj)
            res = getParameter(obj,'cgs_b');
        end
        function obj = set.cgd_s(obj,val)
            obj = setParameter(obj,'cgd_s',val,0,'real');
        end
        function res = get.cgd_s(obj)
            res = getParameter(obj,'cgd_s');
        end
        function obj = set.cgd_d(obj,val)
            obj = setParameter(obj,'cgd_d',val,0,'real');
        end
        function res = get.cgd_d(obj)
            res = getParameter(obj,'cgd_d');
        end
        function obj = set.cgd_g(obj,val)
            obj = setParameter(obj,'cgd_g',val,0,'real');
        end
        function res = get.cgd_g(obj)
            res = getParameter(obj,'cgd_g');
        end
        function obj = set.cgd_b(obj,val)
            obj = setParameter(obj,'cgd_b',val,0,'real');
        end
        function res = get.cgd_b(obj)
            res = getParameter(obj,'cgd_b');
        end
        function obj = set.cbs_s(obj,val)
            obj = setParameter(obj,'cbs_s',val,0,'real');
        end
        function res = get.cbs_s(obj)
            res = getParameter(obj,'cbs_s');
        end
        function obj = set.cbs_d(obj,val)
            obj = setParameter(obj,'cbs_d',val,0,'real');
        end
        function res = get.cbs_d(obj)
            res = getParameter(obj,'cbs_d');
        end
        function obj = set.cbs_g(obj,val)
            obj = setParameter(obj,'cbs_g',val,0,'real');
        end
        function res = get.cbs_g(obj)
            res = getParameter(obj,'cbs_g');
        end
        function obj = set.cbs_b(obj,val)
            obj = setParameter(obj,'cbs_b',val,0,'real');
        end
        function res = get.cbs_b(obj)
            res = getParameter(obj,'cbs_b');
        end
        function obj = set.cbd_s(obj,val)
            obj = setParameter(obj,'cbd_s',val,0,'real');
        end
        function res = get.cbd_s(obj)
            res = getParameter(obj,'cbd_s');
        end
        function obj = set.cbd_d(obj,val)
            obj = setParameter(obj,'cbd_d',val,0,'real');
        end
        function res = get.cbd_d(obj)
            res = getParameter(obj,'cbd_d');
        end
        function obj = set.cbd_g(obj,val)
            obj = setParameter(obj,'cbd_g',val,0,'real');
        end
        function res = get.cbd_g(obj)
            res = getParameter(obj,'cbd_g');
        end
        function obj = set.cbd_b(obj,val)
            obj = setParameter(obj,'cbd_b',val,0,'real');
        end
        function res = get.cbd_b(obj)
            res = getParameter(obj,'cbd_b');
        end
        function obj = set.Qgs(obj,val)
            obj = setParameter(obj,'Qgs',val,0,'real');
        end
        function res = get.Qgs(obj)
            res = getParameter(obj,'Qgs');
        end
        function obj = set.Qgd(obj,val)
            obj = setParameter(obj,'Qgd',val,0,'real');
        end
        function res = get.Qgd(obj)
            res = getParameter(obj,'Qgd');
        end
        function obj = set.Qbs(obj,val)
            obj = setParameter(obj,'Qbs',val,0,'real');
        end
        function res = get.Qbs(obj)
            res = getParameter(obj,'Qbs');
        end
        function obj = set.Qbd(obj,val)
            obj = setParameter(obj,'Qbd',val,0,'real');
        end
        function res = get.Qbd(obj)
            res = getParameter(obj,'Qbd');
        end
        function obj = set.Qds(obj,val)
            obj = setParameter(obj,'Qds',val,0,'real');
        end
        function res = get.Qds(obj)
            res = getParameter(obj,'Qds');
        end
    end
end

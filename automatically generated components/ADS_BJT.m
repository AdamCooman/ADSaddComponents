classdef ADS_BJT < ADScomponent
    % ADS_BJT matlab representation for the ADS BJT component
    % Bipolar Junction Transistor
    % ModelName [:Name] collector base emitter ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Junction area factor (smorr) 
        Area
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small Signal Base Emmiter Conductance (---rr) Unit: S
        Gbe
        % Small Signal Base Emmiter Capacitance (---rr) Unit: F
        Cbe
        % Small Signal External Base Conductance (---rr) Unit: S
        Gb
        % Small Signal Internal Base Collector Capacitance (---rr) Unit: F
        Cbc
        % Small Signal External Base Collector Capacitance (---rr) Unit: F
        Cbcx
        % Small Signal Collector to Substrate Capacitance (---rr) Unit: F
        Ccs
        % Small Signal Vbc To Qbe Transcapacitance (---rr) Unit: F
        dQbe_dVbc
        % Small Signal Forward Transconductance gm (---rr) Unit: S
        dIce_dVbe
        % Small Signal Reverse Transconductance gmr (---rr) Unit: S
        dIce_dVbc
        % Small Signal Reverse Transconductance gmr (---rr) Unit: S
        dIbe_dVbc
        % External Base Transconductance dIbx_dVbe (---rr) Unit: S
        dIbx_dVbe
        % External Base Transconductance dIbx_dVbc (---rr) Unit: S
        dIbx_dVbc
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gbe(obj,val)
            obj = setParameter(obj,'Gbe',val,0,'real');
        end
        function res = get.Gbe(obj)
            res = getParameter(obj,'Gbe');
        end
        function obj = set.Cbe(obj,val)
            obj = setParameter(obj,'Cbe',val,0,'real');
        end
        function res = get.Cbe(obj)
            res = getParameter(obj,'Cbe');
        end
        function obj = set.Gb(obj,val)
            obj = setParameter(obj,'Gb',val,0,'real');
        end
        function res = get.Gb(obj)
            res = getParameter(obj,'Gb');
        end
        function obj = set.Cbc(obj,val)
            obj = setParameter(obj,'Cbc',val,0,'real');
        end
        function res = get.Cbc(obj)
            res = getParameter(obj,'Cbc');
        end
        function obj = set.Cbcx(obj,val)
            obj = setParameter(obj,'Cbcx',val,0,'real');
        end
        function res = get.Cbcx(obj)
            res = getParameter(obj,'Cbcx');
        end
        function obj = set.Ccs(obj,val)
            obj = setParameter(obj,'Ccs',val,0,'real');
        end
        function res = get.Ccs(obj)
            res = getParameter(obj,'Ccs');
        end
        function obj = set.dQbe_dVbc(obj,val)
            obj = setParameter(obj,'dQbe_dVbc',val,0,'real');
        end
        function res = get.dQbe_dVbc(obj)
            res = getParameter(obj,'dQbe_dVbc');
        end
        function obj = set.dIce_dVbe(obj,val)
            obj = setParameter(obj,'dIce_dVbe',val,0,'real');
        end
        function res = get.dIce_dVbe(obj)
            res = getParameter(obj,'dIce_dVbe');
        end
        function obj = set.dIce_dVbc(obj,val)
            obj = setParameter(obj,'dIce_dVbc',val,0,'real');
        end
        function res = get.dIce_dVbc(obj)
            res = getParameter(obj,'dIce_dVbc');
        end
        function obj = set.dIbe_dVbc(obj,val)
            obj = setParameter(obj,'dIbe_dVbc',val,0,'real');
        end
        function res = get.dIbe_dVbc(obj)
            res = getParameter(obj,'dIbe_dVbc');
        end
        function obj = set.dIbx_dVbe(obj,val)
            obj = setParameter(obj,'dIbx_dVbe',val,0,'real');
        end
        function res = get.dIbx_dVbe(obj)
            res = getParameter(obj,'dIbx_dVbe');
        end
        function obj = set.dIbx_dVbc(obj,val)
            obj = setParameter(obj,'dIbx_dVbc',val,0,'real');
        end
        function res = get.dIbx_dVbc(obj)
            res = getParameter(obj,'dIbx_dVbc');
        end
    end
end

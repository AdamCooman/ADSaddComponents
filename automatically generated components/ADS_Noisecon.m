classdef ADS_Noisecon < ADSnodeless
    % ADS_Noisecon matlab representation for the ADS Noisecon component
    % Noisecon Definition
    % Noisecon [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Positive node name (repeatable) (s---s) 
        NoiseNode
        % Negative node name (repeatable) (s---s) 
        NoiseNodeMinus
        % Noise frequency (s---r) Unit: Hz
        FreqForNoise
        % Mixer input frequency (s---r) Unit: Hz
        InputFreq
        % Mixer input port (s---i) 
        NoiseInputPort
        % Mixer output port (s---i) 
        NoiseOutputPort
        % Noise sweep frequency plan (repeatable) (s---d) 
        NoiseFreqPlan
        % Phase noise (s---b) 
        PhaseNoise
        % Include port noise (s---b) 
        IncludePortNoise
        % Sort noise contributions (s---i) 
        SortNoise
        % Noise contribution threshold (s---r) 
        NoiseThresh
        % Noise bandwidth (s---r) Unit: Hz
        BandwidthForNoise
        % Carrier frequency for phase noise (s---r) Unit: Hz
        CarrierFreq
        % Carrier frequency for phase noise (repeatable) (s---i) 
        CarrierIndex
        % Compute noisy two-port parameters: Sopt, RN and NFmin (s---b) 
        NoisyTwoPort
    end
    methods
        function obj = set.NoiseNode(obj,val)
            obj = setParameter(obj,'NoiseNode',val,1,'string');
        end
        function res = get.NoiseNode(obj)
            res = getParameter(obj,'NoiseNode');
        end
        function obj = set.NoiseNodeMinus(obj,val)
            obj = setParameter(obj,'NoiseNodeMinus',val,1,'string');
        end
        function res = get.NoiseNodeMinus(obj)
            res = getParameter(obj,'NoiseNodeMinus');
        end
        function obj = set.FreqForNoise(obj,val)
            obj = setParameter(obj,'FreqForNoise',val,0,'real');
        end
        function res = get.FreqForNoise(obj)
            res = getParameter(obj,'FreqForNoise');
        end
        function obj = set.InputFreq(obj,val)
            obj = setParameter(obj,'InputFreq',val,0,'real');
        end
        function res = get.InputFreq(obj)
            res = getParameter(obj,'InputFreq');
        end
        function obj = set.NoiseInputPort(obj,val)
            obj = setParameter(obj,'NoiseInputPort',val,0,'integer');
        end
        function res = get.NoiseInputPort(obj)
            res = getParameter(obj,'NoiseInputPort');
        end
        function obj = set.NoiseOutputPort(obj,val)
            obj = setParameter(obj,'NoiseOutputPort',val,0,'integer');
        end
        function res = get.NoiseOutputPort(obj)
            res = getParameter(obj,'NoiseOutputPort');
        end
        function obj = set.NoiseFreqPlan(obj,val)
            obj = setParameter(obj,'NoiseFreqPlan',val,1,'string');
        end
        function res = get.NoiseFreqPlan(obj)
            res = getParameter(obj,'NoiseFreqPlan');
        end
        function obj = set.PhaseNoise(obj,val)
            obj = setParameter(obj,'PhaseNoise',val,0,'boolean');
        end
        function res = get.PhaseNoise(obj)
            res = getParameter(obj,'PhaseNoise');
        end
        function obj = set.IncludePortNoise(obj,val)
            obj = setParameter(obj,'IncludePortNoise',val,0,'boolean');
        end
        function res = get.IncludePortNoise(obj)
            res = getParameter(obj,'IncludePortNoise');
        end
        function obj = set.SortNoise(obj,val)
            obj = setParameter(obj,'SortNoise',val,0,'integer');
        end
        function res = get.SortNoise(obj)
            res = getParameter(obj,'SortNoise');
        end
        function obj = set.NoiseThresh(obj,val)
            obj = setParameter(obj,'NoiseThresh',val,0,'real');
        end
        function res = get.NoiseThresh(obj)
            res = getParameter(obj,'NoiseThresh');
        end
        function obj = set.BandwidthForNoise(obj,val)
            obj = setParameter(obj,'BandwidthForNoise',val,0,'real');
        end
        function res = get.BandwidthForNoise(obj)
            res = getParameter(obj,'BandwidthForNoise');
        end
        function obj = set.CarrierFreq(obj,val)
            obj = setParameter(obj,'CarrierFreq',val,0,'real');
        end
        function res = get.CarrierFreq(obj)
            res = getParameter(obj,'CarrierFreq');
        end
        function obj = set.CarrierIndex(obj,val)
            obj = setParameter(obj,'CarrierIndex',val,1,'integer');
        end
        function res = get.CarrierIndex(obj)
            res = getParameter(obj,'CarrierIndex');
        end
        function obj = set.NoisyTwoPort(obj,val)
            obj = setParameter(obj,'NoisyTwoPort',val,0,'boolean');
        end
        function res = get.NoisyTwoPort(obj)
            res = getParameter(obj,'NoisyTwoPort');
        end
    end
end

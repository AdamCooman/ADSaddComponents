classdef ADS_SAGELIN < ADScomponent
    % ADS_SAGELIN matlab representation for the ADS SAGELIN component
    % SAGE Lab Wireline hybrid or coupled transmission line
    % SAGELIN [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Physical length of Sage_Wireline (Smorr) Unit: m
        L
        % Code for bandwidth selection: "narrow" or "octave" (Sm-rs) Unit: String
        BW_Code
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.BW_Code(obj,val)
            obj = setParameter(obj,'BW_Code',val,0,'string');
        end
        function res = get.BW_Code(obj)
            res = getParameter(obj,'BW_Code');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

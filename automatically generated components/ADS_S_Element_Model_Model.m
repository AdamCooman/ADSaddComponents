classdef ADS_S_Element_Model_Model < ADSmodel
    % ADS_S_Element_Model_Model matlab representation for the ADS S_Element_Model_Model component
    % User Defined Linear S Parameter N Port Model model
    % model ModelName S_Element_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of tstone file (sm-rs) 
        File
        % S model dimention (sm-ri) 
        N
        % Parameter type (sm-rs) 
        Type
        % Mixed mode flag (sm-ri) 
        Mixedmode
        % Passive checker flag (sm-ri) 
        Passive
    end
    methods
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Mixedmode(obj,val)
            obj = setParameter(obj,'Mixedmode',val,0,'integer');
        end
        function res = get.Mixedmode(obj)
            res = getParameter(obj,'Mixedmode');
        end
        function obj = set.Passive(obj,val)
            obj = setParameter(obj,'Passive',val,0,'integer');
        end
        function res = get.Passive(obj)
            res = getParameter(obj,'Passive');
        end
    end
end

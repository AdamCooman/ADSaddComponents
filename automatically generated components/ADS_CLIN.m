classdef ADS_CLIN < ADScomponent
    % ADS_CLIN matlab representation for the ADS CLIN component
    % Ideal Coupled Transmission Lines
    % CLIN [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Even-Mode characteristic Impedance (Smorr) Unit: ohms
        Ze
        % Odd-Mode characteristic impedance (Smorr) Unit: ohms
        Zo
        % Electrical Length (Smorr) Unit: deg
        E
        % Reference Frequency for Electrical Length (Smorr) Unit: hz
        F
    end
    methods
        function obj = set.Ze(obj,val)
            obj = setParameter(obj,'Ze',val,0,'real');
        end
        function res = get.Ze(obj)
            res = getParameter(obj,'Ze');
        end
        function obj = set.Zo(obj,val)
            obj = setParameter(obj,'Zo',val,0,'real');
        end
        function res = get.Zo(obj)
            res = getParameter(obj,'Zo');
        end
        function obj = set.E(obj,val)
            obj = setParameter(obj,'E',val,0,'real');
        end
        function res = get.E(obj)
            res = getParameter(obj,'E');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
    end
end

classdef ADS_Substrate_Model < ADSmodel
    % ADS_Substrate_Model matlab representation for the ADS Substrate_Model component
    % Substrate Parameter Definition model
    % model ModelName Substrate ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Relative dielectric constant (smorr) 
        Er
        % Relative permeability (smorr) 
        Mur
        % Substrate thickness (smorr) Unit: m
        H
        % Cover height (smorr) Unit: m
        Hu
        % Ground plane/substrate separation (smorr) Unit: m
        Hl
        % Conductor thickness (smorr) Unit: m
        T
        % 1/Etch Factor=Ratio of lateral etch and conductor thickness (smorr) 
        OneOverEtchFactor
        % Conductor conductivity (smorr) Unit: S/m
        Cond
        % Dielectric Loss Tangent (smorr) 
        TanD
        % Conductor surface roughness: tooth height (smorr) Unit: m
        Rough
        % Conductor surface roughness model. 1: Hammerstad, 2: Multi-level hemisphere (sm-ri) 
        RoughnessModel
        % Conductor surface roughness: tooth base width (smorr) Unit: m
        Bbase
        % Conductor surface roughness: distances between tooth peaks (smorr) Unit: m
        Dpeaks
        % Conductor surface roughness: heigh protrusions of level 2. (smorr) Unit: m
        L2Rough
        % Conductor surface roughness: tooth base width of level 2 (smorr) Unit: m
        L2Bbase
        % Conductor surface roughness: distances between tooth peaks of level 2 (smorr) Unit: m
        L2Dpeaks
        % Conductor surface roughness: heigh protrusions of level 3. (smorr) Unit: m
        L3Rough
        % Conductor surface roughness: tooth base width of level 3 (smorr) Unit: m
        L3Bbase
        % Conductor surface roughness: distances between tooth peaks of level 3 (smorr) Unit: m
        L3Dpeaks
        % Dielectric Conductivity (smorr) Unit: S/m
        Sigma
        % Distance between sidewalls (smorr) 
        W
        % Finline enclosure inside width (smorr) 
        Fa
        % Finline enclosure inside height (smorr) 
        Fb
        % Finline dielectric width (smorr) 
        Fdw
        % Stripline inter-layer spacing (smorr) 
        S
        % Number of metal layers (s---i) 
        N
        % Layer type (s---i) 
        LayerType
        % Secured Substrate parameters (s---b) 
        Secured
        % Frequency at which Er and TanD are measured (smorr) Unit: Hz
        FreqForEpsrTanD
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) 
        DielectricLossModel
        % High end frequency in the Svensson/Djordjevic model (smorr) Unit: Hz
        HighFreqForTanD
        % Low end frequency in the Svensson/Djordjevic model (smorr) Unit: Hz
        LowFreqForTanD
    end
    methods
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.Hu(obj,val)
            obj = setParameter(obj,'Hu',val,0,'real');
        end
        function res = get.Hu(obj)
            res = getParameter(obj,'Hu');
        end
        function obj = set.Hl(obj,val)
            obj = setParameter(obj,'Hl',val,0,'real');
        end
        function res = get.Hl(obj)
            res = getParameter(obj,'Hl');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.OneOverEtchFactor(obj,val)
            obj = setParameter(obj,'OneOverEtchFactor',val,0,'real');
        end
        function res = get.OneOverEtchFactor(obj)
            res = getParameter(obj,'OneOverEtchFactor');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Rough(obj,val)
            obj = setParameter(obj,'Rough',val,0,'real');
        end
        function res = get.Rough(obj)
            res = getParameter(obj,'Rough');
        end
        function obj = set.RoughnessModel(obj,val)
            obj = setParameter(obj,'RoughnessModel',val,0,'integer');
        end
        function res = get.RoughnessModel(obj)
            res = getParameter(obj,'RoughnessModel');
        end
        function obj = set.Bbase(obj,val)
            obj = setParameter(obj,'Bbase',val,0,'real');
        end
        function res = get.Bbase(obj)
            res = getParameter(obj,'Bbase');
        end
        function obj = set.Dpeaks(obj,val)
            obj = setParameter(obj,'Dpeaks',val,0,'real');
        end
        function res = get.Dpeaks(obj)
            res = getParameter(obj,'Dpeaks');
        end
        function obj = set.L2Rough(obj,val)
            obj = setParameter(obj,'L2Rough',val,0,'real');
        end
        function res = get.L2Rough(obj)
            res = getParameter(obj,'L2Rough');
        end
        function obj = set.L2Bbase(obj,val)
            obj = setParameter(obj,'L2Bbase',val,0,'real');
        end
        function res = get.L2Bbase(obj)
            res = getParameter(obj,'L2Bbase');
        end
        function obj = set.L2Dpeaks(obj,val)
            obj = setParameter(obj,'L2Dpeaks',val,0,'real');
        end
        function res = get.L2Dpeaks(obj)
            res = getParameter(obj,'L2Dpeaks');
        end
        function obj = set.L3Rough(obj,val)
            obj = setParameter(obj,'L3Rough',val,0,'real');
        end
        function res = get.L3Rough(obj)
            res = getParameter(obj,'L3Rough');
        end
        function obj = set.L3Bbase(obj,val)
            obj = setParameter(obj,'L3Bbase',val,0,'real');
        end
        function res = get.L3Bbase(obj)
            res = getParameter(obj,'L3Bbase');
        end
        function obj = set.L3Dpeaks(obj,val)
            obj = setParameter(obj,'L3Dpeaks',val,0,'real');
        end
        function res = get.L3Dpeaks(obj)
            res = getParameter(obj,'L3Dpeaks');
        end
        function obj = set.Sigma(obj,val)
            obj = setParameter(obj,'Sigma',val,0,'real');
        end
        function res = get.Sigma(obj)
            res = getParameter(obj,'Sigma');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Fa(obj,val)
            obj = setParameter(obj,'Fa',val,0,'real');
        end
        function res = get.Fa(obj)
            res = getParameter(obj,'Fa');
        end
        function obj = set.Fb(obj,val)
            obj = setParameter(obj,'Fb',val,0,'real');
        end
        function res = get.Fb(obj)
            res = getParameter(obj,'Fb');
        end
        function obj = set.Fdw(obj,val)
            obj = setParameter(obj,'Fdw',val,0,'real');
        end
        function res = get.Fdw(obj)
            res = getParameter(obj,'Fdw');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.LayerType(obj,val)
            obj = setParameter(obj,'LayerType',val,0,'integer');
        end
        function res = get.LayerType(obj)
            res = getParameter(obj,'LayerType');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.FreqForEpsrTanD(obj,val)
            obj = setParameter(obj,'FreqForEpsrTanD',val,0,'real');
        end
        function res = get.FreqForEpsrTanD(obj)
            res = getParameter(obj,'FreqForEpsrTanD');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.HighFreqForTanD(obj,val)
            obj = setParameter(obj,'HighFreqForTanD',val,0,'real');
        end
        function res = get.HighFreqForTanD(obj)
            res = getParameter(obj,'HighFreqForTanD');
        end
        function obj = set.LowFreqForTanD(obj,val)
            obj = setParameter(obj,'LowFreqForTanD',val,0,'real');
        end
        function res = get.LowFreqForTanD(obj)
            res = getParameter(obj,'LowFreqForTanD');
        end
    end
end

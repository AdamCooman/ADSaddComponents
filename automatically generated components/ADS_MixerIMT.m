classdef ADS_MixerIMT < ADScomponent
    % ADS_MixerIMT matlab representation for the ADS MixerIMT component
    % Intermodulation Table Mixer
    % MixerIMT [:Name] In Out Lo
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % USB=1, LSB=-1 (s---i) 
        SS_SideBand
        % Conversion Gain Factor (smorc) 
        ConvGain
        % Input reflection coefficient (smorc) 
        S11
        % Output reflection coefficient (smorc) 
        S22
        % LO reflection coefficient (smorc) 
        S33
        % Nominal Local Oscillator Power (smorr) Unit: dBm
        PloNom
        % Nominal Input Power (smorr) Unit: dBm
        PinNom
        % RF Input Threshold (smorr) Unit: V
        InThresh
        % LO Input Threshold (smorr) Unit: V
        LoThresh
        % Reference Resistance, All Ports (smorr) Unit: Ohms
        Rref
        % Port1 Reference Resistance (smorr) Unit: Ohms
        R1
        % Port2 Reference Resistance (smorr) Unit: Ohms
        R2
        % Port3 Reference Resistance (smorr) Unit: Ohms
        R3
        % Intermodulation Table(IMT) File (sm--d) 
        IMT_File
        % Mixer noise figure ( in db ) (smorr) 
        NF
        % Minimum noise figure ( in db ) (smorr) 
        NFmin
        % Noise resistance (smorr) Unit: Ohms
        Rn
        % Optimum noise input match (smorc) 
        Sopt
    end
    methods
        function obj = set.SS_SideBand(obj,val)
            obj = setParameter(obj,'SS_SideBand',val,0,'integer');
        end
        function res = get.SS_SideBand(obj)
            res = getParameter(obj,'SS_SideBand');
        end
        function obj = set.ConvGain(obj,val)
            obj = setParameter(obj,'ConvGain',val,0,'complex');
        end
        function res = get.ConvGain(obj)
            res = getParameter(obj,'ConvGain');
        end
        function obj = set.S11(obj,val)
            obj = setParameter(obj,'S11',val,0,'complex');
        end
        function res = get.S11(obj)
            res = getParameter(obj,'S11');
        end
        function obj = set.S22(obj,val)
            obj = setParameter(obj,'S22',val,0,'complex');
        end
        function res = get.S22(obj)
            res = getParameter(obj,'S22');
        end
        function obj = set.S33(obj,val)
            obj = setParameter(obj,'S33',val,0,'complex');
        end
        function res = get.S33(obj)
            res = getParameter(obj,'S33');
        end
        function obj = set.PloNom(obj,val)
            obj = setParameter(obj,'PloNom',val,0,'real');
        end
        function res = get.PloNom(obj)
            res = getParameter(obj,'PloNom');
        end
        function obj = set.PinNom(obj,val)
            obj = setParameter(obj,'PinNom',val,0,'real');
        end
        function res = get.PinNom(obj)
            res = getParameter(obj,'PinNom');
        end
        function obj = set.InThresh(obj,val)
            obj = setParameter(obj,'InThresh',val,0,'real');
        end
        function res = get.InThresh(obj)
            res = getParameter(obj,'InThresh');
        end
        function obj = set.LoThresh(obj,val)
            obj = setParameter(obj,'LoThresh',val,0,'real');
        end
        function res = get.LoThresh(obj)
            res = getParameter(obj,'LoThresh');
        end
        function obj = set.Rref(obj,val)
            obj = setParameter(obj,'Rref',val,0,'real');
        end
        function res = get.Rref(obj)
            res = getParameter(obj,'Rref');
        end
        function obj = set.R1(obj,val)
            obj = setParameter(obj,'R1',val,0,'real');
        end
        function res = get.R1(obj)
            res = getParameter(obj,'R1');
        end
        function obj = set.R2(obj,val)
            obj = setParameter(obj,'R2',val,0,'real');
        end
        function res = get.R2(obj)
            res = getParameter(obj,'R2');
        end
        function obj = set.R3(obj,val)
            obj = setParameter(obj,'R3',val,0,'real');
        end
        function res = get.R3(obj)
            res = getParameter(obj,'R3');
        end
        function obj = set.IMT_File(obj,val)
            obj = setParameter(obj,'IMT_File',val,0,'string');
        end
        function res = get.IMT_File(obj)
            res = getParameter(obj,'IMT_File');
        end
        function obj = set.NF(obj,val)
            obj = setParameter(obj,'NF',val,0,'real');
        end
        function res = get.NF(obj)
            res = getParameter(obj,'NF');
        end
        function obj = set.NFmin(obj,val)
            obj = setParameter(obj,'NFmin',val,0,'real');
        end
        function res = get.NFmin(obj)
            res = getParameter(obj,'NFmin');
        end
        function obj = set.Rn(obj,val)
            obj = setParameter(obj,'Rn',val,0,'real');
        end
        function res = get.Rn(obj)
            res = getParameter(obj,'Rn');
        end
        function obj = set.Sopt(obj,val)
            obj = setParameter(obj,'Sopt',val,0,'complex');
        end
        function res = get.Sopt(obj)
            res = getParameter(obj,'Sopt');
        end
    end
end

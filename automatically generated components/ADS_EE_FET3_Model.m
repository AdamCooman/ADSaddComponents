classdef ADS_EE_FET3_Model < ADSmodel
    % ADS_EE_FET3_Model matlab representation for the ADS EE_FET3_Model component
    % EESOF Scalable Transistor Model model
    % model ModelName EE_FET3 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Zero-bias threshold parameter (smorr) Unit: V
        Vto
        % Transconductance parameter (smorr) Unit: V^-1
        Gamma
        % Gate-source voltage where transconductance maximum (smorr) Unit: V
        Vgo
        % Parameter which controls linearization point (smorr) Unit: V
        Vdelt
        % Gate-source voltage where Gamma no longer effects I-V (smorr) Unit: V
        Vch
        % Peak transconductance parameter (smorr) Unit: S
        Gmmax
        % Drain voltage where Vo dependence is nominal (smorr) Unit: V
        Vdso
        % Drain-source current saturation parameter (smorr) Unit: V
        Vsat
        % Output conductance parameter (smorr) Unit: V^-1
        Kapa
        % Channel to backside self-heating parameter (smorr) Unit: W
        Peff
        % Subthreshold onset voltage (smorr) Unit: V
        Vtso
        % Gate junction reverse saturation current (smorr) Unit: A
        Is
        % Gate junction ideality factor (smorr) 
        N
        % Source end channel resistance (smorr) Unit: Ohms
        Ris
        % Drain end channel resistance (smorr) Unit: Ohms
        Rid
        % Gate transit time delay (smorr) Unit: s
        Tau
        % Drain source inter-electrode capacitance (smorr) Unit: F
        Cdso
        % Dispersion source output impedance (smorr) Unit: Ohms
        Rdb
        % Trapping-state capacitance (smorr) Unit: F
        Cbs
        % Zero-bias threshold parameter (AC) (smorr) Unit: V
        Vtoac
        % Transconductance parameter (AC) (smorr) Unit: V^-1
        Gammaac
        % Parameter which controls linearization point (AC) (smorr) Unit: V
        Vdeltac
        % Peak transconductance parameter (AC) (smorr) Unit: S
        Gmmaxac
        % Output conductance parameter (AC) (smorr) Unit: V^-1
        Kapaac
        % Channel to backside self-heating parameter (AC) (smorr) Unit: W
        Peffac
        % Subthreshold onset voltage (AC) (smorr) Unit: V
        Vtsoac
        % Additional d-b branch conductance at Vds = VDSM (smorr) Unit: S
        Gdbm
        % Dependence of d-b branch conductance with Vds (smorr) 
        Kdb
        % Voltage where additional d-b conductance is constant (smorr) Unit: V
        Vdsm
        % Maximum input capacitance for VDS=VDSO>DELTDS (smorr) Unit: F
        C11o
        % Minimum (threshold) input capacitance for Vds=VDSO (smorr) Unit: F
        C11th
        % Inflection point in C11-Vgs characteristic (smorr) Unit: V
        Vinfl
        % C11TH to C11O transition voltage (smorr) Unit: V
        Deltgs
        % Linear to saturation region transition parameter (smorr) Unit: V
        Deltds
        % C11-Vds characteristic slope parameter (smorr) Unit: V^-1
        Lambda
        % Input transcapacitance for Vgs=VINFL and Vds>DELTDS (smorr) Unit: F
        C12sat
        % Gate drain capacitance for Vds>DELTDS (smorr) Unit: F
        Cgdsat
        % Breakdown current coefficient at threshold (smorr) 
        Kbk
        % Drain gate voltage breakdown (smorr) Unit: V
        Vbr
        % Breakdown current exponent parameter (smorr) 
        Nbr
        % Open channel (maximum) value of Ids (smorr) Unit: A
        Idsoc
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Gate resistance (smorr) Unit: Ohms
        Rg
        % Unit gate width of device (smorr) Unit: m
        Ugw
        % Number of device gate fingers (smorr) 
        Ngf
        % Voltage where transconductance compression begins (smorr) Unit: V
        Vco
        % Transconductance compression tail-off parameter (smorr) Unit: V
        Vba
        % Transconductance roll-off to tail-off voltage (smorr) Unit: V
        Vbc
        % Vo dependent transconductance compression parameter (smorr) 
        Mu
        % Slope of transconductance compression characteristic (smorr) Unit: S/V
        Deltgm
        % Slope of transconductance compression char. (AC) (smorr) Unit: S/V
        Deltgmac
        % Transconductance saturation to compression transition (smorr) Unit: V
        Alpha
        % Library model number (s---i) 
        Kmod
        % Version number (s---i) 
        Kver
        % Linear temperature coefficient for RG 1/degC (smorr) Unit: 1/deg C
        Rgtc
        % Linear temperature coefficient for RD 1/degC (smorr) Unit: 1/deg C
        Rdtc
        % Linear temperature coefficient for RS 1/degC (smorr) Unit: 1/deg C
        Rstc
        % Linear temperature coefficient for Gmmax (smorr) 
        Gmmaxtc
        % Linear temperature coefficient for pinchoff voltage (smorr) Unit: V/deg C
        Vtotc
        % Linear temperature coefficient for Gamma (smorr) 
        Gammatc
        % Linear temperature coefficient for Vinfl (smorr) 
        Vinfltc
        % Linear temperature coefficient for Gmmaxac (smorr) 
        Gmmaxactc
        % Linear temperature coefficient for Vtoac (smorr) Unit: V/deg C
        Vtoactc
        % Linear temperature coefficient for Gammaac (smorr) 
        Gammaactc
        % Saturation current temperature exponent (smorr) 
        Xti
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Gate junction forward bias (warning) (s--rr) Unit: V
        wVgfwd
        % Gate-source reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgs
        % Gate-drain reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgd
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.Gamma(obj,val)
            obj = setParameter(obj,'Gamma',val,0,'real');
        end
        function res = get.Gamma(obj)
            res = getParameter(obj,'Gamma');
        end
        function obj = set.Vgo(obj,val)
            obj = setParameter(obj,'Vgo',val,0,'real');
        end
        function res = get.Vgo(obj)
            res = getParameter(obj,'Vgo');
        end
        function obj = set.Vdelt(obj,val)
            obj = setParameter(obj,'Vdelt',val,0,'real');
        end
        function res = get.Vdelt(obj)
            res = getParameter(obj,'Vdelt');
        end
        function obj = set.Vch(obj,val)
            obj = setParameter(obj,'Vch',val,0,'real');
        end
        function res = get.Vch(obj)
            res = getParameter(obj,'Vch');
        end
        function obj = set.Gmmax(obj,val)
            obj = setParameter(obj,'Gmmax',val,0,'real');
        end
        function res = get.Gmmax(obj)
            res = getParameter(obj,'Gmmax');
        end
        function obj = set.Vdso(obj,val)
            obj = setParameter(obj,'Vdso',val,0,'real');
        end
        function res = get.Vdso(obj)
            res = getParameter(obj,'Vdso');
        end
        function obj = set.Vsat(obj,val)
            obj = setParameter(obj,'Vsat',val,0,'real');
        end
        function res = get.Vsat(obj)
            res = getParameter(obj,'Vsat');
        end
        function obj = set.Kapa(obj,val)
            obj = setParameter(obj,'Kapa',val,0,'real');
        end
        function res = get.Kapa(obj)
            res = getParameter(obj,'Kapa');
        end
        function obj = set.Peff(obj,val)
            obj = setParameter(obj,'Peff',val,0,'real');
        end
        function res = get.Peff(obj)
            res = getParameter(obj,'Peff');
        end
        function obj = set.Vtso(obj,val)
            obj = setParameter(obj,'Vtso',val,0,'real');
        end
        function res = get.Vtso(obj)
            res = getParameter(obj,'Vtso');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Ris(obj,val)
            obj = setParameter(obj,'Ris',val,0,'real');
        end
        function res = get.Ris(obj)
            res = getParameter(obj,'Ris');
        end
        function obj = set.Rid(obj,val)
            obj = setParameter(obj,'Rid',val,0,'real');
        end
        function res = get.Rid(obj)
            res = getParameter(obj,'Rid');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Cdso(obj,val)
            obj = setParameter(obj,'Cdso',val,0,'real');
        end
        function res = get.Cdso(obj)
            res = getParameter(obj,'Cdso');
        end
        function obj = set.Rdb(obj,val)
            obj = setParameter(obj,'Rdb',val,0,'real');
        end
        function res = get.Rdb(obj)
            res = getParameter(obj,'Rdb');
        end
        function obj = set.Cbs(obj,val)
            obj = setParameter(obj,'Cbs',val,0,'real');
        end
        function res = get.Cbs(obj)
            res = getParameter(obj,'Cbs');
        end
        function obj = set.Vtoac(obj,val)
            obj = setParameter(obj,'Vtoac',val,0,'real');
        end
        function res = get.Vtoac(obj)
            res = getParameter(obj,'Vtoac');
        end
        function obj = set.Gammaac(obj,val)
            obj = setParameter(obj,'Gammaac',val,0,'real');
        end
        function res = get.Gammaac(obj)
            res = getParameter(obj,'Gammaac');
        end
        function obj = set.Vdeltac(obj,val)
            obj = setParameter(obj,'Vdeltac',val,0,'real');
        end
        function res = get.Vdeltac(obj)
            res = getParameter(obj,'Vdeltac');
        end
        function obj = set.Gmmaxac(obj,val)
            obj = setParameter(obj,'Gmmaxac',val,0,'real');
        end
        function res = get.Gmmaxac(obj)
            res = getParameter(obj,'Gmmaxac');
        end
        function obj = set.Kapaac(obj,val)
            obj = setParameter(obj,'Kapaac',val,0,'real');
        end
        function res = get.Kapaac(obj)
            res = getParameter(obj,'Kapaac');
        end
        function obj = set.Peffac(obj,val)
            obj = setParameter(obj,'Peffac',val,0,'real');
        end
        function res = get.Peffac(obj)
            res = getParameter(obj,'Peffac');
        end
        function obj = set.Vtsoac(obj,val)
            obj = setParameter(obj,'Vtsoac',val,0,'real');
        end
        function res = get.Vtsoac(obj)
            res = getParameter(obj,'Vtsoac');
        end
        function obj = set.Gdbm(obj,val)
            obj = setParameter(obj,'Gdbm',val,0,'real');
        end
        function res = get.Gdbm(obj)
            res = getParameter(obj,'Gdbm');
        end
        function obj = set.Kdb(obj,val)
            obj = setParameter(obj,'Kdb',val,0,'real');
        end
        function res = get.Kdb(obj)
            res = getParameter(obj,'Kdb');
        end
        function obj = set.Vdsm(obj,val)
            obj = setParameter(obj,'Vdsm',val,0,'real');
        end
        function res = get.Vdsm(obj)
            res = getParameter(obj,'Vdsm');
        end
        function obj = set.C11o(obj,val)
            obj = setParameter(obj,'C11o',val,0,'real');
        end
        function res = get.C11o(obj)
            res = getParameter(obj,'C11o');
        end
        function obj = set.C11th(obj,val)
            obj = setParameter(obj,'C11th',val,0,'real');
        end
        function res = get.C11th(obj)
            res = getParameter(obj,'C11th');
        end
        function obj = set.Vinfl(obj,val)
            obj = setParameter(obj,'Vinfl',val,0,'real');
        end
        function res = get.Vinfl(obj)
            res = getParameter(obj,'Vinfl');
        end
        function obj = set.Deltgs(obj,val)
            obj = setParameter(obj,'Deltgs',val,0,'real');
        end
        function res = get.Deltgs(obj)
            res = getParameter(obj,'Deltgs');
        end
        function obj = set.Deltds(obj,val)
            obj = setParameter(obj,'Deltds',val,0,'real');
        end
        function res = get.Deltds(obj)
            res = getParameter(obj,'Deltds');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.C12sat(obj,val)
            obj = setParameter(obj,'C12sat',val,0,'real');
        end
        function res = get.C12sat(obj)
            res = getParameter(obj,'C12sat');
        end
        function obj = set.Cgdsat(obj,val)
            obj = setParameter(obj,'Cgdsat',val,0,'real');
        end
        function res = get.Cgdsat(obj)
            res = getParameter(obj,'Cgdsat');
        end
        function obj = set.Kbk(obj,val)
            obj = setParameter(obj,'Kbk',val,0,'real');
        end
        function res = get.Kbk(obj)
            res = getParameter(obj,'Kbk');
        end
        function obj = set.Vbr(obj,val)
            obj = setParameter(obj,'Vbr',val,0,'real');
        end
        function res = get.Vbr(obj)
            res = getParameter(obj,'Vbr');
        end
        function obj = set.Nbr(obj,val)
            obj = setParameter(obj,'Nbr',val,0,'real');
        end
        function res = get.Nbr(obj)
            res = getParameter(obj,'Nbr');
        end
        function obj = set.Idsoc(obj,val)
            obj = setParameter(obj,'Idsoc',val,0,'real');
        end
        function res = get.Idsoc(obj)
            res = getParameter(obj,'Idsoc');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Ugw(obj,val)
            obj = setParameter(obj,'Ugw',val,0,'real');
        end
        function res = get.Ugw(obj)
            res = getParameter(obj,'Ugw');
        end
        function obj = set.Ngf(obj,val)
            obj = setParameter(obj,'Ngf',val,0,'real');
        end
        function res = get.Ngf(obj)
            res = getParameter(obj,'Ngf');
        end
        function obj = set.Vco(obj,val)
            obj = setParameter(obj,'Vco',val,0,'real');
        end
        function res = get.Vco(obj)
            res = getParameter(obj,'Vco');
        end
        function obj = set.Vba(obj,val)
            obj = setParameter(obj,'Vba',val,0,'real');
        end
        function res = get.Vba(obj)
            res = getParameter(obj,'Vba');
        end
        function obj = set.Vbc(obj,val)
            obj = setParameter(obj,'Vbc',val,0,'real');
        end
        function res = get.Vbc(obj)
            res = getParameter(obj,'Vbc');
        end
        function obj = set.Mu(obj,val)
            obj = setParameter(obj,'Mu',val,0,'real');
        end
        function res = get.Mu(obj)
            res = getParameter(obj,'Mu');
        end
        function obj = set.Deltgm(obj,val)
            obj = setParameter(obj,'Deltgm',val,0,'real');
        end
        function res = get.Deltgm(obj)
            res = getParameter(obj,'Deltgm');
        end
        function obj = set.Deltgmac(obj,val)
            obj = setParameter(obj,'Deltgmac',val,0,'real');
        end
        function res = get.Deltgmac(obj)
            res = getParameter(obj,'Deltgmac');
        end
        function obj = set.Alpha(obj,val)
            obj = setParameter(obj,'Alpha',val,0,'real');
        end
        function res = get.Alpha(obj)
            res = getParameter(obj,'Alpha');
        end
        function obj = set.Kmod(obj,val)
            obj = setParameter(obj,'Kmod',val,0,'integer');
        end
        function res = get.Kmod(obj)
            res = getParameter(obj,'Kmod');
        end
        function obj = set.Kver(obj,val)
            obj = setParameter(obj,'Kver',val,0,'integer');
        end
        function res = get.Kver(obj)
            res = getParameter(obj,'Kver');
        end
        function obj = set.Rgtc(obj,val)
            obj = setParameter(obj,'Rgtc',val,0,'real');
        end
        function res = get.Rgtc(obj)
            res = getParameter(obj,'Rgtc');
        end
        function obj = set.Rdtc(obj,val)
            obj = setParameter(obj,'Rdtc',val,0,'real');
        end
        function res = get.Rdtc(obj)
            res = getParameter(obj,'Rdtc');
        end
        function obj = set.Rstc(obj,val)
            obj = setParameter(obj,'Rstc',val,0,'real');
        end
        function res = get.Rstc(obj)
            res = getParameter(obj,'Rstc');
        end
        function obj = set.Gmmaxtc(obj,val)
            obj = setParameter(obj,'Gmmaxtc',val,0,'real');
        end
        function res = get.Gmmaxtc(obj)
            res = getParameter(obj,'Gmmaxtc');
        end
        function obj = set.Vtotc(obj,val)
            obj = setParameter(obj,'Vtotc',val,0,'real');
        end
        function res = get.Vtotc(obj)
            res = getParameter(obj,'Vtotc');
        end
        function obj = set.Gammatc(obj,val)
            obj = setParameter(obj,'Gammatc',val,0,'real');
        end
        function res = get.Gammatc(obj)
            res = getParameter(obj,'Gammatc');
        end
        function obj = set.Vinfltc(obj,val)
            obj = setParameter(obj,'Vinfltc',val,0,'real');
        end
        function res = get.Vinfltc(obj)
            res = getParameter(obj,'Vinfltc');
        end
        function obj = set.Gmmaxactc(obj,val)
            obj = setParameter(obj,'Gmmaxactc',val,0,'real');
        end
        function res = get.Gmmaxactc(obj)
            res = getParameter(obj,'Gmmaxactc');
        end
        function obj = set.Vtoactc(obj,val)
            obj = setParameter(obj,'Vtoactc',val,0,'real');
        end
        function res = get.Vtoactc(obj)
            res = getParameter(obj,'Vtoactc');
        end
        function obj = set.Gammaactc(obj,val)
            obj = setParameter(obj,'Gammaactc',val,0,'real');
        end
        function res = get.Gammaactc(obj)
            res = getParameter(obj,'Gammaactc');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.wVgfwd(obj,val)
            obj = setParameter(obj,'wVgfwd',val,0,'real');
        end
        function res = get.wVgfwd(obj)
            res = getParameter(obj,'wVgfwd');
        end
        function obj = set.wBvgs(obj,val)
            obj = setParameter(obj,'wBvgs',val,0,'real');
        end
        function res = get.wBvgs(obj)
            res = getParameter(obj,'wBvgs');
        end
        function obj = set.wBvgd(obj,val)
            obj = setParameter(obj,'wBvgd',val,0,'real');
        end
        function res = get.wBvgd(obj)
            res = getParameter(obj,'wBvgd');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

classdef ADS_CIND2 < ADScomponent
    % ADS_CIND2 matlab representation for the ADS CIND2 component
    % Lossy Toroidal Inductor
    % CIND2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of turns (Sm-ri) Unit: 1
        N
        % inductance index (Smorr) Unit: H
        AL
        % totoal winding resistance (Smorr) Unit: oh
        R
        % core quality factor (Smorr) Unit: 1
        Q
        % frequency at which q is specified (Smorr) Unit: Hz
        Freq
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.AL(obj,val)
            obj = setParameter(obj,'AL',val,0,'real');
        end
        function res = get.AL(obj)
            res = getParameter(obj,'AL');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

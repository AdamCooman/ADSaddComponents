classdef ADS_MSIDCF < ADScomponent
    % ADS_MSIDCF matlab representation for the ADS MSIDCF component
    % Microstrip Capacitor - Center-fed
    % MSIDCF [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Finger Width (smorr) Unit: m
        W
        % Finger Spacing (smorr) Unit: m
        S
        % Finger Length (smorr) Unit: m
        L
        % End Gap (smorr) Unit: m
        G
        % Number of Fingers (smorr) 
        N
        % Terminal Strip Width (smorr) Unit: m
        Wt
        % Feeder Strip Width (smorr) Unit: m
        Wf
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Wt(obj,val)
            obj = setParameter(obj,'Wt',val,0,'real');
        end
        function res = get.Wt(obj)
            res = getParameter(obj,'Wt');
        end
        function obj = set.Wf(obj,val)
            obj = setParameter(obj,'Wf',val,0,'real');
        end
        function res = get.Wf(obj)
            res = getParameter(obj,'Wf');
        end
    end
end

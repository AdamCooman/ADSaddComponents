classdef ADS_ChannelSim < ADSnodeless
    % ADS_ChannelSim matlab representation for the ADS ChannelSim component
    % Channel Analysis
    % ChannelSim [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Type of analysis (s---s) 
        Type
        % Tolerance mode (s---i) 
        ToleranceMode
        % Maximum impulse response length in bit (s---i) 
        MaxImpulseLength
        % Enforce passivity in S-parameters (s---b) 
        EnforcePassivity
        % Enforce strict passivity in S-parameters (s---b) 
        EnforceStrictPassivity
        % Save to dataset (s---b) 
        SaveToDataset
        % Number of time points per UI (s---i) 
        NumberTimePtPerUI
        % Degree of annotation (s---i) 
        StatusLevel
        % Anti-aliasing window (s---i) 
        AntiAliasingWindow
        % BER floor is lower than 1e-16 (s---b) 
        LowBERFloor
        % Maximum characterization freqeuncy of impulse response (s---r) 
        ImpMaxFreq
        % Controller is DDRSim (s---b) 
        isDDR
    end
    methods
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.ToleranceMode(obj,val)
            obj = setParameter(obj,'ToleranceMode',val,0,'integer');
        end
        function res = get.ToleranceMode(obj)
            res = getParameter(obj,'ToleranceMode');
        end
        function obj = set.MaxImpulseLength(obj,val)
            obj = setParameter(obj,'MaxImpulseLength',val,0,'integer');
        end
        function res = get.MaxImpulseLength(obj)
            res = getParameter(obj,'MaxImpulseLength');
        end
        function obj = set.EnforcePassivity(obj,val)
            obj = setParameter(obj,'EnforcePassivity',val,0,'boolean');
        end
        function res = get.EnforcePassivity(obj)
            res = getParameter(obj,'EnforcePassivity');
        end
        function obj = set.EnforceStrictPassivity(obj,val)
            obj = setParameter(obj,'EnforceStrictPassivity',val,0,'boolean');
        end
        function res = get.EnforceStrictPassivity(obj)
            res = getParameter(obj,'EnforceStrictPassivity');
        end
        function obj = set.SaveToDataset(obj,val)
            obj = setParameter(obj,'SaveToDataset',val,0,'boolean');
        end
        function res = get.SaveToDataset(obj)
            res = getParameter(obj,'SaveToDataset');
        end
        function obj = set.NumberTimePtPerUI(obj,val)
            obj = setParameter(obj,'NumberTimePtPerUI',val,0,'integer');
        end
        function res = get.NumberTimePtPerUI(obj)
            res = getParameter(obj,'NumberTimePtPerUI');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.AntiAliasingWindow(obj,val)
            obj = setParameter(obj,'AntiAliasingWindow',val,0,'integer');
        end
        function res = get.AntiAliasingWindow(obj)
            res = getParameter(obj,'AntiAliasingWindow');
        end
        function obj = set.LowBERFloor(obj,val)
            obj = setParameter(obj,'LowBERFloor',val,0,'boolean');
        end
        function res = get.LowBERFloor(obj)
            res = getParameter(obj,'LowBERFloor');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.isDDR(obj,val)
            obj = setParameter(obj,'isDDR',val,0,'boolean');
        end
        function res = get.isDDR(obj)
            res = getParameter(obj,'isDDR');
        end
    end
end

classdef ADS_MSOC < ADScomponent
    % ADS_MSOC matlab representation for the ADS MSOC component
    % Microstrip Open Circuit
    % MSOC [:Name] t1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Length (smorr) Unit: m
        L
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
    end
end

classdef ADS_Angelov_Model < ADSmodel
    % ADS_Angelov_Model matlab representation for the ADS Angelov_Model component
    % Angelov HEMT/MESFET Model model
    % model ModelName Angelov ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Current for maximum transconductance (smorr) Unit: A
        Ipk0
        % Gate voltage for maximum transconductance (smorr) Unit: V
        Vpks
        % Delta gate voltage at peak Gm (smorr) Unit: V
        Dvpks
        % Polynomial coefficient for channel current (smorr) 
        P1
        % Polynomial coefficient for channel current (smorr) 
        P2
        % Polynomial coefficient for channel current (smorr) 
        P3
        % Saturation parameter (smorr) 
        Alphar
        % Saturation parameter (smorr) 
        Alphas
        % Knee voltage (smorr) Unit: V
        Vkn
        % Channel length modulation parameter (smorr) 
        Lambda
        % Channel length modulation parameter (smorr) 
        Lambda1
        % Coefficient for Lambda parameter (smorr) 
        Lvg
        % Unsaturated coefficient for P1 (smorr) 
        B1
        % Unsaturated coefficient for P2 (smorr) 
        B2
        % Soft breakdown model parameter (smorr) 
        Lsb0
        % Threshold voltage for breakdown (smorr) Unit: V
        Vtr
        % Surface breakdown model parameter (smorr) 
        Vsb2
        % Time delay (smorr) Unit: s
        Dmin
        % Drain-source capacitance (smorr) Unit: F
        Cds
        % Gate-source pinch-off capacitance (smorr) Unit: F
        Cgspi
        % Gate-source capacitance (smorr) Unit: F
        Cgs0
        % Gate-drain pinch-off capacitance (smorr) Unit: F
        Cgdpi
        % Gate-drain capacitance (smorr) Unit: F
        Cgd0
        % External gate-drain capacitance (smorr) Unit: F
        Cgdpe
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P10
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P11
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P20
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P21
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P30
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P31
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P40
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P41
        % Polynomial coefficient for capacitance (smorr) Unit: F
        P111
        % Gate fwd saturation current (smorr) Unit: A
        Ij
        % Gate current parameter (smorr) 
        Pg
        % Gate current parameter (smorr) Unit: V
        Vjg
        % Gate resistance (smorr) Unit: Ohms
        Rg
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Gate-source resistance (smorr) Unit: Ohms
        Ri
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Gate-drain resistance (smorr) Unit: Ohms
        Rgd
        % Gate inductance (smorr) Unit: H
        Lg
        % Drain inductance (smorr) Unit: H
        Ld
        % Source inductance (smorr) Unit: H
        Ls
        % Internal time delay (smorr) Unit: s
        Tau
        % Minimum value of Rc resistance (smorr) Unit: Ohms
        Rcmin
        % R for frequency dependent output conductance (smorr) Unit: Ohms
        Rc
        % C for frequency dependent output conductance (smorr) Unit: F
        Crf
        % Coefficient for voltage dependence of Rc resistance (smorr) 
        E1
        % R for frequency dependent input conductance (smorr) Unit: Ohms
        Rcin
        % C for frequency dependent input conductance (smorr) Unit: F
        Crfin
        % Thermal resistance (smorr) Unit: Ohms
        Rth
        % Thermal capacitance (smorr) Unit: F
        Cth
        % Temperature coefficient of Ipk0 parameter (smorr) 
        Tcipk0
        % Temperature coefficient of P1 parameter (smorr) 
        Tcp1
        % Temperature coefficient of Cgs0 parameter (smorr) 
        Tccgs0
        % Temperature coefficient of Cgd0 parameter (smorr) 
        Tccgd0
        % Temperature coefficient of Lsb0 parameter (smorr) 
        Tclsb0
        % Temperature coefficient of Rc parameter (smorr) 
        Tcrc
        % Temperature coefficient of Crf parameter (smorr) 
        Tccrf
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Flag denoting self-heating (s--ri) 
        Selft
        % Ids Model flag (s--ri) 
        Idsmod
        % Ig Model flag (s--ri) 
        Igmod
        % Capacitance model selector (s--ri) 
        Capmod
        % Noise model selector (s--ri) 
        Noimod
        % Gate noise coefficient (smorr) 
        NoiseR
        % Drain noise coefficient (smorr) 
        NoiseP
        % Gate-drain noise correlation coefficient (smorr) 
        NoiseC
        % Flicker-noise corner frequency (smorr) Unit: Hz
        Fnc
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Gate equivalent temperature (smorr) Unit: deg C
        Tg
        % Drain equivalent temperature coefficient (smorr) Unit: deg C
        Td
        % Drain equivalent temperature coefficient (smorr) Unit: deg C
        Td1
        % Optimum noise current (smorr) Unit: A
        Ioptn
        % Noise fitting coefficient (smorr) 
        Tmn
        % Noise fitting coefficient (smorr) 
        Knd2
        % Noise fitting coefficient (smorr) 
        Kng
        % Flicker noise coefficient (smorr) 
        Klf
        % Generation-recombination frequency corner (smorr) Unit: Hz
        Fgr
        % Flicker noise frequency exponent (smorr) 
        Np
        % Effective gate noise width (smorr) Unit: m
        Lw
        % Ideality factor (smorr) 
        Ne
    end
    methods
        function obj = set.Ipk0(obj,val)
            obj = setParameter(obj,'Ipk0',val,0,'real');
        end
        function res = get.Ipk0(obj)
            res = getParameter(obj,'Ipk0');
        end
        function obj = set.Vpks(obj,val)
            obj = setParameter(obj,'Vpks',val,0,'real');
        end
        function res = get.Vpks(obj)
            res = getParameter(obj,'Vpks');
        end
        function obj = set.Dvpks(obj,val)
            obj = setParameter(obj,'Dvpks',val,0,'real');
        end
        function res = get.Dvpks(obj)
            res = getParameter(obj,'Dvpks');
        end
        function obj = set.P1(obj,val)
            obj = setParameter(obj,'P1',val,0,'real');
        end
        function res = get.P1(obj)
            res = getParameter(obj,'P1');
        end
        function obj = set.P2(obj,val)
            obj = setParameter(obj,'P2',val,0,'real');
        end
        function res = get.P2(obj)
            res = getParameter(obj,'P2');
        end
        function obj = set.P3(obj,val)
            obj = setParameter(obj,'P3',val,0,'real');
        end
        function res = get.P3(obj)
            res = getParameter(obj,'P3');
        end
        function obj = set.Alphar(obj,val)
            obj = setParameter(obj,'Alphar',val,0,'real');
        end
        function res = get.Alphar(obj)
            res = getParameter(obj,'Alphar');
        end
        function obj = set.Alphas(obj,val)
            obj = setParameter(obj,'Alphas',val,0,'real');
        end
        function res = get.Alphas(obj)
            res = getParameter(obj,'Alphas');
        end
        function obj = set.Vkn(obj,val)
            obj = setParameter(obj,'Vkn',val,0,'real');
        end
        function res = get.Vkn(obj)
            res = getParameter(obj,'Vkn');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Lambda1(obj,val)
            obj = setParameter(obj,'Lambda1',val,0,'real');
        end
        function res = get.Lambda1(obj)
            res = getParameter(obj,'Lambda1');
        end
        function obj = set.Lvg(obj,val)
            obj = setParameter(obj,'Lvg',val,0,'real');
        end
        function res = get.Lvg(obj)
            res = getParameter(obj,'Lvg');
        end
        function obj = set.B1(obj,val)
            obj = setParameter(obj,'B1',val,0,'real');
        end
        function res = get.B1(obj)
            res = getParameter(obj,'B1');
        end
        function obj = set.B2(obj,val)
            obj = setParameter(obj,'B2',val,0,'real');
        end
        function res = get.B2(obj)
            res = getParameter(obj,'B2');
        end
        function obj = set.Lsb0(obj,val)
            obj = setParameter(obj,'Lsb0',val,0,'real');
        end
        function res = get.Lsb0(obj)
            res = getParameter(obj,'Lsb0');
        end
        function obj = set.Vtr(obj,val)
            obj = setParameter(obj,'Vtr',val,0,'real');
        end
        function res = get.Vtr(obj)
            res = getParameter(obj,'Vtr');
        end
        function obj = set.Vsb2(obj,val)
            obj = setParameter(obj,'Vsb2',val,0,'real');
        end
        function res = get.Vsb2(obj)
            res = getParameter(obj,'Vsb2');
        end
        function obj = set.Dmin(obj,val)
            obj = setParameter(obj,'Dmin',val,0,'real');
        end
        function res = get.Dmin(obj)
            res = getParameter(obj,'Dmin');
        end
        function obj = set.Cds(obj,val)
            obj = setParameter(obj,'Cds',val,0,'real');
        end
        function res = get.Cds(obj)
            res = getParameter(obj,'Cds');
        end
        function obj = set.Cgspi(obj,val)
            obj = setParameter(obj,'Cgspi',val,0,'real');
        end
        function res = get.Cgspi(obj)
            res = getParameter(obj,'Cgspi');
        end
        function obj = set.Cgs0(obj,val)
            obj = setParameter(obj,'Cgs0',val,0,'real');
        end
        function res = get.Cgs0(obj)
            res = getParameter(obj,'Cgs0');
        end
        function obj = set.Cgdpi(obj,val)
            obj = setParameter(obj,'Cgdpi',val,0,'real');
        end
        function res = get.Cgdpi(obj)
            res = getParameter(obj,'Cgdpi');
        end
        function obj = set.Cgd0(obj,val)
            obj = setParameter(obj,'Cgd0',val,0,'real');
        end
        function res = get.Cgd0(obj)
            res = getParameter(obj,'Cgd0');
        end
        function obj = set.Cgdpe(obj,val)
            obj = setParameter(obj,'Cgdpe',val,0,'real');
        end
        function res = get.Cgdpe(obj)
            res = getParameter(obj,'Cgdpe');
        end
        function obj = set.P10(obj,val)
            obj = setParameter(obj,'P10',val,0,'real');
        end
        function res = get.P10(obj)
            res = getParameter(obj,'P10');
        end
        function obj = set.P11(obj,val)
            obj = setParameter(obj,'P11',val,0,'real');
        end
        function res = get.P11(obj)
            res = getParameter(obj,'P11');
        end
        function obj = set.P20(obj,val)
            obj = setParameter(obj,'P20',val,0,'real');
        end
        function res = get.P20(obj)
            res = getParameter(obj,'P20');
        end
        function obj = set.P21(obj,val)
            obj = setParameter(obj,'P21',val,0,'real');
        end
        function res = get.P21(obj)
            res = getParameter(obj,'P21');
        end
        function obj = set.P30(obj,val)
            obj = setParameter(obj,'P30',val,0,'real');
        end
        function res = get.P30(obj)
            res = getParameter(obj,'P30');
        end
        function obj = set.P31(obj,val)
            obj = setParameter(obj,'P31',val,0,'real');
        end
        function res = get.P31(obj)
            res = getParameter(obj,'P31');
        end
        function obj = set.P40(obj,val)
            obj = setParameter(obj,'P40',val,0,'real');
        end
        function res = get.P40(obj)
            res = getParameter(obj,'P40');
        end
        function obj = set.P41(obj,val)
            obj = setParameter(obj,'P41',val,0,'real');
        end
        function res = get.P41(obj)
            res = getParameter(obj,'P41');
        end
        function obj = set.P111(obj,val)
            obj = setParameter(obj,'P111',val,0,'real');
        end
        function res = get.P111(obj)
            res = getParameter(obj,'P111');
        end
        function obj = set.Ij(obj,val)
            obj = setParameter(obj,'Ij',val,0,'real');
        end
        function res = get.Ij(obj)
            res = getParameter(obj,'Ij');
        end
        function obj = set.Pg(obj,val)
            obj = setParameter(obj,'Pg',val,0,'real');
        end
        function res = get.Pg(obj)
            res = getParameter(obj,'Pg');
        end
        function obj = set.Vjg(obj,val)
            obj = setParameter(obj,'Vjg',val,0,'real');
        end
        function res = get.Vjg(obj)
            res = getParameter(obj,'Vjg');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Ri(obj,val)
            obj = setParameter(obj,'Ri',val,0,'real');
        end
        function res = get.Ri(obj)
            res = getParameter(obj,'Ri');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rgd(obj,val)
            obj = setParameter(obj,'Rgd',val,0,'real');
        end
        function res = get.Rgd(obj)
            res = getParameter(obj,'Rgd');
        end
        function obj = set.Lg(obj,val)
            obj = setParameter(obj,'Lg',val,0,'real');
        end
        function res = get.Lg(obj)
            res = getParameter(obj,'Lg');
        end
        function obj = set.Ld(obj,val)
            obj = setParameter(obj,'Ld',val,0,'real');
        end
        function res = get.Ld(obj)
            res = getParameter(obj,'Ld');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Rcmin(obj,val)
            obj = setParameter(obj,'Rcmin',val,0,'real');
        end
        function res = get.Rcmin(obj)
            res = getParameter(obj,'Rcmin');
        end
        function obj = set.Rc(obj,val)
            obj = setParameter(obj,'Rc',val,0,'real');
        end
        function res = get.Rc(obj)
            res = getParameter(obj,'Rc');
        end
        function obj = set.Crf(obj,val)
            obj = setParameter(obj,'Crf',val,0,'real');
        end
        function res = get.Crf(obj)
            res = getParameter(obj,'Crf');
        end
        function obj = set.E1(obj,val)
            obj = setParameter(obj,'E1',val,0,'real');
        end
        function res = get.E1(obj)
            res = getParameter(obj,'E1');
        end
        function obj = set.Rcin(obj,val)
            obj = setParameter(obj,'Rcin',val,0,'real');
        end
        function res = get.Rcin(obj)
            res = getParameter(obj,'Rcin');
        end
        function obj = set.Crfin(obj,val)
            obj = setParameter(obj,'Crfin',val,0,'real');
        end
        function res = get.Crfin(obj)
            res = getParameter(obj,'Crfin');
        end
        function obj = set.Rth(obj,val)
            obj = setParameter(obj,'Rth',val,0,'real');
        end
        function res = get.Rth(obj)
            res = getParameter(obj,'Rth');
        end
        function obj = set.Cth(obj,val)
            obj = setParameter(obj,'Cth',val,0,'real');
        end
        function res = get.Cth(obj)
            res = getParameter(obj,'Cth');
        end
        function obj = set.Tcipk0(obj,val)
            obj = setParameter(obj,'Tcipk0',val,0,'real');
        end
        function res = get.Tcipk0(obj)
            res = getParameter(obj,'Tcipk0');
        end
        function obj = set.Tcp1(obj,val)
            obj = setParameter(obj,'Tcp1',val,0,'real');
        end
        function res = get.Tcp1(obj)
            res = getParameter(obj,'Tcp1');
        end
        function obj = set.Tccgs0(obj,val)
            obj = setParameter(obj,'Tccgs0',val,0,'real');
        end
        function res = get.Tccgs0(obj)
            res = getParameter(obj,'Tccgs0');
        end
        function obj = set.Tccgd0(obj,val)
            obj = setParameter(obj,'Tccgd0',val,0,'real');
        end
        function res = get.Tccgd0(obj)
            res = getParameter(obj,'Tccgd0');
        end
        function obj = set.Tclsb0(obj,val)
            obj = setParameter(obj,'Tclsb0',val,0,'real');
        end
        function res = get.Tclsb0(obj)
            res = getParameter(obj,'Tclsb0');
        end
        function obj = set.Tcrc(obj,val)
            obj = setParameter(obj,'Tcrc',val,0,'real');
        end
        function res = get.Tcrc(obj)
            res = getParameter(obj,'Tcrc');
        end
        function obj = set.Tccrf(obj,val)
            obj = setParameter(obj,'Tccrf',val,0,'real');
        end
        function res = get.Tccrf(obj)
            res = getParameter(obj,'Tccrf');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Selft(obj,val)
            obj = setParameter(obj,'Selft',val,0,'integer');
        end
        function res = get.Selft(obj)
            res = getParameter(obj,'Selft');
        end
        function obj = set.Idsmod(obj,val)
            obj = setParameter(obj,'Idsmod',val,0,'integer');
        end
        function res = get.Idsmod(obj)
            res = getParameter(obj,'Idsmod');
        end
        function obj = set.Igmod(obj,val)
            obj = setParameter(obj,'Igmod',val,0,'integer');
        end
        function res = get.Igmod(obj)
            res = getParameter(obj,'Igmod');
        end
        function obj = set.Capmod(obj,val)
            obj = setParameter(obj,'Capmod',val,0,'integer');
        end
        function res = get.Capmod(obj)
            res = getParameter(obj,'Capmod');
        end
        function obj = set.Noimod(obj,val)
            obj = setParameter(obj,'Noimod',val,0,'integer');
        end
        function res = get.Noimod(obj)
            res = getParameter(obj,'Noimod');
        end
        function obj = set.NoiseR(obj,val)
            obj = setParameter(obj,'NoiseR',val,0,'real');
        end
        function res = get.NoiseR(obj)
            res = getParameter(obj,'NoiseR');
        end
        function obj = set.NoiseP(obj,val)
            obj = setParameter(obj,'NoiseP',val,0,'real');
        end
        function res = get.NoiseP(obj)
            res = getParameter(obj,'NoiseP');
        end
        function obj = set.NoiseC(obj,val)
            obj = setParameter(obj,'NoiseC',val,0,'real');
        end
        function res = get.NoiseC(obj)
            res = getParameter(obj,'NoiseC');
        end
        function obj = set.Fnc(obj,val)
            obj = setParameter(obj,'Fnc',val,0,'real');
        end
        function res = get.Fnc(obj)
            res = getParameter(obj,'Fnc');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Tg(obj,val)
            obj = setParameter(obj,'Tg',val,0,'real');
        end
        function res = get.Tg(obj)
            res = getParameter(obj,'Tg');
        end
        function obj = set.Td(obj,val)
            obj = setParameter(obj,'Td',val,0,'real');
        end
        function res = get.Td(obj)
            res = getParameter(obj,'Td');
        end
        function obj = set.Td1(obj,val)
            obj = setParameter(obj,'Td1',val,0,'real');
        end
        function res = get.Td1(obj)
            res = getParameter(obj,'Td1');
        end
        function obj = set.Ioptn(obj,val)
            obj = setParameter(obj,'Ioptn',val,0,'real');
        end
        function res = get.Ioptn(obj)
            res = getParameter(obj,'Ioptn');
        end
        function obj = set.Tmn(obj,val)
            obj = setParameter(obj,'Tmn',val,0,'real');
        end
        function res = get.Tmn(obj)
            res = getParameter(obj,'Tmn');
        end
        function obj = set.Knd2(obj,val)
            obj = setParameter(obj,'Knd2',val,0,'real');
        end
        function res = get.Knd2(obj)
            res = getParameter(obj,'Knd2');
        end
        function obj = set.Kng(obj,val)
            obj = setParameter(obj,'Kng',val,0,'real');
        end
        function res = get.Kng(obj)
            res = getParameter(obj,'Kng');
        end
        function obj = set.Klf(obj,val)
            obj = setParameter(obj,'Klf',val,0,'real');
        end
        function res = get.Klf(obj)
            res = getParameter(obj,'Klf');
        end
        function obj = set.Fgr(obj,val)
            obj = setParameter(obj,'Fgr',val,0,'real');
        end
        function res = get.Fgr(obj)
            res = getParameter(obj,'Fgr');
        end
        function obj = set.Np(obj,val)
            obj = setParameter(obj,'Np',val,0,'real');
        end
        function res = get.Np(obj)
            res = getParameter(obj,'Np');
        end
        function obj = set.Lw(obj,val)
            obj = setParameter(obj,'Lw',val,0,'real');
        end
        function res = get.Lw(obj)
            res = getParameter(obj,'Lw');
        end
        function obj = set.Ne(obj,val)
            obj = setParameter(obj,'Ne',val,0,'real');
        end
        function res = get.Ne(obj)
            res = getParameter(obj,'Ne');
        end
    end
end

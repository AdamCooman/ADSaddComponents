classdef ADS_CLINP < ADScomponent
    % ADS_CLINP matlab representation for the ADS CLINP component
    % Lossy Coupled Transmission Lines
    % CLINP [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Even-Mode characteristic Impedance (Smorr) Unit: ohms
        Ze
        % Odd-Mode characteristic impedance (Smorr) Unit: ohms
        Zo
        % Physical Length (Smorr) Unit: m
        L
        % Even-Mode effective dielectric constant (Smorr) Unit: unknown
        Ke
        % Odd-Mode effective dielectric constant (Smorr) Unit: unknown
        Ko
        % Even-Mode attenuation (Smorr) Unit: DB
        Ae
        % Odd-Mode attenuation (Smorr) Unit: DB
        Ao
        % Physical Temperature (smorr) Unit: C
        Temp
        % Model with or without distortion (sm-ri) Unit: unknown
        Distortion
        % Frequency for Scaling attenuation (Smorr) Unit: hz
        F
        % Dielectric loss tangent value (Smorr) Unit: unknown
        TanD
        % Relative Permeability (Smorr) Unit: unknown
        Mur
        % Magnetic Loss Tangent (Smorr) Unit: unknown
        TanM
        % Dielectric Conductivity value (Smorr) Unit: siemens
        Sigma
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) Unit: unknown
        DielectricLossModel
        % Frequency at which Er and TanD are measured (smorr) Unit: hz
        FreqForEpsrTand
        % Low roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        LowFreqForTand
        % High roll-off frequency in the Svensson/Djordjevic model (smorr) Unit: hz
        HighFreqForTand
    end
    methods
        function obj = set.Ze(obj,val)
            obj = setParameter(obj,'Ze',val,0,'real');
        end
        function res = get.Ze(obj)
            res = getParameter(obj,'Ze');
        end
        function obj = set.Zo(obj,val)
            obj = setParameter(obj,'Zo',val,0,'real');
        end
        function res = get.Zo(obj)
            res = getParameter(obj,'Zo');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Ke(obj,val)
            obj = setParameter(obj,'Ke',val,0,'real');
        end
        function res = get.Ke(obj)
            res = getParameter(obj,'Ke');
        end
        function obj = set.Ko(obj,val)
            obj = setParameter(obj,'Ko',val,0,'real');
        end
        function res = get.Ko(obj)
            res = getParameter(obj,'Ko');
        end
        function obj = set.Ae(obj,val)
            obj = setParameter(obj,'Ae',val,0,'real');
        end
        function res = get.Ae(obj)
            res = getParameter(obj,'Ae');
        end
        function obj = set.Ao(obj,val)
            obj = setParameter(obj,'Ao',val,0,'real');
        end
        function res = get.Ao(obj)
            res = getParameter(obj,'Ao');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Distortion(obj,val)
            obj = setParameter(obj,'Distortion',val,0,'integer');
        end
        function res = get.Distortion(obj)
            res = getParameter(obj,'Distortion');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Mur(obj,val)
            obj = setParameter(obj,'Mur',val,0,'real');
        end
        function res = get.Mur(obj)
            res = getParameter(obj,'Mur');
        end
        function obj = set.TanM(obj,val)
            obj = setParameter(obj,'TanM',val,0,'real');
        end
        function res = get.TanM(obj)
            res = getParameter(obj,'TanM');
        end
        function obj = set.Sigma(obj,val)
            obj = setParameter(obj,'Sigma',val,0,'real');
        end
        function res = get.Sigma(obj)
            res = getParameter(obj,'Sigma');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.FreqForEpsrTand(obj,val)
            obj = setParameter(obj,'FreqForEpsrTand',val,0,'real');
        end
        function res = get.FreqForEpsrTand(obj)
            res = getParameter(obj,'FreqForEpsrTand');
        end
        function obj = set.LowFreqForTand(obj,val)
            obj = setParameter(obj,'LowFreqForTand',val,0,'real');
        end
        function res = get.LowFreqForTand(obj)
            res = getParameter(obj,'LowFreqForTand');
        end
        function obj = set.HighFreqForTand(obj,val)
            obj = setParameter(obj,'HighFreqForTand',val,0,'real');
        end
        function res = get.HighFreqForTand(obj)
            res = getParameter(obj,'HighFreqForTand');
        end
    end
end

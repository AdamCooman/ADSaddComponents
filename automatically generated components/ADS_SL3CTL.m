classdef ADS_SL3CTL < ADScomponent
    % ADS_SL3CTL matlab representation for the ADS SL3CTL component
    % Three Coupled Striplines
    % SL3CTL [:Name] l1 l2 l3 r1 r2 r3 ...
    properties (Access=protected)
        NumberOfNodes = 6
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width at terminal 1 (smorr) Unit: m
        W1
        % Line Width at terminal 2 (smorr) Unit: m
        W2
        % Line Width at terminal 3 (smorr) Unit: m
        W3
        % Length (smorr) Unit: m
        L
        % Spacing between strips 1 and 2 (smorr) Unit: m
        S1
        % Spacing between strips 2 and 3 (smorr) Unit: m
        S2
        % Strip Height (smorr) Unit: m
        Hs
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.S2(obj,val)
            obj = setParameter(obj,'S2',val,0,'real');
        end
        function res = get.S2(obj)
            res = getParameter(obj,'S2');
        end
        function obj = set.Hs(obj,val)
            obj = setParameter(obj,'Hs',val,0,'real');
        end
        function res = get.Hs(obj)
            res = getParameter(obj,'Hs');
        end
    end
end

classdef ADS_W_Element < ADScomponent
    % ADS_W_Element matlab representation for the ADS W_Element component
    % User-defined model
    % W_Element [:Name] n1 n2 n3 n4 ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Number of signal lines (s--ri) Unit: unknown
        N
        % Length of all transmission lines (smorr) Unit: m
        Length
        % Model type: 0 - static, 1 - tabular (frequency dependent matrices) (s--ri) Unit: unknown
        Model_type
        % Name of the file containing static RLGC matrices (sm-rs) Unit: unknown
        RLGCfile
        % Name of the file containing tabular frequency dependent L matrices (sm-rs) Unit: unknown
        Lfile
        % Name of the file containing tabular frequency dependent C matrices (sm-rs) Unit: unknown
        Cfile
        % Name of the file containing tabular frequency dependent R matrices (sm-rs) Unit: unknown
        Rfile
        % Name of the file containing tabular frequency dependent G matrices (sm-rs) Unit: unknown
        Gfile
        % Array of the lower triangular elements of static/tabular L matrix (smorr) Unit: H/m
        Ldata
        % Array of the lower triangular elements of static/tabular C matrix (smorr) Unit: F/m
        Cdata
        % Array of the lower triangular elements of static Rdc or tabular R matrix (smorr) Unit: Ohm/m
        Rdata
        % Array of the lower triangular elements of static Gdc or tabular G matrix (smorr) Unit: S/m
        Gdata
        % Array of the lower triangular elements of static Rs matrix (smorr) Unit: Ohm/m
        RSdata
        % Array of the lower triangular elements of static Gd matrix (smorr) Unit: S/m
        GDdata
        % Cut-off frequency of dielectric loss (zero means infinity) (smorr) Unit: Hz
        Fgd
        % Type of sweep in Ldata/Lfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) Unit: unknown
        LfreqSweep
        % Frequency of the first data set in the Ldata/Lfile (LfreqSweep=0,1,2,3) (smorr) Unit: Hz
        LfreqStart
        % Frequency of the last data set in the Ldata/Lfile (LfreqSweep=0,1) (smorr) Unit: Hz
        LfreqStop
        % Number of frequency points per decade in Ldata/Lfile (LfreqSweep=2) (sm-ri) Unit: unknown
        LfreqsPerDec
        % Number of frequency points per octave in Ldata/Lfile (LfreqSweep=3) (sm-ri) Unit: unknown
        LfreqsPerOct
        % Type of sweep in Cdata/Cfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) Unit: unknown
        CfreqSweep
        % Frequency of the first data set in the Cdata/Cfile (CfreqSweep=0,1,2,3) (smorr) Unit: Hz
        CfreqStart
        % Frequency of the last data set in the Cdata/Cfile (CfreqSweep=0,1) (smorr) Unit: Hz
        CfreqStop
        % Number of frequency points per decade in Cdata/Cfile (CfreqSweep=2) (sm-ri) Unit: unknown
        CfreqsPerDec
        % Number of frequency points per octave in Cdata/Cfile (CfreqSweep=3) (sm-ri) Unit: unknown
        CfreqsPerOct
        % Type of sweep in Rdata/Rfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) Unit: unknown
        RfreqSweep
        % Frequency of the first data set in the Rdata/Rfile (RfreqSweep=0,1,2,3) (smorr) Unit: Hz
        RfreqStart
        % Frequency of the last data set in the Rdata/Rfile (RfreqSweep=0,1) (smorr) Unit: Hz
        RfreqStop
        % Number of frequency points per decade in Rdata/Rfile (RfreqSweep=2) (sm-ri) Unit: unknown
        RfreqsPerDec
        % Number of frequency points per octave in Rdata/Rfile (RfreqSweep=3) (sm-ri) Unit: unknown
        RfreqsPerOct
        % Type of sweep in Gdata/Gfile: 0 - linear, 1,2,3 - log, 4 - points (sm-ri) Unit: unknown
        GfreqSweep
        % Frequency of the first data set in the Gdata/Gfile (GfreqSweep=0,1,2,3) (smorr) Unit: Hz
        GfreqStart
        % Frequency of the last data set in the Gdata/Gfile (GfreqSweep=0,1) (smorr) Unit: Hz
        GfreqStop
        % Number of frequency points per decade in Gdata/Gfile (GfreqSweep=2) (sm-ri) Unit: unknown
        GfreqsPerDec
        % Number of frequency points per octave in Gdata/Gfile (GfreqSweep=3) (sm-ri) Unit: unknown
        GfreqsPerOct
        % Interpolation mode for matrix L data: "linear"/"spline"/"cubic" (sm-rs) Unit: unknown
        LinterpMode
        % Interpolation mode mode for matrix C data: "linear"/"spline"/"cubic" (sm-rs) Unit: unknown
        CinterpMode
        % Interpolation mode mode for matrix R data: "linear"/"spline"/"cubic" (sm-rs) Unit: unknown
        RinterpMode
        % Interpolation mode mode for matrix G data: "linear"/"spline"/"cubic" (sm-rs) Unit: unknown
        GinterpMode
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Model_type(obj,val)
            obj = setParameter(obj,'Model_type',val,0,'integer');
        end
        function res = get.Model_type(obj)
            res = getParameter(obj,'Model_type');
        end
        function obj = set.RLGCfile(obj,val)
            obj = setParameter(obj,'RLGCfile',val,0,'string');
        end
        function res = get.RLGCfile(obj)
            res = getParameter(obj,'RLGCfile');
        end
        function obj = set.Lfile(obj,val)
            obj = setParameter(obj,'Lfile',val,0,'string');
        end
        function res = get.Lfile(obj)
            res = getParameter(obj,'Lfile');
        end
        function obj = set.Cfile(obj,val)
            obj = setParameter(obj,'Cfile',val,0,'string');
        end
        function res = get.Cfile(obj)
            res = getParameter(obj,'Cfile');
        end
        function obj = set.Rfile(obj,val)
            obj = setParameter(obj,'Rfile',val,0,'string');
        end
        function res = get.Rfile(obj)
            res = getParameter(obj,'Rfile');
        end
        function obj = set.Gfile(obj,val)
            obj = setParameter(obj,'Gfile',val,0,'string');
        end
        function res = get.Gfile(obj)
            res = getParameter(obj,'Gfile');
        end
        function obj = set.Ldata(obj,val)
            obj = setParameter(obj,'Ldata',val,0,'real');
        end
        function res = get.Ldata(obj)
            res = getParameter(obj,'Ldata');
        end
        function obj = set.Cdata(obj,val)
            obj = setParameter(obj,'Cdata',val,0,'real');
        end
        function res = get.Cdata(obj)
            res = getParameter(obj,'Cdata');
        end
        function obj = set.Rdata(obj,val)
            obj = setParameter(obj,'Rdata',val,0,'real');
        end
        function res = get.Rdata(obj)
            res = getParameter(obj,'Rdata');
        end
        function obj = set.Gdata(obj,val)
            obj = setParameter(obj,'Gdata',val,0,'real');
        end
        function res = get.Gdata(obj)
            res = getParameter(obj,'Gdata');
        end
        function obj = set.RSdata(obj,val)
            obj = setParameter(obj,'RSdata',val,0,'real');
        end
        function res = get.RSdata(obj)
            res = getParameter(obj,'RSdata');
        end
        function obj = set.GDdata(obj,val)
            obj = setParameter(obj,'GDdata',val,0,'real');
        end
        function res = get.GDdata(obj)
            res = getParameter(obj,'GDdata');
        end
        function obj = set.Fgd(obj,val)
            obj = setParameter(obj,'Fgd',val,0,'real');
        end
        function res = get.Fgd(obj)
            res = getParameter(obj,'Fgd');
        end
        function obj = set.LfreqSweep(obj,val)
            obj = setParameter(obj,'LfreqSweep',val,0,'integer');
        end
        function res = get.LfreqSweep(obj)
            res = getParameter(obj,'LfreqSweep');
        end
        function obj = set.LfreqStart(obj,val)
            obj = setParameter(obj,'LfreqStart',val,0,'real');
        end
        function res = get.LfreqStart(obj)
            res = getParameter(obj,'LfreqStart');
        end
        function obj = set.LfreqStop(obj,val)
            obj = setParameter(obj,'LfreqStop',val,0,'real');
        end
        function res = get.LfreqStop(obj)
            res = getParameter(obj,'LfreqStop');
        end
        function obj = set.LfreqsPerDec(obj,val)
            obj = setParameter(obj,'LfreqsPerDec',val,0,'integer');
        end
        function res = get.LfreqsPerDec(obj)
            res = getParameter(obj,'LfreqsPerDec');
        end
        function obj = set.LfreqsPerOct(obj,val)
            obj = setParameter(obj,'LfreqsPerOct',val,0,'integer');
        end
        function res = get.LfreqsPerOct(obj)
            res = getParameter(obj,'LfreqsPerOct');
        end
        function obj = set.CfreqSweep(obj,val)
            obj = setParameter(obj,'CfreqSweep',val,0,'integer');
        end
        function res = get.CfreqSweep(obj)
            res = getParameter(obj,'CfreqSweep');
        end
        function obj = set.CfreqStart(obj,val)
            obj = setParameter(obj,'CfreqStart',val,0,'real');
        end
        function res = get.CfreqStart(obj)
            res = getParameter(obj,'CfreqStart');
        end
        function obj = set.CfreqStop(obj,val)
            obj = setParameter(obj,'CfreqStop',val,0,'real');
        end
        function res = get.CfreqStop(obj)
            res = getParameter(obj,'CfreqStop');
        end
        function obj = set.CfreqsPerDec(obj,val)
            obj = setParameter(obj,'CfreqsPerDec',val,0,'integer');
        end
        function res = get.CfreqsPerDec(obj)
            res = getParameter(obj,'CfreqsPerDec');
        end
        function obj = set.CfreqsPerOct(obj,val)
            obj = setParameter(obj,'CfreqsPerOct',val,0,'integer');
        end
        function res = get.CfreqsPerOct(obj)
            res = getParameter(obj,'CfreqsPerOct');
        end
        function obj = set.RfreqSweep(obj,val)
            obj = setParameter(obj,'RfreqSweep',val,0,'integer');
        end
        function res = get.RfreqSweep(obj)
            res = getParameter(obj,'RfreqSweep');
        end
        function obj = set.RfreqStart(obj,val)
            obj = setParameter(obj,'RfreqStart',val,0,'real');
        end
        function res = get.RfreqStart(obj)
            res = getParameter(obj,'RfreqStart');
        end
        function obj = set.RfreqStop(obj,val)
            obj = setParameter(obj,'RfreqStop',val,0,'real');
        end
        function res = get.RfreqStop(obj)
            res = getParameter(obj,'RfreqStop');
        end
        function obj = set.RfreqsPerDec(obj,val)
            obj = setParameter(obj,'RfreqsPerDec',val,0,'integer');
        end
        function res = get.RfreqsPerDec(obj)
            res = getParameter(obj,'RfreqsPerDec');
        end
        function obj = set.RfreqsPerOct(obj,val)
            obj = setParameter(obj,'RfreqsPerOct',val,0,'integer');
        end
        function res = get.RfreqsPerOct(obj)
            res = getParameter(obj,'RfreqsPerOct');
        end
        function obj = set.GfreqSweep(obj,val)
            obj = setParameter(obj,'GfreqSweep',val,0,'integer');
        end
        function res = get.GfreqSweep(obj)
            res = getParameter(obj,'GfreqSweep');
        end
        function obj = set.GfreqStart(obj,val)
            obj = setParameter(obj,'GfreqStart',val,0,'real');
        end
        function res = get.GfreqStart(obj)
            res = getParameter(obj,'GfreqStart');
        end
        function obj = set.GfreqStop(obj,val)
            obj = setParameter(obj,'GfreqStop',val,0,'real');
        end
        function res = get.GfreqStop(obj)
            res = getParameter(obj,'GfreqStop');
        end
        function obj = set.GfreqsPerDec(obj,val)
            obj = setParameter(obj,'GfreqsPerDec',val,0,'integer');
        end
        function res = get.GfreqsPerDec(obj)
            res = getParameter(obj,'GfreqsPerDec');
        end
        function obj = set.GfreqsPerOct(obj,val)
            obj = setParameter(obj,'GfreqsPerOct',val,0,'integer');
        end
        function res = get.GfreqsPerOct(obj)
            res = getParameter(obj,'GfreqsPerOct');
        end
        function obj = set.LinterpMode(obj,val)
            obj = setParameter(obj,'LinterpMode',val,0,'string');
        end
        function res = get.LinterpMode(obj)
            res = getParameter(obj,'LinterpMode');
        end
        function obj = set.CinterpMode(obj,val)
            obj = setParameter(obj,'CinterpMode',val,0,'string');
        end
        function res = get.CinterpMode(obj)
            res = getParameter(obj,'CinterpMode');
        end
        function obj = set.RinterpMode(obj,val)
            obj = setParameter(obj,'RinterpMode',val,0,'string');
        end
        function res = get.RinterpMode(obj)
            res = getParameter(obj,'RinterpMode');
        end
        function obj = set.GinterpMode(obj,val)
            obj = setParameter(obj,'GinterpMode',val,0,'string');
        end
        function res = get.GinterpMode(obj)
            res = getParameter(obj,'GinterpMode');
        end
    end
end

classdef ADS_S_Port < ADScomponent
    % ADS_S_Port matlab representation for the ADS S_Port component
    % User Defined Linear S Parameter N Port
    % S_Port [:Name] p1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Port impedance (smorc) Unit: Ohms
        Z
        % S Parameter (smorc) 
        S
        % Port is reciprocal. (s---b) 
        Recip
        % SYM array pointer to all S parameters (smo-c) 
        All
        % scalar/SYM_array_pointer to all Port impedance parameters (smo-c) 
        Z_All
        % Minimum noise figure in dB (smorr) 
        NFmin
        % Noise resistance (smorr) Unit: Ohms
        Rn
        % Optimum noise match S-Parameter (smorc) 
        Sopt
        % device noise temperature (smorr) Unit: deg C
        Temp
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
        % Check S_Port for passivity. (s---b) 
        CheckPassivity
        % Minimum frequency of data file (smo-r) Unit: Hz
        MinFileFreq
        % Maximum frequency of data file (smo-r) Unit: Hz
        MaxFileFreq
        % Enforce passivity (s---b) 
        EnforcePassivity
        % Envelope fitting bandwidth override (Hz) (smo-r) Unit: Hz
        EnvFitBwHz
        % Cache impulse response (s---b) 
        ImpCache
        % Number of ports (s---i) 
        NumPorts
        % Port mapping type: 0 - none (default), 1 - standard, 2 - custom (s--ri) 
        PortMappingType
        % Port mapping (if PortMappingType > 1): integer array (s--ri) 
        PortMapping
        % For internally defined devices: the current frequency (smorr) 
        InternalFreq
        % For internally defined devices: data pointer (sm---) 
        InternalData
        % Name of the data file in SnP (sm-rs) 
        FileName
        % Lib name (sm-rs) 
        Lib
        % Block name (sm-rs) 
        Block
        % DeEmbed flag (s---b) 
        DeEmbed
        % Interpolation domain in SnP (sm-rs) 
        InterpDom
        % Interpolation method in SnP (sm-rs) 
        InterpMode
        % Extrapolation method in SnP (sm-rs) 
        ExtrapMode
        % Touchstone data file name (sm-rs) 
        TSfile
        % Frequency for Touchstone file data (smorr) 
        TSfreq
        % Envelope Polynomial Coefficient File (s--rs) 
        envfile
        % Mode of port reduction: 0 - No, 1 - YesAll, 2 - YesExceptNamed (s--ri) 
        RemoveUnconnectedTerminals
    end
    methods
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,1,'complex');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,2,'complex');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Recip(obj,val)
            obj = setParameter(obj,'Recip',val,0,'boolean');
        end
        function res = get.Recip(obj)
            res = getParameter(obj,'Recip');
        end
        function obj = set.All(obj,val)
            obj = setParameter(obj,'All',val,0,'complex');
        end
        function res = get.All(obj)
            res = getParameter(obj,'All');
        end
        function obj = set.Z_All(obj,val)
            obj = setParameter(obj,'Z_All',val,0,'complex');
        end
        function res = get.Z_All(obj)
            res = getParameter(obj,'Z_All');
        end
        function obj = set.NFmin(obj,val)
            obj = setParameter(obj,'NFmin',val,0,'real');
        end
        function res = get.NFmin(obj)
            res = getParameter(obj,'NFmin');
        end
        function obj = set.Rn(obj,val)
            obj = setParameter(obj,'Rn',val,0,'real');
        end
        function res = get.Rn(obj)
            res = getParameter(obj,'Rn');
        end
        function obj = set.Sopt(obj,val)
            obj = setParameter(obj,'Sopt',val,0,'complex');
        end
        function res = get.Sopt(obj)
            res = getParameter(obj,'Sopt');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
        function obj = set.CheckPassivity(obj,val)
            obj = setParameter(obj,'CheckPassivity',val,0,'boolean');
        end
        function res = get.CheckPassivity(obj)
            res = getParameter(obj,'CheckPassivity');
        end
        function obj = set.MinFileFreq(obj,val)
            obj = setParameter(obj,'MinFileFreq',val,0,'real');
        end
        function res = get.MinFileFreq(obj)
            res = getParameter(obj,'MinFileFreq');
        end
        function obj = set.MaxFileFreq(obj,val)
            obj = setParameter(obj,'MaxFileFreq',val,0,'real');
        end
        function res = get.MaxFileFreq(obj)
            res = getParameter(obj,'MaxFileFreq');
        end
        function obj = set.EnforcePassivity(obj,val)
            obj = setParameter(obj,'EnforcePassivity',val,0,'boolean');
        end
        function res = get.EnforcePassivity(obj)
            res = getParameter(obj,'EnforcePassivity');
        end
        function obj = set.EnvFitBwHz(obj,val)
            obj = setParameter(obj,'EnvFitBwHz',val,0,'real');
        end
        function res = get.EnvFitBwHz(obj)
            res = getParameter(obj,'EnvFitBwHz');
        end
        function obj = set.ImpCache(obj,val)
            obj = setParameter(obj,'ImpCache',val,0,'boolean');
        end
        function res = get.ImpCache(obj)
            res = getParameter(obj,'ImpCache');
        end
        function obj = set.NumPorts(obj,val)
            obj = setParameter(obj,'NumPorts',val,0,'integer');
        end
        function res = get.NumPorts(obj)
            res = getParameter(obj,'NumPorts');
        end
        function obj = set.PortMappingType(obj,val)
            obj = setParameter(obj,'PortMappingType',val,0,'integer');
        end
        function res = get.PortMappingType(obj)
            res = getParameter(obj,'PortMappingType');
        end
        function obj = set.PortMapping(obj,val)
            obj = setParameter(obj,'PortMapping',val,1,'integer');
        end
        function res = get.PortMapping(obj)
            res = getParameter(obj,'PortMapping');
        end
        function obj = set.InternalFreq(obj,val)
            obj = setParameter(obj,'InternalFreq',val,0,'real');
        end
        function res = get.InternalFreq(obj)
            res = getParameter(obj,'InternalFreq');
        end
        function obj = set.InternalData(obj,val)
            obj = setParameter(obj,'InternalData',val,1,'string');
        end
        function res = get.InternalData(obj)
            res = getParameter(obj,'InternalData');
        end
        function obj = set.FileName(obj,val)
            obj = setParameter(obj,'FileName',val,0,'string');
        end
        function res = get.FileName(obj)
            res = getParameter(obj,'FileName');
        end
        function obj = set.Lib(obj,val)
            obj = setParameter(obj,'Lib',val,0,'string');
        end
        function res = get.Lib(obj)
            res = getParameter(obj,'Lib');
        end
        function obj = set.Block(obj,val)
            obj = setParameter(obj,'Block',val,0,'string');
        end
        function res = get.Block(obj)
            res = getParameter(obj,'Block');
        end
        function obj = set.DeEmbed(obj,val)
            obj = setParameter(obj,'DeEmbed',val,0,'boolean');
        end
        function res = get.DeEmbed(obj)
            res = getParameter(obj,'DeEmbed');
        end
        function obj = set.InterpDom(obj,val)
            obj = setParameter(obj,'InterpDom',val,0,'string');
        end
        function res = get.InterpDom(obj)
            res = getParameter(obj,'InterpDom');
        end
        function obj = set.InterpMode(obj,val)
            obj = setParameter(obj,'InterpMode',val,0,'string');
        end
        function res = get.InterpMode(obj)
            res = getParameter(obj,'InterpMode');
        end
        function obj = set.ExtrapMode(obj,val)
            obj = setParameter(obj,'ExtrapMode',val,0,'string');
        end
        function res = get.ExtrapMode(obj)
            res = getParameter(obj,'ExtrapMode');
        end
        function obj = set.TSfile(obj,val)
            obj = setParameter(obj,'TSfile',val,0,'string');
        end
        function res = get.TSfile(obj)
            res = getParameter(obj,'TSfile');
        end
        function obj = set.TSfreq(obj,val)
            obj = setParameter(obj,'TSfreq',val,0,'real');
        end
        function res = get.TSfreq(obj)
            res = getParameter(obj,'TSfreq');
        end
        function obj = set.envfile(obj,val)
            obj = setParameter(obj,'envfile',val,0,'string');
        end
        function res = get.envfile(obj)
            res = getParameter(obj,'envfile');
        end
        function obj = set.RemoveUnconnectedTerminals(obj,val)
            obj = setParameter(obj,'RemoveUnconnectedTerminals',val,0,'integer');
        end
        function res = get.RemoveUnconnectedTerminals(obj)
            res = getParameter(obj,'RemoveUnconnectedTerminals');
        end
    end
end

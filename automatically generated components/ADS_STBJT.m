classdef ADS_STBJT < ADScomponent
    % ADS_STBJT matlab representation for the ADS STBJT component
    % ST Bipolar Junction Transistor
    % ModelName [:Name] collector base emitter ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Junction area factor (smorr) 
        Area
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Input Conductance (---rr) Unit: S
        gpi
        % Feedback Conductance (---rr) Unit: S
        gmu
        % dIct_dVbe (---rr) Unit: S
        gmm
        % Output Conductance (---rr) Unit: S
        go
        % Transconductance (---rr) Unit: S
        gm
        % Emitter conductance (---rr) Unit: S
        ge
        % Collector Conductance (---rr) Unit: S
        gcv
        % Variable base conductance (---rr) Unit: S
        gx
        % Small Signal internal base collector capacitance (---rr) Unit: F
        cpi
        % Small Signal internal base collector capacitance (---rr) Unit: F
        cmu
        % Small Signal external base collector capacitance (---rr) Unit: F
        cbx
        % Small Signal internal base substrate capacitance (---rr) Unit: F
        cbs
        % Small Signal external base substrate capacitance (---rr) Unit: F
        cxs
        % Small Signal internal collector substrate capacitance (---rr) Unit: F
        ccs
        % dQbe_dVbc (---rr) Unit: F
        gecb
        % Base emitter charge (---rr) Unit: C
        qbe
        % Base collector charge (---rr) Unit: C
        qbc
        % External base collector charge (---rr) Unit: C
        qbx
        % External base substrate charge (---rr) Unit: C
        qbs
        % External Base Substrate charge (---rr) Unit: C
        qxs
        % Collector Substrate charge (---rr) Unit: C
        qcs
        % Dissipated power (---rr) Unit: W
        power
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.gpi(obj,val)
            obj = setParameter(obj,'gpi',val,0,'real');
        end
        function res = get.gpi(obj)
            res = getParameter(obj,'gpi');
        end
        function obj = set.gmu(obj,val)
            obj = setParameter(obj,'gmu',val,0,'real');
        end
        function res = get.gmu(obj)
            res = getParameter(obj,'gmu');
        end
        function obj = set.gmm(obj,val)
            obj = setParameter(obj,'gmm',val,0,'real');
        end
        function res = get.gmm(obj)
            res = getParameter(obj,'gmm');
        end
        function obj = set.go(obj,val)
            obj = setParameter(obj,'go',val,0,'real');
        end
        function res = get.go(obj)
            res = getParameter(obj,'go');
        end
        function obj = set.gm(obj,val)
            obj = setParameter(obj,'gm',val,0,'real');
        end
        function res = get.gm(obj)
            res = getParameter(obj,'gm');
        end
        function obj = set.ge(obj,val)
            obj = setParameter(obj,'ge',val,0,'real');
        end
        function res = get.ge(obj)
            res = getParameter(obj,'ge');
        end
        function obj = set.gcv(obj,val)
            obj = setParameter(obj,'gcv',val,0,'real');
        end
        function res = get.gcv(obj)
            res = getParameter(obj,'gcv');
        end
        function obj = set.gx(obj,val)
            obj = setParameter(obj,'gx',val,0,'real');
        end
        function res = get.gx(obj)
            res = getParameter(obj,'gx');
        end
        function obj = set.cpi(obj,val)
            obj = setParameter(obj,'cpi',val,0,'real');
        end
        function res = get.cpi(obj)
            res = getParameter(obj,'cpi');
        end
        function obj = set.cmu(obj,val)
            obj = setParameter(obj,'cmu',val,0,'real');
        end
        function res = get.cmu(obj)
            res = getParameter(obj,'cmu');
        end
        function obj = set.cbx(obj,val)
            obj = setParameter(obj,'cbx',val,0,'real');
        end
        function res = get.cbx(obj)
            res = getParameter(obj,'cbx');
        end
        function obj = set.cbs(obj,val)
            obj = setParameter(obj,'cbs',val,0,'real');
        end
        function res = get.cbs(obj)
            res = getParameter(obj,'cbs');
        end
        function obj = set.cxs(obj,val)
            obj = setParameter(obj,'cxs',val,0,'real');
        end
        function res = get.cxs(obj)
            res = getParameter(obj,'cxs');
        end
        function obj = set.ccs(obj,val)
            obj = setParameter(obj,'ccs',val,0,'real');
        end
        function res = get.ccs(obj)
            res = getParameter(obj,'ccs');
        end
        function obj = set.gecb(obj,val)
            obj = setParameter(obj,'gecb',val,0,'real');
        end
        function res = get.gecb(obj)
            res = getParameter(obj,'gecb');
        end
        function obj = set.qbe(obj,val)
            obj = setParameter(obj,'qbe',val,0,'real');
        end
        function res = get.qbe(obj)
            res = getParameter(obj,'qbe');
        end
        function obj = set.qbc(obj,val)
            obj = setParameter(obj,'qbc',val,0,'real');
        end
        function res = get.qbc(obj)
            res = getParameter(obj,'qbc');
        end
        function obj = set.qbx(obj,val)
            obj = setParameter(obj,'qbx',val,0,'real');
        end
        function res = get.qbx(obj)
            res = getParameter(obj,'qbx');
        end
        function obj = set.qbs(obj,val)
            obj = setParameter(obj,'qbs',val,0,'real');
        end
        function res = get.qbs(obj)
            res = getParameter(obj,'qbs');
        end
        function obj = set.qxs(obj,val)
            obj = setParameter(obj,'qxs',val,0,'real');
        end
        function res = get.qxs(obj)
            res = getParameter(obj,'qxs');
        end
        function obj = set.qcs(obj,val)
            obj = setParameter(obj,'qcs',val,0,'real');
        end
        function res = get.qcs(obj)
            res = getParameter(obj,'qcs');
        end
        function obj = set.power(obj,val)
            obj = setParameter(obj,'power',val,0,'real');
        end
        function res = get.power(obj)
            res = getParameter(obj,'power');
        end
    end
end

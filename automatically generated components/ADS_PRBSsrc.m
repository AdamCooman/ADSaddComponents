classdef ADS_PRBSsrc < ADScomponent
    % ADS_PRBSsrc matlab representation for the ADS PRBSsrc component
    % Pseudo-Random Bit Sequence Source
    % PRBSsrc [:Name] outPos outNeg trigPos trigNeg
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Source of digital input (smorr) 
        Mode
        % LFSR length (Mode=Maximal Length LFSR) (smorr) 
        RegisterLength
        % LFSR taps for XOR-ing (Mode=User Defined LFSR) (s--rs) 
        Taps
        % LFSR seed (Mode=User Defined LFSR) (s--rs) 
        Seed
        % Bit sequence (Mode=Explicit Bit Sequence) (s--rs) 
        BitSequence
        % ASCII file containing bit sequence (Mode=Bit File) (sm-rs) 
        BitFile
        % Source of trigger signal (smorr) 
        Trigger
        % Threshold voltage for detecting external trigger (smorr) Unit: V
        VtriggerThreshold
        % Edge sensitivity of external trigger (smorr) 
        TriggerEdge
        % User-supplied repeatable length of bit sequence (smorr) 
        SequenceLength
        % Lowest voltage of dynamic range (smorr) Unit: V
        Vlow
        % Highest voltage of dynamic range (smorr) Unit: V
        Vhigh
        % Output resistance across Vplus and Vminus (smorr) Unit: Ohms
        Rout
        % Encoding scheme for pulse amplitude modulation (smorr) 
        PAMencoding
        % Number of levels for pulse amplitude modulation (smorr) 
        PAMlevels
        % Flag for enabling de-empahsis feature (sm-rb) 
        EnableDeEmphasis
        % DeEmphasis specification unit (smorr) 
        DeEmphasisMode
        % De-emphasized dynamic range relative to peak range (smorr) 
        DeEmphasis
        % Rational multiple of bit interval used for pre-emphasis (smorr) 
        EmphasisSpan
        % Number of de-emphasis levels (smorr) 
        DeEmphasisTaps
        % Analytical edge shape during transitions (smorr) 
        EdgeShape
        % Clock rate of generated bit stream (smorr) Unit: Hz
        BitRate
        % Duration of transition from low level to high level (smorr) Unit: s
        RiseTime
        % Duration of transition from high level to low level (smorr) Unit: s
        FallTime
        % Lower level of transition region as % of dynamic range (smorr) 
        TransitReference
        % Initial time delay (smorr) Unit: s
        Delay
        % Flag for enabling Random Jitter (sm-rb) 
        EnableRJ
        % Standard deviation of Random Jitter (smorr) Unit: s
        RJrms
        % Bandwidth for measurement of Random Jitter (smorr) Unit: Hz
        RJbw
        % Flag for enabling Periodic Jitter (sm-rb) 
        EnablePJ
        % Periodic Jitter wave shape (repeated) (smorr) 
        PJwave
        % Periodic Jitter amplitude (repeated) (smorr) Unit: s
        PJamp
        % Periodic Jitter frequency (repeated) (smorr) Unit: Hz
        PJfreq
        % Flag for generating TIEdata (sm-ri) 
        TIEdata
        % Flag for generating raw jitter (sm-ri) 
        RawJitter
    end
    methods
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'real');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.RegisterLength(obj,val)
            obj = setParameter(obj,'RegisterLength',val,0,'real');
        end
        function res = get.RegisterLength(obj)
            res = getParameter(obj,'RegisterLength');
        end
        function obj = set.Taps(obj,val)
            obj = setParameter(obj,'Taps',val,0,'string');
        end
        function res = get.Taps(obj)
            res = getParameter(obj,'Taps');
        end
        function obj = set.Seed(obj,val)
            obj = setParameter(obj,'Seed',val,0,'string');
        end
        function res = get.Seed(obj)
            res = getParameter(obj,'Seed');
        end
        function obj = set.BitSequence(obj,val)
            obj = setParameter(obj,'BitSequence',val,0,'string');
        end
        function res = get.BitSequence(obj)
            res = getParameter(obj,'BitSequence');
        end
        function obj = set.BitFile(obj,val)
            obj = setParameter(obj,'BitFile',val,0,'string');
        end
        function res = get.BitFile(obj)
            res = getParameter(obj,'BitFile');
        end
        function obj = set.Trigger(obj,val)
            obj = setParameter(obj,'Trigger',val,0,'real');
        end
        function res = get.Trigger(obj)
            res = getParameter(obj,'Trigger');
        end
        function obj = set.VtriggerThreshold(obj,val)
            obj = setParameter(obj,'VtriggerThreshold',val,0,'real');
        end
        function res = get.VtriggerThreshold(obj)
            res = getParameter(obj,'VtriggerThreshold');
        end
        function obj = set.TriggerEdge(obj,val)
            obj = setParameter(obj,'TriggerEdge',val,0,'real');
        end
        function res = get.TriggerEdge(obj)
            res = getParameter(obj,'TriggerEdge');
        end
        function obj = set.SequenceLength(obj,val)
            obj = setParameter(obj,'SequenceLength',val,0,'real');
        end
        function res = get.SequenceLength(obj)
            res = getParameter(obj,'SequenceLength');
        end
        function obj = set.Vlow(obj,val)
            obj = setParameter(obj,'Vlow',val,0,'real');
        end
        function res = get.Vlow(obj)
            res = getParameter(obj,'Vlow');
        end
        function obj = set.Vhigh(obj,val)
            obj = setParameter(obj,'Vhigh',val,0,'real');
        end
        function res = get.Vhigh(obj)
            res = getParameter(obj,'Vhigh');
        end
        function obj = set.Rout(obj,val)
            obj = setParameter(obj,'Rout',val,0,'real');
        end
        function res = get.Rout(obj)
            res = getParameter(obj,'Rout');
        end
        function obj = set.PAMencoding(obj,val)
            obj = setParameter(obj,'PAMencoding',val,0,'real');
        end
        function res = get.PAMencoding(obj)
            res = getParameter(obj,'PAMencoding');
        end
        function obj = set.PAMlevels(obj,val)
            obj = setParameter(obj,'PAMlevels',val,0,'real');
        end
        function res = get.PAMlevels(obj)
            res = getParameter(obj,'PAMlevels');
        end
        function obj = set.EnableDeEmphasis(obj,val)
            obj = setParameter(obj,'EnableDeEmphasis',val,0,'boolean');
        end
        function res = get.EnableDeEmphasis(obj)
            res = getParameter(obj,'EnableDeEmphasis');
        end
        function obj = set.DeEmphasisMode(obj,val)
            obj = setParameter(obj,'DeEmphasisMode',val,0,'real');
        end
        function res = get.DeEmphasisMode(obj)
            res = getParameter(obj,'DeEmphasisMode');
        end
        function obj = set.DeEmphasis(obj,val)
            obj = setParameter(obj,'DeEmphasis',val,0,'real');
        end
        function res = get.DeEmphasis(obj)
            res = getParameter(obj,'DeEmphasis');
        end
        function obj = set.EmphasisSpan(obj,val)
            obj = setParameter(obj,'EmphasisSpan',val,0,'real');
        end
        function res = get.EmphasisSpan(obj)
            res = getParameter(obj,'EmphasisSpan');
        end
        function obj = set.DeEmphasisTaps(obj,val)
            obj = setParameter(obj,'DeEmphasisTaps',val,0,'real');
        end
        function res = get.DeEmphasisTaps(obj)
            res = getParameter(obj,'DeEmphasisTaps');
        end
        function obj = set.EdgeShape(obj,val)
            obj = setParameter(obj,'EdgeShape',val,0,'real');
        end
        function res = get.EdgeShape(obj)
            res = getParameter(obj,'EdgeShape');
        end
        function obj = set.BitRate(obj,val)
            obj = setParameter(obj,'BitRate',val,0,'real');
        end
        function res = get.BitRate(obj)
            res = getParameter(obj,'BitRate');
        end
        function obj = set.RiseTime(obj,val)
            obj = setParameter(obj,'RiseTime',val,0,'real');
        end
        function res = get.RiseTime(obj)
            res = getParameter(obj,'RiseTime');
        end
        function obj = set.FallTime(obj,val)
            obj = setParameter(obj,'FallTime',val,0,'real');
        end
        function res = get.FallTime(obj)
            res = getParameter(obj,'FallTime');
        end
        function obj = set.TransitReference(obj,val)
            obj = setParameter(obj,'TransitReference',val,0,'real');
        end
        function res = get.TransitReference(obj)
            res = getParameter(obj,'TransitReference');
        end
        function obj = set.Delay(obj,val)
            obj = setParameter(obj,'Delay',val,0,'real');
        end
        function res = get.Delay(obj)
            res = getParameter(obj,'Delay');
        end
        function obj = set.EnableRJ(obj,val)
            obj = setParameter(obj,'EnableRJ',val,0,'boolean');
        end
        function res = get.EnableRJ(obj)
            res = getParameter(obj,'EnableRJ');
        end
        function obj = set.RJrms(obj,val)
            obj = setParameter(obj,'RJrms',val,0,'real');
        end
        function res = get.RJrms(obj)
            res = getParameter(obj,'RJrms');
        end
        function obj = set.RJbw(obj,val)
            obj = setParameter(obj,'RJbw',val,0,'real');
        end
        function res = get.RJbw(obj)
            res = getParameter(obj,'RJbw');
        end
        function obj = set.EnablePJ(obj,val)
            obj = setParameter(obj,'EnablePJ',val,0,'boolean');
        end
        function res = get.EnablePJ(obj)
            res = getParameter(obj,'EnablePJ');
        end
        function obj = set.PJwave(obj,val)
            obj = setParameter(obj,'PJwave',val,1,'real');
        end
        function res = get.PJwave(obj)
            res = getParameter(obj,'PJwave');
        end
        function obj = set.PJamp(obj,val)
            obj = setParameter(obj,'PJamp',val,1,'real');
        end
        function res = get.PJamp(obj)
            res = getParameter(obj,'PJamp');
        end
        function obj = set.PJfreq(obj,val)
            obj = setParameter(obj,'PJfreq',val,1,'real');
        end
        function res = get.PJfreq(obj)
            res = getParameter(obj,'PJfreq');
        end
        function obj = set.TIEdata(obj,val)
            obj = setParameter(obj,'TIEdata',val,0,'integer');
        end
        function res = get.TIEdata(obj)
            res = getParameter(obj,'TIEdata');
        end
        function obj = set.RawJitter(obj,val)
            obj = setParameter(obj,'RawJitter',val,0,'integer');
        end
        function res = get.RawJitter(obj)
            res = getParameter(obj,'RawJitter');
        end
    end
end

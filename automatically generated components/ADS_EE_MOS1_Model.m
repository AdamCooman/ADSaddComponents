classdef ADS_EE_MOS1_Model < ADSmodel
    % ADS_EE_MOS1_Model matlab representation for the ADS EE_MOS1_Model component
    % EESOF Mos Transistor Model model
    % model ModelName EE_MOS1 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Zero-bias threshold voltage (smorr) Unit: V
        Vto
        % Vds dependent threshold parameter (smorr) Unit: V^-1
        Gamma
        % Gate-source voltage where transconductance maximum (smorr) Unit: V
        Vgo
        % Peak transconductance parameter (smorr) Unit: S
        Gmmax
        % Transconductance tail-off rate parameter (smorr) Unit: V
        Delt
        % Tail-off voltage (smorr) Unit: V
        Vbreak
        % Output conductance parameter (smorr) Unit: V^-1
        Lambda
        % Maximum saturation voltage (smorr) Unit: V
        Vsatm
        % Vgs where saturation voltage is Vsatm (smorr) Unit: V
        Vgm
        % Reverse saturation current (smorr) Unit: A
        Is
        % Junction ideality factor (smorr) 
        N
        % Zero-bias output capacitance (smorr) Unit: F
        Cdso
        % Diode built-in potential (smorr) Unit: V
        Vbi
        % Junction grading coefficient (smorr) 
        Mj
        % Depletion capacitance linearization point (smorr) 
        Fc
        % Breakdown current coefficient (smorr) 
        Kbo
        % Vds where breakdown current begins (smorr) Unit: V
        Vbr
        % Breakdown current exponent parameter (smorr) 
        Nbr
        % Dispersion source output impedance (smorr) Unit: Ohms
        Rdb
        % Trapping-state capacitance (smorr) Unit: F
        Cbs
        % Additional d-b branch conductance at Vds = VDSM (smorr) Unit: S
        Gdbm
        % Dependence of d-b branch conductance with Vds (smorr) 
        Kdb
        % Voltage where additional d-b conductance is constant (smorr) Unit: V
        Vdsm
        % Gate-source voltage where transconductance maximum (smorr) Unit: V
        Vgoac
        % Peak transconductance parameter (AC) (smorr) Unit: S
        Gmmaxac
        % Transconductance tail-off rate parameter(AC) (smorr) Unit: V
        Deltac
        % Tail-off voltage(AC) (smorr) Unit: V
        Vbreakac
        % Output conductance parameter(AC) (smorr) Unit: V^-1
        Lambdaac
        % Maximum saturation voltage (AC) (smorr) Unit: V
        Vsatmac
        % Vgs where saturation voltage is Vsatm (AC) (smorr) Unit: V
        Vgmac
        % Maximum of Cgs (smorr) Unit: F
        Cgsmax
        % Constant portion of gate-source capacitance (smorr) Unit: F
        Cgso
        % Constant portion of gate-source capacitance (smorr) Unit: F
        Cgdo
        % Inflection point in Cgs-Vgs characteristic (smorr) Unit: V
        Vinfl
        % Cgs-Vgs transition voltage (smorr) Unit: V
        Deltgs
        % Linear to saturation region transition parameter (smorr) Unit: V
        Deltds
        % Source end channel resistance (smorr) Unit: Ohms
        Ris
        % Drain end channel resistance (smorr) Unit: Ohms
        Rid
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Gate resistance (smorr) Unit: Ohms
        Rg
        % Gate oxide breakdown voltage (warning) (s--rr) Unit: V
        wBvg
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.Gamma(obj,val)
            obj = setParameter(obj,'Gamma',val,0,'real');
        end
        function res = get.Gamma(obj)
            res = getParameter(obj,'Gamma');
        end
        function obj = set.Vgo(obj,val)
            obj = setParameter(obj,'Vgo',val,0,'real');
        end
        function res = get.Vgo(obj)
            res = getParameter(obj,'Vgo');
        end
        function obj = set.Gmmax(obj,val)
            obj = setParameter(obj,'Gmmax',val,0,'real');
        end
        function res = get.Gmmax(obj)
            res = getParameter(obj,'Gmmax');
        end
        function obj = set.Delt(obj,val)
            obj = setParameter(obj,'Delt',val,0,'real');
        end
        function res = get.Delt(obj)
            res = getParameter(obj,'Delt');
        end
        function obj = set.Vbreak(obj,val)
            obj = setParameter(obj,'Vbreak',val,0,'real');
        end
        function res = get.Vbreak(obj)
            res = getParameter(obj,'Vbreak');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Vsatm(obj,val)
            obj = setParameter(obj,'Vsatm',val,0,'real');
        end
        function res = get.Vsatm(obj)
            res = getParameter(obj,'Vsatm');
        end
        function obj = set.Vgm(obj,val)
            obj = setParameter(obj,'Vgm',val,0,'real');
        end
        function res = get.Vgm(obj)
            res = getParameter(obj,'Vgm');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Cdso(obj,val)
            obj = setParameter(obj,'Cdso',val,0,'real');
        end
        function res = get.Cdso(obj)
            res = getParameter(obj,'Cdso');
        end
        function obj = set.Vbi(obj,val)
            obj = setParameter(obj,'Vbi',val,0,'real');
        end
        function res = get.Vbi(obj)
            res = getParameter(obj,'Vbi');
        end
        function obj = set.Mj(obj,val)
            obj = setParameter(obj,'Mj',val,0,'real');
        end
        function res = get.Mj(obj)
            res = getParameter(obj,'Mj');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Kbo(obj,val)
            obj = setParameter(obj,'Kbo',val,0,'real');
        end
        function res = get.Kbo(obj)
            res = getParameter(obj,'Kbo');
        end
        function obj = set.Vbr(obj,val)
            obj = setParameter(obj,'Vbr',val,0,'real');
        end
        function res = get.Vbr(obj)
            res = getParameter(obj,'Vbr');
        end
        function obj = set.Nbr(obj,val)
            obj = setParameter(obj,'Nbr',val,0,'real');
        end
        function res = get.Nbr(obj)
            res = getParameter(obj,'Nbr');
        end
        function obj = set.Rdb(obj,val)
            obj = setParameter(obj,'Rdb',val,0,'real');
        end
        function res = get.Rdb(obj)
            res = getParameter(obj,'Rdb');
        end
        function obj = set.Cbs(obj,val)
            obj = setParameter(obj,'Cbs',val,0,'real');
        end
        function res = get.Cbs(obj)
            res = getParameter(obj,'Cbs');
        end
        function obj = set.Gdbm(obj,val)
            obj = setParameter(obj,'Gdbm',val,0,'real');
        end
        function res = get.Gdbm(obj)
            res = getParameter(obj,'Gdbm');
        end
        function obj = set.Kdb(obj,val)
            obj = setParameter(obj,'Kdb',val,0,'real');
        end
        function res = get.Kdb(obj)
            res = getParameter(obj,'Kdb');
        end
        function obj = set.Vdsm(obj,val)
            obj = setParameter(obj,'Vdsm',val,0,'real');
        end
        function res = get.Vdsm(obj)
            res = getParameter(obj,'Vdsm');
        end
        function obj = set.Vgoac(obj,val)
            obj = setParameter(obj,'Vgoac',val,0,'real');
        end
        function res = get.Vgoac(obj)
            res = getParameter(obj,'Vgoac');
        end
        function obj = set.Gmmaxac(obj,val)
            obj = setParameter(obj,'Gmmaxac',val,0,'real');
        end
        function res = get.Gmmaxac(obj)
            res = getParameter(obj,'Gmmaxac');
        end
        function obj = set.Deltac(obj,val)
            obj = setParameter(obj,'Deltac',val,0,'real');
        end
        function res = get.Deltac(obj)
            res = getParameter(obj,'Deltac');
        end
        function obj = set.Vbreakac(obj,val)
            obj = setParameter(obj,'Vbreakac',val,0,'real');
        end
        function res = get.Vbreakac(obj)
            res = getParameter(obj,'Vbreakac');
        end
        function obj = set.Lambdaac(obj,val)
            obj = setParameter(obj,'Lambdaac',val,0,'real');
        end
        function res = get.Lambdaac(obj)
            res = getParameter(obj,'Lambdaac');
        end
        function obj = set.Vsatmac(obj,val)
            obj = setParameter(obj,'Vsatmac',val,0,'real');
        end
        function res = get.Vsatmac(obj)
            res = getParameter(obj,'Vsatmac');
        end
        function obj = set.Vgmac(obj,val)
            obj = setParameter(obj,'Vgmac',val,0,'real');
        end
        function res = get.Vgmac(obj)
            res = getParameter(obj,'Vgmac');
        end
        function obj = set.Cgsmax(obj,val)
            obj = setParameter(obj,'Cgsmax',val,0,'real');
        end
        function res = get.Cgsmax(obj)
            res = getParameter(obj,'Cgsmax');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Vinfl(obj,val)
            obj = setParameter(obj,'Vinfl',val,0,'real');
        end
        function res = get.Vinfl(obj)
            res = getParameter(obj,'Vinfl');
        end
        function obj = set.Deltgs(obj,val)
            obj = setParameter(obj,'Deltgs',val,0,'real');
        end
        function res = get.Deltgs(obj)
            res = getParameter(obj,'Deltgs');
        end
        function obj = set.Deltds(obj,val)
            obj = setParameter(obj,'Deltds',val,0,'real');
        end
        function res = get.Deltds(obj)
            res = getParameter(obj,'Deltds');
        end
        function obj = set.Ris(obj,val)
            obj = setParameter(obj,'Ris',val,0,'real');
        end
        function res = get.Ris(obj)
            res = getParameter(obj,'Ris');
        end
        function obj = set.Rid(obj,val)
            obj = setParameter(obj,'Rid',val,0,'real');
        end
        function res = get.Rid(obj)
            res = getParameter(obj,'Rid');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

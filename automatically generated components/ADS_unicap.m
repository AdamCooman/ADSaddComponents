classdef ADS_unicap < ADScomponent
    % ADS_unicap matlab representation for the ADS unicap component
    % Substrate Independent Model for SMT Capacitors
    % unicap [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Nominal capacitance (smorr) Unit: F
        Cnom
        % Substrate instance name (Sm-rs) Unit: unknown
        Subst
        % Width of the land pattern (smorr) Unit: mil
        Width
        % Effective length of land pattern 1 (smorr) Unit: mil
        LenLand1
        % Effective length of land pattern 2 (smorr) Unit: mil
        LenLand2
        % Width of the transmission line that land pattern 1 is connected to (smorr) Unit: mil
        Wcl1
        % Width of the transmission line that land pattern 2 is connected to (smorr) Unit: mil
        Wcl2
        % Type of connection of land pattern 1: 'series' or 'shunt' (s--rs) 
        Conn1
        % Type of connection of land pattern 2: 'series' or 'shunt' (s--rs) 
        Conn2
        % Equivalent internal electrical length (smorr) Unit: mil
        Len_ei
        % Parasitic resistance (smorr) Unit: Ohm
        Rlead
        % First coefficient of the series inductance (smorr) Unit: pH
        Lsa
        % Second coefficient of the series inductance (smorr) Unit: pH
        Lsb
        % Third coefficient of the series inductance (smorr) Unit: mil
        Lsc
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.Cnom(obj,val)
            obj = setParameter(obj,'Cnom',val,0,'real');
        end
        function res = get.Cnom(obj)
            res = getParameter(obj,'Cnom');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.LenLand1(obj,val)
            obj = setParameter(obj,'LenLand1',val,0,'real');
        end
        function res = get.LenLand1(obj)
            res = getParameter(obj,'LenLand1');
        end
        function obj = set.LenLand2(obj,val)
            obj = setParameter(obj,'LenLand2',val,0,'real');
        end
        function res = get.LenLand2(obj)
            res = getParameter(obj,'LenLand2');
        end
        function obj = set.Wcl1(obj,val)
            obj = setParameter(obj,'Wcl1',val,0,'real');
        end
        function res = get.Wcl1(obj)
            res = getParameter(obj,'Wcl1');
        end
        function obj = set.Wcl2(obj,val)
            obj = setParameter(obj,'Wcl2',val,0,'real');
        end
        function res = get.Wcl2(obj)
            res = getParameter(obj,'Wcl2');
        end
        function obj = set.Conn1(obj,val)
            obj = setParameter(obj,'Conn1',val,0,'string');
        end
        function res = get.Conn1(obj)
            res = getParameter(obj,'Conn1');
        end
        function obj = set.Conn2(obj,val)
            obj = setParameter(obj,'Conn2',val,0,'string');
        end
        function res = get.Conn2(obj)
            res = getParameter(obj,'Conn2');
        end
        function obj = set.Len_ei(obj,val)
            obj = setParameter(obj,'Len_ei',val,0,'real');
        end
        function res = get.Len_ei(obj)
            res = getParameter(obj,'Len_ei');
        end
        function obj = set.Rlead(obj,val)
            obj = setParameter(obj,'Rlead',val,0,'real');
        end
        function res = get.Rlead(obj)
            res = getParameter(obj,'Rlead');
        end
        function obj = set.Lsa(obj,val)
            obj = setParameter(obj,'Lsa',val,0,'real');
        end
        function res = get.Lsa(obj)
            res = getParameter(obj,'Lsa');
        end
        function obj = set.Lsb(obj,val)
            obj = setParameter(obj,'Lsb',val,0,'real');
        end
        function res = get.Lsb(obj)
            res = getParameter(obj,'Lsb');
        end
        function obj = set.Lsc(obj,val)
            obj = setParameter(obj,'Lsc',val,0,'real');
        end
        function res = get.Lsc(obj)
            res = getParameter(obj,'Lsc');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

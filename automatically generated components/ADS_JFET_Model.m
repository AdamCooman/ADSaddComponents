classdef ADS_JFET_Model < ADSmodel
    % ADS_JFET_Model matlab representation for the ADS JFET_Model component
    % Junction Field Effect Transistor model
    % model ModelName JFET ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % N type JFET (s---b) 
        NFET
        % P type JFET (s---b) 
        PFET
        % Pinch-off voltage (smorr) Unit: V
        Vto
        % Transconductance parameter (smorr) Unit: A/(V*m)^2
        Beta
        % Channel length modulation parameter (smorr) Unit: V^-1
        Lambda
        % Drain resistance (smorr) Unit: Ohms
        Rd
        % Source resistance (smorr) Unit: Ohms
        Rs
        % Gate saturation current (smorr) Unit: A
        Is
        % Zero bias gate-source junction capacitance (smorr) Unit: F
        Cgs
        % Zero bias gate-drain junction capacitance (smorr) Unit: F
        Cgd
        % Gate junction potential (smorr) Unit: V
        Pb
        % Forward-bias depletion capacitance threshold (smorr) 
        Fc
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise above ambient (smorr) Unit: deg C
        Trise
        % Flicker-noise coefficient (smorr) 
        Kf
        % Flicker-noise exponent (smorr) 
        Af
        % Explosion current (smorr) Unit: A
        Imax
        % Explosion current (smorr) Unit: A
        Imelt
        % Gate P-N emission coefficient (smorr) 
        N
        % Gate P-N recombination current parameter (smorr) Unit: A
        Isr
        % ISR emission coefficient (smorr) 
        Nr
        % Ionization coefficient (smorr) Unit: V^-1
        Alpha
        % Ionization knee voltage (smorr) Unit: V
        Vk
        % Gate P-N grading coefficient (smorr) 
        M
        % VTO temperature coefficient (smorr) Unit: V/deg C
        Vtotc
        % BETA temperature coefficient (smorr) Unit: 1/deg C
        Betatce
        % Temperature coefficient (smorr) 
        Xti
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Include noise from gds (sm--b) 
        Gdsnoise
        % Gate junction forward bias (warning) (s--rr) Unit: V
        wVgfwd
        % Gate-source reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgs
        % Gate-drain reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvgd
        % Drain-source breakdown voltage (warning) (s--rr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (s--rr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.NFET(obj,val)
            obj = setParameter(obj,'NFET',val,0,'boolean');
        end
        function res = get.NFET(obj)
            res = getParameter(obj,'NFET');
        end
        function obj = set.PFET(obj,val)
            obj = setParameter(obj,'PFET',val,0,'boolean');
        end
        function res = get.PFET(obj)
            res = getParameter(obj,'PFET');
        end
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.Beta(obj,val)
            obj = setParameter(obj,'Beta',val,0,'real');
        end
        function res = get.Beta(obj)
            res = getParameter(obj,'Beta');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Cgs(obj,val)
            obj = setParameter(obj,'Cgs',val,0,'real');
        end
        function res = get.Cgs(obj)
            res = getParameter(obj,'Cgs');
        end
        function obj = set.Cgd(obj,val)
            obj = setParameter(obj,'Cgd',val,0,'real');
        end
        function res = get.Cgd(obj)
            res = getParameter(obj,'Cgd');
        end
        function obj = set.Pb(obj,val)
            obj = setParameter(obj,'Pb',val,0,'real');
        end
        function res = get.Pb(obj)
            res = getParameter(obj,'Pb');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Isr(obj,val)
            obj = setParameter(obj,'Isr',val,0,'real');
        end
        function res = get.Isr(obj)
            res = getParameter(obj,'Isr');
        end
        function obj = set.Nr(obj,val)
            obj = setParameter(obj,'Nr',val,0,'real');
        end
        function res = get.Nr(obj)
            res = getParameter(obj,'Nr');
        end
        function obj = set.Alpha(obj,val)
            obj = setParameter(obj,'Alpha',val,0,'real');
        end
        function res = get.Alpha(obj)
            res = getParameter(obj,'Alpha');
        end
        function obj = set.Vk(obj,val)
            obj = setParameter(obj,'Vk',val,0,'real');
        end
        function res = get.Vk(obj)
            res = getParameter(obj,'Vk');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Vtotc(obj,val)
            obj = setParameter(obj,'Vtotc',val,0,'real');
        end
        function res = get.Vtotc(obj)
            res = getParameter(obj,'Vtotc');
        end
        function obj = set.Betatce(obj,val)
            obj = setParameter(obj,'Betatce',val,0,'real');
        end
        function res = get.Betatce(obj)
            res = getParameter(obj,'Betatce');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Gdsnoise(obj,val)
            obj = setParameter(obj,'Gdsnoise',val,0,'boolean');
        end
        function res = get.Gdsnoise(obj)
            res = getParameter(obj,'Gdsnoise');
        end
        function obj = set.wVgfwd(obj,val)
            obj = setParameter(obj,'wVgfwd',val,0,'real');
        end
        function res = get.wVgfwd(obj)
            res = getParameter(obj,'wVgfwd');
        end
        function obj = set.wBvgs(obj,val)
            obj = setParameter(obj,'wBvgs',val,0,'real');
        end
        function res = get.wBvgs(obj)
            res = getParameter(obj,'wBvgs');
        end
        function obj = set.wBvgd(obj,val)
            obj = setParameter(obj,'wBvgd',val,0,'real');
        end
        function res = get.wBvgd(obj)
            res = getParameter(obj,'wBvgd');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

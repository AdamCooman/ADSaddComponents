classdef ADS_PCSUB_Model < ADSmodel
    % ADS_PCSUB_Model matlab representation for the ADS PCSUB_Model component
    % EEsof PC Board Substrate Parameter Definition model
    % model ModelName PCSUB ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Relative dielectric constant (smorr) 
        Er
        % Substrate thickness (smorr) Unit: m
        H
        % Cover height (smorr) Unit: m
        Hu
        % Ground plane/substrate separation (smorr) Unit: m
        Hl
        % Conductor thickness (smorr) Unit: m
        T
        % Conductor conductivity (smorr) Unit: S/m
        Cond
        % Dielectric Conductivity (smorr) Unit: S/m
        Sigma
        % Dielectric Loss Tangent (smorr) 
        TanD
        % Distance between sidewalls (smorr) 
        W
        % Number of metal layers (s---i) 
        N
        % Secured Substrate parameters (s---b) 
        Secured
        % Frequency at which Er and TanD are measured (smorr) Unit: Hz
        FreqForEpsrTanD
        % 0: Frequency independent, 1: Svensson/Djordjevic (default) (sm-ri) 
        DielectricLossModel
        % High end frequency in the Svensson/Djordjevic model (smorr) Unit: Hz
        HighFreqForTanD
        % Low end frequency in the Svensson/Djordjevic model (smorr) Unit: Hz
        LowFreqForTanD
    end
    methods
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.Hu(obj,val)
            obj = setParameter(obj,'Hu',val,0,'real');
        end
        function res = get.Hu(obj)
            res = getParameter(obj,'Hu');
        end
        function obj = set.Hl(obj,val)
            obj = setParameter(obj,'Hl',val,0,'real');
        end
        function res = get.Hl(obj)
            res = getParameter(obj,'Hl');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
        function obj = set.Cond(obj,val)
            obj = setParameter(obj,'Cond',val,0,'real');
        end
        function res = get.Cond(obj)
            res = getParameter(obj,'Cond');
        end
        function obj = set.Sigma(obj,val)
            obj = setParameter(obj,'Sigma',val,0,'real');
        end
        function res = get.Sigma(obj)
            res = getParameter(obj,'Sigma');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.FreqForEpsrTanD(obj,val)
            obj = setParameter(obj,'FreqForEpsrTanD',val,0,'real');
        end
        function res = get.FreqForEpsrTanD(obj)
            res = getParameter(obj,'FreqForEpsrTanD');
        end
        function obj = set.DielectricLossModel(obj,val)
            obj = setParameter(obj,'DielectricLossModel',val,0,'integer');
        end
        function res = get.DielectricLossModel(obj)
            res = getParameter(obj,'DielectricLossModel');
        end
        function obj = set.HighFreqForTanD(obj,val)
            obj = setParameter(obj,'HighFreqForTanD',val,0,'real');
        end
        function res = get.HighFreqForTanD(obj)
            res = getParameter(obj,'HighFreqForTanD');
        end
        function obj = set.LowFreqForTanD(obj,val)
            obj = setParameter(obj,'LowFreqForTanD',val,0,'real');
        end
        function res = get.LowFreqForTanD(obj)
            res = getParameter(obj,'LowFreqForTanD');
        end
    end
end

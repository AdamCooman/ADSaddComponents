classdef ADS_TOM3_Model < ADSmodel
    % ADS_TOM3_Model matlab representation for the ADS TOM3_Model component
    % TOM3 Field Effect Transistor model
    % model ModelName TOM3 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % N-channel type (s---b) 
        NFET
        % P-channel type (s---b) 
        PFET
        % Model temperature at which all parameters were derived (s--rr) Unit: deg C
        Tnom
        % Threshold voltage (smorr) Unit: V
        Vto
        % Saturation parameter in Ids equation (smorr) Unit: V^-1
        Alpha
        % Transconductance parameter in Ids equation (smorr) 
        Beta
        % Channel length modulation / output conductance (smorr) Unit: V^-1
        Lambda
        % Coefficient for pinch-off change with respect to Vds (smorr) 
        Gamma
        % Power generalizing the square-law for Ids current (smorr) 
        Q
        % Knee function power law coefficient (smorr) 
        K
        % Subthreshold slope voltage (smorr) Unit: V
        Vst
        % Parameter for subthreshold slope voltage dependence on Vds (smorr) Unit: V^-1
        Mst
        % Reverse leakage saturation current - diode models (smorr) Unit: A
        Ilk
        % Reverse leakage reference voltage - diode models (smorr) Unit: V
        Plk
        % Feedback coefficient for the internal VCVS (smorr) 
        Kgamma
        % Low-power gate charge nonlinear term coefficient (smorr) Unit: C
        Qgql
        % High-power gate charge nonlinear term coefficient (smorr) Unit: C
        Qgqh
        % Reference current in high-power gate charge nonlinear Ids term (smorr) Unit: A
        Qgi0
        % Low-power gate charge nonlinear term exponential coefficient (smorr) Unit: V^-1
        Qgag
        % Low-power gate charge nonlinear term Vds coefficient (smorr) Unit: V^-1
        Qgad
        % Transition coefficient for combined low-high power charge (smorr) Unit: 1/W
        Qggb
        % Low-power gate charge linear terms coefficient (smorr) Unit: F
        Qgcl
        % High-power gate charge linear Vgsi term coefficient (smorr) Unit: F
        Qgsh
        % High-power gate charge linear Vgdi term coefficient (smorr) Unit: F
        Qgdh
        % Combined low-high power additional linear terms coefficient (smorr) Unit: F
        Qgg0
        % Capacitance model: 1 - bias-dependent capacitances, 2 - charge (s--ri) 
        Capmod
        % Drain-source capacitance (smorr) Unit: F
        Cds
        % Transit time under gate (smorr) Unit: s
        Tau
        % Drain ohmic resistance (smorr) Unit: Ohms
        Rd
        % Temperature linear coefficient (relative) for Rd (smorr) Unit: K^-1
        Rdtc
        % Gate resistance (smorr) Unit: Ohms
        Rg
        % Gate metal resistance (smorr) Unit: Ohms
        Rgmet
        % Source ohmic resistance (smorr) Unit: Ohms
        Rs
        % Temperature linear coefficient (relative) for Rs (smorr) Unit: K^-1
        Rstc
        % Saturation current in forward gate diode models (smorr) Unit: A
        Is
        % Emission coefficient for gate diode models (smorr) 
        Eta
        % Temperature exponential coefficient for Alpha (smorr) Unit: K^-1
        Alphatce
        % Temperature linear coefficient for Gamma (smorr) Unit: K^-1
        Gammatc
        % Not used (placeholder for temp exp coef for Cgs) (smorr) Unit: K^-1
        Cgstce
        % Not used (placeholder for temp exp coef for Cgd) (smorr) Unit: K^-1
        Cgdtce
        % Temperature linear coefficient for Mst (smorr) Unit: V^-1/K
        Msttc
        % Temperature linear coefficient for Vst (smorr) Unit: V/K
        Vsttc
        % Temperature linear coefficient for Vto (smorr) Unit: V/K
        Vtotc
        % Temperature exponential coefficient for Beta (smorr) Unit: K^-1
        Betatce
        % Series Ctau-Rtau time constant (implicit definition of Rtau) (smorr) Unit: s
        Taugd
        % Dispersion model capacitance (smorr) Unit: F
        Ctau
        % Temperature exponent for saturation current (smorr) 
        Xti
        % Energy gap for temperature effect on Is (smorr) Unit: eV
        Eg
        % Explosion current (smorr) Unit: A
        Imax
        % For foundry use (sheet-resistance scaling factor for Rd and Rs) (smorr) 
        kRsh
        % For foundry use (scaling factor for Is) (smorr) 
        kIs
        % Drain inductance - currently not used (smorr) Unit: H
        Ld
        % Gate inductance - currently not used (smorr) Unit: H
        Lg
        % Source inductance - currently not used (smorr) Unit: H
        Ls
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
        % Flicker noise frequency exponent (smorr) 
        Ffe
        % Flicker noise corner frequency (smorr) Unit: Hz
        Fnc
        % Gate noise coefficient (smorr) 
        R
        % Drain noise coefficient (smorr) 
        P
        % Gate-drain noise correlation coefficient (smorr) 
        C
        % Gate width to which model params are normalized (s--rr) Unit: m
        Ugw
        % No. of gate fingers to which model params are normalized (s--rr) 
        Ngf
        % Gate junction forward bias warning (s---r) Unit: V
        wVgfwd
        % Gate-source reverse breakdown voltage warning (s---r) Unit: V
        wBvgs
        % Gate-drain reverse breakdown voltage warning (s---r) Unit: V
        wBvgd
        % Drain-source breakdown voltage warning (s---r) Unit: V
        wBvds
        % Maximum drain-source current warning (s---r) Unit: A
        wIdsmax
        % Maximum power dissipation warning (s---r) Unit: W
        wPmax
        % Explosion current - currently not used (smorr) Unit: A
        Imelt
        % Secured model parameters (s---b) 
        Secured
    end
    methods
        function obj = set.NFET(obj,val)
            obj = setParameter(obj,'NFET',val,0,'boolean');
        end
        function res = get.NFET(obj)
            res = getParameter(obj,'NFET');
        end
        function obj = set.PFET(obj,val)
            obj = setParameter(obj,'PFET',val,0,'boolean');
        end
        function res = get.PFET(obj)
            res = getParameter(obj,'PFET');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Vto(obj,val)
            obj = setParameter(obj,'Vto',val,0,'real');
        end
        function res = get.Vto(obj)
            res = getParameter(obj,'Vto');
        end
        function obj = set.Alpha(obj,val)
            obj = setParameter(obj,'Alpha',val,0,'real');
        end
        function res = get.Alpha(obj)
            res = getParameter(obj,'Alpha');
        end
        function obj = set.Beta(obj,val)
            obj = setParameter(obj,'Beta',val,0,'real');
        end
        function res = get.Beta(obj)
            res = getParameter(obj,'Beta');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Gamma(obj,val)
            obj = setParameter(obj,'Gamma',val,0,'real');
        end
        function res = get.Gamma(obj)
            res = getParameter(obj,'Gamma');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.K(obj,val)
            obj = setParameter(obj,'K',val,0,'real');
        end
        function res = get.K(obj)
            res = getParameter(obj,'K');
        end
        function obj = set.Vst(obj,val)
            obj = setParameter(obj,'Vst',val,0,'real');
        end
        function res = get.Vst(obj)
            res = getParameter(obj,'Vst');
        end
        function obj = set.Mst(obj,val)
            obj = setParameter(obj,'Mst',val,0,'real');
        end
        function res = get.Mst(obj)
            res = getParameter(obj,'Mst');
        end
        function obj = set.Ilk(obj,val)
            obj = setParameter(obj,'Ilk',val,0,'real');
        end
        function res = get.Ilk(obj)
            res = getParameter(obj,'Ilk');
        end
        function obj = set.Plk(obj,val)
            obj = setParameter(obj,'Plk',val,0,'real');
        end
        function res = get.Plk(obj)
            res = getParameter(obj,'Plk');
        end
        function obj = set.Kgamma(obj,val)
            obj = setParameter(obj,'Kgamma',val,0,'real');
        end
        function res = get.Kgamma(obj)
            res = getParameter(obj,'Kgamma');
        end
        function obj = set.Qgql(obj,val)
            obj = setParameter(obj,'Qgql',val,0,'real');
        end
        function res = get.Qgql(obj)
            res = getParameter(obj,'Qgql');
        end
        function obj = set.Qgqh(obj,val)
            obj = setParameter(obj,'Qgqh',val,0,'real');
        end
        function res = get.Qgqh(obj)
            res = getParameter(obj,'Qgqh');
        end
        function obj = set.Qgi0(obj,val)
            obj = setParameter(obj,'Qgi0',val,0,'real');
        end
        function res = get.Qgi0(obj)
            res = getParameter(obj,'Qgi0');
        end
        function obj = set.Qgag(obj,val)
            obj = setParameter(obj,'Qgag',val,0,'real');
        end
        function res = get.Qgag(obj)
            res = getParameter(obj,'Qgag');
        end
        function obj = set.Qgad(obj,val)
            obj = setParameter(obj,'Qgad',val,0,'real');
        end
        function res = get.Qgad(obj)
            res = getParameter(obj,'Qgad');
        end
        function obj = set.Qggb(obj,val)
            obj = setParameter(obj,'Qggb',val,0,'real');
        end
        function res = get.Qggb(obj)
            res = getParameter(obj,'Qggb');
        end
        function obj = set.Qgcl(obj,val)
            obj = setParameter(obj,'Qgcl',val,0,'real');
        end
        function res = get.Qgcl(obj)
            res = getParameter(obj,'Qgcl');
        end
        function obj = set.Qgsh(obj,val)
            obj = setParameter(obj,'Qgsh',val,0,'real');
        end
        function res = get.Qgsh(obj)
            res = getParameter(obj,'Qgsh');
        end
        function obj = set.Qgdh(obj,val)
            obj = setParameter(obj,'Qgdh',val,0,'real');
        end
        function res = get.Qgdh(obj)
            res = getParameter(obj,'Qgdh');
        end
        function obj = set.Qgg0(obj,val)
            obj = setParameter(obj,'Qgg0',val,0,'real');
        end
        function res = get.Qgg0(obj)
            res = getParameter(obj,'Qgg0');
        end
        function obj = set.Capmod(obj,val)
            obj = setParameter(obj,'Capmod',val,0,'integer');
        end
        function res = get.Capmod(obj)
            res = getParameter(obj,'Capmod');
        end
        function obj = set.Cds(obj,val)
            obj = setParameter(obj,'Cds',val,0,'real');
        end
        function res = get.Cds(obj)
            res = getParameter(obj,'Cds');
        end
        function obj = set.Tau(obj,val)
            obj = setParameter(obj,'Tau',val,0,'real');
        end
        function res = get.Tau(obj)
            res = getParameter(obj,'Tau');
        end
        function obj = set.Rd(obj,val)
            obj = setParameter(obj,'Rd',val,0,'real');
        end
        function res = get.Rd(obj)
            res = getParameter(obj,'Rd');
        end
        function obj = set.Rdtc(obj,val)
            obj = setParameter(obj,'Rdtc',val,0,'real');
        end
        function res = get.Rdtc(obj)
            res = getParameter(obj,'Rdtc');
        end
        function obj = set.Rg(obj,val)
            obj = setParameter(obj,'Rg',val,0,'real');
        end
        function res = get.Rg(obj)
            res = getParameter(obj,'Rg');
        end
        function obj = set.Rgmet(obj,val)
            obj = setParameter(obj,'Rgmet',val,0,'real');
        end
        function res = get.Rgmet(obj)
            res = getParameter(obj,'Rgmet');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Rstc(obj,val)
            obj = setParameter(obj,'Rstc',val,0,'real');
        end
        function res = get.Rstc(obj)
            res = getParameter(obj,'Rstc');
        end
        function obj = set.Is(obj,val)
            obj = setParameter(obj,'Is',val,0,'real');
        end
        function res = get.Is(obj)
            res = getParameter(obj,'Is');
        end
        function obj = set.Eta(obj,val)
            obj = setParameter(obj,'Eta',val,0,'real');
        end
        function res = get.Eta(obj)
            res = getParameter(obj,'Eta');
        end
        function obj = set.Alphatce(obj,val)
            obj = setParameter(obj,'Alphatce',val,0,'real');
        end
        function res = get.Alphatce(obj)
            res = getParameter(obj,'Alphatce');
        end
        function obj = set.Gammatc(obj,val)
            obj = setParameter(obj,'Gammatc',val,0,'real');
        end
        function res = get.Gammatc(obj)
            res = getParameter(obj,'Gammatc');
        end
        function obj = set.Cgstce(obj,val)
            obj = setParameter(obj,'Cgstce',val,0,'real');
        end
        function res = get.Cgstce(obj)
            res = getParameter(obj,'Cgstce');
        end
        function obj = set.Cgdtce(obj,val)
            obj = setParameter(obj,'Cgdtce',val,0,'real');
        end
        function res = get.Cgdtce(obj)
            res = getParameter(obj,'Cgdtce');
        end
        function obj = set.Msttc(obj,val)
            obj = setParameter(obj,'Msttc',val,0,'real');
        end
        function res = get.Msttc(obj)
            res = getParameter(obj,'Msttc');
        end
        function obj = set.Vsttc(obj,val)
            obj = setParameter(obj,'Vsttc',val,0,'real');
        end
        function res = get.Vsttc(obj)
            res = getParameter(obj,'Vsttc');
        end
        function obj = set.Vtotc(obj,val)
            obj = setParameter(obj,'Vtotc',val,0,'real');
        end
        function res = get.Vtotc(obj)
            res = getParameter(obj,'Vtotc');
        end
        function obj = set.Betatce(obj,val)
            obj = setParameter(obj,'Betatce',val,0,'real');
        end
        function res = get.Betatce(obj)
            res = getParameter(obj,'Betatce');
        end
        function obj = set.Taugd(obj,val)
            obj = setParameter(obj,'Taugd',val,0,'real');
        end
        function res = get.Taugd(obj)
            res = getParameter(obj,'Taugd');
        end
        function obj = set.Ctau(obj,val)
            obj = setParameter(obj,'Ctau',val,0,'real');
        end
        function res = get.Ctau(obj)
            res = getParameter(obj,'Ctau');
        end
        function obj = set.Xti(obj,val)
            obj = setParameter(obj,'Xti',val,0,'real');
        end
        function res = get.Xti(obj)
            res = getParameter(obj,'Xti');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.kRsh(obj,val)
            obj = setParameter(obj,'kRsh',val,0,'real');
        end
        function res = get.kRsh(obj)
            res = getParameter(obj,'kRsh');
        end
        function obj = set.kIs(obj,val)
            obj = setParameter(obj,'kIs',val,0,'real');
        end
        function res = get.kIs(obj)
            res = getParameter(obj,'kIs');
        end
        function obj = set.Ld(obj,val)
            obj = setParameter(obj,'Ld',val,0,'real');
        end
        function res = get.Ld(obj)
            res = getParameter(obj,'Ld');
        end
        function obj = set.Lg(obj,val)
            obj = setParameter(obj,'Lg',val,0,'real');
        end
        function res = get.Lg(obj)
            res = getParameter(obj,'Lg');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Ffe(obj,val)
            obj = setParameter(obj,'Ffe',val,0,'real');
        end
        function res = get.Ffe(obj)
            res = getParameter(obj,'Ffe');
        end
        function obj = set.Fnc(obj,val)
            obj = setParameter(obj,'Fnc',val,0,'real');
        end
        function res = get.Fnc(obj)
            res = getParameter(obj,'Fnc');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.P(obj,val)
            obj = setParameter(obj,'P',val,0,'real');
        end
        function res = get.P(obj)
            res = getParameter(obj,'P');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Ugw(obj,val)
            obj = setParameter(obj,'Ugw',val,0,'real');
        end
        function res = get.Ugw(obj)
            res = getParameter(obj,'Ugw');
        end
        function obj = set.Ngf(obj,val)
            obj = setParameter(obj,'Ngf',val,0,'real');
        end
        function res = get.Ngf(obj)
            res = getParameter(obj,'Ngf');
        end
        function obj = set.wVgfwd(obj,val)
            obj = setParameter(obj,'wVgfwd',val,0,'real');
        end
        function res = get.wVgfwd(obj)
            res = getParameter(obj,'wVgfwd');
        end
        function obj = set.wBvgs(obj,val)
            obj = setParameter(obj,'wBvgs',val,0,'real');
        end
        function res = get.wBvgs(obj)
            res = getParameter(obj,'wBvgs');
        end
        function obj = set.wBvgd(obj,val)
            obj = setParameter(obj,'wBvgd',val,0,'real');
        end
        function res = get.wBvgd(obj)
            res = getParameter(obj,'wBvgd');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

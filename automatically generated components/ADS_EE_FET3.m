classdef ADS_EE_FET3 < ADScomponent
    % ADS_EE_FET3 matlab representation for the ADS EE_FET3 component
    % EESOF Scalable Transistor Model
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Device unit gate width (smorr) Unit: m
        Ugw
        % Number of device gate fingers (smorr) 
        N
        % Noise generation on/off (s---b) 
        Noise
        % Ambient device temperature (smorr) Unit: deg C
        Temp
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Small signal gate to source conductance (---rr) Unit: S
        Ggs
        % Small signal gate to drain conductance (---rr) Unit: S
        Ggd
        % Small signal drain to source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vgs to Igd transconductance (---rr) Unit: S
        dIgd_dVgs
        % Small signal Vgd to Ids transconductance (---rr) Unit: S
        dIds_dVgd
        % Small signal Vgs to Idb transconductance (---rr) Unit: S
        dIdb_dVgs
        % Small signal Vgd to Idb transconductance (---rr) Unit: S
        dIdb_dVgd
        % Small signal Vds to Idb transconductance (---rr) Unit: S
        dIdb_dVds
        % Small signal gate source capacitance (---rr) Unit: F
        Cgc
        % Small signal Vgy to Qgc transcapacitance (---rr) Unit: F
        Cgcgy
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgy
        % Small signal Vgc to Qgy transcapacitance (---rr) Unit: F
        Cgygc
    end
    methods
        function obj = set.Ugw(obj,val)
            obj = setParameter(obj,'Ugw',val,0,'real');
        end
        function res = get.Ugw(obj)
            res = getParameter(obj,'Ugw');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Ggs(obj,val)
            obj = setParameter(obj,'Ggs',val,0,'real');
        end
        function res = get.Ggs(obj)
            res = getParameter(obj,'Ggs');
        end
        function obj = set.Ggd(obj,val)
            obj = setParameter(obj,'Ggd',val,0,'real');
        end
        function res = get.Ggd(obj)
            res = getParameter(obj,'Ggd');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.dIgd_dVgs(obj,val)
            obj = setParameter(obj,'dIgd_dVgs',val,0,'real');
        end
        function res = get.dIgd_dVgs(obj)
            res = getParameter(obj,'dIgd_dVgs');
        end
        function obj = set.dIds_dVgd(obj,val)
            obj = setParameter(obj,'dIds_dVgd',val,0,'real');
        end
        function res = get.dIds_dVgd(obj)
            res = getParameter(obj,'dIds_dVgd');
        end
        function obj = set.dIdb_dVgs(obj,val)
            obj = setParameter(obj,'dIdb_dVgs',val,0,'real');
        end
        function res = get.dIdb_dVgs(obj)
            res = getParameter(obj,'dIdb_dVgs');
        end
        function obj = set.dIdb_dVgd(obj,val)
            obj = setParameter(obj,'dIdb_dVgd',val,0,'real');
        end
        function res = get.dIdb_dVgd(obj)
            res = getParameter(obj,'dIdb_dVgd');
        end
        function obj = set.dIdb_dVds(obj,val)
            obj = setParameter(obj,'dIdb_dVds',val,0,'real');
        end
        function res = get.dIdb_dVds(obj)
            res = getParameter(obj,'dIdb_dVds');
        end
        function obj = set.Cgc(obj,val)
            obj = setParameter(obj,'Cgc',val,0,'real');
        end
        function res = get.Cgc(obj)
            res = getParameter(obj,'Cgc');
        end
        function obj = set.Cgcgy(obj,val)
            obj = setParameter(obj,'Cgcgy',val,0,'real');
        end
        function res = get.Cgcgy(obj)
            res = getParameter(obj,'Cgcgy');
        end
        function obj = set.Cgy(obj,val)
            obj = setParameter(obj,'Cgy',val,0,'real');
        end
        function res = get.Cgy(obj)
            res = getParameter(obj,'Cgy');
        end
        function obj = set.Cgygc(obj,val)
            obj = setParameter(obj,'Cgygc',val,0,'real');
        end
        function res = get.Cgygc(obj)
            res = getParameter(obj,'Cgygc');
        end
    end
end

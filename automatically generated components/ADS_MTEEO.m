classdef ADS_MTEEO < ADScomponent
    % ADS_MTEEO matlab representation for the ADS MTEEO component
    % Libra Microstrip T-Junction
    % MTEEO [:Name] n1 n2 n3
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Conductor Width at pin 1 (Smorr) Unit: m
        W1
        % Conductor Width at pin 2 (Smorr) Unit: m
        W2
        % Conductor Width at pin 3 (Smorr) Unit: m
        W3
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_MSSVIA < ADScomponent
    % ADS_MSSVIA matlab representation for the ADS MSSVIA component
    % Microstrip Via, Thru
    % MSSVIA [:Name] top bottom ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Outside Diameter (smorr) Unit: m
        OD
        % Conductor Width (smorr) Unit: m
        W
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.OD(obj,val)
            obj = setParameter(obj,'OD',val,0,'real');
        end
        function res = get.OD(obj)
            res = getParameter(obj,'OD');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
    end
end

classdef ADS_ADS_FET < ADScomponent
    % ADS_ADS_FET matlab representation for the ADS ADS_FET component
    % ADS-Root field effect transistor
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Total device gate width (smorr) Unit: m
        Wtot
        % Number of device gate fingers (sm-ri) 
        N
        % Small signal gate to source conductance (---rr) Unit: S
        Ggs
        % Small signal drain to source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vds to Ig transconductance (---rr) Unit: S
        Gmr
        % Small signal Vds to Qg transcapacitance (---rr) 
        dQg_dVds
        % Small signal Vgs to Qg capacitance (---rr) 
        dQg_dVgs
        % Small signal Vds to Qd capacitance (---rr) 
        dQd_dVds
        % Small signal Vgs to Qd transcapacitance (---rr) 
        dQd_dVgs
    end
    methods
        function obj = set.Wtot(obj,val)
            obj = setParameter(obj,'Wtot',val,0,'real');
        end
        function res = get.Wtot(obj)
            res = getParameter(obj,'Wtot');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Ggs(obj,val)
            obj = setParameter(obj,'Ggs',val,0,'real');
        end
        function res = get.Ggs(obj)
            res = getParameter(obj,'Ggs');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.dQg_dVds(obj,val)
            obj = setParameter(obj,'dQg_dVds',val,0,'real');
        end
        function res = get.dQg_dVds(obj)
            res = getParameter(obj,'dQg_dVds');
        end
        function obj = set.dQg_dVgs(obj,val)
            obj = setParameter(obj,'dQg_dVgs',val,0,'real');
        end
        function res = get.dQg_dVgs(obj)
            res = getParameter(obj,'dQg_dVgs');
        end
        function obj = set.dQd_dVds(obj,val)
            obj = setParameter(obj,'dQd_dVds',val,0,'real');
        end
        function res = get.dQd_dVds(obj)
            res = getParameter(obj,'dQd_dVds');
        end
        function obj = set.dQd_dVgs(obj,val)
            obj = setParameter(obj,'dQd_dVgs',val,0,'real');
        end
        function res = get.dQd_dVgs(obj)
            res = getParameter(obj,'dQd_dVgs');
        end
    end
end

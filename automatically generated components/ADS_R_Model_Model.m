classdef ADS_R_Model_Model < ADSmodel
    % ADS_R_Model_Model matlab representation for the ADS R_Model_Model component
    % Linear Two Terminal Resistor with Model model
    % model ModelName R_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Resistance (smorr) Unit: Ohms
        R
        % Sheet Resistance (smorr) Unit: Ohms/square
        Rsh
        % Default Length (smorr) Unit: m
        Length
        % Default Width (smorr) Unit: m
        Width
        % Length and width narrowing due to etching (smorr) Unit: m
        Narrow
        % Width narrowing due to etching (smorr) Unit: m
        Dw
        % Length narrowing due to etching (smorr) Unit: m
        Dl
        % Nominal temperature of resistor in degrees Celsius (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Maximum current (warning) (s--rr) Unit: A/m
        wImax
        % Resistance scaling factor (smorr) 
        Scale
        % Flicker noise coefficient (smorr) 
        Kf
        % Flicker noise current exponent (smorr) 
        Af
        % Flicker noise W exponent (smorr) 
        Wdexp
        % Flicker noise L exponent (smorr) 
        Ldexp
        % Flicker noise Weff exponent (smorr) 
        Weexp
        % Flicker noise Leff exponent (smorr) 
        Leexp
        % Flicker noise frequency exponent (smorr) 
        Fexp
        % Nonlinear resistor polynomial coeffs, syntax: Coeffs=list(c1,c2,...) (smorr) 
        Coeffs
        % Form of nonlinear resistance, 0=g, 1=r (s--ri) 
        Nonlinform
        % Symmetric nonlinear resistance model, 0=none, 1=absolute (s--ri) 
        Symmetric
        % Shrink Factor for Length and Width (smorr) 
        Shrink
        % Default reference node for the parasitic capacitances (s---s) 
        Bulk
        % Default parasitic capacitance (smorr) Unit: F
        Cap
        % Sidewall fringe capacitance (smorr) Unit: F/m
        Capsw
        % Bottom-Wall capacitance (smorr) Unit: F/m^2
        Cox
        % Relative dielectric constant (smorr) 
        Di
        % First order temperature coefficient for capacitance (smorr) Unit: 1/deg C
        Tc1c
        % Second order temperature coefficient for capacitance (smorr) Unit: 1/(deg C)^2
        Tc2c
        % Dielectric thickness (smorr) Unit: m
        Thick
        % Ratio to allcate parasitic capacitance (smorr) 
        Cratio
        % Flag to enable/disable resistor hspice compatibility (s--rb) 
        Hspice
        % Maximum operating voltage (TSMC SOA warning) (smorr) Unit: V
        Bv_max
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Rsh(obj,val)
            obj = setParameter(obj,'Rsh',val,0,'real');
        end
        function res = get.Rsh(obj)
            res = getParameter(obj,'Rsh');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Narrow(obj,val)
            obj = setParameter(obj,'Narrow',val,0,'real');
        end
        function res = get.Narrow(obj)
            res = getParameter(obj,'Narrow');
        end
        function obj = set.Dw(obj,val)
            obj = setParameter(obj,'Dw',val,0,'real');
        end
        function res = get.Dw(obj)
            res = getParameter(obj,'Dw');
        end
        function obj = set.Dl(obj,val)
            obj = setParameter(obj,'Dl',val,0,'real');
        end
        function res = get.Dl(obj)
            res = getParameter(obj,'Dl');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.wImax(obj,val)
            obj = setParameter(obj,'wImax',val,0,'real');
        end
        function res = get.wImax(obj)
            res = getParameter(obj,'wImax');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Wdexp(obj,val)
            obj = setParameter(obj,'Wdexp',val,0,'real');
        end
        function res = get.Wdexp(obj)
            res = getParameter(obj,'Wdexp');
        end
        function obj = set.Ldexp(obj,val)
            obj = setParameter(obj,'Ldexp',val,0,'real');
        end
        function res = get.Ldexp(obj)
            res = getParameter(obj,'Ldexp');
        end
        function obj = set.Weexp(obj,val)
            obj = setParameter(obj,'Weexp',val,0,'real');
        end
        function res = get.Weexp(obj)
            res = getParameter(obj,'Weexp');
        end
        function obj = set.Leexp(obj,val)
            obj = setParameter(obj,'Leexp',val,0,'real');
        end
        function res = get.Leexp(obj)
            res = getParameter(obj,'Leexp');
        end
        function obj = set.Fexp(obj,val)
            obj = setParameter(obj,'Fexp',val,0,'real');
        end
        function res = get.Fexp(obj)
            res = getParameter(obj,'Fexp');
        end
        function obj = set.Coeffs(obj,val)
            obj = setParameter(obj,'Coeffs',val,0,'real');
        end
        function res = get.Coeffs(obj)
            res = getParameter(obj,'Coeffs');
        end
        function obj = set.Nonlinform(obj,val)
            obj = setParameter(obj,'Nonlinform',val,0,'integer');
        end
        function res = get.Nonlinform(obj)
            res = getParameter(obj,'Nonlinform');
        end
        function obj = set.Symmetric(obj,val)
            obj = setParameter(obj,'Symmetric',val,0,'integer');
        end
        function res = get.Symmetric(obj)
            res = getParameter(obj,'Symmetric');
        end
        function obj = set.Shrink(obj,val)
            obj = setParameter(obj,'Shrink',val,0,'real');
        end
        function res = get.Shrink(obj)
            res = getParameter(obj,'Shrink');
        end
        function obj = set.Bulk(obj,val)
            obj = setParameter(obj,'Bulk',val,0,'string');
        end
        function res = get.Bulk(obj)
            res = getParameter(obj,'Bulk');
        end
        function obj = set.Cap(obj,val)
            obj = setParameter(obj,'Cap',val,0,'real');
        end
        function res = get.Cap(obj)
            res = getParameter(obj,'Cap');
        end
        function obj = set.Capsw(obj,val)
            obj = setParameter(obj,'Capsw',val,0,'real');
        end
        function res = get.Capsw(obj)
            res = getParameter(obj,'Capsw');
        end
        function obj = set.Cox(obj,val)
            obj = setParameter(obj,'Cox',val,0,'real');
        end
        function res = get.Cox(obj)
            res = getParameter(obj,'Cox');
        end
        function obj = set.Di(obj,val)
            obj = setParameter(obj,'Di',val,0,'real');
        end
        function res = get.Di(obj)
            res = getParameter(obj,'Di');
        end
        function obj = set.Tc1c(obj,val)
            obj = setParameter(obj,'Tc1c',val,0,'real');
        end
        function res = get.Tc1c(obj)
            res = getParameter(obj,'Tc1c');
        end
        function obj = set.Tc2c(obj,val)
            obj = setParameter(obj,'Tc2c',val,0,'real');
        end
        function res = get.Tc2c(obj)
            res = getParameter(obj,'Tc2c');
        end
        function obj = set.Thick(obj,val)
            obj = setParameter(obj,'Thick',val,0,'real');
        end
        function res = get.Thick(obj)
            res = getParameter(obj,'Thick');
        end
        function obj = set.Cratio(obj,val)
            obj = setParameter(obj,'Cratio',val,0,'real');
        end
        function res = get.Cratio(obj)
            res = getParameter(obj,'Cratio');
        end
        function obj = set.Hspice(obj,val)
            obj = setParameter(obj,'Hspice',val,0,'boolean');
        end
        function res = get.Hspice(obj)
            res = getParameter(obj,'Hspice');
        end
        function obj = set.Bv_max(obj,val)
            obj = setParameter(obj,'Bv_max',val,0,'real');
        end
        function res = get.Bv_max(obj)
            res = getParameter(obj,'Bv_max');
        end
    end
end

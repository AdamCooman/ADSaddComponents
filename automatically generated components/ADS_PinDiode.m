classdef ADS_PinDiode < ADScomponent
    % ADS_PinDiode matlab representation for the ADS PinDiode component
    % P-I-N Diode
    % ModelName [:Name] anode cathode
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Junction area (smorr) 
        Area
        % DC operating region, 0=off, 1=on (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Nonlinear spectral model on/off (s--ri) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal conductance (---rr) Unit: S
        Gd
        % Small signal capacitance (---rr) Unit: F
        Cd
    end
    methods
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gd(obj,val)
            obj = setParameter(obj,'Gd',val,0,'real');
        end
        function res = get.Gd(obj)
            res = getParameter(obj,'Gd');
        end
        function obj = set.Cd(obj,val)
            obj = setParameter(obj,'Cd',val,0,'real');
        end
        function res = get.Cd(obj)
            res = getParameter(obj,'Cd');
        end
    end
end

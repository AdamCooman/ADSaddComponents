classdef ADS_ggvmeter < ADScomponent
    % ADS_ggvmeter matlab representation for the ADS ggvmeter component
    % GG voltage meter
    % ggvmeter [:Name] plus minus
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Alias (s--ri) 
        Alias
        % Save all currents (s--ri) 
        SaveAll
        % Mode (s--ri) 
        Mode
    end
    methods
        function obj = set.Alias(obj,val)
            obj = setParameter(obj,'Alias',val,0,'integer');
        end
        function res = get.Alias(obj)
            res = getParameter(obj,'Alias');
        end
        function obj = set.SaveAll(obj,val)
            obj = setParameter(obj,'SaveAll',val,0,'integer');
        end
        function res = get.SaveAll(obj)
            res = getParameter(obj,'SaveAll');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
    end
end

classdef ADS_INDQ < ADScomponent
    % ADS_INDQ matlab representation for the ADS INDQ component
    % Lossy Inductor with Quality Factor Mode 1, 2, or 3
    % INDQ [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % inductance (Smorr) Unit: H
        L
        % quality factor (Smorr) Unit: 1
        Q
        % frequency at which q is given (Smorr) Unit: Hz
        F
        % frequency depending mode of q (Sm-ri) Unit: 1
        Mode
        % DC resistance for modes 2 and 3 (smorr) Unit: Ohm
        Rdc
        % Physical Temperature (smorr) Unit: C
        Temp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,0,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

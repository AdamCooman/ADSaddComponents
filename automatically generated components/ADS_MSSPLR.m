classdef ADS_MSSPLR < ADScomponent
    % ADS_MSSPLR matlab representation for the ADS MSSPLR component
    % Microstrip Spiral Inductor (Round)
    % MSSPLR [:Name] outer inner ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Gap (smorr) Unit: m
        S
        % Outer Radius (smorr) Unit: m
        Ro
        % Number of Turns (smorr) 
        N
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Ro(obj,val)
            obj = setParameter(obj,'Ro',val,0,'real');
        end
        function res = get.Ro(obj)
            res = getParameter(obj,'Ro');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
    end
end

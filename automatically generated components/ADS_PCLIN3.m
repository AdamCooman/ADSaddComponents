classdef ADS_PCLIN3 < ADScomponent
    % ADS_PCLIN3 matlab representation for the ADS PCLIN3 component
    % 3 Printed Circuit Coupled Lines
    % PCLIN3 [:Name] n1 n2 n3 n4 n5 n6
    properties (Access=protected)
        NumberOfNodes = 6
    end
    properties (Dependent)
        % Width of line #1 (Smorr) Unit: m
        W1
        % Distance from line #1 to left wall (Smorr) Unit: m
        S1
        % Conductor layer number - line #1 (Sm-ri) Unit: unknown
        CLayer1
        % Width of line #2 (Smorr) Unit: m
        W2
        % Distance from line #2 to left wall (Smorr) Unit: m
        S2
        % Conductor layer number - line #2 (Sm-ri) Unit: unknown
        CLayer2
        % Width of line #3 (Smorr) Unit: m
        W3
        % Distance from line #3 to left wall (Smorr) Unit: m
        S3
        % Conductor layer number - line #3 (Sm-ri) Unit: unknown
        CLayer3
        % Length of the lines (Smorr) Unit: m
        L
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
        % Factor to refine the background grid (Sm-ri) Unit: unknown
        Refine_grid_factor
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.CLayer1(obj,val)
            obj = setParameter(obj,'CLayer1',val,0,'integer');
        end
        function res = get.CLayer1(obj)
            res = getParameter(obj,'CLayer1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.S2(obj,val)
            obj = setParameter(obj,'S2',val,0,'real');
        end
        function res = get.S2(obj)
            res = getParameter(obj,'S2');
        end
        function obj = set.CLayer2(obj,val)
            obj = setParameter(obj,'CLayer2',val,0,'integer');
        end
        function res = get.CLayer2(obj)
            res = getParameter(obj,'CLayer2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.S3(obj,val)
            obj = setParameter(obj,'S3',val,0,'real');
        end
        function res = get.S3(obj)
            res = getParameter(obj,'S3');
        end
        function obj = set.CLayer3(obj,val)
            obj = setParameter(obj,'CLayer3',val,0,'integer');
        end
        function res = get.CLayer3(obj)
            res = getParameter(obj,'CLayer3');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Refine_grid_factor(obj,val)
            obj = setParameter(obj,'Refine_grid_factor',val,0,'integer');
        end
        function res = get.Refine_grid_factor(obj)
            res = getParameter(obj,'Refine_grid_factor');
        end
    end
end

classdef ADS_InoiseBD < ADScomponent
    % ADS_InoiseBD matlab representation for the ADS InoiseBD component
    % Bias-dependent Noise Current Source
    % InoiseBD [:Name] p n
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Multiplicative constant (smorr) 
        K
        % DC bias current exponent (smorr) 
        Ie
        % Additive constant in the denominator (smorr) 
        A0
        % Multiplication factor for the frequency (smorr) 
        A1
        % Frequency exponent (smorr) 
        Fe
        % Controlling current (s---d) 
        Elem
        % Element pin number or name (s---d) 
        Pin
    end
    methods
        function obj = set.K(obj,val)
            obj = setParameter(obj,'K',val,0,'real');
        end
        function res = get.K(obj)
            res = getParameter(obj,'K');
        end
        function obj = set.Ie(obj,val)
            obj = setParameter(obj,'Ie',val,0,'real');
        end
        function res = get.Ie(obj)
            res = getParameter(obj,'Ie');
        end
        function obj = set.A0(obj,val)
            obj = setParameter(obj,'A0',val,0,'real');
        end
        function res = get.A0(obj)
            res = getParameter(obj,'A0');
        end
        function obj = set.A1(obj,val)
            obj = setParameter(obj,'A1',val,0,'real');
        end
        function res = get.A1(obj)
            res = getParameter(obj,'A1');
        end
        function obj = set.Fe(obj,val)
            obj = setParameter(obj,'Fe',val,0,'real');
        end
        function res = get.Fe(obj)
            res = getParameter(obj,'Fe');
        end
        function obj = set.Elem(obj,val)
            obj = setParameter(obj,'Elem',val,0,'string');
        end
        function res = get.Elem(obj)
            res = getParameter(obj,'Elem');
        end
        function obj = set.Pin(obj,val)
            obj = setParameter(obj,'Pin',val,0,'string');
        end
        function res = get.Pin(obj)
            res = getParameter(obj,'Pin');
        end
    end
end

classdef ADS_EE_MOS1 < ADScomponent
    % ADS_EE_MOS1 matlab representation for the ADS EE_MOS1 component
    % EESOF Mos Transistor Model
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Noise generation on/off (s---b) 
        Noise
        % Small signal drain to source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vgd to Ids transconductance (---rr) Unit: S
        dIds_dVgd
        % Small signal Vgs to Idb transconductance (---rr) Unit: S
        dIdb_dVgs
        % Small signal Vgd to Idb transconductance (---rr) Unit: S
        dIdb_dVgd
        % Small signal Vds to Idb transconductance (---rr) Unit: S
        dIdb_dVds
        % Small signal gate source capacitance (---rr) Unit: F
        Cgc
        % Small signal Vgy to Qgc transcapacitance (---rr) Unit: F
        Cgcgy
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgy
        % Small signal Vgc to Qgy transcapacitance (---rr) Unit: F
        Cgygc
    end
    methods
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.dIds_dVgd(obj,val)
            obj = setParameter(obj,'dIds_dVgd',val,0,'real');
        end
        function res = get.dIds_dVgd(obj)
            res = getParameter(obj,'dIds_dVgd');
        end
        function obj = set.dIdb_dVgs(obj,val)
            obj = setParameter(obj,'dIdb_dVgs',val,0,'real');
        end
        function res = get.dIdb_dVgs(obj)
            res = getParameter(obj,'dIdb_dVgs');
        end
        function obj = set.dIdb_dVgd(obj,val)
            obj = setParameter(obj,'dIdb_dVgd',val,0,'real');
        end
        function res = get.dIdb_dVgd(obj)
            res = getParameter(obj,'dIdb_dVgd');
        end
        function obj = set.dIdb_dVds(obj,val)
            obj = setParameter(obj,'dIdb_dVds',val,0,'real');
        end
        function res = get.dIdb_dVds(obj)
            res = getParameter(obj,'dIdb_dVds');
        end
        function obj = set.Cgc(obj,val)
            obj = setParameter(obj,'Cgc',val,0,'real');
        end
        function res = get.Cgc(obj)
            res = getParameter(obj,'Cgc');
        end
        function obj = set.Cgcgy(obj,val)
            obj = setParameter(obj,'Cgcgy',val,0,'real');
        end
        function res = get.Cgcgy(obj)
            res = getParameter(obj,'Cgcgy');
        end
        function obj = set.Cgy(obj,val)
            obj = setParameter(obj,'Cgy',val,0,'real');
        end
        function res = get.Cgy(obj)
            res = getParameter(obj,'Cgy');
        end
        function obj = set.Cgygc(obj,val)
            obj = setParameter(obj,'Cgygc',val,0,'real');
        end
        function res = get.Cgygc(obj)
            res = getParameter(obj,'Cgygc');
        end
    end
end

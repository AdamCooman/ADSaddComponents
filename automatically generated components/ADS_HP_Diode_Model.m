classdef ADS_HP_Diode_Model < ADSmodel
    % ADS_HP_Diode_Model matlab representation for the ADS HP_Diode_Model component
    % This device model has been renamed to ADS_Diode model
    % model ModelName HP_Diode ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % data file name. (sm-rs) 
        File
        % library file name. (sm-rs) 
        Library
        % library item name. (sm-rs) 
        Item
        % Series resistance (smorr) Unit: Ohms/m^2
        Rs
        % Parasitic inductance (smorr) Unit: H
        Ls
        % Transit time (smorr) Unit: s
        Tt
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
    end
    methods
        function obj = set.File(obj,val)
            obj = setParameter(obj,'File',val,0,'string');
        end
        function res = get.File(obj)
            res = getParameter(obj,'File');
        end
        function obj = set.Library(obj,val)
            obj = setParameter(obj,'Library',val,0,'string');
        end
        function res = get.Library(obj)
            res = getParameter(obj,'Library');
        end
        function obj = set.Item(obj,val)
            obj = setParameter(obj,'Item',val,0,'string');
        end
        function res = get.Item(obj)
            res = getParameter(obj,'Item');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Tt(obj,val)
            obj = setParameter(obj,'Tt',val,0,'real');
        end
        function res = get.Tt(obj)
            res = getParameter(obj,'Tt');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
    end
end

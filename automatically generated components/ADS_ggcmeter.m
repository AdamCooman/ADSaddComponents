classdef ADS_ggcmeter < ADSnodeless
    % ADS_ggcmeter matlab representation for the ADS ggcmeter component
    % GG current meter
    % ggcmeter [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of the element to measure a pin current (s--rs) 
        Elmt
        % Element pin at which to measure current (s--ri) 
        Pin
        % Alias (s--ri) 
        Alias
        % Save all currents (s--ri) 
        SaveAll
        % Mode (s--ri) 
        Mode
    end
    methods
        function obj = set.Elmt(obj,val)
            obj = setParameter(obj,'Elmt',val,1,'string');
        end
        function res = get.Elmt(obj)
            res = getParameter(obj,'Elmt');
        end
        function obj = set.Pin(obj,val)
            obj = setParameter(obj,'Pin',val,1,'integer');
        end
        function res = get.Pin(obj)
            res = getParameter(obj,'Pin');
        end
        function obj = set.Alias(obj,val)
            obj = setParameter(obj,'Alias',val,0,'integer');
        end
        function res = get.Alias(obj)
            res = getParameter(obj,'Alias');
        end
        function obj = set.SaveAll(obj,val)
            obj = setParameter(obj,'SaveAll',val,0,'integer');
        end
        function res = get.SaveAll(obj)
            res = getParameter(obj,'SaveAll');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
    end
end

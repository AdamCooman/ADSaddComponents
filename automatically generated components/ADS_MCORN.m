classdef ADS_MCORN < ADScomponent
    % ADS_MCORN matlab representation for the ADS MCORN component
    % 90-Degree Microstrip Bend (Unmitered)
    % MCORN [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor Width (Smorr) Unit: m
        W
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

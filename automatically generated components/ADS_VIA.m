classdef ADS_VIA < ADScomponent
    % ADS_VIA matlab representation for the ADS VIA component
    % Tapered Via Hole in Microstrip
    % VIA [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Diameter at pin 1 (Smorr) Unit: m
        D1
        % Diameter at pin 2 (Smorr) Unit: m
        D2
        % Substrate Thickness (Smorr) Unit: m
        H
        % Conductor Thickness (Smorr) Unit: m
        T
    end
    methods
        function obj = set.D1(obj,val)
            obj = setParameter(obj,'D1',val,0,'real');
        end
        function res = get.D1(obj)
            res = getParameter(obj,'D1');
        end
        function obj = set.D2(obj,val)
            obj = setParameter(obj,'D2',val,0,'real');
        end
        function res = get.D2(obj)
            res = getParameter(obj,'D2');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,0,'real');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.T(obj,val)
            obj = setParameter(obj,'T',val,0,'real');
        end
        function res = get.T(obj)
            res = getParameter(obj,'T');
        end
    end
end

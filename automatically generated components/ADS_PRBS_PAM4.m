classdef ADS_PRBS_PAM4 < ADScomponent
    % ADS_PRBS_PAM4 matlab representation for the ADS PRBS_PAM4 component
    % Pseudo-Random Bit Sequence Source for PAM4
    % PRBS_PAM4 [:Name] outPos outNeg trigPos trigNeg
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Source of digital input (smorr) 
        Mode
        % LFSR length (Mode=Maximal Length LFSR) (smorr) 
        RegisterLength
        % LFSR taps for XOR-ing (Mode=User Defined LFSR) (s--rs) 
        Taps
        % LFSR seed (Mode=User Defined LFSR) (s--rs) 
        Seed
        % Bit sequence (Mode=Explicit Bit Sequence) (s--rs) 
        BitSequence
        % ASCII file containing bit sequence (Mode=Bit File) (sm-rs) 
        BitFile
        % Source of trigger signal (smorr) 
        Trigger
        % Threshold voltage for detecting external trigger (smorr) Unit: V
        VtriggerThreshold
        % Edge sensitivity of external trigger (smorr) 
        TriggerEdge
        % User-supplied repeatable length of bit sequence (smorr) 
        SequenceLength
        % Logic low voltage for plus node (smorr) Unit: V
        V_logic_low_plus
        % Logic high voltage for plus node (smorr) Unit: V
        V_logic_high_plus
        % Lowest voltage of dynamic range (smorr) Unit: V
        V_logic_low_minus
        % Highest voltage of dynamic range for minus pin (smorr) Unit: V
        V_logic_high_minus
        % Output resistance across Vplus and ground (smorr) Unit: Ohms
        Rout_plus
        % Output resistance across Vminus and ground (smorr) Unit: Ohms
        Rout_minus
        % Encoding scheme for pulse amplitude modulation (smorr) 
        PAMencoding
        % Number of levels for pulse amplitude modulation (smorr) 
        PAMlevels
        % Encoding scheme (smorr) 
        Encoder
        % Logic level 0 voltage for plus node (smorr) Unit: V
        PAM4_V_0_plus
        % Logic level 1 voltage for plus node (smorr) Unit: V
        PAM4_V_1_plus
        % Logic level 2 voltage for plus node (smorr) Unit: V
        PAM4_V_2_plus
        % Logic level 3 voltage for plus node (smorr) Unit: V
        PAM4_V_3_plus
        % Logic level 0 voltage for minus node (smorr) Unit: V
        PAM4_V_0_minus
        % Logic level 1 voltage for minus node (smorr) Unit: V
        PAM4_V_1_minus
        % Logic level 2 voltage for minus node (smorr) Unit: V
        PAM4_V_2_minus
        % Logic level 3 voltage for minus node (smorr) Unit: V
        PAM4_V_3_minus
        % Flag for enabling de-empahsis feature (sm-rb) 
        EnableDeEmphasis
        % DeEmphasis specification unit (smorr) 
        DeEmphasisMode
        % De-emphasized dynamic range relative to peak range (smorr) 
        DeEmphasis
        % Rational multiple of bit interval used for pre-emphasis (smorr) 
        EmphasisSpan
        % Number of de-emphasis levels (smorr) 
        DeEmphasisTaps
        % Analytical edge shape during transitions (smorr) 
        EdgeShape
        % Flag for enabling EQ FIR feature (sm-rb) 
        EnableEQ
        % Pre Cursor (repeated) (smorr) 
        PreCursor
        % Main Cursor (smorr) 
        MainCursor
        % Post Cursor (repeated) (smorr) 
        PostCursor
        % Tap_Interval in UI (smorr) 
        Tap_Interval
        % Clock rate of generated bit stream (smorr) Unit: Hz
        BitRate
        % Duration of transition from low level to high level (smorr) Unit: s
        RiseTime
        % Duration of transition from high level to low level (smorr) Unit: s
        FallTime
        % Duration of transition from level 0 to level 1 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_0_1
        % Duration of transition from level 1 to level 0 for PAM4 (smorr) Unit: s
        PAM4_FallTime_1_0
        % Duration of transition from level 0 to level 2 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_0_2
        % Duration of transition from level 2 to level 0 for PAM4 (smorr) Unit: s
        PAM4_FallTime_2_0
        % Duration of transition from level 0 to level 3 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_0_3
        % Duration of transition from level 3 to level 0 for PAM4 (smorr) Unit: s
        PAM4_FallTime_3_0
        % Duration of transition from level 1 to level 2 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_1_2
        % Duration of transition from level 2 to level 1 for PAM4 (smorr) Unit: s
        PAM4_FallTime_2_1
        % Duration of transition from level 1 to level 3 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_1_3
        % Duration of transition from level 3 to level 1 for PAM4 (smorr) Unit: s
        PAM4_FallTime_3_1
        % Duration of transition from level 2 to level 3 for PAM4 (smorr) Unit: s
        PAM4_RiseTime_2_3
        % Duration of transition from level 3 to level 2 for PAM4 (smorr) Unit: s
        PAM4_FallTime_3_2
        % Lower level of transition region as % of dynamic range (smorr) 
        TransitReference
        % Initial time delay (smorr) Unit: s
        Delay
        % Time delay between plus and minus pins (smorr) Unit: s
        Skew
        % Flag for enabling Random Jitter (sm-rb) 
        EnableRJ
        % Standard deviation of Random Jitter (smorr) Unit: s
        RJrms
        % Bandwidth for measurement of Random Jitter (smorr) Unit: Hz
        RJbw
        % Flag for enabling Periodic Jitter (sm-rb) 
        EnablePJ
        % Periodic Jitter wave shape (repeated) (smorr) 
        PJwave
        % Periodic Jitter amplitude (repeated) (smorr) Unit: s
        PJamp
        % Periodic Jitter frequency (repeated) (smorr) Unit: Hz
        PJfreq
        % Flag for enabling DCD Jitter (sm-rb) 
        EnableDCD
        % DCD Jitter Value (smorr) Unit: s
        DCDinUI
        % Flag for enabling ClockDCD Jitter (sm-rb) 
        EnableClockDCD
        % ClockDCD Jitter Value (smorr) Unit: s
        ClockDCDinUI
        % Flag for generating TIEdata (sm-ri) 
        TIEdata
        % Flag for generating raw jitter (sm-ri) 
        RawJitter
    end
    methods
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'real');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.RegisterLength(obj,val)
            obj = setParameter(obj,'RegisterLength',val,0,'real');
        end
        function res = get.RegisterLength(obj)
            res = getParameter(obj,'RegisterLength');
        end
        function obj = set.Taps(obj,val)
            obj = setParameter(obj,'Taps',val,0,'string');
        end
        function res = get.Taps(obj)
            res = getParameter(obj,'Taps');
        end
        function obj = set.Seed(obj,val)
            obj = setParameter(obj,'Seed',val,0,'string');
        end
        function res = get.Seed(obj)
            res = getParameter(obj,'Seed');
        end
        function obj = set.BitSequence(obj,val)
            obj = setParameter(obj,'BitSequence',val,0,'string');
        end
        function res = get.BitSequence(obj)
            res = getParameter(obj,'BitSequence');
        end
        function obj = set.BitFile(obj,val)
            obj = setParameter(obj,'BitFile',val,0,'string');
        end
        function res = get.BitFile(obj)
            res = getParameter(obj,'BitFile');
        end
        function obj = set.Trigger(obj,val)
            obj = setParameter(obj,'Trigger',val,0,'real');
        end
        function res = get.Trigger(obj)
            res = getParameter(obj,'Trigger');
        end
        function obj = set.VtriggerThreshold(obj,val)
            obj = setParameter(obj,'VtriggerThreshold',val,0,'real');
        end
        function res = get.VtriggerThreshold(obj)
            res = getParameter(obj,'VtriggerThreshold');
        end
        function obj = set.TriggerEdge(obj,val)
            obj = setParameter(obj,'TriggerEdge',val,0,'real');
        end
        function res = get.TriggerEdge(obj)
            res = getParameter(obj,'TriggerEdge');
        end
        function obj = set.SequenceLength(obj,val)
            obj = setParameter(obj,'SequenceLength',val,0,'real');
        end
        function res = get.SequenceLength(obj)
            res = getParameter(obj,'SequenceLength');
        end
        function obj = set.V_logic_low_plus(obj,val)
            obj = setParameter(obj,'V_logic_low_plus',val,0,'real');
        end
        function res = get.V_logic_low_plus(obj)
            res = getParameter(obj,'V_logic_low_plus');
        end
        function obj = set.V_logic_high_plus(obj,val)
            obj = setParameter(obj,'V_logic_high_plus',val,0,'real');
        end
        function res = get.V_logic_high_plus(obj)
            res = getParameter(obj,'V_logic_high_plus');
        end
        function obj = set.V_logic_low_minus(obj,val)
            obj = setParameter(obj,'V_logic_low_minus',val,0,'real');
        end
        function res = get.V_logic_low_minus(obj)
            res = getParameter(obj,'V_logic_low_minus');
        end
        function obj = set.V_logic_high_minus(obj,val)
            obj = setParameter(obj,'V_logic_high_minus',val,0,'real');
        end
        function res = get.V_logic_high_minus(obj)
            res = getParameter(obj,'V_logic_high_minus');
        end
        function obj = set.Rout_plus(obj,val)
            obj = setParameter(obj,'Rout_plus',val,0,'real');
        end
        function res = get.Rout_plus(obj)
            res = getParameter(obj,'Rout_plus');
        end
        function obj = set.Rout_minus(obj,val)
            obj = setParameter(obj,'Rout_minus',val,0,'real');
        end
        function res = get.Rout_minus(obj)
            res = getParameter(obj,'Rout_minus');
        end
        function obj = set.PAMencoding(obj,val)
            obj = setParameter(obj,'PAMencoding',val,0,'real');
        end
        function res = get.PAMencoding(obj)
            res = getParameter(obj,'PAMencoding');
        end
        function obj = set.PAMlevels(obj,val)
            obj = setParameter(obj,'PAMlevels',val,0,'real');
        end
        function res = get.PAMlevels(obj)
            res = getParameter(obj,'PAMlevels');
        end
        function obj = set.Encoder(obj,val)
            obj = setParameter(obj,'Encoder',val,0,'real');
        end
        function res = get.Encoder(obj)
            res = getParameter(obj,'Encoder');
        end
        function obj = set.PAM4_V_0_plus(obj,val)
            obj = setParameter(obj,'PAM4_V_0_plus',val,0,'real');
        end
        function res = get.PAM4_V_0_plus(obj)
            res = getParameter(obj,'PAM4_V_0_plus');
        end
        function obj = set.PAM4_V_1_plus(obj,val)
            obj = setParameter(obj,'PAM4_V_1_plus',val,0,'real');
        end
        function res = get.PAM4_V_1_plus(obj)
            res = getParameter(obj,'PAM4_V_1_plus');
        end
        function obj = set.PAM4_V_2_plus(obj,val)
            obj = setParameter(obj,'PAM4_V_2_plus',val,0,'real');
        end
        function res = get.PAM4_V_2_plus(obj)
            res = getParameter(obj,'PAM4_V_2_plus');
        end
        function obj = set.PAM4_V_3_plus(obj,val)
            obj = setParameter(obj,'PAM4_V_3_plus',val,0,'real');
        end
        function res = get.PAM4_V_3_plus(obj)
            res = getParameter(obj,'PAM4_V_3_plus');
        end
        function obj = set.PAM4_V_0_minus(obj,val)
            obj = setParameter(obj,'PAM4_V_0_minus',val,0,'real');
        end
        function res = get.PAM4_V_0_minus(obj)
            res = getParameter(obj,'PAM4_V_0_minus');
        end
        function obj = set.PAM4_V_1_minus(obj,val)
            obj = setParameter(obj,'PAM4_V_1_minus',val,0,'real');
        end
        function res = get.PAM4_V_1_minus(obj)
            res = getParameter(obj,'PAM4_V_1_minus');
        end
        function obj = set.PAM4_V_2_minus(obj,val)
            obj = setParameter(obj,'PAM4_V_2_minus',val,0,'real');
        end
        function res = get.PAM4_V_2_minus(obj)
            res = getParameter(obj,'PAM4_V_2_minus');
        end
        function obj = set.PAM4_V_3_minus(obj,val)
            obj = setParameter(obj,'PAM4_V_3_minus',val,0,'real');
        end
        function res = get.PAM4_V_3_minus(obj)
            res = getParameter(obj,'PAM4_V_3_minus');
        end
        function obj = set.EnableDeEmphasis(obj,val)
            obj = setParameter(obj,'EnableDeEmphasis',val,0,'boolean');
        end
        function res = get.EnableDeEmphasis(obj)
            res = getParameter(obj,'EnableDeEmphasis');
        end
        function obj = set.DeEmphasisMode(obj,val)
            obj = setParameter(obj,'DeEmphasisMode',val,0,'real');
        end
        function res = get.DeEmphasisMode(obj)
            res = getParameter(obj,'DeEmphasisMode');
        end
        function obj = set.DeEmphasis(obj,val)
            obj = setParameter(obj,'DeEmphasis',val,0,'real');
        end
        function res = get.DeEmphasis(obj)
            res = getParameter(obj,'DeEmphasis');
        end
        function obj = set.EmphasisSpan(obj,val)
            obj = setParameter(obj,'EmphasisSpan',val,0,'real');
        end
        function res = get.EmphasisSpan(obj)
            res = getParameter(obj,'EmphasisSpan');
        end
        function obj = set.DeEmphasisTaps(obj,val)
            obj = setParameter(obj,'DeEmphasisTaps',val,0,'real');
        end
        function res = get.DeEmphasisTaps(obj)
            res = getParameter(obj,'DeEmphasisTaps');
        end
        function obj = set.EdgeShape(obj,val)
            obj = setParameter(obj,'EdgeShape',val,0,'real');
        end
        function res = get.EdgeShape(obj)
            res = getParameter(obj,'EdgeShape');
        end
        function obj = set.EnableEQ(obj,val)
            obj = setParameter(obj,'EnableEQ',val,0,'boolean');
        end
        function res = get.EnableEQ(obj)
            res = getParameter(obj,'EnableEQ');
        end
        function obj = set.PreCursor(obj,val)
            obj = setParameter(obj,'PreCursor',val,1,'real');
        end
        function res = get.PreCursor(obj)
            res = getParameter(obj,'PreCursor');
        end
        function obj = set.MainCursor(obj,val)
            obj = setParameter(obj,'MainCursor',val,0,'real');
        end
        function res = get.MainCursor(obj)
            res = getParameter(obj,'MainCursor');
        end
        function obj = set.PostCursor(obj,val)
            obj = setParameter(obj,'PostCursor',val,1,'real');
        end
        function res = get.PostCursor(obj)
            res = getParameter(obj,'PostCursor');
        end
        function obj = set.Tap_Interval(obj,val)
            obj = setParameter(obj,'Tap_Interval',val,0,'real');
        end
        function res = get.Tap_Interval(obj)
            res = getParameter(obj,'Tap_Interval');
        end
        function obj = set.BitRate(obj,val)
            obj = setParameter(obj,'BitRate',val,0,'real');
        end
        function res = get.BitRate(obj)
            res = getParameter(obj,'BitRate');
        end
        function obj = set.RiseTime(obj,val)
            obj = setParameter(obj,'RiseTime',val,0,'real');
        end
        function res = get.RiseTime(obj)
            res = getParameter(obj,'RiseTime');
        end
        function obj = set.FallTime(obj,val)
            obj = setParameter(obj,'FallTime',val,0,'real');
        end
        function res = get.FallTime(obj)
            res = getParameter(obj,'FallTime');
        end
        function obj = set.PAM4_RiseTime_0_1(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_0_1',val,0,'real');
        end
        function res = get.PAM4_RiseTime_0_1(obj)
            res = getParameter(obj,'PAM4_RiseTime_0_1');
        end
        function obj = set.PAM4_FallTime_1_0(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_1_0',val,0,'real');
        end
        function res = get.PAM4_FallTime_1_0(obj)
            res = getParameter(obj,'PAM4_FallTime_1_0');
        end
        function obj = set.PAM4_RiseTime_0_2(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_0_2',val,0,'real');
        end
        function res = get.PAM4_RiseTime_0_2(obj)
            res = getParameter(obj,'PAM4_RiseTime_0_2');
        end
        function obj = set.PAM4_FallTime_2_0(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_2_0',val,0,'real');
        end
        function res = get.PAM4_FallTime_2_0(obj)
            res = getParameter(obj,'PAM4_FallTime_2_0');
        end
        function obj = set.PAM4_RiseTime_0_3(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_0_3',val,0,'real');
        end
        function res = get.PAM4_RiseTime_0_3(obj)
            res = getParameter(obj,'PAM4_RiseTime_0_3');
        end
        function obj = set.PAM4_FallTime_3_0(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_3_0',val,0,'real');
        end
        function res = get.PAM4_FallTime_3_0(obj)
            res = getParameter(obj,'PAM4_FallTime_3_0');
        end
        function obj = set.PAM4_RiseTime_1_2(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_1_2',val,0,'real');
        end
        function res = get.PAM4_RiseTime_1_2(obj)
            res = getParameter(obj,'PAM4_RiseTime_1_2');
        end
        function obj = set.PAM4_FallTime_2_1(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_2_1',val,0,'real');
        end
        function res = get.PAM4_FallTime_2_1(obj)
            res = getParameter(obj,'PAM4_FallTime_2_1');
        end
        function obj = set.PAM4_RiseTime_1_3(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_1_3',val,0,'real');
        end
        function res = get.PAM4_RiseTime_1_3(obj)
            res = getParameter(obj,'PAM4_RiseTime_1_3');
        end
        function obj = set.PAM4_FallTime_3_1(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_3_1',val,0,'real');
        end
        function res = get.PAM4_FallTime_3_1(obj)
            res = getParameter(obj,'PAM4_FallTime_3_1');
        end
        function obj = set.PAM4_RiseTime_2_3(obj,val)
            obj = setParameter(obj,'PAM4_RiseTime_2_3',val,0,'real');
        end
        function res = get.PAM4_RiseTime_2_3(obj)
            res = getParameter(obj,'PAM4_RiseTime_2_3');
        end
        function obj = set.PAM4_FallTime_3_2(obj,val)
            obj = setParameter(obj,'PAM4_FallTime_3_2',val,0,'real');
        end
        function res = get.PAM4_FallTime_3_2(obj)
            res = getParameter(obj,'PAM4_FallTime_3_2');
        end
        function obj = set.TransitReference(obj,val)
            obj = setParameter(obj,'TransitReference',val,0,'real');
        end
        function res = get.TransitReference(obj)
            res = getParameter(obj,'TransitReference');
        end
        function obj = set.Delay(obj,val)
            obj = setParameter(obj,'Delay',val,0,'real');
        end
        function res = get.Delay(obj)
            res = getParameter(obj,'Delay');
        end
        function obj = set.Skew(obj,val)
            obj = setParameter(obj,'Skew',val,0,'real');
        end
        function res = get.Skew(obj)
            res = getParameter(obj,'Skew');
        end
        function obj = set.EnableRJ(obj,val)
            obj = setParameter(obj,'EnableRJ',val,0,'boolean');
        end
        function res = get.EnableRJ(obj)
            res = getParameter(obj,'EnableRJ');
        end
        function obj = set.RJrms(obj,val)
            obj = setParameter(obj,'RJrms',val,0,'real');
        end
        function res = get.RJrms(obj)
            res = getParameter(obj,'RJrms');
        end
        function obj = set.RJbw(obj,val)
            obj = setParameter(obj,'RJbw',val,0,'real');
        end
        function res = get.RJbw(obj)
            res = getParameter(obj,'RJbw');
        end
        function obj = set.EnablePJ(obj,val)
            obj = setParameter(obj,'EnablePJ',val,0,'boolean');
        end
        function res = get.EnablePJ(obj)
            res = getParameter(obj,'EnablePJ');
        end
        function obj = set.PJwave(obj,val)
            obj = setParameter(obj,'PJwave',val,1,'real');
        end
        function res = get.PJwave(obj)
            res = getParameter(obj,'PJwave');
        end
        function obj = set.PJamp(obj,val)
            obj = setParameter(obj,'PJamp',val,1,'real');
        end
        function res = get.PJamp(obj)
            res = getParameter(obj,'PJamp');
        end
        function obj = set.PJfreq(obj,val)
            obj = setParameter(obj,'PJfreq',val,1,'real');
        end
        function res = get.PJfreq(obj)
            res = getParameter(obj,'PJfreq');
        end
        function obj = set.EnableDCD(obj,val)
            obj = setParameter(obj,'EnableDCD',val,0,'boolean');
        end
        function res = get.EnableDCD(obj)
            res = getParameter(obj,'EnableDCD');
        end
        function obj = set.DCDinUI(obj,val)
            obj = setParameter(obj,'DCDinUI',val,0,'real');
        end
        function res = get.DCDinUI(obj)
            res = getParameter(obj,'DCDinUI');
        end
        function obj = set.EnableClockDCD(obj,val)
            obj = setParameter(obj,'EnableClockDCD',val,0,'boolean');
        end
        function res = get.EnableClockDCD(obj)
            res = getParameter(obj,'EnableClockDCD');
        end
        function obj = set.ClockDCDinUI(obj,val)
            obj = setParameter(obj,'ClockDCDinUI',val,0,'real');
        end
        function res = get.ClockDCDinUI(obj)
            res = getParameter(obj,'ClockDCDinUI');
        end
        function obj = set.TIEdata(obj,val)
            obj = setParameter(obj,'TIEdata',val,0,'integer');
        end
        function res = get.TIEdata(obj)
            res = getParameter(obj,'TIEdata');
        end
        function obj = set.RawJitter(obj,val)
            obj = setParameter(obj,'RawJitter',val,0,'integer');
        end
        function res = get.RawJitter(obj)
            res = getParameter(obj,'RawJitter');
        end
    end
end

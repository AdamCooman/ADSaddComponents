classdef ADS_TL_New < ADScomponent
    % ADS_TL_New matlab representation for the ADS TL_New component
    % Ideal transmission line
    % TL_New [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Length of transmission line (Smorr) Unit: m
        L
        % Impedance of transmission line (Smorr) Unit: oh
        Z
        % Relative velocity of transmission line (Smorr) Unit: 1
        V
        % Series Resistance of transmission line (smorr) Unit: oh/m
        R
        % Shunt Conductance of transmission line (smorr) Unit: Sie/m
        G
        % Reference frequency of transmission line (smorr) Unit: Hz
        Freq
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Z(obj,val)
            obj = setParameter(obj,'Z',val,0,'real');
        end
        function res = get.Z(obj)
            res = getParameter(obj,'Z');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.Freq(obj,val)
            obj = setParameter(obj,'Freq',val,0,'real');
        end
        function res = get.Freq(obj)
            res = getParameter(obj,'Freq');
        end
    end
end

classdef ADS_PC_Taper < ADScomponent
    % ADS_PC_Taper matlab representation for the ADS PC_Taper component
    % User-defined model
    % PC_Taper [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        W_Left
        % User device parameter (Smorr) Unit: unknown
        W_Right
        % User device parameter (Smorr) Unit: unknown
        Layer
        % User device parameter (Smorr) Unit: unknown
        S_Left
        % User device parameter (Smorr) Unit: unknown
        S_Right
        % User device parameter (Smorr) Unit: unknown
        X_Offset
        % User device parameter (Smorr) Unit: unknown
        Y_Offset
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.W_Left(obj,val)
            obj = setParameter(obj,'W_Left',val,0,'real');
        end
        function res = get.W_Left(obj)
            res = getParameter(obj,'W_Left');
        end
        function obj = set.W_Right(obj,val)
            obj = setParameter(obj,'W_Right',val,0,'real');
        end
        function res = get.W_Right(obj)
            res = getParameter(obj,'W_Right');
        end
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'real');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.S_Left(obj,val)
            obj = setParameter(obj,'S_Left',val,0,'real');
        end
        function res = get.S_Left(obj)
            res = getParameter(obj,'S_Left');
        end
        function obj = set.S_Right(obj,val)
            obj = setParameter(obj,'S_Right',val,0,'real');
        end
        function res = get.S_Right(obj)
            res = getParameter(obj,'S_Right');
        end
        function obj = set.X_Offset(obj,val)
            obj = setParameter(obj,'X_Offset',val,0,'real');
        end
        function res = get.X_Offset(obj)
            res = getParameter(obj,'X_Offset');
        end
        function obj = set.Y_Offset(obj,val)
            obj = setParameter(obj,'Y_Offset',val,0,'real');
        end
        function res = get.Y_Offset(obj)
            res = getParameter(obj,'Y_Offset');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

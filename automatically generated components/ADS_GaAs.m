classdef ADS_GaAs < ADScomponent
    % ADS_GaAs matlab representation for the ADS GaAs component
    % GaAs MESFET Transistor
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise above ambient (smorr) Unit: deg C
        Trise
        % Models 7 and 9: gate finger width (in meters). (smorr) Unit: m
        W
        % Models 7 and 9: number of gate fingers. (sm-ri) 
        N
        % scaling factor (smorr) 
        Area
        % N type GaAs MESFET (s---b) 
        NFET
        % P type GaAs MESFET (s---b) 
        PFET
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Francois Tin param (s---r) Unit: deg C
        Tin
        % Francois Tout param (s---r) Unit: deg C
        Tout
        % Small signal drain source conductance (---rr) Unit: S
        Gds
        % Small signal Vcs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vcd to Ids transconductance (---rr) Unit: S
        Gmr
        % Small signal gate source conductance (---rr) Unit: S
        Ggs
        % Small signal gate drain conductance dIgd_dVgd (---rr) Unit: S
        Ggd
        % Small signal drain source cap. (part due to A5) (---rr) Unit: F
        Cds
        % Small signal Vgd to Igs transconductance (---rr) Unit: S
        dIgs_dVgd
        % Small signal Vgs to Igd transconductance (---rr) Unit: S
        dIgd_dVgs
        % Small signal backgate to Ids conductance (---rr) Unit: S
        dIds_dVgb
        % Small signal Vgdc to Qgs transcapacitance (---rr) Unit: F
        dQgs_dVgdc
        % Small signal Vgsc to Qgd transcapacitance (---rr) Unit: F
        dQgd_dVgsc
        % Small signal Ids current to Vgs (due to Tau and A5) (---rr) 
        dQds_dVgs
        % Small signal Ids to Igs gain (due to R2) (---rr) 
        dIgs_dIds
        % Small signal Ids to Igd gain (due to R2) (---rr) 
        dIgd_dIds
    end
    methods
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Area(obj,val)
            obj = setParameter(obj,'Area',val,0,'real');
        end
        function res = get.Area(obj)
            res = getParameter(obj,'Area');
        end
        function obj = set.NFET(obj,val)
            obj = setParameter(obj,'NFET',val,0,'boolean');
        end
        function res = get.NFET(obj)
            res = getParameter(obj,'NFET');
        end
        function obj = set.PFET(obj,val)
            obj = setParameter(obj,'PFET',val,0,'boolean');
        end
        function res = get.PFET(obj)
            res = getParameter(obj,'PFET');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Tin(obj,val)
            obj = setParameter(obj,'Tin',val,0,'real');
        end
        function res = get.Tin(obj)
            res = getParameter(obj,'Tin');
        end
        function obj = set.Tout(obj,val)
            obj = setParameter(obj,'Tout',val,0,'real');
        end
        function res = get.Tout(obj)
            res = getParameter(obj,'Tout');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Ggs(obj,val)
            obj = setParameter(obj,'Ggs',val,0,'real');
        end
        function res = get.Ggs(obj)
            res = getParameter(obj,'Ggs');
        end
        function obj = set.Ggd(obj,val)
            obj = setParameter(obj,'Ggd',val,0,'real');
        end
        function res = get.Ggd(obj)
            res = getParameter(obj,'Ggd');
        end
        function obj = set.Cds(obj,val)
            obj = setParameter(obj,'Cds',val,0,'real');
        end
        function res = get.Cds(obj)
            res = getParameter(obj,'Cds');
        end
        function obj = set.dIgs_dVgd(obj,val)
            obj = setParameter(obj,'dIgs_dVgd',val,0,'real');
        end
        function res = get.dIgs_dVgd(obj)
            res = getParameter(obj,'dIgs_dVgd');
        end
        function obj = set.dIgd_dVgs(obj,val)
            obj = setParameter(obj,'dIgd_dVgs',val,0,'real');
        end
        function res = get.dIgd_dVgs(obj)
            res = getParameter(obj,'dIgd_dVgs');
        end
        function obj = set.dIds_dVgb(obj,val)
            obj = setParameter(obj,'dIds_dVgb',val,0,'real');
        end
        function res = get.dIds_dVgb(obj)
            res = getParameter(obj,'dIds_dVgb');
        end
        function obj = set.dQgs_dVgdc(obj,val)
            obj = setParameter(obj,'dQgs_dVgdc',val,0,'real');
        end
        function res = get.dQgs_dVgdc(obj)
            res = getParameter(obj,'dQgs_dVgdc');
        end
        function obj = set.dQgd_dVgsc(obj,val)
            obj = setParameter(obj,'dQgd_dVgsc',val,0,'real');
        end
        function res = get.dQgd_dVgsc(obj)
            res = getParameter(obj,'dQgd_dVgsc');
        end
        function obj = set.dQds_dVgs(obj,val)
            obj = setParameter(obj,'dQds_dVgs',val,0,'real');
        end
        function res = get.dQds_dVgs(obj)
            res = getParameter(obj,'dQds_dVgs');
        end
        function obj = set.dIgs_dIds(obj,val)
            obj = setParameter(obj,'dIgs_dIds',val,0,'real');
        end
        function res = get.dIgs_dIds(obj)
            res = getParameter(obj,'dIgs_dIds');
        end
        function obj = set.dIgd_dIds(obj,val)
            obj = setParameter(obj,'dIgd_dIds',val,0,'real');
        end
        function res = get.dIgd_dIds(obj)
            res = getParameter(obj,'dIgd_dIds');
        end
    end
end

classdef ADS_PCBEND < ADScomponent
    % ADS_PCBEND matlab representation for the ADS PCBEND component
    % PCB Bend (Arbitrary Angle/Miter)
    % PCBEND [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor width (Smorr) Unit: m
        W
        % Conductor layer number (Sm-ri) Unit: unknown
        CLayer
        % Angle of bend (Smorr) Unit: deg
        Angle
        % Miter fraction (Smorr) Unit: unknown
        M
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
        % Factor to refine the background grid (Sm-ri) Unit: unknown
        Refine_grid_factor
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.CLayer(obj,val)
            obj = setParameter(obj,'CLayer',val,0,'integer');
        end
        function res = get.CLayer(obj)
            res = getParameter(obj,'CLayer');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Refine_grid_factor(obj,val)
            obj = setParameter(obj,'Refine_grid_factor',val,0,'integer');
        end
        function res = get.Refine_grid_factor(obj)
            res = getParameter(obj,'Refine_grid_factor');
        end
    end
end

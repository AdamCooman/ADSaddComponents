classdef ADS_MRINDNBR < ADScomponent
    % ADS_MRINDNBR matlab representation for the ADS MRINDNBR component
    % Microstrip Rectangular Inductor (No Bridge)
    % MRINDNBR [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Number of Segments (Sm-ri) Unit: unknown
        Ns
        % Length of First Segment (Smorr) Unit: m
        L1
        % Length of second Segment (Smorr) Unit: m
        L2
        % Length of Third Segment (Smorr) Unit: m
        L3
        % Length of Last Segment (Smorr) Unit: m
        Ln
        % Conductor Width (Smorr) Unit: m
        W
        % Conductor Spacing (Smorr) Unit: m
        S
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'integer');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.L1(obj,val)
            obj = setParameter(obj,'L1',val,0,'real');
        end
        function res = get.L1(obj)
            res = getParameter(obj,'L1');
        end
        function obj = set.L2(obj,val)
            obj = setParameter(obj,'L2',val,0,'real');
        end
        function res = get.L2(obj)
            res = getParameter(obj,'L2');
        end
        function obj = set.L3(obj,val)
            obj = setParameter(obj,'L3',val,0,'real');
        end
        function res = get.L3(obj)
            res = getParameter(obj,'L3');
        end
        function obj = set.Ln(obj,val)
            obj = setParameter(obj,'Ln',val,0,'real');
        end
        function res = get.Ln(obj)
            res = getParameter(obj,'Ln');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

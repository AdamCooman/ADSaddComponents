classdef ADS_Crossover < ADScomponent
    % ADS_Crossover matlab representation for the ADS Crossover component
    % User-defined model
    % Crossover [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % User device parameter (Sm-ri) Unit: unknown
        Layer1
        % User device parameter (Smorr) Unit: unknown
        W1
        % User device parameter (Sm-ri) Unit: unknown
        Layer2
        % User device parameter (Smorr) Unit: unknown
        W2
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.Layer1(obj,val)
            obj = setParameter(obj,'Layer1',val,0,'integer');
        end
        function res = get.Layer1(obj)
            res = getParameter(obj,'Layer1');
        end
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.Layer2(obj,val)
            obj = setParameter(obj,'Layer2',val,0,'integer');
        end
        function res = get.Layer2(obj)
            res = getParameter(obj,'Layer2');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

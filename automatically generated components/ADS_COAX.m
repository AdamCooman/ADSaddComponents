classdef ADS_COAX < ADScomponent
    % ADS_COAX matlab representation for the ADS COAX component
    % Coaxial Cable
    % COAX [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Diameter of inner Conductor (Smorr) Unit: m
        Di
        % Inner diameter of outer conductor (Smorr) Unit: m
        Do
        % Length (Smorr) Unit: m
        L
        % Dielectric Constant of dielectric between inner and outer conductors (Smorr) Unit: unknown
        Er
        % Dielectric Loss Tangent (Smorr) Unit: unknown
        TanD
        % Conductor resistivity (relative to copper) (Smorr) Unit: unknown
        Rho
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.Di(obj,val)
            obj = setParameter(obj,'Di',val,0,'real');
        end
        function res = get.Di(obj)
            res = getParameter(obj,'Di');
        end
        function obj = set.Do(obj,val)
            obj = setParameter(obj,'Do',val,0,'real');
        end
        function res = get.Do(obj)
            res = getParameter(obj,'Do');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

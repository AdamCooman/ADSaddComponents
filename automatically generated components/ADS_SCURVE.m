classdef ADS_SCURVE < ADScomponent
    % ADS_SCURVE matlab representation for the ADS SCURVE component
    % Curved Line in Stripline
    % SCURVE [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Conductor width (Smorr) Unit: m
        W
        % Angle subtended by transmission line (Smorr) Unit: deg
        Angle
        % Radius (measured to strip center line) (Smorr) Unit: m
        Radius
        % Stripline substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Angle(obj,val)
            obj = setParameter(obj,'Angle',val,0,'real');
        end
        function res = get.Angle(obj)
            res = getParameter(obj,'Angle');
        end
        function obj = set.Radius(obj,val)
            obj = setParameter(obj,'Radius',val,0,'real');
        end
        function res = get.Radius(obj)
            res = getParameter(obj,'Radius');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

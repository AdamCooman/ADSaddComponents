classdef ADS_Mutual < ADSnodeless
    % ADS_Mutual matlab representation for the ADS Mutual component
    % Mutual Inductor
    % Mutual [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Mutual inductor coupling coefficient (smorr) 
        K
        % Mutual inductance (smorr) Unit: H
        M
        % Inductor one name (S--rd) 
        Inductor1
        % Inductor two name (S--rd) 
        Inductor2
    end
    methods
        function obj = set.K(obj,val)
            obj = setParameter(obj,'K',val,0,'real');
        end
        function res = get.K(obj)
            res = getParameter(obj,'K');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Inductor1(obj,val)
            obj = setParameter(obj,'Inductor1',val,0,'string');
        end
        function res = get.Inductor1(obj)
            res = getParameter(obj,'Inductor1');
        end
        function obj = set.Inductor2(obj,val)
            obj = setParameter(obj,'Inductor2',val,0,'string');
        end
        function res = get.Inductor2(obj)
            res = getParameter(obj,'Inductor2');
        end
    end
end

classdef ADS_ReDriver < ADScomponent
    % ADS_ReDriver matlab representation for the ADS ReDriver component
    % AMI ReDriver model for Channel Simulation.
    % ReDriver [:Name] p ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % The ReDriver model (s--rs) 
        Model
    end
    methods
        function obj = set.Model(obj,val)
            obj = setParameter(obj,'Model',val,0,'string');
        end
        function res = get.Model(obj)
            res = getParameter(obj,'Model');
        end
    end
end

classdef ADS_FINLINE < ADScomponent
    % ADS_FINLINE matlab representation for the ADS FINLINE component
    % Finline Transmission Line
    % FINLINE [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width (smorr) Unit: m
        A
        % Height (smorr) Unit: m
        B
        % Length (smorr) Unit: m
        L
        % Permittivity (smorr) 
        Er
        % Gap (smorr) Unit: m
        G
        % Dielectric thickness (smorr) Unit: m
        Dw
        % Distance from left wall to dielectric (smorr) Unit: m
        Wl
        % Impedance Definition 1, 2 or 3 (smorr) 
        Code
    end
    methods
        function obj = set.A(obj,val)
            obj = setParameter(obj,'A',val,0,'real');
        end
        function res = get.A(obj)
            res = getParameter(obj,'A');
        end
        function obj = set.B(obj,val)
            obj = setParameter(obj,'B',val,0,'real');
        end
        function res = get.B(obj)
            res = getParameter(obj,'B');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Er(obj,val)
            obj = setParameter(obj,'Er',val,0,'real');
        end
        function res = get.Er(obj)
            res = getParameter(obj,'Er');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.Dw(obj,val)
            obj = setParameter(obj,'Dw',val,0,'real');
        end
        function res = get.Dw(obj)
            res = getParameter(obj,'Dw');
        end
        function obj = set.Wl(obj,val)
            obj = setParameter(obj,'Wl',val,0,'real');
        end
        function res = get.Wl(obj)
            res = getParameter(obj,'Wl');
        end
        function obj = set.Code(obj,val)
            obj = setParameter(obj,'Code',val,0,'real');
        end
        function res = get.Code(obj)
            res = getParameter(obj,'Code');
        end
    end
end

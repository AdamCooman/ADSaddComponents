classdef ADS_TOM3 < ADScomponent
    % ADS_TOM3 matlab representation for the ADS TOM3 component
    % TOM3 Field Effect Transistor
    % ModelName [:Name] drain gate source
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Gate width in length units (smorr) Unit: m
        W
        % Number of gate fingers (smorr) 
        Ng
        % Device instance temperature (smorr) Unit: deg C
        Temp
        % Device temperature relative to circuit ambient (if Temp not specified) (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal drain to source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Reverse operation small signal Vgd to Isd transconductance (---rr) Unit: S
        Gmr
        % Small signal gate to source D2 conductance (---rr) Unit: S
        Ggse
        % Small signal gate to source D1 conductance (---rr) Unit: S
        Ggde
        % Small signal gate to source D4 conductance (---rr) Unit: S
        Ggsi
        % Small signal gate to source D3 conductance (---rr) Unit: S
        Ggdi
        % Small signal gate to source capacitance (---rr) Unit: F
        Cgsi
        % Small signal gate to source transcapacitance (---rr) Unit: F
        CgsiVgdi
        % Small signal gate to drain capacitance (---rr) Unit: F
        Cgdi
        % Small signal gate to drain transcapacitance (---rr) Unit: F
        CgdiVgsi
        % Small signal VCVS controlling coefficient kGamma (---rr) 
        Kg
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Ng(obj,val)
            obj = setParameter(obj,'Ng',val,0,'real');
        end
        function res = get.Ng(obj)
            res = getParameter(obj,'Ng');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Ggse(obj,val)
            obj = setParameter(obj,'Ggse',val,0,'real');
        end
        function res = get.Ggse(obj)
            res = getParameter(obj,'Ggse');
        end
        function obj = set.Ggde(obj,val)
            obj = setParameter(obj,'Ggde',val,0,'real');
        end
        function res = get.Ggde(obj)
            res = getParameter(obj,'Ggde');
        end
        function obj = set.Ggsi(obj,val)
            obj = setParameter(obj,'Ggsi',val,0,'real');
        end
        function res = get.Ggsi(obj)
            res = getParameter(obj,'Ggsi');
        end
        function obj = set.Ggdi(obj,val)
            obj = setParameter(obj,'Ggdi',val,0,'real');
        end
        function res = get.Ggdi(obj)
            res = getParameter(obj,'Ggdi');
        end
        function obj = set.Cgsi(obj,val)
            obj = setParameter(obj,'Cgsi',val,0,'real');
        end
        function res = get.Cgsi(obj)
            res = getParameter(obj,'Cgsi');
        end
        function obj = set.CgsiVgdi(obj,val)
            obj = setParameter(obj,'CgsiVgdi',val,0,'real');
        end
        function res = get.CgsiVgdi(obj)
            res = getParameter(obj,'CgsiVgdi');
        end
        function obj = set.Cgdi(obj,val)
            obj = setParameter(obj,'Cgdi',val,0,'real');
        end
        function res = get.Cgdi(obj)
            res = getParameter(obj,'Cgdi');
        end
        function obj = set.CgdiVgsi(obj,val)
            obj = setParameter(obj,'CgdiVgsi',val,0,'real');
        end
        function res = get.CgdiVgsi(obj)
            res = getParameter(obj,'CgdiVgsi');
        end
        function obj = set.Kg(obj,val)
            obj = setParameter(obj,'Kg',val,0,'real');
        end
        function res = get.Kg(obj)
            res = getParameter(obj,'Kg');
        end
    end
end

classdef ADS_StatCorr < ADSnodeless
    % ADS_StatCorr matlab representation for the ADS StatCorr component
    % Device to correlate statistical variables
    % StatCorr [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Name of first stat variable to correlate (repeatable) (s---s) 
        StatVarA
        % Name of second stat variable to correlate (repeatable) (s---s) 
        StatVarB
        % Correlation Value (repeatable) (s---r) 
        CorrVal
        % Enable Correlation (sm--b) 
        Enable
    end
    methods
        function obj = set.StatVarA(obj,val)
            obj = setParameter(obj,'StatVarA',val,1,'string');
        end
        function res = get.StatVarA(obj)
            res = getParameter(obj,'StatVarA');
        end
        function obj = set.StatVarB(obj,val)
            obj = setParameter(obj,'StatVarB',val,1,'string');
        end
        function res = get.StatVarB(obj)
            res = getParameter(obj,'StatVarB');
        end
        function obj = set.CorrVal(obj,val)
            obj = setParameter(obj,'CorrVal',val,1,'real');
        end
        function res = get.CorrVal(obj)
            res = getParameter(obj,'CorrVal');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
    end
end

classdef ADS_CCVS_ads < ADScomponent
    % ADS_CCVS_ads matlab representation for the ADS CCVS_ads component
    % Current-controlled Voltage Source
    % CCVS_ads [:Name] outp outn
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Gain of VCCS (smorr) 
        V
        % Probe name (S--rd) 
        CurrentProbe
        % Minimum value (smorr) 
        Min_val
        % Maximum value (smorr) 
        Max_val
        % Absolute value (sm-ri) 
        abs
        % Linear Temperature coefficient (smorr) 
        tc1
        % Quadratic Temperature coefficient (smorr) 
        tc2
    end
    methods
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.CurrentProbe(obj,val)
            obj = setParameter(obj,'CurrentProbe',val,0,'string');
        end
        function res = get.CurrentProbe(obj)
            res = getParameter(obj,'CurrentProbe');
        end
        function obj = set.Min_val(obj,val)
            obj = setParameter(obj,'Min_val',val,0,'real');
        end
        function res = get.Min_val(obj)
            res = getParameter(obj,'Min_val');
        end
        function obj = set.Max_val(obj,val)
            obj = setParameter(obj,'Max_val',val,0,'real');
        end
        function res = get.Max_val(obj)
            res = getParameter(obj,'Max_val');
        end
        function obj = set.abs(obj,val)
            obj = setParameter(obj,'abs',val,0,'integer');
        end
        function res = get.abs(obj)
            res = getParameter(obj,'abs');
        end
        function obj = set.tc1(obj,val)
            obj = setParameter(obj,'tc1',val,0,'real');
        end
        function res = get.tc1(obj)
            res = getParameter(obj,'tc1');
        end
        function obj = set.tc2(obj,val)
            obj = setParameter(obj,'tc2',val,0,'real');
        end
        function res = get.tc2(obj)
            res = getParameter(obj,'tc2');
        end
    end
end

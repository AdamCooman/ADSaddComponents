classdef ADS_SLUTL < ADScomponent
    % ADS_SLUTL matlab representation for the ADS SLUTL component
    % Unbalanced Stripline
    % SLUTL [:Name] t1 t2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width (smorr) Unit: m
        W
        % Length (smorr) Unit: m
        L
        % Strip Height (smorr) Unit: m
        Hs
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Hs(obj,val)
            obj = setParameter(obj,'Hs',val,0,'real');
        end
        function res = get.Hs(obj)
            res = getParameter(obj,'Hs');
        end
    end
end

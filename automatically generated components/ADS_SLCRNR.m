classdef ADS_SLCRNR < ADScomponent
    % ADS_SLCRNR matlab representation for the ADS SLCRNR component
    % Stripline Corner
    % SLCRNR [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of stripline corner (Smorr) Unit: m
        W
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

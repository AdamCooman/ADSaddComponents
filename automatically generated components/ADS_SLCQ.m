classdef ADS_SLCQ < ADScomponent
    % ADS_SLCQ matlab representation for the ADS SLCQ component
    % Series Inductor-Capacitor with Q
    % SLCQ [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % inductance (Smorr) Unit: H
        L
        % quality factor of inductor (Smorr) Unit: 1
        Ql
        % frequency at which inductor ql is given (Smorr) Unit: Hz
        Fl
        % frequency depending mode of inductor ql (Sm-ri) Unit: 1
        ModL
        % capacitance (Smorr) Unit: F
        C
        % quality factor of capacitor (Smorr) Unit: 1
        Qc
        % frequency at which capacitor qc is given (Smorr) Unit: Hz
        Fc
        % frequency depending mode of capacitor qc (Sm-ri) Unit: 1
        ModC
        % DC resistance for inductor modes 2 and 3 (smorr) Unit: Ohm
        Rdc
        % Physical Temperature (smorr) Unit: C
        Temp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Ql(obj,val)
            obj = setParameter(obj,'Ql',val,0,'real');
        end
        function res = get.Ql(obj)
            res = getParameter(obj,'Ql');
        end
        function obj = set.Fl(obj,val)
            obj = setParameter(obj,'Fl',val,0,'real');
        end
        function res = get.Fl(obj)
            res = getParameter(obj,'Fl');
        end
        function obj = set.ModL(obj,val)
            obj = setParameter(obj,'ModL',val,0,'integer');
        end
        function res = get.ModL(obj)
            res = getParameter(obj,'ModL');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Qc(obj,val)
            obj = setParameter(obj,'Qc',val,0,'real');
        end
        function res = get.Qc(obj)
            res = getParameter(obj,'Qc');
        end
        function obj = set.Fc(obj,val)
            obj = setParameter(obj,'Fc',val,0,'real');
        end
        function res = get.Fc(obj)
            res = getParameter(obj,'Fc');
        end
        function obj = set.ModC(obj,val)
            obj = setParameter(obj,'ModC',val,0,'integer');
        end
        function res = get.ModC(obj)
            res = getParameter(obj,'ModC');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

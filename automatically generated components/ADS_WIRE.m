classdef ADS_WIRE < ADScomponent
    % ADS_WIRE matlab representation for the ADS WIRE component
    % Round Wire
    % WIRE [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Wire Diameter (Smorr) Unit: m
        D
        % Wire Length (Smorr) Unit: m
        L
        % Metal resistivity (relative to Gold) (Smorr) Unit: unknown
        Rho
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

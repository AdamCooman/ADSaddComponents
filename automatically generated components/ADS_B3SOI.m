classdef ADS_B3SOI < ADScomponent
    % ADS_B3SOI matlab representation for the ADS B3SOI component
    % PD SOI Metal Oxide Semiconductor Transistor
    % ModelName [:Name] drain gate source substrate ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Channel length (smorr) Unit: m
        Length
        % Channel width (smorr) Unit: m
        Width
        % Area of the drain diffusion (smorr) Unit: m^2
        Ad
        % Area of the source diffusion (smorr) Unit: m^2
        As
        % Perimeter of the drain junction (smorr) Unit: m
        Pd
        % Perimeter of the source junction (smorr) Unit: m
        Ps
        % Number of squares of the drain diffusion (smorr) 
        Nrd
        % Number of squares of the source difussion (smorr) 
        Nrs
        % Number of squares in body (smorr) 
        Nrb
        % BJT on/off flag (s---i) 
        Bjtoff
        % Debug flag (s---i) 
        Debug
        % Instance Thermal Resistance (smorr) Unit: Ohms
        Rth0
        % Instance Thermal Capacitance (smorr) Unit: F
        Cth0
        % Number of body contact isolation edge (smorr) 
        Nbc
        % Number segments for width partitioning (smorr) 
        Nseg
        % Perimeter length for bc parasitics at drain side (smorr) Unit: m
        Pdbcp
        % Perimeter length for bc parasitics at source side (smorr) Unit: m
        Psbcp
        % Gate to body overlap area for bc parasitics (smorr) Unit: m^2
        Agbcp
        % Substrate to body overlap area for bc parasitics (smorr) Unit: m^2
        Aebcp
        % Vbs specified by the user (smorr) Unit: V
        Vbsusr
        % Flag indicating external temp node (s---i) 
        Tnodeout
        % DC operating region, 0=off, 1=on, 2=rev, 3=sat (s---i) 
        Region
        % Device operating temperature (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nonlinear spectral model on/off (s---i) 
        Mode
        % Noise generation on/off (s---b) 
        Noise
        % Small signal drain source conductance (---rr) Unit: S
        Gds
        % Small signal Vgs to Ids transconductance (---rr) Unit: S
        Gm
        % Small signal Vbs to Ids transconductance (---rr) Unit: S
        Gmbs
        % Small signal Vgd to Ids transconductance (---rr) Unit: S
        Gmr
        % Small signal Vbd to Ids transconductance (---rr) Unit: S
        Gmbsr
        % Small signal bulk drain junction capacitance (---rr) Unit: F
        Capbd
        % Small signal bulk source junction capacitance (---rr) Unit: F
        Capbs
        % Small signal gate capacitance (---rr) Unit: F
        Cggb
        % Small signal gate drain capacitance (---rr) Unit: F
        Cgdb
        % Small signal gate source capacitance (---rr) Unit: F
        Cgsb
        % Small signal drain gate capacitance (---rr) Unit: F
        Cdgb
        % Small signal drain capacitance (---rr) Unit: F
        Cddb
        % Small signal drain source capacitance (---rr) Unit: F
        Cdsb
        % Small signal bulk gate capacitance (---rr) Unit: F
        Cbgb
        % Small signal bulk drain capacitance (---rr) Unit: F
        Cbdb
        % Small signal bulk source capacitance (---rr) Unit: F
        Cbsb
        % Small signal source gate capacitance (---rr) Unit: F
        Csgb
        % Small signal source drain capacitance (---rr) Unit: F
        Csdb
        % Small signal source capacitance (---rr) Unit: F
        Cssb
        % Gate charge (---rr) Unit: C
        Qg
        % Drain charge (---rr) Unit: C
        Qd
        % Bulk charge (---rr) Unit: C
        Qb
        % NMOS type B3SOI MOSFET (---rb) 
        NMOS
        % PMOS type B3SOI MOSFET (---rb) 
        PMOS
    end
    methods
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Ad(obj,val)
            obj = setParameter(obj,'Ad',val,0,'real');
        end
        function res = get.Ad(obj)
            res = getParameter(obj,'Ad');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.Pd(obj,val)
            obj = setParameter(obj,'Pd',val,0,'real');
        end
        function res = get.Pd(obj)
            res = getParameter(obj,'Pd');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Nrd(obj,val)
            obj = setParameter(obj,'Nrd',val,0,'real');
        end
        function res = get.Nrd(obj)
            res = getParameter(obj,'Nrd');
        end
        function obj = set.Nrs(obj,val)
            obj = setParameter(obj,'Nrs',val,0,'real');
        end
        function res = get.Nrs(obj)
            res = getParameter(obj,'Nrs');
        end
        function obj = set.Nrb(obj,val)
            obj = setParameter(obj,'Nrb',val,0,'real');
        end
        function res = get.Nrb(obj)
            res = getParameter(obj,'Nrb');
        end
        function obj = set.Bjtoff(obj,val)
            obj = setParameter(obj,'Bjtoff',val,0,'integer');
        end
        function res = get.Bjtoff(obj)
            res = getParameter(obj,'Bjtoff');
        end
        function obj = set.Debug(obj,val)
            obj = setParameter(obj,'Debug',val,0,'integer');
        end
        function res = get.Debug(obj)
            res = getParameter(obj,'Debug');
        end
        function obj = set.Rth0(obj,val)
            obj = setParameter(obj,'Rth0',val,0,'real');
        end
        function res = get.Rth0(obj)
            res = getParameter(obj,'Rth0');
        end
        function obj = set.Cth0(obj,val)
            obj = setParameter(obj,'Cth0',val,0,'real');
        end
        function res = get.Cth0(obj)
            res = getParameter(obj,'Cth0');
        end
        function obj = set.Nbc(obj,val)
            obj = setParameter(obj,'Nbc',val,0,'real');
        end
        function res = get.Nbc(obj)
            res = getParameter(obj,'Nbc');
        end
        function obj = set.Nseg(obj,val)
            obj = setParameter(obj,'Nseg',val,0,'real');
        end
        function res = get.Nseg(obj)
            res = getParameter(obj,'Nseg');
        end
        function obj = set.Pdbcp(obj,val)
            obj = setParameter(obj,'Pdbcp',val,0,'real');
        end
        function res = get.Pdbcp(obj)
            res = getParameter(obj,'Pdbcp');
        end
        function obj = set.Psbcp(obj,val)
            obj = setParameter(obj,'Psbcp',val,0,'real');
        end
        function res = get.Psbcp(obj)
            res = getParameter(obj,'Psbcp');
        end
        function obj = set.Agbcp(obj,val)
            obj = setParameter(obj,'Agbcp',val,0,'real');
        end
        function res = get.Agbcp(obj)
            res = getParameter(obj,'Agbcp');
        end
        function obj = set.Aebcp(obj,val)
            obj = setParameter(obj,'Aebcp',val,0,'real');
        end
        function res = get.Aebcp(obj)
            res = getParameter(obj,'Aebcp');
        end
        function obj = set.Vbsusr(obj,val)
            obj = setParameter(obj,'Vbsusr',val,0,'real');
        end
        function res = get.Vbsusr(obj)
            res = getParameter(obj,'Vbsusr');
        end
        function obj = set.Tnodeout(obj,val)
            obj = setParameter(obj,'Tnodeout',val,0,'integer');
        end
        function res = get.Tnodeout(obj)
            res = getParameter(obj,'Tnodeout');
        end
        function obj = set.Region(obj,val)
            obj = setParameter(obj,'Region',val,0,'integer');
        end
        function res = get.Region(obj)
            res = getParameter(obj,'Region');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Mode(obj,val)
            obj = setParameter(obj,'Mode',val,0,'integer');
        end
        function res = get.Mode(obj)
            res = getParameter(obj,'Mode');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Gds(obj,val)
            obj = setParameter(obj,'Gds',val,0,'real');
        end
        function res = get.Gds(obj)
            res = getParameter(obj,'Gds');
        end
        function obj = set.Gm(obj,val)
            obj = setParameter(obj,'Gm',val,0,'real');
        end
        function res = get.Gm(obj)
            res = getParameter(obj,'Gm');
        end
        function obj = set.Gmbs(obj,val)
            obj = setParameter(obj,'Gmbs',val,0,'real');
        end
        function res = get.Gmbs(obj)
            res = getParameter(obj,'Gmbs');
        end
        function obj = set.Gmr(obj,val)
            obj = setParameter(obj,'Gmr',val,0,'real');
        end
        function res = get.Gmr(obj)
            res = getParameter(obj,'Gmr');
        end
        function obj = set.Gmbsr(obj,val)
            obj = setParameter(obj,'Gmbsr',val,0,'real');
        end
        function res = get.Gmbsr(obj)
            res = getParameter(obj,'Gmbsr');
        end
        function obj = set.Capbd(obj,val)
            obj = setParameter(obj,'Capbd',val,0,'real');
        end
        function res = get.Capbd(obj)
            res = getParameter(obj,'Capbd');
        end
        function obj = set.Capbs(obj,val)
            obj = setParameter(obj,'Capbs',val,0,'real');
        end
        function res = get.Capbs(obj)
            res = getParameter(obj,'Capbs');
        end
        function obj = set.Cggb(obj,val)
            obj = setParameter(obj,'Cggb',val,0,'real');
        end
        function res = get.Cggb(obj)
            res = getParameter(obj,'Cggb');
        end
        function obj = set.Cgdb(obj,val)
            obj = setParameter(obj,'Cgdb',val,0,'real');
        end
        function res = get.Cgdb(obj)
            res = getParameter(obj,'Cgdb');
        end
        function obj = set.Cgsb(obj,val)
            obj = setParameter(obj,'Cgsb',val,0,'real');
        end
        function res = get.Cgsb(obj)
            res = getParameter(obj,'Cgsb');
        end
        function obj = set.Cdgb(obj,val)
            obj = setParameter(obj,'Cdgb',val,0,'real');
        end
        function res = get.Cdgb(obj)
            res = getParameter(obj,'Cdgb');
        end
        function obj = set.Cddb(obj,val)
            obj = setParameter(obj,'Cddb',val,0,'real');
        end
        function res = get.Cddb(obj)
            res = getParameter(obj,'Cddb');
        end
        function obj = set.Cdsb(obj,val)
            obj = setParameter(obj,'Cdsb',val,0,'real');
        end
        function res = get.Cdsb(obj)
            res = getParameter(obj,'Cdsb');
        end
        function obj = set.Cbgb(obj,val)
            obj = setParameter(obj,'Cbgb',val,0,'real');
        end
        function res = get.Cbgb(obj)
            res = getParameter(obj,'Cbgb');
        end
        function obj = set.Cbdb(obj,val)
            obj = setParameter(obj,'Cbdb',val,0,'real');
        end
        function res = get.Cbdb(obj)
            res = getParameter(obj,'Cbdb');
        end
        function obj = set.Cbsb(obj,val)
            obj = setParameter(obj,'Cbsb',val,0,'real');
        end
        function res = get.Cbsb(obj)
            res = getParameter(obj,'Cbsb');
        end
        function obj = set.Csgb(obj,val)
            obj = setParameter(obj,'Csgb',val,0,'real');
        end
        function res = get.Csgb(obj)
            res = getParameter(obj,'Csgb');
        end
        function obj = set.Csdb(obj,val)
            obj = setParameter(obj,'Csdb',val,0,'real');
        end
        function res = get.Csdb(obj)
            res = getParameter(obj,'Csdb');
        end
        function obj = set.Cssb(obj,val)
            obj = setParameter(obj,'Cssb',val,0,'real');
        end
        function res = get.Cssb(obj)
            res = getParameter(obj,'Cssb');
        end
        function obj = set.Qg(obj,val)
            obj = setParameter(obj,'Qg',val,0,'real');
        end
        function res = get.Qg(obj)
            res = getParameter(obj,'Qg');
        end
        function obj = set.Qd(obj,val)
            obj = setParameter(obj,'Qd',val,0,'real');
        end
        function res = get.Qd(obj)
            res = getParameter(obj,'Qd');
        end
        function obj = set.Qb(obj,val)
            obj = setParameter(obj,'Qb',val,0,'real');
        end
        function res = get.Qb(obj)
            res = getParameter(obj,'Qb');
        end
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
    end
end

classdef ADS_BROCTL < ADScomponent
    % ADS_BROCTL matlab representation for the ADS BROCTL component
    % Offset Broadside Coupled Transmission Lines
    % BROCTL [:Name] TL bl tr br ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Substrate label (Sm--s) 
        Subst
        % Line Width at terminal 1 (smorr) Unit: m
        W1
        % Line Width at terminal 2 (smorr) Unit: m
        W2
        % Length (smorr) Unit: m
        L
        % Spacing between strips 1 and 2 (smorr) Unit: m
        S
        % Dielectric Constant Top Layer (smorr) 
        Eru
        % Dielectric Constant Bottom Layer (smorr) 
        Erl
    end
    methods
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Eru(obj,val)
            obj = setParameter(obj,'Eru',val,0,'real');
        end
        function res = get.Eru(obj)
            res = getParameter(obj,'Eru');
        end
        function obj = set.Erl(obj,val)
            obj = setParameter(obj,'Erl',val,0,'real');
        end
        function res = get.Erl(obj)
            res = getParameter(obj,'Erl');
        end
    end
end

classdef ADS_TQFET2 < ADScomponent
    % ADS_TQFET2 matlab representation for the ADS TQFET2 component
    % Triquint GaAs FET2
    % TQFET2 [:Name] td tg ts ...
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % FET type (s---r) 
        Type
        % Gate  Width (smorr) Unit: m
        W
        % Number of Gate Fingers (s---r) 
        N
        % Drain to Source Voltage (smorr) Unit: V
        Vds
        % Gate to Source Voltage (smorr) Unit: V
        Vgs
    end
    methods
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'real');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'real');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Vds(obj,val)
            obj = setParameter(obj,'Vds',val,0,'real');
        end
        function res = get.Vds(obj)
            res = getParameter(obj,'Vds');
        end
        function obj = set.Vgs(obj,val)
            obj = setParameter(obj,'Vgs',val,0,'real');
        end
        function res = get.Vgs(obj)
            res = getParameter(obj,'Vgs');
        end
    end
end

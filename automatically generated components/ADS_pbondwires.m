classdef ADS_pbondwires < ADScomponent
    % ADS_pbondwires matlab representation for the ADS pbondwires component
    % User-defined model
    % pbondwires [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        RW
        % User device parameter (Smorr) Unit: unknown
        COND
        % User device parameter (Smorr) Unit: unknown
        SEPX
        % User device parameter (Smorr) Unit: unknown
        SEPY
        % User device parameter (Smorr) Unit: unknown
        ZOFFSET
        % User device parameter (Smorr) Unit: unknown
        SEPANGLE
        % User device parameter (Smorr) Unit: unknown
        wl
        % User device parameter (Smorr) Unit: unknown
        wdx
        % User device parameter (Smorr) Unit: unknown
        wdy
        % User device parameter (Smorr) Unit: unknown
        wdz
        % User device parameter (Smorr) Unit: unknown
        wda
        % User device parameter (Smorr) Unit: unknown
        sx1
        % User device parameter (Smorr) Unit: unknown
        sx2
        % User device parameter (Smorr) Unit: unknown
        sx3
        % User device parameter (Smorr) Unit: unknown
        sx4
        % User device parameter (Smorr) Unit: unknown
        sx5
        % User device parameter (Smorr) Unit: unknown
        sx6
        % User device parameter (Smorr) Unit: unknown
        sy1
        % User device parameter (Smorr) Unit: unknown
        sy2
        % User device parameter (Smorr) Unit: unknown
        sy3
        % User device parameter (Smorr) Unit: unknown
        sy4
        % User device parameter (Smorr) Unit: unknown
        sy5
        % User device parameter (Smorr) Unit: unknown
        sy6
        % User device parameter (Smorr) Unit: unknown
        sz1
        % User device parameter (Smorr) Unit: unknown
        sz2
        % User device parameter (Smorr) Unit: unknown
        sz3
        % User device parameter (Smorr) Unit: unknown
        sz4
        % User device parameter (Smorr) Unit: unknown
        sz5
        % User device parameter (Smorr) Unit: unknown
        sz6
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.RW(obj,val)
            obj = setParameter(obj,'RW',val,0,'real');
        end
        function res = get.RW(obj)
            res = getParameter(obj,'RW');
        end
        function obj = set.COND(obj,val)
            obj = setParameter(obj,'COND',val,0,'real');
        end
        function res = get.COND(obj)
            res = getParameter(obj,'COND');
        end
        function obj = set.SEPX(obj,val)
            obj = setParameter(obj,'SEPX',val,0,'real');
        end
        function res = get.SEPX(obj)
            res = getParameter(obj,'SEPX');
        end
        function obj = set.SEPY(obj,val)
            obj = setParameter(obj,'SEPY',val,0,'real');
        end
        function res = get.SEPY(obj)
            res = getParameter(obj,'SEPY');
        end
        function obj = set.ZOFFSET(obj,val)
            obj = setParameter(obj,'ZOFFSET',val,0,'real');
        end
        function res = get.ZOFFSET(obj)
            res = getParameter(obj,'ZOFFSET');
        end
        function obj = set.SEPANGLE(obj,val)
            obj = setParameter(obj,'SEPANGLE',val,0,'real');
        end
        function res = get.SEPANGLE(obj)
            res = getParameter(obj,'SEPANGLE');
        end
        function obj = set.wl(obj,val)
            obj = setParameter(obj,'wl',val,0,'real');
        end
        function res = get.wl(obj)
            res = getParameter(obj,'wl');
        end
        function obj = set.wdx(obj,val)
            obj = setParameter(obj,'wdx',val,0,'real');
        end
        function res = get.wdx(obj)
            res = getParameter(obj,'wdx');
        end
        function obj = set.wdy(obj,val)
            obj = setParameter(obj,'wdy',val,0,'real');
        end
        function res = get.wdy(obj)
            res = getParameter(obj,'wdy');
        end
        function obj = set.wdz(obj,val)
            obj = setParameter(obj,'wdz',val,0,'real');
        end
        function res = get.wdz(obj)
            res = getParameter(obj,'wdz');
        end
        function obj = set.wda(obj,val)
            obj = setParameter(obj,'wda',val,0,'real');
        end
        function res = get.wda(obj)
            res = getParameter(obj,'wda');
        end
        function obj = set.sx1(obj,val)
            obj = setParameter(obj,'sx1',val,0,'real');
        end
        function res = get.sx1(obj)
            res = getParameter(obj,'sx1');
        end
        function obj = set.sx2(obj,val)
            obj = setParameter(obj,'sx2',val,0,'real');
        end
        function res = get.sx2(obj)
            res = getParameter(obj,'sx2');
        end
        function obj = set.sx3(obj,val)
            obj = setParameter(obj,'sx3',val,0,'real');
        end
        function res = get.sx3(obj)
            res = getParameter(obj,'sx3');
        end
        function obj = set.sx4(obj,val)
            obj = setParameter(obj,'sx4',val,0,'real');
        end
        function res = get.sx4(obj)
            res = getParameter(obj,'sx4');
        end
        function obj = set.sx5(obj,val)
            obj = setParameter(obj,'sx5',val,0,'real');
        end
        function res = get.sx5(obj)
            res = getParameter(obj,'sx5');
        end
        function obj = set.sx6(obj,val)
            obj = setParameter(obj,'sx6',val,0,'real');
        end
        function res = get.sx6(obj)
            res = getParameter(obj,'sx6');
        end
        function obj = set.sy1(obj,val)
            obj = setParameter(obj,'sy1',val,0,'real');
        end
        function res = get.sy1(obj)
            res = getParameter(obj,'sy1');
        end
        function obj = set.sy2(obj,val)
            obj = setParameter(obj,'sy2',val,0,'real');
        end
        function res = get.sy2(obj)
            res = getParameter(obj,'sy2');
        end
        function obj = set.sy3(obj,val)
            obj = setParameter(obj,'sy3',val,0,'real');
        end
        function res = get.sy3(obj)
            res = getParameter(obj,'sy3');
        end
        function obj = set.sy4(obj,val)
            obj = setParameter(obj,'sy4',val,0,'real');
        end
        function res = get.sy4(obj)
            res = getParameter(obj,'sy4');
        end
        function obj = set.sy5(obj,val)
            obj = setParameter(obj,'sy5',val,0,'real');
        end
        function res = get.sy5(obj)
            res = getParameter(obj,'sy5');
        end
        function obj = set.sy6(obj,val)
            obj = setParameter(obj,'sy6',val,0,'real');
        end
        function res = get.sy6(obj)
            res = getParameter(obj,'sy6');
        end
        function obj = set.sz1(obj,val)
            obj = setParameter(obj,'sz1',val,0,'real');
        end
        function res = get.sz1(obj)
            res = getParameter(obj,'sz1');
        end
        function obj = set.sz2(obj,val)
            obj = setParameter(obj,'sz2',val,0,'real');
        end
        function res = get.sz2(obj)
            res = getParameter(obj,'sz2');
        end
        function obj = set.sz3(obj,val)
            obj = setParameter(obj,'sz3',val,0,'real');
        end
        function res = get.sz3(obj)
            res = getParameter(obj,'sz3');
        end
        function obj = set.sz4(obj,val)
            obj = setParameter(obj,'sz4',val,0,'real');
        end
        function res = get.sz4(obj)
            res = getParameter(obj,'sz4');
        end
        function obj = set.sz5(obj,val)
            obj = setParameter(obj,'sz5',val,0,'real');
        end
        function res = get.sz5(obj)
            res = getParameter(obj,'sz5');
        end
        function obj = set.sz6(obj,val)
            obj = setParameter(obj,'sz6',val,0,'real');
        end
        function res = get.sz6(obj)
            res = getParameter(obj,'sz6');
        end
    end
end

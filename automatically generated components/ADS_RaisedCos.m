classdef ADS_RaisedCos < ADScomponent
    % ADS_RaisedCos matlab representation for the ADS RaisedCos component
    % Raised Cosine Filter
    % RaisedCos [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Rolloff Factor (Smorr) 
        Beta
        % Corner Frequency (smorr) Unit: Hz
        FreqCorner
        % Lower Edge Frequency (smorr) Unit: Hz
        FreqLow
        % Upper Edge Frequency (smorr) Unit: Hz
        FreqHigh
        % Center Frequency (smorr) Unit: Hz
        FreqCenter
        % Bandwidth (smorr) Unit: Hz
        BW_Pass
        % Symbol rate of filter (smorr) Unit: Hz
        SymbolRate
        % Input duty cycle (smorr) 
        InputDutyCycle
        % Yes for applying exponent on sinc(x) correction (sm-ri) 
        SincE
        % exponent factor (Smorr) 
        Exponent
        % filter delay in terms of symbol periods (smorr) 
        DelaySymbols
        % Input impedance of filter (smorr) Unit: Ohms
        Z1
        % Output impedance of filter (smorr) Unit: Ohms
        Z2
        % Maximum rejection in dB (smorr) Unit: dB
        MaxRej
        % Filter type: 'hp', 'lp', 'bs', or 'bp' (S--rs) 
        FilterType
        % Non-causal function impulse response order (sm-ri) Unit: (unknown units)
        ImpNoncausalLength
        % Convolution mode (sm-ri) Unit: (unknown units)
        ImpMode
        % Maximum Frequency (smorr) Unit: (unknown units)
        ImpMaxFreq
        % Sample Frequency (smorr) Unit: (unknown units)
        ImpDeltaFreq
        % maximum allowed impulse response order (sm-ri) Unit: (unknown units)
        ImpMaxPts
        % Smoothing Window (sm-ri) Unit: (unknown units)
        ImpWindow
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpRelTrunc
        % Relative impulse Response truncation factor (smorr) Unit: (unknown units)
        ImpAbsTrunc
    end
    methods
        function obj = set.Beta(obj,val)
            obj = setParameter(obj,'Beta',val,0,'real');
        end
        function res = get.Beta(obj)
            res = getParameter(obj,'Beta');
        end
        function obj = set.FreqCorner(obj,val)
            obj = setParameter(obj,'FreqCorner',val,0,'real');
        end
        function res = get.FreqCorner(obj)
            res = getParameter(obj,'FreqCorner');
        end
        function obj = set.FreqLow(obj,val)
            obj = setParameter(obj,'FreqLow',val,0,'real');
        end
        function res = get.FreqLow(obj)
            res = getParameter(obj,'FreqLow');
        end
        function obj = set.FreqHigh(obj,val)
            obj = setParameter(obj,'FreqHigh',val,0,'real');
        end
        function res = get.FreqHigh(obj)
            res = getParameter(obj,'FreqHigh');
        end
        function obj = set.FreqCenter(obj,val)
            obj = setParameter(obj,'FreqCenter',val,0,'real');
        end
        function res = get.FreqCenter(obj)
            res = getParameter(obj,'FreqCenter');
        end
        function obj = set.BW_Pass(obj,val)
            obj = setParameter(obj,'BW_Pass',val,0,'real');
        end
        function res = get.BW_Pass(obj)
            res = getParameter(obj,'BW_Pass');
        end
        function obj = set.SymbolRate(obj,val)
            obj = setParameter(obj,'SymbolRate',val,0,'real');
        end
        function res = get.SymbolRate(obj)
            res = getParameter(obj,'SymbolRate');
        end
        function obj = set.InputDutyCycle(obj,val)
            obj = setParameter(obj,'InputDutyCycle',val,0,'real');
        end
        function res = get.InputDutyCycle(obj)
            res = getParameter(obj,'InputDutyCycle');
        end
        function obj = set.SincE(obj,val)
            obj = setParameter(obj,'SincE',val,0,'integer');
        end
        function res = get.SincE(obj)
            res = getParameter(obj,'SincE');
        end
        function obj = set.Exponent(obj,val)
            obj = setParameter(obj,'Exponent',val,0,'real');
        end
        function res = get.Exponent(obj)
            res = getParameter(obj,'Exponent');
        end
        function obj = set.DelaySymbols(obj,val)
            obj = setParameter(obj,'DelaySymbols',val,0,'real');
        end
        function res = get.DelaySymbols(obj)
            res = getParameter(obj,'DelaySymbols');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'real');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'real');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.MaxRej(obj,val)
            obj = setParameter(obj,'MaxRej',val,0,'real');
        end
        function res = get.MaxRej(obj)
            res = getParameter(obj,'MaxRej');
        end
        function obj = set.FilterType(obj,val)
            obj = setParameter(obj,'FilterType',val,0,'string');
        end
        function res = get.FilterType(obj)
            res = getParameter(obj,'FilterType');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxPts(obj,val)
            obj = setParameter(obj,'ImpMaxPts',val,0,'integer');
        end
        function res = get.ImpMaxPts(obj)
            res = getParameter(obj,'ImpMaxPts');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTrunc(obj,val)
            obj = setParameter(obj,'ImpRelTrunc',val,0,'real');
        end
        function res = get.ImpRelTrunc(obj)
            res = getParameter(obj,'ImpRelTrunc');
        end
        function obj = set.ImpAbsTrunc(obj,val)
            obj = setParameter(obj,'ImpAbsTrunc',val,0,'real');
        end
        function res = get.ImpAbsTrunc(obj)
            res = getParameter(obj,'ImpAbsTrunc');
        end
    end
end

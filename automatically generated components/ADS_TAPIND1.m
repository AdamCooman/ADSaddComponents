classdef ADS_TAPIND1 < ADScomponent
    % ADS_TAPIND1 matlab representation for the ADS TAPIND1 component
    % Tapped Aircore Inductor (Wire Diameter)
    % TAPIND1 [:Name] n1 n2 n3
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Number of turns between pins 1 and 3 (Smorr) Unit: unknown
        N1
        % Number of turns between pins 2 and 3 (Smorr) Unit: unknown
        N2
        % Diameter of coil (Smorr) Unit: m
        D
        % Length of coil (Smorr) Unit: m
        L
        % Wire diameter (Smorr) Unit: m
        WD
        % Conductor resistivity (rel. to copper) (Smorr) Unit: unknown
        Rho
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.N1(obj,val)
            obj = setParameter(obj,'N1',val,0,'real');
        end
        function res = get.N1(obj)
            res = getParameter(obj,'N1');
        end
        function obj = set.N2(obj,val)
            obj = setParameter(obj,'N2',val,0,'real');
        end
        function res = get.N2(obj)
            res = getParameter(obj,'N2');
        end
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.WD(obj,val)
            obj = setParameter(obj,'WD',val,0,'real');
        end
        function res = get.WD(obj)
            res = getParameter(obj,'WD');
        end
        function obj = set.Rho(obj,val)
            obj = setParameter(obj,'Rho',val,0,'real');
        end
        function res = get.Rho(obj)
            res = getParameter(obj,'Rho');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

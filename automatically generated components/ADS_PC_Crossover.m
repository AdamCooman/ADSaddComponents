classdef ADS_PC_Crossover < ADScomponent
    % ADS_PC_Crossover matlab representation for the ADS PC_Crossover component
    % User-defined model
    % PC_Crossover [:Name] n1 n2 n3 n4 ...
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (S--ri) Unit: unknown
        NumTop
        % User device parameter (S--ri) Unit: unknown
        NumBottom
        % User device parameter (Smorr) Unit: unknown
        W_Top
        % User device parameter (Smorr) Unit: unknown
        S_Top
        % User device parameter (Smorr) Unit: unknown
        LayerTop
        % User device parameter (Smorr) Unit: unknown
        W_Bottom
        % User device parameter (Smorr) Unit: unknown
        S_Bottom
        % User device parameter (Smorr) Unit: unknown
        LayerBottom
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.NumTop(obj,val)
            obj = setParameter(obj,'NumTop',val,0,'integer');
        end
        function res = get.NumTop(obj)
            res = getParameter(obj,'NumTop');
        end
        function obj = set.NumBottom(obj,val)
            obj = setParameter(obj,'NumBottom',val,0,'integer');
        end
        function res = get.NumBottom(obj)
            res = getParameter(obj,'NumBottom');
        end
        function obj = set.W_Top(obj,val)
            obj = setParameter(obj,'W_Top',val,0,'real');
        end
        function res = get.W_Top(obj)
            res = getParameter(obj,'W_Top');
        end
        function obj = set.S_Top(obj,val)
            obj = setParameter(obj,'S_Top',val,0,'real');
        end
        function res = get.S_Top(obj)
            res = getParameter(obj,'S_Top');
        end
        function obj = set.LayerTop(obj,val)
            obj = setParameter(obj,'LayerTop',val,0,'real');
        end
        function res = get.LayerTop(obj)
            res = getParameter(obj,'LayerTop');
        end
        function obj = set.W_Bottom(obj,val)
            obj = setParameter(obj,'W_Bottom',val,0,'real');
        end
        function res = get.W_Bottom(obj)
            res = getParameter(obj,'W_Bottom');
        end
        function obj = set.S_Bottom(obj,val)
            obj = setParameter(obj,'S_Bottom',val,0,'real');
        end
        function res = get.S_Bottom(obj)
            res = getParameter(obj,'S_Bottom');
        end
        function obj = set.LayerBottom(obj,val)
            obj = setParameter(obj,'LayerBottom',val,0,'real');
        end
        function res = get.LayerBottom(obj)
            res = getParameter(obj,'LayerBottom');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

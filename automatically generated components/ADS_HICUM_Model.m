classdef ADS_HICUM_Model < ADSmodel
    % ADS_HICUM_Model matlab representation for the ADS HICUM_Model component
    % HICUM Bipolar Junction Transistor version 2.1 model
    % model ModelName HICUM ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NPN bipolar transistor (s---b) 
        NPN
        % PNP bipolar transistor (s---b) 
        PNP
        % Constant for ICCR (smorr) 
        C10
        % Zero-bias hole charge (smorr) 
        Qp0
        % High-current correction for 2D/3D-ICCR (smorr) Unit: A
        Ich
        % Emiter minority charge weighting factor in HBTs (smorr) 
        Hfe
        % Collector minority charge weighting factor in HBTs (smorr) 
        Hfc
        % B-E depletion charge weighting factor in HBTs (smorr) 
        Hjei
        % B-C depletion charge weighting factor for Qjci in HBTs (smorr) 
        Hjci
        % Forward current emission coefficient (smorr) 
        Mcf
        % Factor for additional delay time of iT (smorr) 
        Alit
        % Zero-bias value (smorr) Unit: F
        Cjei0
        % Built-in voltage (smorr) Unit: V
        Vdei
        % Exponent coefficient (smorr) 
        Zei
        % Factor for adjusting the maximum value of Cjei (smorr) 
        Aljei
        % Zero-bias value (smorr) Unit: F
        Cjci0
        % Built-in voltage (smorr) Unit: V
        Vdci
        % Exponent cefficient (smorr) 
        Zci
        % Punch-through voltage (smorr) Unit: V
        Vptci
        % Low-current transit time at VBC=0 (smorr) Unit: s
        T0
        % Time constant for base and BC SCR width modulation (smorr) Unit: s
        Dt0h
        % Voltage (carrier jam) (smorr) Unit: s
        Tbvl
        % Storage time in neutral emitter (smorr) Unit: s
        Tef0
        % Exponent factor for current dependent emitter transit time (smorr) 
        Gtfe
        % Saturation time constant at high current densities (smorr) Unit: s
        Thcs
        % Smoothing factor for current dependent C and B transit time (smorr) 
        Alhc
        % Partitioning factor for base and collector portion (smorr) 
        Fthc
        % Factor for additional delay time of Qf (smorr) 
        Alqf
        % Low-field resistance of internal collector region (incl. scaling) (smorr) Unit: Ohms
        Rci0
        % Limitation voltage (separating ohmic and SCR regime) (smorr) Unit: V
        Vlim
        % Punch-through voltage of BS SCR through (epi)collector (smorr) Unit: V
        Vpt
        % Internal CE saturation voltage (smorr) Unit: V
        Vces
        % Time constant (smorr) Unit: s
        Tr
        % Internal BE saturation current (smorr) Unit: A
        Ibeis
        % Internal BE non-ideality factor (smorr) 
        Mbei
        % Internal BE recombination saturation current (smorr) Unit: A
        Ireis
        % Internal BE recombination non-ideality factor (smorr) 
        Mrei
        % Internal BC saturation current (smorr) Unit: A
        Ibcis
        % Internal BC non-ideality factor (smorr) 
        Mbci
        % Pre-factor for Avalanche effect (smorr) Unit: V^-1
        Favl
        % Exponent factor for Avalanche effect (smorr) 
        Qavl
        % Value at zero-bias (smorr) Unit: Ohms
        Rbi0
        % Correction factor for modulation (smorr) 
        Fdqr0
        % Geometry factor (default value corr. to long emitter stripe) (smorr) 
        Fgeo
        % Ratio of internal to total minority charge (smorr) 
        Fqi
        % Ratio of h.f.shunt to total internal capacitance (smorr) 
        Fcrbi
        % Scaling factor for (collector) minority charge in direction of bE (smorr) 
        Latb
        % Scaling factor for (collector) minority charge in direction of lE (smorr) 
        Latl
        % Zero-bias value (smorr) Unit: F
        Cjep0
        % Built-in voltage (smorr) Unit: V
        Vdep
        % Depletion coefficient (smorr) 
        Zep
        % Factor for adjusting maximum value of Cjep (smorr) 
        Aljep
        % Saturation current (smorr) Unit: A
        Ibeps
        % Non-ideality factor (smorr) 
        Mbep
        % Recombination saturation current (smorr) Unit: A
        Ireps
        % Recombination non-ideality factor (smorr) 
        Mrep
        % Saturation current (smorr) Unit: A
        Ibets
        % Exponent coefficient (smorr) 
        Abet
        % Zero-bias depletion value (smorr) Unit: F
        Cjcx0
        % Built-in voltage (smorr) Unit: V
        Vdcx
        % Exponent coefficient (smorr) 
        Zcx
        % Punch-through voltage (smorr) Unit: V
        Vptcx
        % Collector-oxide capacitance (smorr) Unit: F
        Ccox
        % Partitioning factor for Ccbx (smorr) 
        Fbc
        % Saturation current (smorr) Unit: A
        Ibcxs
        % Non-ideality factor (smorr) 
        Mbcx
        % Emitter oxide(overlap) capacitance (smorr) Unit: F
        Ceox
        % External base series resistance (smorr) Unit: Ohms
        Rbx
        % Emitter series resistance (smorr) Unit: Ohms
        Re
        % External collector series resistance (smorr) Unit: Ohms
        Rcx
        % Transfer saturation current (smorr) Unit: A
        Itss
        % Non-ideality factor for forward transfer current component (smorr) 
        Msf
        % Non-ideality factor for inverse transfer current component (smorr) 
        Msr
        % Saturation current of CS diode  (latch-up modeling) (smorr) Unit: A
        Iscs
        % Non-ideality factor of CS diode (smorr) 
        Msc
        % Storage time (constant) for minority charge (smorr) 
        Tsf
        % Zero-bias value of CS depletion capacitance (smorr) Unit: F
        Cjs0
        % Built-in voltage (smorr) Unit: V
        Vds
        % Exponent coefficient (smorr) 
        Zs
        % Punch-through voltage (smorr) Unit: V
        Vpts
        % Substrate series resistance (smorr) Unit: Ohms
        Rsu
        % Substrate cap. given by permittivity of bulk material (smorr) Unit: F
        Csu
        % Flicker noise factor (no unit only for Af=2) (smorr) 
        Kf
        % Flicker noise exponent (smorr) 
        Af
        % Factor for internal base resistance (smorr) 
        Krbi
        % Bandgap-voltage (smorr) Unit: V
        Vgb
        % Relative temperature coefficient of current gain (smorr) Unit: K^-1
        Alb
        % Temperature coefficient (mobility) for epi-collector (smorr) 
        Zetaci
        % Relative temperature coefficient of satur. drift velocity (smorr) Unit: K^-1
        Alvs
        % Temp. coeff. for low-current transit time t0 (lin.term) (smorr) Unit: K^-1
        Alt0
        % Temp. coeff. for low-current transit time t0 (quad.term) (smorr) Unit: K^-1
        Kt0
        % Relative temperature coefficient of Vces (smorr) Unit: K^-1
        Alces
        % Temperature coefficient (mobility) internal base resistance (smorr) 
        Zetarbi
        % Temperature coefficient (mobility) external base resistance (smorr) 
        Zetarbx
        % Temperature coefficient (mobility) external collector resistance (smorr) 
        Zetarcx
        % Temperature coefficient of emitter resistance (smorr) 
        Zetare
        % Relative temperature coefficient for Favl (smorr) Unit: K^-1
        Alfav
        % Relative temperature coefficient for Qavl (smorr) Unit: K^-1
        Alqav
        % Temperature for which parameters are valid (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Thermal resistance (smorr) Unit: Ohms
        Rth
        % Thermal capacitance (smorr) Unit: F
        Cth
        % Explosion current (smorr) Unit: A
        Imax
        % Explosion current (smorr) Unit: A
        Imelt
        % Substrate junction forward bias (warning) (s--rr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvsub
        % Base-emitter reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbe
        % Base-collector reverse breakdown voltage (warning) (s--rr) Unit: V
        wBvbc
        % Base-collector forward bias (warning) (s--rr) Unit: V
        wVbcfwd
        % Maximum base current (warning) (s--rr) Unit: A
        wIbmax
        % Maximum collector current (warning) (s--rr) Unit: A
        wIcmax
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Small signal (= 1), consistent with charge model (= 2) (s---i) 
        AcModel
        % 1: Spectre, 2: Eldo (s---i) 
        ModelLevel
        % Dummy NOT in UI (s---i) 
        Gender
        % Simplified model (= 1), full model (= 2), spv210u2 (=3) (s---i) 
        SelfheatingModel
        % Modele Approximation Level: 0=no approximation, 1=some (s---i) 
        ApproxLevel
    end
    methods
        function obj = set.NPN(obj,val)
            obj = setParameter(obj,'NPN',val,0,'boolean');
        end
        function res = get.NPN(obj)
            res = getParameter(obj,'NPN');
        end
        function obj = set.PNP(obj,val)
            obj = setParameter(obj,'PNP',val,0,'boolean');
        end
        function res = get.PNP(obj)
            res = getParameter(obj,'PNP');
        end
        function obj = set.C10(obj,val)
            obj = setParameter(obj,'C10',val,0,'real');
        end
        function res = get.C10(obj)
            res = getParameter(obj,'C10');
        end
        function obj = set.Qp0(obj,val)
            obj = setParameter(obj,'Qp0',val,0,'real');
        end
        function res = get.Qp0(obj)
            res = getParameter(obj,'Qp0');
        end
        function obj = set.Ich(obj,val)
            obj = setParameter(obj,'Ich',val,0,'real');
        end
        function res = get.Ich(obj)
            res = getParameter(obj,'Ich');
        end
        function obj = set.Hfe(obj,val)
            obj = setParameter(obj,'Hfe',val,0,'real');
        end
        function res = get.Hfe(obj)
            res = getParameter(obj,'Hfe');
        end
        function obj = set.Hfc(obj,val)
            obj = setParameter(obj,'Hfc',val,0,'real');
        end
        function res = get.Hfc(obj)
            res = getParameter(obj,'Hfc');
        end
        function obj = set.Hjei(obj,val)
            obj = setParameter(obj,'Hjei',val,0,'real');
        end
        function res = get.Hjei(obj)
            res = getParameter(obj,'Hjei');
        end
        function obj = set.Hjci(obj,val)
            obj = setParameter(obj,'Hjci',val,0,'real');
        end
        function res = get.Hjci(obj)
            res = getParameter(obj,'Hjci');
        end
        function obj = set.Mcf(obj,val)
            obj = setParameter(obj,'Mcf',val,0,'real');
        end
        function res = get.Mcf(obj)
            res = getParameter(obj,'Mcf');
        end
        function obj = set.Alit(obj,val)
            obj = setParameter(obj,'Alit',val,0,'real');
        end
        function res = get.Alit(obj)
            res = getParameter(obj,'Alit');
        end
        function obj = set.Cjei0(obj,val)
            obj = setParameter(obj,'Cjei0',val,0,'real');
        end
        function res = get.Cjei0(obj)
            res = getParameter(obj,'Cjei0');
        end
        function obj = set.Vdei(obj,val)
            obj = setParameter(obj,'Vdei',val,0,'real');
        end
        function res = get.Vdei(obj)
            res = getParameter(obj,'Vdei');
        end
        function obj = set.Zei(obj,val)
            obj = setParameter(obj,'Zei',val,0,'real');
        end
        function res = get.Zei(obj)
            res = getParameter(obj,'Zei');
        end
        function obj = set.Aljei(obj,val)
            obj = setParameter(obj,'Aljei',val,0,'real');
        end
        function res = get.Aljei(obj)
            res = getParameter(obj,'Aljei');
        end
        function obj = set.Cjci0(obj,val)
            obj = setParameter(obj,'Cjci0',val,0,'real');
        end
        function res = get.Cjci0(obj)
            res = getParameter(obj,'Cjci0');
        end
        function obj = set.Vdci(obj,val)
            obj = setParameter(obj,'Vdci',val,0,'real');
        end
        function res = get.Vdci(obj)
            res = getParameter(obj,'Vdci');
        end
        function obj = set.Zci(obj,val)
            obj = setParameter(obj,'Zci',val,0,'real');
        end
        function res = get.Zci(obj)
            res = getParameter(obj,'Zci');
        end
        function obj = set.Vptci(obj,val)
            obj = setParameter(obj,'Vptci',val,0,'real');
        end
        function res = get.Vptci(obj)
            res = getParameter(obj,'Vptci');
        end
        function obj = set.T0(obj,val)
            obj = setParameter(obj,'T0',val,0,'real');
        end
        function res = get.T0(obj)
            res = getParameter(obj,'T0');
        end
        function obj = set.Dt0h(obj,val)
            obj = setParameter(obj,'Dt0h',val,0,'real');
        end
        function res = get.Dt0h(obj)
            res = getParameter(obj,'Dt0h');
        end
        function obj = set.Tbvl(obj,val)
            obj = setParameter(obj,'Tbvl',val,0,'real');
        end
        function res = get.Tbvl(obj)
            res = getParameter(obj,'Tbvl');
        end
        function obj = set.Tef0(obj,val)
            obj = setParameter(obj,'Tef0',val,0,'real');
        end
        function res = get.Tef0(obj)
            res = getParameter(obj,'Tef0');
        end
        function obj = set.Gtfe(obj,val)
            obj = setParameter(obj,'Gtfe',val,0,'real');
        end
        function res = get.Gtfe(obj)
            res = getParameter(obj,'Gtfe');
        end
        function obj = set.Thcs(obj,val)
            obj = setParameter(obj,'Thcs',val,0,'real');
        end
        function res = get.Thcs(obj)
            res = getParameter(obj,'Thcs');
        end
        function obj = set.Alhc(obj,val)
            obj = setParameter(obj,'Alhc',val,0,'real');
        end
        function res = get.Alhc(obj)
            res = getParameter(obj,'Alhc');
        end
        function obj = set.Fthc(obj,val)
            obj = setParameter(obj,'Fthc',val,0,'real');
        end
        function res = get.Fthc(obj)
            res = getParameter(obj,'Fthc');
        end
        function obj = set.Alqf(obj,val)
            obj = setParameter(obj,'Alqf',val,0,'real');
        end
        function res = get.Alqf(obj)
            res = getParameter(obj,'Alqf');
        end
        function obj = set.Rci0(obj,val)
            obj = setParameter(obj,'Rci0',val,0,'real');
        end
        function res = get.Rci0(obj)
            res = getParameter(obj,'Rci0');
        end
        function obj = set.Vlim(obj,val)
            obj = setParameter(obj,'Vlim',val,0,'real');
        end
        function res = get.Vlim(obj)
            res = getParameter(obj,'Vlim');
        end
        function obj = set.Vpt(obj,val)
            obj = setParameter(obj,'Vpt',val,0,'real');
        end
        function res = get.Vpt(obj)
            res = getParameter(obj,'Vpt');
        end
        function obj = set.Vces(obj,val)
            obj = setParameter(obj,'Vces',val,0,'real');
        end
        function res = get.Vces(obj)
            res = getParameter(obj,'Vces');
        end
        function obj = set.Tr(obj,val)
            obj = setParameter(obj,'Tr',val,0,'real');
        end
        function res = get.Tr(obj)
            res = getParameter(obj,'Tr');
        end
        function obj = set.Ibeis(obj,val)
            obj = setParameter(obj,'Ibeis',val,0,'real');
        end
        function res = get.Ibeis(obj)
            res = getParameter(obj,'Ibeis');
        end
        function obj = set.Mbei(obj,val)
            obj = setParameter(obj,'Mbei',val,0,'real');
        end
        function res = get.Mbei(obj)
            res = getParameter(obj,'Mbei');
        end
        function obj = set.Ireis(obj,val)
            obj = setParameter(obj,'Ireis',val,0,'real');
        end
        function res = get.Ireis(obj)
            res = getParameter(obj,'Ireis');
        end
        function obj = set.Mrei(obj,val)
            obj = setParameter(obj,'Mrei',val,0,'real');
        end
        function res = get.Mrei(obj)
            res = getParameter(obj,'Mrei');
        end
        function obj = set.Ibcis(obj,val)
            obj = setParameter(obj,'Ibcis',val,0,'real');
        end
        function res = get.Ibcis(obj)
            res = getParameter(obj,'Ibcis');
        end
        function obj = set.Mbci(obj,val)
            obj = setParameter(obj,'Mbci',val,0,'real');
        end
        function res = get.Mbci(obj)
            res = getParameter(obj,'Mbci');
        end
        function obj = set.Favl(obj,val)
            obj = setParameter(obj,'Favl',val,0,'real');
        end
        function res = get.Favl(obj)
            res = getParameter(obj,'Favl');
        end
        function obj = set.Qavl(obj,val)
            obj = setParameter(obj,'Qavl',val,0,'real');
        end
        function res = get.Qavl(obj)
            res = getParameter(obj,'Qavl');
        end
        function obj = set.Rbi0(obj,val)
            obj = setParameter(obj,'Rbi0',val,0,'real');
        end
        function res = get.Rbi0(obj)
            res = getParameter(obj,'Rbi0');
        end
        function obj = set.Fdqr0(obj,val)
            obj = setParameter(obj,'Fdqr0',val,0,'real');
        end
        function res = get.Fdqr0(obj)
            res = getParameter(obj,'Fdqr0');
        end
        function obj = set.Fgeo(obj,val)
            obj = setParameter(obj,'Fgeo',val,0,'real');
        end
        function res = get.Fgeo(obj)
            res = getParameter(obj,'Fgeo');
        end
        function obj = set.Fqi(obj,val)
            obj = setParameter(obj,'Fqi',val,0,'real');
        end
        function res = get.Fqi(obj)
            res = getParameter(obj,'Fqi');
        end
        function obj = set.Fcrbi(obj,val)
            obj = setParameter(obj,'Fcrbi',val,0,'real');
        end
        function res = get.Fcrbi(obj)
            res = getParameter(obj,'Fcrbi');
        end
        function obj = set.Latb(obj,val)
            obj = setParameter(obj,'Latb',val,0,'real');
        end
        function res = get.Latb(obj)
            res = getParameter(obj,'Latb');
        end
        function obj = set.Latl(obj,val)
            obj = setParameter(obj,'Latl',val,0,'real');
        end
        function res = get.Latl(obj)
            res = getParameter(obj,'Latl');
        end
        function obj = set.Cjep0(obj,val)
            obj = setParameter(obj,'Cjep0',val,0,'real');
        end
        function res = get.Cjep0(obj)
            res = getParameter(obj,'Cjep0');
        end
        function obj = set.Vdep(obj,val)
            obj = setParameter(obj,'Vdep',val,0,'real');
        end
        function res = get.Vdep(obj)
            res = getParameter(obj,'Vdep');
        end
        function obj = set.Zep(obj,val)
            obj = setParameter(obj,'Zep',val,0,'real');
        end
        function res = get.Zep(obj)
            res = getParameter(obj,'Zep');
        end
        function obj = set.Aljep(obj,val)
            obj = setParameter(obj,'Aljep',val,0,'real');
        end
        function res = get.Aljep(obj)
            res = getParameter(obj,'Aljep');
        end
        function obj = set.Ibeps(obj,val)
            obj = setParameter(obj,'Ibeps',val,0,'real');
        end
        function res = get.Ibeps(obj)
            res = getParameter(obj,'Ibeps');
        end
        function obj = set.Mbep(obj,val)
            obj = setParameter(obj,'Mbep',val,0,'real');
        end
        function res = get.Mbep(obj)
            res = getParameter(obj,'Mbep');
        end
        function obj = set.Ireps(obj,val)
            obj = setParameter(obj,'Ireps',val,0,'real');
        end
        function res = get.Ireps(obj)
            res = getParameter(obj,'Ireps');
        end
        function obj = set.Mrep(obj,val)
            obj = setParameter(obj,'Mrep',val,0,'real');
        end
        function res = get.Mrep(obj)
            res = getParameter(obj,'Mrep');
        end
        function obj = set.Ibets(obj,val)
            obj = setParameter(obj,'Ibets',val,0,'real');
        end
        function res = get.Ibets(obj)
            res = getParameter(obj,'Ibets');
        end
        function obj = set.Abet(obj,val)
            obj = setParameter(obj,'Abet',val,0,'real');
        end
        function res = get.Abet(obj)
            res = getParameter(obj,'Abet');
        end
        function obj = set.Cjcx0(obj,val)
            obj = setParameter(obj,'Cjcx0',val,0,'real');
        end
        function res = get.Cjcx0(obj)
            res = getParameter(obj,'Cjcx0');
        end
        function obj = set.Vdcx(obj,val)
            obj = setParameter(obj,'Vdcx',val,0,'real');
        end
        function res = get.Vdcx(obj)
            res = getParameter(obj,'Vdcx');
        end
        function obj = set.Zcx(obj,val)
            obj = setParameter(obj,'Zcx',val,0,'real');
        end
        function res = get.Zcx(obj)
            res = getParameter(obj,'Zcx');
        end
        function obj = set.Vptcx(obj,val)
            obj = setParameter(obj,'Vptcx',val,0,'real');
        end
        function res = get.Vptcx(obj)
            res = getParameter(obj,'Vptcx');
        end
        function obj = set.Ccox(obj,val)
            obj = setParameter(obj,'Ccox',val,0,'real');
        end
        function res = get.Ccox(obj)
            res = getParameter(obj,'Ccox');
        end
        function obj = set.Fbc(obj,val)
            obj = setParameter(obj,'Fbc',val,0,'real');
        end
        function res = get.Fbc(obj)
            res = getParameter(obj,'Fbc');
        end
        function obj = set.Ibcxs(obj,val)
            obj = setParameter(obj,'Ibcxs',val,0,'real');
        end
        function res = get.Ibcxs(obj)
            res = getParameter(obj,'Ibcxs');
        end
        function obj = set.Mbcx(obj,val)
            obj = setParameter(obj,'Mbcx',val,0,'real');
        end
        function res = get.Mbcx(obj)
            res = getParameter(obj,'Mbcx');
        end
        function obj = set.Ceox(obj,val)
            obj = setParameter(obj,'Ceox',val,0,'real');
        end
        function res = get.Ceox(obj)
            res = getParameter(obj,'Ceox');
        end
        function obj = set.Rbx(obj,val)
            obj = setParameter(obj,'Rbx',val,0,'real');
        end
        function res = get.Rbx(obj)
            res = getParameter(obj,'Rbx');
        end
        function obj = set.Re(obj,val)
            obj = setParameter(obj,'Re',val,0,'real');
        end
        function res = get.Re(obj)
            res = getParameter(obj,'Re');
        end
        function obj = set.Rcx(obj,val)
            obj = setParameter(obj,'Rcx',val,0,'real');
        end
        function res = get.Rcx(obj)
            res = getParameter(obj,'Rcx');
        end
        function obj = set.Itss(obj,val)
            obj = setParameter(obj,'Itss',val,0,'real');
        end
        function res = get.Itss(obj)
            res = getParameter(obj,'Itss');
        end
        function obj = set.Msf(obj,val)
            obj = setParameter(obj,'Msf',val,0,'real');
        end
        function res = get.Msf(obj)
            res = getParameter(obj,'Msf');
        end
        function obj = set.Msr(obj,val)
            obj = setParameter(obj,'Msr',val,0,'real');
        end
        function res = get.Msr(obj)
            res = getParameter(obj,'Msr');
        end
        function obj = set.Iscs(obj,val)
            obj = setParameter(obj,'Iscs',val,0,'real');
        end
        function res = get.Iscs(obj)
            res = getParameter(obj,'Iscs');
        end
        function obj = set.Msc(obj,val)
            obj = setParameter(obj,'Msc',val,0,'real');
        end
        function res = get.Msc(obj)
            res = getParameter(obj,'Msc');
        end
        function obj = set.Tsf(obj,val)
            obj = setParameter(obj,'Tsf',val,0,'real');
        end
        function res = get.Tsf(obj)
            res = getParameter(obj,'Tsf');
        end
        function obj = set.Cjs0(obj,val)
            obj = setParameter(obj,'Cjs0',val,0,'real');
        end
        function res = get.Cjs0(obj)
            res = getParameter(obj,'Cjs0');
        end
        function obj = set.Vds(obj,val)
            obj = setParameter(obj,'Vds',val,0,'real');
        end
        function res = get.Vds(obj)
            res = getParameter(obj,'Vds');
        end
        function obj = set.Zs(obj,val)
            obj = setParameter(obj,'Zs',val,0,'real');
        end
        function res = get.Zs(obj)
            res = getParameter(obj,'Zs');
        end
        function obj = set.Vpts(obj,val)
            obj = setParameter(obj,'Vpts',val,0,'real');
        end
        function res = get.Vpts(obj)
            res = getParameter(obj,'Vpts');
        end
        function obj = set.Rsu(obj,val)
            obj = setParameter(obj,'Rsu',val,0,'real');
        end
        function res = get.Rsu(obj)
            res = getParameter(obj,'Rsu');
        end
        function obj = set.Csu(obj,val)
            obj = setParameter(obj,'Csu',val,0,'real');
        end
        function res = get.Csu(obj)
            res = getParameter(obj,'Csu');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Krbi(obj,val)
            obj = setParameter(obj,'Krbi',val,0,'real');
        end
        function res = get.Krbi(obj)
            res = getParameter(obj,'Krbi');
        end
        function obj = set.Vgb(obj,val)
            obj = setParameter(obj,'Vgb',val,0,'real');
        end
        function res = get.Vgb(obj)
            res = getParameter(obj,'Vgb');
        end
        function obj = set.Alb(obj,val)
            obj = setParameter(obj,'Alb',val,0,'real');
        end
        function res = get.Alb(obj)
            res = getParameter(obj,'Alb');
        end
        function obj = set.Zetaci(obj,val)
            obj = setParameter(obj,'Zetaci',val,0,'real');
        end
        function res = get.Zetaci(obj)
            res = getParameter(obj,'Zetaci');
        end
        function obj = set.Alvs(obj,val)
            obj = setParameter(obj,'Alvs',val,0,'real');
        end
        function res = get.Alvs(obj)
            res = getParameter(obj,'Alvs');
        end
        function obj = set.Alt0(obj,val)
            obj = setParameter(obj,'Alt0',val,0,'real');
        end
        function res = get.Alt0(obj)
            res = getParameter(obj,'Alt0');
        end
        function obj = set.Kt0(obj,val)
            obj = setParameter(obj,'Kt0',val,0,'real');
        end
        function res = get.Kt0(obj)
            res = getParameter(obj,'Kt0');
        end
        function obj = set.Alces(obj,val)
            obj = setParameter(obj,'Alces',val,0,'real');
        end
        function res = get.Alces(obj)
            res = getParameter(obj,'Alces');
        end
        function obj = set.Zetarbi(obj,val)
            obj = setParameter(obj,'Zetarbi',val,0,'real');
        end
        function res = get.Zetarbi(obj)
            res = getParameter(obj,'Zetarbi');
        end
        function obj = set.Zetarbx(obj,val)
            obj = setParameter(obj,'Zetarbx',val,0,'real');
        end
        function res = get.Zetarbx(obj)
            res = getParameter(obj,'Zetarbx');
        end
        function obj = set.Zetarcx(obj,val)
            obj = setParameter(obj,'Zetarcx',val,0,'real');
        end
        function res = get.Zetarcx(obj)
            res = getParameter(obj,'Zetarcx');
        end
        function obj = set.Zetare(obj,val)
            obj = setParameter(obj,'Zetare',val,0,'real');
        end
        function res = get.Zetare(obj)
            res = getParameter(obj,'Zetare');
        end
        function obj = set.Alfav(obj,val)
            obj = setParameter(obj,'Alfav',val,0,'real');
        end
        function res = get.Alfav(obj)
            res = getParameter(obj,'Alfav');
        end
        function obj = set.Alqav(obj,val)
            obj = setParameter(obj,'Alqav',val,0,'real');
        end
        function res = get.Alqav(obj)
            res = getParameter(obj,'Alqav');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Rth(obj,val)
            obj = setParameter(obj,'Rth',val,0,'real');
        end
        function res = get.Rth(obj)
            res = getParameter(obj,'Rth');
        end
        function obj = set.Cth(obj,val)
            obj = setParameter(obj,'Cth',val,0,'real');
        end
        function res = get.Cth(obj)
            res = getParameter(obj,'Cth');
        end
        function obj = set.Imax(obj,val)
            obj = setParameter(obj,'Imax',val,0,'real');
        end
        function res = get.Imax(obj)
            res = getParameter(obj,'Imax');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvbe(obj,val)
            obj = setParameter(obj,'wBvbe',val,0,'real');
        end
        function res = get.wBvbe(obj)
            res = getParameter(obj,'wBvbe');
        end
        function obj = set.wBvbc(obj,val)
            obj = setParameter(obj,'wBvbc',val,0,'real');
        end
        function res = get.wBvbc(obj)
            res = getParameter(obj,'wBvbc');
        end
        function obj = set.wVbcfwd(obj,val)
            obj = setParameter(obj,'wVbcfwd',val,0,'real');
        end
        function res = get.wVbcfwd(obj)
            res = getParameter(obj,'wVbcfwd');
        end
        function obj = set.wIbmax(obj,val)
            obj = setParameter(obj,'wIbmax',val,0,'real');
        end
        function res = get.wIbmax(obj)
            res = getParameter(obj,'wIbmax');
        end
        function obj = set.wIcmax(obj,val)
            obj = setParameter(obj,'wIcmax',val,0,'real');
        end
        function res = get.wIcmax(obj)
            res = getParameter(obj,'wIcmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.AcModel(obj,val)
            obj = setParameter(obj,'AcModel',val,0,'integer');
        end
        function res = get.AcModel(obj)
            res = getParameter(obj,'AcModel');
        end
        function obj = set.ModelLevel(obj,val)
            obj = setParameter(obj,'ModelLevel',val,0,'integer');
        end
        function res = get.ModelLevel(obj)
            res = getParameter(obj,'ModelLevel');
        end
        function obj = set.Gender(obj,val)
            obj = setParameter(obj,'Gender',val,0,'integer');
        end
        function res = get.Gender(obj)
            res = getParameter(obj,'Gender');
        end
        function obj = set.SelfheatingModel(obj,val)
            obj = setParameter(obj,'SelfheatingModel',val,0,'integer');
        end
        function res = get.SelfheatingModel(obj)
            res = getParameter(obj,'SelfheatingModel');
        end
        function obj = set.ApproxLevel(obj,val)
            obj = setParameter(obj,'ApproxLevel',val,0,'integer');
        end
        function res = get.ApproxLevel(obj)
            res = getParameter(obj,'ApproxLevel');
        end
    end
end

classdef ADS_SpecLimitLine < ADSnodeless
    % ADS_SpecLimitLine matlab representation for the ADS SpecLimitLine component
    % Specification limit line
    % SpecLimitLine [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Limit line name (s--rs) 
        Name
        % Type of limit line (s--rs) 
        Type
        % Target Min (smorr) 
        Min
        % Target Max (smorr) 
        Max
        % Weight factor (smorr) 
        Weight
        % name of range variable (s---s) 
        RangeVar
        % minimum acceptable value for range variable (s---r) 
        RangeMin
        % maximum acceptable value for range variable (s---r) 
        RangeMax
        % name of range variable (s---s) 
        IndepVar
        % minimum acceptable value for range variable (s---r) 
        IndepMin
        % maximum acceptable value for range variable (s---r) 
        IndepMax
    end
    methods
        function obj = set.Name(obj,val)
            obj = setParameter(obj,'Name',val,0,'string');
        end
        function res = get.Name(obj)
            res = getParameter(obj,'Name');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Min(obj,val)
            obj = setParameter(obj,'Min',val,0,'real');
        end
        function res = get.Min(obj)
            res = getParameter(obj,'Min');
        end
        function obj = set.Max(obj,val)
            obj = setParameter(obj,'Max',val,0,'real');
        end
        function res = get.Max(obj)
            res = getParameter(obj,'Max');
        end
        function obj = set.Weight(obj,val)
            obj = setParameter(obj,'Weight',val,0,'real');
        end
        function res = get.Weight(obj)
            res = getParameter(obj,'Weight');
        end
        function obj = set.RangeVar(obj,val)
            obj = setParameter(obj,'RangeVar',val,1,'string');
        end
        function res = get.RangeVar(obj)
            res = getParameter(obj,'RangeVar');
        end
        function obj = set.RangeMin(obj,val)
            obj = setParameter(obj,'RangeMin',val,1,'real');
        end
        function res = get.RangeMin(obj)
            res = getParameter(obj,'RangeMin');
        end
        function obj = set.RangeMax(obj,val)
            obj = setParameter(obj,'RangeMax',val,1,'real');
        end
        function res = get.RangeMax(obj)
            res = getParameter(obj,'RangeMax');
        end
        function obj = set.IndepVar(obj,val)
            obj = setParameter(obj,'IndepVar',val,1,'string');
        end
        function res = get.IndepVar(obj)
            res = getParameter(obj,'IndepVar');
        end
        function obj = set.IndepMin(obj,val)
            obj = setParameter(obj,'IndepMin',val,1,'real');
        end
        function res = get.IndepMin(obj)
            res = getParameter(obj,'IndepMin');
        end
        function obj = set.IndepMax(obj,val)
            obj = setParameter(obj,'IndepMax',val,1,'real');
        end
        function res = get.IndepMax(obj)
            res = getParameter(obj,'IndepMax');
        end
    end
end

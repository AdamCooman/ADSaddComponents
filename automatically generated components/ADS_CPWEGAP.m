classdef ADS_CPWEGAP < ADScomponent
    % ADS_CPWEGAP matlab representation for the ADS CPWEGAP component
    % Coplanar waveguide, End gap
    % CPWEGAP [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Center conductor width (Smorr) Unit: m
        W
        % Gap (spacing) between center conductor and ground plane (Smorr) Unit: m
        G
        % Gap between end of center conductor and ground plane (Smorr) Unit: m
        S
        % Center conductor length (Smorr) Unit: m
        L
        % Coplanar waveguide substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.G(obj,val)
            obj = setParameter(obj,'G',val,0,'real');
        end
        function res = get.G(obj)
            res = getParameter(obj,'G');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_ACPWTL < ADScomponent
    % ADS_ACPWTL matlab representation for the ADS ACPWTL component
    % Asymmetry Coplanar transmission line
    % ACPWTL [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Width of asymmetry Coplanar transmission line (Smorr) Unit: m
        W
        % Space 1 of asymmetry Coplanar transmission line (Smorr) Unit: m
        S1
        % Space 2 of asymmetry Coplanar transmission line (Smorr) Unit: m
        S2
        % Length of asymmetry Coplanar transmission line (Smorr) Unit: m
        L
        % Substrate label (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.S2(obj,val)
            obj = setParameter(obj,'S2',val,0,'real');
        end
        function res = get.S2(obj)
            res = getParameter(obj,'S2');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

classdef ADS_PIN2 < ADScomponent
    % ADS_PIN2 matlab representation for the ADS PIN2 component
    % pin2 Diode, Packaged Model
    % PIN2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Junction capacitance (Smorr) Unit: f
        Cj
        % Junction resistance (Smorr) Unit: ohm
        Rj
        % Diode series resistance (Smorr) Unit: ohm
        Rs
        % Diode series inductance (Smorr) Unit: h
        Ls
        % Package capacitance (Smorr) Unit: f
        Cp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.Cj(obj,val)
            obj = setParameter(obj,'Cj',val,0,'real');
        end
        function res = get.Cj(obj)
            res = getParameter(obj,'Cj');
        end
        function obj = set.Rj(obj,val)
            obj = setParameter(obj,'Rj',val,0,'real');
        end
        function res = get.Rj(obj)
            res = getParameter(obj,'Rj');
        end
        function obj = set.Rs(obj,val)
            obj = setParameter(obj,'Rs',val,0,'real');
        end
        function res = get.Rs(obj)
            res = getParameter(obj,'Rs');
        end
        function obj = set.Ls(obj,val)
            obj = setParameter(obj,'Ls',val,0,'real');
        end
        function res = get.Ls(obj)
            res = getParameter(obj,'Ls');
        end
        function obj = set.Cp(obj,val)
            obj = setParameter(obj,'Cp',val,0,'real');
        end
        function res = get.Cp(obj)
            res = getParameter(obj,'Cp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

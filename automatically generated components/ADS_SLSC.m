classdef ADS_SLSC < ADScomponent
    % ADS_SLSC matlab representation for the ADS SLSC component
    % Stripline Short-Circuited Stub
    % SLSC [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Line width (Smorr) Unit: m
        W
        % Line length (Smorr) Unit: m
        L
        % Stripline substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_MACLIN3 < ADScomponent
    % ADS_MACLIN3 matlab representation for the ADS MACLIN3 component
    % Microstrip 3-Conductor Asymmetric Coupled Lines
    % MACLIN3 [:Name] n1 n2 n3 n4 n5 n6
    properties (Access=protected)
        NumberOfNodes = 6
    end
    properties (Dependent)
        % Width of Conductor 1 (Smorr) Unit: m
        W1
        % Width of Conductor 2 (Smorr) Unit: m
        W2
        % Width of Conductor 3 (Smorr) Unit: m
        W3
        % Spacing between Conductors 1 and 2 (Smorr) Unit: m
        S1
        % Spacing between Conductors 2 and 3 (Smorr) Unit: m
        S2
        % Conductor Length (Smorr) Unit: m
        L
        % Microstrip Substrate (Sm-rs) Unit: unknown
        Subst
        % Physical Temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.S2(obj,val)
            obj = setParameter(obj,'S2',val,0,'real');
        end
        function res = get.S2(obj)
            res = getParameter(obj,'S2');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

classdef ADS_BSIM4_Model < ADSmodel
    % ADS_BSIM4_Model matlab representation for the ADS BSIM4_Model component
    % Berkeley Short Channel IGFET Model 4 model
    % model ModelName BSIM4 ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % NMOS type BSIM4 MOSFET (s---b) 
        NMOS
        % PMOS type BSIM4 MOSFET (s---b) 
        PMOS
        % Capacitance model selector (s--ri) 
        Capmod
        % Diode IV model selector (s--ri) 
        Diomod
        % Bias-dependent S/D resistance model selector (s--ri) 
        Rdsmod
        % Transient NQS model selector (s--ri) 
        Trnqsmod
        % AC NQS model selector (s--ri) 
        Acnqsmod
        % Mobility model selector (s--ri) 
        Mobmod
        % Distributed body R model selector (s--ri) 
        Rbodymod
        % Gate R model selector (s--ri) 
        Rgatemod
        % S/D resistance and contact model selector (sm-ri) 
        Rgeomod
        % Number of fingers (smorr) 
        Nf
        % Pd and Ps model selector (s--ri) 
        Permod
        % Geometry dependent parasitics model selector (s--ri) 
        Geomod
        % Flicker noise model selector (s--ri) 
        Fnoimod
        % Thermal noise model selector (s--ri) 
        Tnoimod
        % Gate-to-channel Ig model selector (s--ri) 
        Igcmod
        % Gate-to-body Ig model selector (s--ri) 
        Igbmod
        % Model parameter checking selector (s--ri) 
        Paramchk
        % Bin  unit  selector (s--ri) 
        Binunit
        % Parameter for model version (s--rr) 
        Version
        % Electrical gate oxide thickness in meters (smorr) 
        Toxe
        % Physical gate oxide thickness in meters (smorr) 
        Toxp
        % Gate oxide thickness at which parameters are extracted (smorr) 
        Toxm
        % Target tox value (smorr) 
        Toxref
        % Defined as (toxe - toxp) (smorr) 
        Dtox
        % Dielectric constant of the gate oxide relative to vacuum (smorr) 
        Epsrox
        % Drain/Source and channel coupling capacitance (smorr) Unit: F/(V*m^2)
        Cdsc
        % Body-bias dependence of cdsc (smorr) Unit: F/(V*m^2)
        Cdscb
        % Drain-bias dependence of cdsc (smorr) Unit: F/(V*m^2)
        Cdscd
        % Interface state capacitance (smorr) Unit: F/(V*m^2)
        Cit
        % Subthreshold swing Coefficient (smorr) 
        Nfactor
        % Junction depth in meters (smorr) Unit: m
        Xj
        % Saturation velocity at tnom (smorr) Unit: m/s
        Vsat
        % Temperature coefficient of vsat (smorr) Unit: m/s
        At
        % Non-uniform depletion width effect coefficient (smorr) 
        A0
        % Gate bias  coefficient of Abulk (smorr) Unit: V^-1
        Ags
        % First non-saturation effect coefficient (smorr) Unit: V^-1
        A1
        % Second non-saturation effect coefficient (smorr) 
        A2
        % Body-bias coefficient of non-uniform depletion width effect (smorr) Unit: V^-1
        Keta
        % Substrate doping concentration (smorr) Unit: cm^-3
        Nsub
        % Channel doping concentration at the depletion edge (smorr) Unit: cm^-3
        Ndep
        % S/D doping concentration (smorr) Unit: cm^-3
        Nsd
        % Adjusting parameter for surface potential due to non-uniform vertical doping (smorr) Unit: V
        Phin
        % Poly-gate doping concentration (smorr) Unit: cm^-3
        Ngate
        % Vth body coefficient (smorr) Unit: V^(1/2)
        Gamma1
        % Vth body coefficient (smorr) Unit: V^(1/2)
        Gamma2
        % Vth transition body Voltage (smorr) Unit: V
        Vbx
        % Maximum body voltage (smorr) Unit: V
        Vbm
        % Doping depth (smorr) Unit: m
        Xt
        % Bulk effect coefficient 1 (smorr) Unit: V^(1/2)
        K1
        % Temperature coefficient of Vth (smorr) Unit: V
        Kt1
        % Temperature coefficient of Vth (smorr) Unit: V*m
        Kt1l
        % Body-coefficient of kt1 (smorr) 
        Kt2
        % Bulk effect coefficient 2 (smorr) 
        K2
        % Narrow width effect coefficient (smorr) 
        K3
        % Body effect coefficient of k3 (smorr) Unit: V^-1
        K3b
        % Narrow width effect parameter (smorr) Unit: m
        W0
        % First parameter for Vth shift due to pocket (smorr) Unit: m
        Dvtp0
        % Second parameter for Vth shift due to pocket (smorr) Unit: V^-1
        Dvtp1
        % Equivalent length of pocket region at zero bias (smorr) Unit: m
        Lpe0
        % Equivalent length of pocket region accounting for body bias (smorr) Unit: m
        Lpeb
        % Short channel effect coeff. 0 (smorr) 
        Dvt0
        % Short channel effect coeff. 1 (smorr) 
        Dvt1
        % Short channel effect coeff. 2 (smorr) Unit: V^-1
        Dvt2
        % Narrow Width coeff. 0 (smorr) 
        Dvt0w
        % Narrow Width effect coeff. 1 (smorr) Unit: m^-1
        Dvt1w
        % Narrow Width effect coeff. 2 (smorr) Unit: V^-1
        Dvt2w
        % DIBL coefficient of output resistance (smorr) 
        Drout
        % DIBL coefficient in the subthreshold region (smorr) 
        Dsub
        % Threshold voltage (smorr) Unit: V
        Vth0
        % Linear gate dependence of mobility (smorr) 
        Ua
        % Temperature coefficient of ua (smorr) Unit: m/V
        Ua1
        % Quadratic gate dependence of mobility (smorr) Unit: (m/V)^2
        Ub
        % Temperature coefficient of ub (smorr) Unit: (m/V)^2
        Ub1
        % Body-bias dependence of mobility (smorr) Unit: V^-1
        Uc
        % Temperature coefficient of uc (smorr) Unit: V^-1
        Uc1
        % Low-field mobility at Tnom (smorr) Unit: m^2/(V*s)
        U0
        % Mobility exponent (smorr) 
        Eu
        % Temperature coefficient of mobility (smorr) 
        Ute
        % Threshold voltage offset (smorr) Unit: V
        Voff
        % Fitting parameter for moderate inversion in Vgsteff (smorr) 
        Minv
        % Length dependence parameter for Vth offset (smorr) Unit: V*m
        Voffl
        % Parameter measurement temperature (smorr) Unit: deg C
        Tnom
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Gate-source overlap capacitance per width (smorr) Unit: F/m
        Cgso
        % Gate-drain overlap capacitance per width (smorr) Unit: F/m
        Cgdo
        % Gate-bulk overlap capacitance per length (smorr) Unit: F/m
        Cgbo
        % Channel charge partitioning (smorr) 
        Xpart
        % Effective Vds parameter (smorr) Unit: V
        Delta
        % Source-drain sheet resistance (smorr) Unit: Ohms/square
        Rsh
        % Source-drain resistance per width (smorr) Unit: Ohms*um
        Rdsw
        % Source-drain resistance per width at high Vg (smorr) Unit: Ohms*um
        Rdswmin
        % Source resistance per width (smorr) Unit: Ohms*um
        Rsw
        % Drain resistance per width (smorr) Unit: Ohms*um
        Rdw
        % Drain resistance per width at high Vg (smorr) Unit: Ohms*um
        Rdwmin
        % Source resistance per width at high Vg (smorr) Unit: Ohms*um
        Rswmin
        % Gate-bias effect on parasitic resistance (smorr) Unit: V^-1
        Prwg
        % Body-effect on parasitic resistance (smorr) Unit: V^(-1/2)
        Prwb
        % Temperature coefficient of parasitic resistance (smorr) Unit: Ohms*um
        Prt
        % Subthreshold region DIBL coefficient (smorr) 
        Eta0
        % Subthreshold region DIBL coefficient (smorr) Unit: V^-1
        Etab
        % Channel length modulation Coefficient (smorr) 
        Pclm
        % Drain-induced barrier lowering coefficient (smorr) 
        Pdiblc1
        % Drain-induced barrier lowering coefficient (smorr) 
        Pdiblc2
        % Body-effect on drain-induced barrier lowering (smorr) Unit: V^-1
        Pdiblcb
        % Rout degradation coefficient for pocket devices (smorr) Unit: V/m^(1/2)
        Fprout
        % Coefficient for drain-induced Vth shifts (smorr) Unit: V^-1
        Pdits
        % Length dependence of drain-induced Vth shifts (smorr) Unit: m^-1
        Pditsl
        % Vds dependence of drain-induced Vth shifts (smorr) Unit: V^-1
        Pditsd
        % Substrate current body-effect coefficient (smorr) Unit: V/m
        Pscbe1
        % Substrate current body-effect coefficient (smorr) Unit: m/V
        Pscbe2
        % Gate dependence of output resistance parameter (smorr) 
        Pvag
        % Bottom source junction reverse saturation current density (smorr) Unit: A/m^2
        Jss
        % Isolation edge sidewall source junction reverse saturation current density (smorr) Unit: A/m
        Jsws
        % Gate edge source junction reverse saturation current density (smorr) Unit: A/m
        Jswgs
        % Source junction built-in potential (smorr) Unit: V
        Pbs
        % Source junction emission coefficient (smorr) 
        Njs
        % Source junction current temperature exponent (smorr) 
        Xtis
        % Source bottom junction capacitance grading coefficient (smorr) 
        Mjs
        % Source sidewall junction capacitance built in potential (smorr) Unit: V
        Pbsws
        % Source sidewall junction capacitance grading coefficient (smorr) 
        Mjsws
        % Source (gate side) sidewall junction capacitance built in potential (smorr) Unit: V
        Pbswgs
        % Source (gate side) sidewall junction capacitance grading coefficient (smorr) 
        Mjswgs
        % Source bottom junction capacitance per unit area (smorr) Unit: F/m^2
        Cjs
        % Source sidewall junction capacitance per unit periphery (smorr) Unit: F/m
        Cjsws
        % Source (gate side) sidewall junction capacitance per unit width (smorr) Unit: F/m
        Cjswgs
        % Bottom drain junction reverse saturation current density (smorr) Unit: A/m^2
        Jsd
        % Isolation edge sidewall drain junction reverse saturation current density (smorr) Unit: A/m
        Jswd
        % Gate edge drain junction reverse saturation current density (smorr) 
        Jswgd
        % Drain junction built-in potential (smorr) Unit: V
        Pbd
        % Drain junction emission coefficient (smorr) 
        Njd
        % Drainjunction current temperature exponent (smorr) 
        Xtid
        % Drain bottom junction capacitance grading coefficient (smorr) 
        Mjd
        % Drain sidewall junction capacitance built in potential (smorr) Unit: V
        Pbswd
        % Drain sidewall junction capacitance grading coefficient (smorr) 
        Mjswd
        % Drain (gate side) sidewall junction capacitance built in potential (smorr) Unit: V
        Pbswgd
        % Drain (gate side) sidewall junction capacitance grading coefficient (smorr) 
        Mjswgd
        % Drain bottom junction capacitance per unit area (smorr) Unit: F/m^2
        Cjd
        % Drain sidewall junction capacitance per unit periphery (smorr) Unit: F/m
        Cjswd
        % Drain (gate side) sidewall junction capacitance per unit width (smorr) Unit: F/m
        Cjswgd
        % Flat Band Voltage parameter for capmod (smorr) Unit: V
        Vfbcv
        % Flat Band Voltage (smorr) Unit: V
        Vfb
        % Temperature coefficient of pb (smorr) Unit: V/K
        Tpb
        % Temperature coefficient of cj (smorr) Unit: K^-1
        Tcj
        % Temperature coefficient of pbsw (smorr) Unit: V/K
        Tpbsw
        % Temperature coefficient of cjsw (smorr) Unit: K^-1
        Tcjsw
        % Temperature coefficient of pbswg (smorr) Unit: V/K
        Tpbswg
        % Temperature coefficient of cjswg (smorr) Unit: K^-1
        Tcjswg
        % Exponential coefficient for finite charge thickness (smorr) Unit: m/V
        Acde
        % Coefficient for gate-bias dependent surface potential (smorr) 
        Moin
        % C-V turn-on/off parameter (smorr) 
        Noff
        % C-V lateral-shift parameter (smorr) Unit: V
        Voffcv
        % Distance of Mid-Contact to Gate edge (smorr) Unit: m
        Dmcg
        % Distance of Mid-Contact to Isolation (smorr) Unit: m
        Dmci
        % Distance of Mid-Diffusion to Gate edge (smorr) Unit: m
        Dmdg
        % Distance of Mid-Contact to Gate edge in Test structures (smorr) Unit: m
        Dmcgt
        % Distance from gate contact center to device edge (smorr) Unit: m
        Xgw
        % Variation in Ldrawn (smorr) Unit: m
        Xgl
        % Gate sheet resistance (smorr) Unit: Ohms/square
        Rshg
        % Number of gate contacts (smorr) 
        Ngcon
        % First fitting parameter the bias-dependent Rg (smorr) 
        Xrcrg1
        % Second fitting parameter the bias-dependent Rg (smorr) 
        Xrcrg2
        % Length reduction parameter (smorr) Unit: m
        Lint
        % Length reduction parameter (smorr) Unit: m
        Ll
        % Length reduction parameter for CV (smorr) Unit: m
        Llc
        % Length reduction parameter (smorr) 
        Lln
        % Length reduction parameter (smorr) Unit: m
        Lw
        % Length reduction parameter for CV (smorr) Unit: m
        Lwc
        % Length reduction parameter (smorr) 
        Lwn
        % Length reduction parameter (smorr) Unit: m
        Lwl
        % Length reduction parameter for CV (smorr) Unit: m
        Lwlc
        % Minimum length for the model (smorr) Unit: m
        Lmin
        % Maximum length for the model (smorr) Unit: m
        Lmax
        % Width dependence of rds (smorr) 
        Wr
        % Width reduction parameter (smorr) Unit: m
        Wint
        % Width reduction parameter (smorr) Unit: m/V
        Dwg
        % Width reduction parameter (smorr) Unit: m/V^(1/2)
        Dwb
        % Width reduction parameter (smorr) Unit: m
        Wl
        % Width reduction parameter for CV (smorr) Unit: m
        Wlc
        % Width reduction parameter (smorr) 
        Wln
        % Width reduction parameter (smorr) Unit: m
        Ww
        % Width reduction parameter for CV (smorr) Unit: m
        Wwc
        % Width reduction parameter (smorr) 
        Wwn
        % Width reduction parameter (smorr) Unit: m
        Wwl
        % Width reduction parameter for CV (smorr) Unit: m
        Wwlc
        % Minimum width for the model (smorr) Unit: m
        Wmin
        % Maximum width for the model (smorr) Unit: m
        Wmax
        % Abulk narrow width parameter (smorr) Unit: m
        B0
        % Abulk narrow width parameter (smorr) Unit: m
        B1
        % New C-V model parameter (smorr) Unit: F/m
        Cgsl
        % New C-V model parameter (smorr) Unit: F/m
        Cgdl
        % S/G overlap C-V parameter (smorr) Unit: V
        Ckappas
        % D/G overlap C-V parameter (smorr) Unit: V
        Ckappad
        % Fringe capacitance parameter (smorr) Unit: F/m
        Cf
        % Vdsat parameter for C-V model (smorr) Unit: m
        Clc
        % Vdsat parameter for C-V model (smorr) 
        Cle
        % Delta W for C-V model (smorr) Unit: m
        Dwc
        % Delta L for C-V model (smorr) Unit: m
        Dlc
        % Delta L for Ig model (smorr) Unit: m
        Dlcig
        % Delta W for S/D junctions (smorr) 
        Dwj
        % substrate current model parameter (smorr) Unit: A*m/V
        Alpha0
        % substrate current model parameter (smorr) Unit: A/V
        Alpha1
        % substrate current model parameter (smorr) Unit: V
        Beta0
        % Pre-exponential constant for GIDL (smorr) Unit: Ohm^-1
        Agidl
        % Exponential constant for GIDL (smorr) Unit: V/m
        Bgidl
        % Parameter for body-bias dependence of GIDL (smorr) Unit: V^3
        Cgidl
        % Fitting parameter for Bandbending (smorr) Unit: V
        Egidl
        % Parameter for Igc (smorr) 
        Aigc
        % Parameter for Igc (smorr) 
        Bigc
        % Parameter for Igc (smorr) Unit: V^-1
        Cigc
        % Parameter for Igs,d (smorr) 
        Aigsd
        % Parameter for Igs,d (smorr) 
        Bigsd
        % Parameter for Igs,d (smorr) Unit: V^-1
        Cigsd
        % Parameter for Igb (smorr) 
        Aigbacc
        % Parameter for Igb (smorr) 
        Bigbacc
        % Parameter for Igb (smorr) Unit: V^-1
        Cigbacc
        % Parameter for Igb (smorr) 
        Aigbinv
        % Parameter for Igb (smorr) 
        Bigbinv
        % Parameter for Igb (smorr) Unit: V^-1
        Cigbinv
        % Parameter for Igc slope (smorr) 
        Nigc
        % Parameter for Igbinv slope (smorr) 
        Nigbinv
        % Parameter for Igbacc slope (smorr) 
        Nigbacc
        % Exponent for Tox ratio (smorr) 
        Ntox
        % Parameter for the Si bandgap for Igbinv (smorr) Unit: V
        Eigbinv
        % Parameter for Igc partition (smorr) 
        Pigcd
        % Factor for the gate edge Tox (smorr) 
        Poxedge
        % Forward drain diode forward limiting current (smorr) Unit: A
        Ijthdfwd
        % Forward source diode forward limiting current (smorr) Unit: A
        Ijthsfwd
        % Reverse drain diode forward limiting current (smorr) Unit: A
        Ijthdrev
        % Reverse source diode forward limiting current (smorr) Unit: A
        Ijthsrev
        % Fitting parameter for drain diode breakdown current (smorr) 
        Xjbvd
        % Fitting parameter for source diode breakdown current (smorr) 
        Xjbvs
        % Drain diode breakdown voltage (smorr) Unit: V
        Bvd
        % Source diode breakdown voltage (smorr) Unit: V
        Bvs
        % Minimum body conductance (smorr) Unit: Ohm^-1
        Gbmin
        % Resistance between bNode and dbNode (smorr) Unit: Ohms
        Rbdb
        % Resistance between bNodePrime and bNode (smorr) Unit: Ohms
        Rbpb
        % Resistance between bNode and sbNode (smorr) Unit: Ohms
        Rbsb
        % Resistance between bNodePrime and sbNode (smorr) Unit: Ohms
        Rbps
        % Resistance between bNodePrime and bNode (smorr) Unit: Ohms
        Rbpd
        % Length dependence of cdsc (smorr) 
        Lcdsc
        % Length dependence of cdscb (smorr) 
        Lcdscb
        % Length dependence of cdscd (smorr) 
        Lcdscd
        % Length dependence of cit (smorr) 
        Lcit
        % Length dependence of nfactor (smorr) 
        Lnfactor
        % Length dependence of xj (smorr) 
        Lxj
        % Length dependence of vsat (smorr) 
        Lvsat
        % Length dependence of at (smorr) 
        Lat
        % Length dependence of a0 (smorr) 
        La0
        % Length dependence of ags (smorr) 
        Lags
        % Length dependence of a1 (smorr) 
        La1
        % Length dependence of a2 (smorr) 
        La2
        % Length dependence of keta (smorr) 
        Lketa
        % Length dependence of nsub (smorr) 
        Lnsub
        % Length dependence of ndep (smorr) 
        Lndep
        % Length dependence of nsd (smorr) 
        Lnsd
        % Length dependence of phin (smorr) 
        Lphin
        % Length dependence of ngate (smorr) 
        Lngate
        % Length dependence of gamma1 (smorr) 
        Lgamma1
        % Length dependence of gamma2 (smorr) 
        Lgamma2
        % Length dependence of vbx (smorr) 
        Lvbx
        % Length dependence of vbm (smorr) 
        Lvbm
        % Length dependence of xt (smorr) 
        Lxt
        % Length dependence of k1 (smorr) 
        Lk1
        % Length dependence of kt1 (smorr) 
        Lkt1
        % Length dependence of kt1l (smorr) 
        Lkt1l
        % Length dependence of kt2 (smorr) 
        Lkt2
        % Length dependence of k2 (smorr) 
        Lk2
        % Length dependence of k3 (smorr) 
        Lk3
        % Length dependence of k3b (smorr) 
        Lk3b
        % Length dependence of w0 (smorr) 
        Lw0
        % Length dependence of dvtp0 (smorr) 
        Ldvtp0
        % Length dependence of dvtp1 (smorr) 
        Ldvtp1
        % Length dependence of lpe0 (smorr) 
        Llpe0
        % Length dependence of lpeb (smorr) 
        Llpeb
        % Length dependence of dvt0 (smorr) 
        Ldvt0
        % Length dependence of dvt1 (smorr) 
        Ldvt1
        % Length dependence of dvt2 (smorr) 
        Ldvt2
        % Length dependence of dvt0w (smorr) 
        Ldvt0w
        % Length dependence of dvt1w (smorr) 
        Ldvt1w
        % Length dependence of dvt2w (smorr) 
        Ldvt2w
        % Length dependence of drout (smorr) 
        Ldrout
        % Length dependence of dsub (smorr) 
        Ldsub
        % Length dependence of vto (smorr) 
        Lvth0
        % Length dependence of vto (smorr) 
        Lvtho
        % Length dependence of ua (smorr) 
        Lua
        % Length dependence of ua1 (smorr) 
        Lua1
        % Length dependence of ub (smorr) 
        Lub
        % Length dependence of ub1 (smorr) 
        Lub1
        % Length dependence of uc (smorr) 
        Luc
        % Length dependence of uc1 (smorr) 
        Luc1
        % Length dependence of u0 (smorr) 
        Lu0
        % Length dependence of ute (smorr) 
        Lute
        % Length dependence of voff (smorr) 
        Lvoff
        % Length dependence of minv (smorr) 
        Lminv
        % Length dependence of delta (smorr) 
        Ldelta
        % Length dependence of rdsw (smorr) 
        Lrdsw
        % Length dependence of rsw (smorr) 
        Lrsw
        % Length dependence of rdw (smorr) 
        Lrdw
        % Length dependence of prwg (smorr) 
        Lprwg
        % Length dependence of prwb (smorr) 
        Lprwb
        % Length dependence of prt (smorr) 
        Lprt
        % Length dependence of eta0 (smorr) 
        Leta0
        % Length dependence of etab (smorr) 
        Letab
        % Length dependence of pclm (smorr) 
        Lpclm
        % Length dependence of pdiblc1 (smorr) 
        Lpdiblc1
        % Length dependence of pdiblc2 (smorr) 
        Lpdiblc2
        % Length dependence of pdiblcb (smorr) 
        Lpdiblcb
        % Length dependence of pdiblcb (smorr) 
        Lfprout
        % Length dependence of pdits (smorr) 
        Lpdits
        % Length dependence of pditsd (smorr) 
        Lpditsd
        % Length dependence of pscbe1 (smorr) 
        Lpscbe1
        % Length dependence of pscbe2 (smorr) 
        Lpscbe2
        % Length dependence of pvag (smorr) 
        Lpvag
        % Length dependence of wr (smorr) 
        Lwr
        % Length dependence of dwg (smorr) 
        Ldwg
        % Length dependence of dwb (smorr) 
        Ldwb
        % Length dependence of b0 (smorr) 
        Lb0
        % Length dependence of b1 (smorr) 
        Lb1
        % Length dependence of cgsl (smorr) 
        Lcgsl
        % Length dependence of cgdl (smorr) 
        Lcgdl
        % Length dependence of ckappas (smorr) 
        Lckappas
        % Length dependence of ckappad (smorr) 
        Lckappad
        % Length dependence of cf (smorr) 
        Lcf
        % Length dependence of clc (smorr) 
        Lclc
        % Length dependence of cle (smorr) 
        Lcle
        % Length dependence of alpha0 (smorr) 
        Lalpha0
        % Length dependence of alpha1 (smorr) 
        Lalpha1
        % Length dependence of beta0 (smorr) 
        Lbeta0
        % Length dependence of agidl (smorr) 
        Lagidl
        % Length dependence of bgidl (smorr) 
        Lbgidl
        % Length dependence of cgidl (smorr) 
        Lcgidl
        % Length dependence of egidl (smorr) 
        Legidl
        % Length dependence of aigc (smorr) 
        Laigc
        % Length dependence of bigc (smorr) 
        Lbigc
        % Length dependence of cigc (smorr) 
        Lcigc
        % Length dependence of aigsd (smorr) 
        Laigsd
        % Length dependence of bigsd (smorr) 
        Lbigsd
        % Length dependence of cigsd (smorr) 
        Lcigsd
        % Length dependence of aigbacc (smorr) 
        Laigbacc
        % Length dependence of bigbacc (smorr) 
        Lbigbacc
        % Length dependence of cigbacc (smorr) 
        Lcigbacc
        % Length dependence of aigbinv (smorr) 
        Laigbinv
        % Length dependence of bigbinv (smorr) 
        Lbigbinv
        % Length dependence of cigbinv (smorr) 
        Lcigbinv
        % Length dependence of nigc (smorr) 
        Lnigc
        % Length dependence of nigbinv (smorr) 
        Lnigbinv
        % Length dependence of nigbacc (smorr) 
        Lnigbacc
        % Length dependence of ntox (smorr) 
        Lntox
        % Length dependence for eigbinv (smorr) 
        Leigbinv
        % Length dependence for pigcd (smorr) 
        Lpigcd
        % Length dependence for poxedge (smorr) 
        Lpoxedge
        % Length dependence of vfbcv (smorr) 
        Lvfbcv
        % Length dependence of vfb (smorr) 
        Lvfb
        % Length dependence of acde (smorr) 
        Lacde
        % Length dependence of moin (smorr) 
        Lmoin
        % Length dependence of noff (smorr) 
        Lnoff
        % Length dependence of voffcv (smorr) 
        Lvoffcv
        % Length dependence of xrcrg1 (smorr) 
        Lxrcrg1
        % Length dependence of xrcrg2 (smorr) 
        Lxrcrg2
        % Length dependence of eu (smorr) 
        Leu
        % Width dependence of cdsc (smorr) 
        Wcdsc
        % Width dependence of cdscb (smorr) 
        Wcdscb
        % Width dependence of cdscd (smorr) 
        Wcdscd
        % Width dependence of cit (smorr) 
        Wcit
        % Width dependence of nfactor (smorr) 
        Wnfactor
        % Width dependence of xj (smorr) 
        Wxj
        % Width dependence of vsat (smorr) 
        Wvsat
        % Width dependence of at (smorr) 
        Wat
        % Width dependence of a0 (smorr) 
        Wa0
        % Width dependence of ags (smorr) 
        Wags
        % Width dependence of a1 (smorr) 
        Wa1
        % Width dependence of a2 (smorr) 
        Wa2
        % Width dependence of keta (smorr) 
        Wketa
        % Width dependence of nsub (smorr) 
        Wnsub
        % Width dependence of ndep (smorr) 
        Wndep
        % Width dependence of nsd (smorr) 
        Wnsd
        % Width dependence of phin (smorr) 
        Wphin
        % Width dependence of ngate (smorr) 
        Wngate
        % Width dependence of gamma1 (smorr) 
        Wgamma1
        % Width dependence of gamma2 (smorr) 
        Wgamma2
        % Width dependence of vbx (smorr) 
        Wvbx
        % Width dependence of vbm (smorr) 
        Wvbm
        % Width dependence of xt (smorr) 
        Wxt
        % Width dependence of k1 (smorr) 
        Wk1
        % Width dependence of kt1 (smorr) 
        Wkt1
        % Width dependence of kt1l (smorr) 
        Wkt1l
        % Width dependence of kt2 (smorr) 
        Wkt2
        % Width dependence of k2 (smorr) 
        Wk2
        % Width dependence of k3 (smorr) 
        Wk3
        % Width dependence of k3b (smorr) 
        Wk3b
        % Width dependence of w0 (smorr) 
        Ww0
        % Width dependence of dvtp0 (smorr) 
        Wdvtp0
        % Width dependence of dvtp1 (smorr) 
        Wdvtp1
        % Width dependence of lpe0 (smorr) 
        Wlpe0
        % Width dependence of lpeb (smorr) 
        Wlpeb
        % Width dependence of dvt0 (smorr) 
        Wdvt0
        % Width dependence of dvt1 (smorr) 
        Wdvt1
        % Width dependence of dvt2 (smorr) 
        Wdvt2
        % Width dependence of dvt0w (smorr) 
        Wdvt0w
        % Width dependence of dvt1w (smorr) 
        Wdvt1w
        % Width dependence of dvt2w (smorr) 
        Wdvt2w
        % Width dependence of drout (smorr) 
        Wdrout
        % Width dependence of dsub (smorr) 
        Wdsub
        % Width dependence of vto (smorr) 
        Wvth0
        % Width dependence of vto (smorr) 
        Wvtho
        % Width dependence of ua (smorr) 
        Wua
        % Width dependence of ua1 (smorr) 
        Wua1
        % Width dependence of ub (smorr) 
        Wub
        % Width dependence of ub1 (smorr) 
        Wub1
        % Width dependence of uc (smorr) 
        Wuc
        % Width dependence of uc1 (smorr) 
        Wuc1
        % Width dependence of u0 (smorr) 
        Wu0
        % Width dependence of ute (smorr) 
        Wute
        % Width dependence of voff (smorr) 
        Wvoff
        % Width dependence of minv (smorr) 
        Wminv
        % Width dependence of delta (smorr) 
        Wdelta
        % Width dependence of rdsw (smorr) 
        Wrdsw
        % Width dependence of rsw (smorr) 
        Wrsw
        % Width dependence of rdw (smorr) 
        Wrdw
        % Width dependence of prwg (smorr) 
        Wprwg
        % Width dependence of prwb (smorr) 
        Wprwb
        % Width dependence of prt (smorr) 
        Wprt
        % Width dependence of eta0 (smorr) 
        Weta0
        % Width dependence of etab (smorr) 
        Wetab
        % Width dependence of pclm (smorr) 
        Wpclm
        % Width dependence of pdiblc1 (smorr) 
        Wpdiblc1
        % Width dependence of pdiblc2 (smorr) 
        Wpdiblc2
        % Width dependence of pdiblcb (smorr) 
        Wpdiblcb
        % Width dependence of pdiblcb (smorr) 
        Wfprout
        % Width dependence of pdits (smorr) 
        Wpdits
        % Width dependence of pditsd (smorr) 
        Wpditsd
        % Width dependence of pscbe1 (smorr) 
        Wpscbe1
        % Width dependence of pscbe2 (smorr) 
        Wpscbe2
        % Width dependence of pvag (smorr) 
        Wpvag
        % Width dependence of wr (smorr) 
        Wwr
        % Width dependence of dwg (smorr) 
        Wdwg
        % Width dependence of dwb (smorr) 
        Wdwb
        % Width dependence of b0 (smorr) 
        Wb0
        % Width dependence of b1 (smorr) 
        Wb1
        % Width dependence of cgsl (smorr) 
        Wcgsl
        % Width dependence of cgdl (smorr) 
        Wcgdl
        % Width dependence of ckappas (smorr) 
        Wckappas
        % Width dependence of ckappad (smorr) 
        Wckappad
        % Width dependence of cf (smorr) 
        Wcf
        % Width dependence of clc (smorr) 
        Wclc
        % Width dependence of cle (smorr) 
        Wcle
        % Width dependence of alpha0 (smorr) 
        Walpha0
        % Width dependence of alpha1 (smorr) 
        Walpha1
        % Width dependence of beta0 (smorr) 
        Wbeta0
        % Width dependence of agidl (smorr) 
        Wagidl
        % Width dependence of bgidl (smorr) 
        Wbgidl
        % Width dependence of cgidl (smorr) 
        Wcgidl
        % Width dependence of egidl (smorr) 
        Wegidl
        % Width dependence of aigc (smorr) 
        Waigc
        % Width dependence of bigc (smorr) 
        Wbigc
        % Width dependence of cigc (smorr) 
        Wcigc
        % Width dependence of aigsd (smorr) 
        Waigsd
        % Width dependence of bigsd (smorr) 
        Wbigsd
        % Width dependence of cigsd (smorr) 
        Wcigsd
        % Width dependence of aigbacc (smorr) 
        Waigbacc
        % Width dependence of bigbacc (smorr) 
        Wbigbacc
        % Width dependence of cigbacc (smorr) 
        Wcigbacc
        % Width dependence of aigbinv (smorr) 
        Waigbinv
        % Width dependence of bigbinv (smorr) 
        Wbigbinv
        % Width dependence of cigbinv (smorr) 
        Wcigbinv
        % Width dependence of nigc (smorr) 
        Wnigc
        % Width dependence of nigbinv (smorr) 
        Wnigbinv
        % Width dependence of nigbacc (smorr) 
        Wnigbacc
        % Width dependence of ntox (smorr) 
        Wntox
        % Width dependence for eigbinv (smorr) 
        Weigbinv
        % Width dependence for pigcd (smorr) 
        Wpigcd
        % Width dependence for poxedge (smorr) 
        Wpoxedge
        % Width dependence of vfbcv (smorr) 
        Wvfbcv
        % Width dependence of vfb (smorr) 
        Wvfb
        % Width dependence of acde (smorr) 
        Wacde
        % Width dependence of moin (smorr) 
        Wmoin
        % Width dependence of noff (smorr) 
        Wnoff
        % Width dependence of voffcv (smorr) 
        Wvoffcv
        % Width dependence of xrcrg1 (smorr) 
        Wxrcrg1
        % Width dependence of xrcrg2 (smorr) 
        Wxrcrg2
        % Width dependence of eu (smorr) 
        Weu
        % Cross-term dependence of cdsc (smorr) 
        Pcdsc
        % Cross-term dependence of cdscb (smorr) 
        Pcdscb
        % Cross-term dependence of cdscd (smorr) 
        Pcdscd
        % Cross-term dependence of cit (smorr) 
        Pcit
        % Cross-term dependence of nfactor (smorr) 
        Pnfactor
        % Cross-term dependence of xj (smorr) 
        Pxj
        % Cross-term dependence of vsat (smorr) 
        Pvsat
        % Cross-term dependence of at (smorr) 
        Pat
        % Cross-term dependence of a0 (smorr) 
        Pa0
        % Cross-term dependence of ags (smorr) 
        Pags
        % Cross-term dependence of a1 (smorr) 
        Pa1
        % Cross-term dependence of a2 (smorr) 
        Pa2
        % Cross-term dependence of keta (smorr) 
        Pketa
        % Cross-term dependence of nsub (smorr) 
        Pnsub
        % Cross-term dependence of ndep (smorr) 
        Pndep
        % Cross-term dependence of nsd (smorr) 
        Pnsd
        % Cross-term dependence of phin (smorr) 
        Pphin
        % Cross-term dependence of ngate (smorr) 
        Pngate
        % Cross-term dependence of gamma1 (smorr) 
        Pgamma1
        % Cross-term dependence of gamma2 (smorr) 
        Pgamma2
        % Cross-term dependence of vbx (smorr) 
        Pvbx
        % Cross-term dependence of vbm (smorr) 
        Pvbm
        % Cross-term dependence of xt (smorr) 
        Pxt
        % Cross-term dependence of k1 (smorr) 
        Pk1
        % Cross-term dependence of kt1 (smorr) 
        Pkt1
        % Cross-term dependence of kt1l (smorr) 
        Pkt1l
        % Cross-term dependence of kt2 (smorr) 
        Pkt2
        % Cross-term dependence of k2 (smorr) 
        Pk2
        % Cross-term dependence of k3 (smorr) 
        Pk3
        % Cross-term dependence of k3b (smorr) 
        Pk3b
        % Cross-term dependence of w0 (smorr) 
        Pw0
        % Cross-term dependence of dvtp0 (smorr) 
        Pdvtp0
        % Cross-term dependence of dvtp1 (smorr) 
        Pdvtp1
        % Cross-term dependence of lpe0 (smorr) 
        Plpe0
        % Cross-term dependence of lpeb (smorr) 
        Plpeb
        % Cross-term dependence of dvt0 (smorr) 
        Pdvt0
        % Cross-term dependence of dvt1 (smorr) 
        Pdvt1
        % Cross-term dependence of dvt2 (smorr) 
        Pdvt2
        % Cross-term dependence of dvt0w (smorr) 
        Pdvt0w
        % Cross-term dependence of dvt1w (smorr) 
        Pdvt1w
        % Cross-term dependence of dvt2w (smorr) 
        Pdvt2w
        % Cross-term dependence of drout (smorr) 
        Pdrout
        % Cross-term dependence of dsub (smorr) 
        Pdsub
        % Cross-term dependence of vto (smorr) 
        Pvth0
        % Cross-term dependence of vto (smorr) 
        Pvtho
        % Cross-term dependence of ua (smorr) 
        Pua
        % Cross-term dependence of ua1 (smorr) 
        Pua1
        % Cross-term dependence of ub (smorr) 
        Pub
        % Cross-term dependence of ub1 (smorr) 
        Pub1
        % Cross-term dependence of uc (smorr) 
        Puc
        % Cross-term dependence of uc1 (smorr) 
        Puc1
        % Cross-term dependence of u0 (smorr) 
        Pu0
        % Cross-term dependence of ute (smorr) 
        Pute
        % Cross-term dependence of voff (smorr) 
        Pvoff
        % Cross-term dependence of minv (smorr) 
        Pminv
        % Cross-term dependence of delta (smorr) 
        Pdelta
        % Cross-term dependence of rdsw (smorr) 
        Prdsw
        % Cross-term dependence of rsw (smorr) 
        Prsw
        % Cross-term dependence of rdw (smorr) 
        Prdw
        % Cross-term dependence of prwg (smorr) 
        Pprwg
        % Cross-term dependence of prwb (smorr) 
        Pprwb
        % Cross-term dependence of prt (smorr) 
        Pprt
        % Cross-term dependence of eta0 (smorr) 
        Peta0
        % Cross-term dependence of etab (smorr) 
        Petab
        % Cross-term dependence of pclm (smorr) 
        Ppclm
        % Cross-term dependence of pdiblc1 (smorr) 
        Ppdiblc1
        % Cross-term dependence of pdiblc2 (smorr) 
        Ppdiblc2
        % Cross-term dependence of pdiblcb (smorr) 
        Ppdiblcb
        % Cross-term dependence of pdiblcb (smorr) 
        Pfprout
        % Cross-term dependence of pdits (smorr) 
        Ppdits
        % Cross-term dependence of pditsd (smorr) 
        Ppditsd
        % Cross-term dependence of pscbe1 (smorr) 
        Ppscbe1
        % Cross-term dependence of pscbe2 (smorr) 
        Ppscbe2
        % Cross-term dependence of pvag (smorr) 
        Ppvag
        % Cross-term dependence of wr (smorr) 
        Pwr
        % Cross-term dependence of dwg (smorr) 
        Pdwg
        % Cross-term dependence of dwb (smorr) 
        Pdwb
        % Cross-term dependence of b0 (smorr) 
        Pb0
        % Cross-term dependence of b1 (smorr) 
        Pb1
        % Cross-term dependence of cgsl (smorr) 
        Pcgsl
        % Cross-term dependence of cgdl (smorr) 
        Pcgdl
        % Cross-term dependence of ckappas (smorr) 
        Pckappas
        % Cross-term dependence of ckappad (smorr) 
        Pckappad
        % Cross-term dependence of cf (smorr) 
        Pcf
        % Cross-term dependence of clc (smorr) 
        Pclc
        % Cross-term dependence of cle (smorr) 
        Pcle
        % Cross-term dependence of alpha0 (smorr) 
        Palpha0
        % Cross-term dependence of alpha1 (smorr) 
        Palpha1
        % Cross-term dependence of beta0 (smorr) 
        Pbeta0
        % Cross-term dependence of agidl (smorr) 
        Pagidl
        % Cross-term dependence of bgidl (smorr) 
        Pbgidl
        % Cross-term dependence of cgidl (smorr) 
        Pcgidl
        % Cross-term dependence of egidl (smorr) 
        Pegidl
        % Cross-term dependence of aigc (smorr) 
        Paigc
        % Cross-term dependence of bigc (smorr) 
        Pbigc
        % Cross-term dependence of cigc (smorr) 
        Pcigc
        % Cross-term dependence of aigsd (smorr) 
        Paigsd
        % Cross-term dependence of bigsd (smorr) 
        Pbigsd
        % Cross-term dependence of cigsd (smorr) 
        Pcigsd
        % Cross-term dependence of aigbacc (smorr) 
        Paigbacc
        % Cross-term dependence of bigbacc (smorr) 
        Pbigbacc
        % Cross-term dependence of cigbacc (smorr) 
        Pcigbacc
        % Cross-term dependence of aigbinv (smorr) 
        Paigbinv
        % Cross-term dependence of bigbinv (smorr) 
        Pbigbinv
        % Cross-term dependence of cigbinv (smorr) 
        Pcigbinv
        % Cross-term dependence of nigc (smorr) 
        Pnigc
        % Cross-term dependence of nigbinv (smorr) 
        Pnigbinv
        % Cross-term dependence of nigbacc (smorr) 
        Pnigbacc
        % Cross-term dependence of ntox (smorr) 
        Pntox
        % Cross-term dependence for eigbinv (smorr) 
        Peigbinv
        % Cross-term dependence for pigcd (smorr) 
        Ppigcd
        % Cross-term dependence for poxedge (smorr) 
        Ppoxedge
        % Cross-term dependence of vfbcv (smorr) 
        Pvfbcv
        % Cross-term dependence of vfb (smorr) 
        Pvfb
        % Cross-term dependence of acde (smorr) 
        Pacde
        % Cross-term dependence of moin (smorr) 
        Pmoin
        % Cross-term dependence of noff (smorr) 
        Pnoff
        % Cross-term dependence of voffcv (smorr) 
        Pvoffcv
        % Cross-term dependence of xrcrg1 (smorr) 
        Pxrcrg1
        % Cross-term dependence of xrcrg2 (smorr) 
        Pxrcrg2
        % Cross-term dependence of eu (smorr) 
        Peu
        % Flicker noise parameter (smorr) 
        Noia
        % Flicker noise parameter (smorr) 
        Noib
        % Flicker noise parameter (smorr) 
        Noic
        % Thermal noise parameter (smorr) 
        Tnoia
        % Thermal noise parameter (smorr) 
        Tnoib
        % Thermal noise parameter (smorr) 
        Ntnoi
        % Flicker noise parameter (smorr) Unit: V/m
        Em
        % Flicker noise frequency exponent (smorr) 
        Ef
        % Flicker noise exponent (smorr) 
        Af
        % Flicker noise coefficient (smorr) 
        Kf
        % W offset for channel width due to mask/etch effect (smorr) Unit: m
        Xw
        % L offset for channel width due to mask/etch effect (smorr) Unit: m
        Xl
        % Channel length (smorr) Unit: m
        L
        % Channel width (smorr) Unit: m
        W
        % Area of the drain diffusion (smorr) Unit: m^2
        Ad
        % Area of the source diffusion (smorr) Unit: m^2
        As
        % Perimeter of the drain junction (smorr) Unit: m
        Pd
        % Perimeter of the source junction (smorr) Unit: m
        Ps
        % Number of squares of the drain diffusion (smorr) 
        Nrd
        % Number of squares of the source difussion (smorr) 
        Nrs
        % Explosion current (smorr) Unit: A
        Imelt
        % Temperature equation selector (0/1/2) (sm--i) 
        Tlev
        % Temperature equation selector for capacitance (0/1/2/3) (sm--i) 
        Tlevc
        % Band gap (smorr) Unit: eV
        Eg
        % Energy gap temperature coefficient alpha (smorr) Unit: V/deg C
        Gap1
        % Energy gap temperature coefficient beta (smorr) Unit: K
        Gap2
        % Substrate junction forward bias (warning) (smorr) Unit: V
        wVsubfwd
        % Substrate junction reverse breakdown voltage (warning) (smorr) Unit: V
        wBvsub
        % Gate oxide breakdown voltage (warning) (smorr) Unit: V
        wBvg
        % Drain-source breakdown voltage (warning) (smorr) Unit: V
        wBvds
        % Maximum drain-source current (warning) (smorr) Unit: A
        wIdsmax
        % Maximum power dissipation (warning) (smorr) Unit: W
        wPmax
        % Secured model parameters (s---b) 
        Secured
        % Temperature model selector (s--ri) 
        Tempmod
        % Velocity overshoot parameter (smorr) 
        Lambda
        % Thermal velocity (smorr) Unit: m/s
        Vtl
        % Velocity back scattering parameter (smorr) Unit: m
        Lc
        % Velocity back scattering coefficient (smorr) 
        Xn
        % Length dependence of Lambda (smorr) 
        Llambda
        % Length dependence of Vtl (smorr) 
        Lvtl
        % Length dependence of Xn (smorr) 
        Lxn
        % Width dependence of Lambda (smorr) 
        Wlambda
        % Width dependence of Vtl (smorr) 
        Wvtl
        % Width dependence of Xn (smorr) 
        Wxn
        % Cross-term dependence of Lambda (smorr) 
        Plambda
        % Cross-term dependence of Vtl (smorr) 
        Pvtl
        % Cross-term dependence of Xn (smorr) 
        Pxn
        % Reference distance between OD edge to poly of one side (smorr) Unit: m
        Saref
        % Reference distance between OD edge to poly of the other side (smorr) Unit: m
        Sbref
        % Width parameter for stress effect (smorr) Unit: m
        Wlod
        % Mobility degradation/enhancement coefficient for LOD (smorr) Unit: m
        Ku0
        % Saturation velocity degradation/enhancement coefficient for LOD (smorr) Unit: m
        Kvsat
        % Threshold shift parameter for LOD (smorr) Unit: V*m
        Kvth0
        % Temperature coefficient of Ku0 (smorr) 
        Tku0
        % Length parameter for U0 LOD effect (smorr) 
        Llodku0
        % Width parameter for U0 LOD effect (smorr) 
        Wlodku0
        % Length parameter for Vth LOD effect (smorr) 
        Llodvth
        % Width parameter for Vth LOD effect (smorr) 
        Wlodvth
        % Length dependence of Ku0 (smorr) 
        Lku0
        % Width dependence of Ku0 (smorr) 
        Wku0
        % Cross-term dependence of Ku0 (smorr) 
        Pku0
        % Length dependence of Kvth0 (smorr) 
        Lkvth0
        % Width dependence of Kvth0 (smorr) 
        Wkvth0
        % Cross-term dependence of Kvth0 (smorr) 
        Pkvth0
        % K2 shift factor related to stress effect on Vth (smorr) Unit: m
        Stk2
        % K2 shift modification factor for stress effect (smorr) 
        Lodk2
        % Eta0 shift factor related to stress effect on Vth (smorr) Unit: m
        Steta0
        % Eta0 shift modification factor for stress effect (smorr) 
        Lodeta0
        % Thermal noise coefficient (smorr) 
        Rnoia
        % Thermal noise coefficient (smorr) 
        Rnoib
        % S/D flatband voltage offset (smorr) 
        Vfbsdoff
        % Lint offset for noise calculation (smorr) 
        Lintnoi
        % Source bottom trap-assisted saturation current density (smorr) 
        Jtss
        % Drain bottom trap-assisted saturation current density (smorr) 
        Jtsd
        % Source STI sidewall trap-assisted saturation current density (smorr) 
        Jtssws
        % Drain STI sidewall trap-assisted saturation current density (smorr) 
        Jtsswd
        % Source gate-edge sidewall trap-assisted saturation current density (smorr) 
        Jtsswgs
        % Drain gate-edge sidewall trap-assisted saturation current density (smorr) 
        Jtsswgd
        % Non-ideality factor for bottom junction (smorr) 
        Njts
        % Non-ideality factor for STI sidewall junction (smorr) 
        Njtssw
        % Non-ideality factor for gate-edge sidewall junction (smorr) 
        Njtsswg
        % Power dependence of Jtss on temperature (smorr) 
        Xtss
        % Power dependence of Jtsd on temperature (smorr) 
        Xtsd
        % Power dependence of Jtssws on temperature (smorr) 
        Xtssws
        % Power dependence of Jtsswd on temperature (smorr) 
        Xtsswd
        % Power dependence of Jtsswgs on temperature (smorr) 
        Xtsswgs
        % Power dependence of Jtsswgd on temperature (smorr) 
        Xtsswgd
        % Temperature coefficient for Njts (smorr) 
        Tnjts
        % Temperature coefficient for Njtssw (smorr) 
        Tnjtssw
        % Temperature coefficient for Njtsswg (smorr) 
        Tnjtsswg
        % Source bottom trap-assisted voltage dependent parameter (smorr) 
        Vtss
        % Drain bottom trap-assisted voltage dependent parameter (smorr) 
        Vtsd
        % Source STI sidewall trap-assisted voltage dependent parameter (smorr) 
        Vtssws
        % Drain STI sidewall trap-assisted voltage dependent parameter (smorr) 
        Vtsswd
        % Source gate-edge sidewall trap-assisted voltage dependent parameter (smorr) 
        Vtsswgs
        % Drain gate-edge sidewall trap-assisted voltage dependent parameter (smorr) 
        Vtsswgd
        % Length dependence of Vfbsdoff (smorr) 
        Lvfbsdoff
        % Width dependence of Vfbsdoff (smorr) 
        Wvfbsdoff
        % Cross-term dependence of Vfbsdoff (smorr) 
        Pvfbsdoff
        % Coulomb scattering factor of mobility (smorr) Unit: m^-2
        Ud
        % Temperature coefficient of Ud (smorr) Unit: m^-2
        Ud1
        % Channel length linear factor of mobility (smorr) Unit: m^-2
        Up
        % Channel length exponential factor of mobility (smorr) Unit: m
        Lp
        % Temperature parameter for Vfbsdoff (smorr) Unit: K^-1
        Tvfbsdoff
        % Temperature parameter for Voff (smorr) Unit: K^-1
        Tvoff
        % Body resistance Rbps scaling (smorr) Unit: Ohms
        Rbps0
        % Body resistance Rbps L scaling (smorr) 
        Rbpsl
        % Body resistance Rbps W scaling (smorr) 
        Rbpsw
        % Body resistance Rbps Nf scaling (smorr) 
        Rbpsnf
        % Body resistance Rbpd scaling (smorr) Unit: Ohms
        Rbpd0
        % Body resistance Rbpd L scaling (smorr) 
        Rbpdl
        % Body resistance Rbpd W scaling (smorr) 
        Rbpdw
        % Body resistance Rbpd Nf scaling (smorr) 
        Rbpdnf
        % Body resistance Rbpbx scaling (smorr) Unit: Ohms
        Rbpbx0
        % Body resistance Rbpbx L scaling (smorr) 
        Rbpbxl
        % Body resistance Rbpbx W scaling (smorr) 
        Rbpbxw
        % Body resistance Rbpbx Nf scaling (smorr) 
        Rbpbxnf
        % Body resistance Rbpby scaling (smorr) Unit: Ohms
        Rbpby0
        % Body resistance Rbpby L scaling (smorr) 
        Rbpbyl
        % Body resistance Rbpby W scaling (smorr) 
        Rbpbyw
        % Body resistance Rbpby Nf scaling (smorr) 
        Rbpbynf
        % Body resistance Rbsbx scaling (smorr) Unit: Ohms
        Rbsbx0
        % Body resistance Rbsby scaling (smorr) Unit: Ohms
        Rbsby0
        % Body resistance Rbdbx scaling (smorr) Unit: Ohms
        Rbdbx0
        % Body resistance Rbdby scaling (smorr) Unit: Ohms
        Rbdby0
        % Body resistance Rbsdbx L scaling (smorr) 
        Rbsdbxl
        % Body resistance Rbsdbx W scaling (smorr) 
        Rbsdbxw
        % Body resistance Rbsdbx Nf scaling (smorr) 
        Rbsdbxnf
        % Body resistance Rbsdby L scaling (smorr) 
        Rbsdbyl
        % Body resistance Rbsdby W scaling (smorr) 
        Rbsdbyw
        % Body resistance Rbsdby Nf scaling (smorr) 
        Rbsdbynf
        % Length dependence of Ud (smorr) 
        Lud
        % Length dependence of Ud1 (smorr) 
        Lud1
        % Length dependence of Up (smorr) 
        Lup
        % Length dependence of Lp (smorr) 
        Llp
        % Length dependence of Tvfbsdoff (smorr) 
        Ltvfbsdoff
        % Length dependence of Tvoff (smorr) 
        Ltvoff
        % Width dependence of Ud (smorr) 
        Wud
        % Width dependence of Ud1 (smorr) 
        Wud1
        % Width dependence of Up (smorr) 
        Wup
        % Width dependence of Lp (smorr) 
        Wlp
        % Width dependence of Tvfbsdoff (smorr) 
        Wtvfbsdoff
        % Width dependence of Tvoff (smorr) 
        Wtvoff
        % Cross-term dependence of Ud (smorr) 
        Pud
        % Cross-term dependence of Ud1 (smorr) 
        Pud1
        % Cross-term dependence of Up (smorr) 
        Pup
        % Cross-term dependence of Lp (smorr) 
        Plp
        % Cross-term dependence of Tvfbsdoff (smorr) 
        Ptvfbsdoff
        % Cross-term dependence of Tvoff (smorr) 
        Ptvoff
        % Coefficient for Scb (smorr) 
        Web
        % Coefficient for Scc (smorr) 
        Wec
        % Threshold shift factor for well proximity effect (smorr) 
        Kvth0we
        % K2 shift factor for well proximity effect (smorr) 
        K2we
        % Mobility degradation factor for well proximity effect (smorr) 
        Ku0we
        % Reference distance to calculate Sca, Scb and Scc (smorr) Unit: m
        Scref
        % Flag for WPE model (Wpemod=1 to activate this model) (s--ri) 
        Wpemod
        % Length dependence of Kvth0we (smorr) 
        Lkvth0we
        % Length dependence of K2we (smorr) 
        Lk2we
        % Length dependence of Ku0we (smorr) 
        Lku0we
        % Width dependence of Kvth0we (smorr) 
        Wkvth0we
        % Width dependence of K2we (smorr) 
        Wk2we
        % Width dependence of Ku0we (smorr) 
        Wku0we
        % Cross-term dependence of Kvth0we (smorr) 
        Pkvth0we
        % Cross-term dependence of K2we (smorr) 
        Pk2we
        % Cross-term dependence of Ku0we (smorr) 
        Pku0we
        % Capacitance Charge model selector (sm-ri) 
        Cvchargemod
        % Charge centroid parameter (smorr) 
        Ados
        % Charge centroid parameter (smorr) 
        Bdos
        % Parameter for non-silicon substrate or metal gate selector (sm-ri) 
        Mtrlmod
        % Equivalent gate oxide thickness in meters (smorr) Unit: m
        Eot
        % Voltage for extraction of Equivalent gate oxide thickness (smorr) Unit: V
        Vddeot
        % Work function of gate (smorr) 
        Phig
        % Dielectric constant of gate relative to vacuum (smorr) 
        Epsrgate
        % Electron affinity of substrate (smorr) Unit: eV
        Easub
        % Dielectric constant of substrate relative to vacuum (smorr) 
        Epsrsub
        % Intrinsic carrier concentration of substrate at 300.15K (smorr) Unit: cm^-3
        Ni0sub
        % Band-gap of substrate at T=0K (smorr) Unit: eV
        Bg0sub
        % First parameter of band-gap change due to temperature (smorr) 
        Tbgasub
        % Second parameter of band-gap change due to temperature (smorr) 
        Tbgbsub
        % Fitting parameter for moderate inversion in Vgsteffcv (smorr) 
        Minvcv
        % Length dependence parameter for Vth offset in CV (smorr) 
        Voffcvl
        % Length dependence of Minvcv (smorr) 
        Lminvcv
        % Width dependence of Minvcv (smorr) 
        Wminvcv
        % Cross-term dependence of Minvcv (smorr) 
        Pminvcv
        % Pre-exponential constant for GISL (smorr) Unit: Ohm^-1
        Agisl
        % Exponential constant for GISL (smorr) Unit: V/m
        Bgisl
        % Fitting parameter for Bandbending (smorr) Unit: V
        Egisl
        % Parameter for body-bias dependence of GISL (smorr) Unit: V^3
        Cgisl
        % Length dependence of Agisl (smorr) 
        Lagisl
        % Length dependence of Bgisl (smorr) 
        Lbgisl
        % Length dependence of Egisl (smorr) 
        Legisl
        % Length dependence of Cgisl (smorr) 
        Lcgisl
        % Width dependence of Agisl (smorr) 
        Wagisl
        % Width dependence of Bgisl (smorr) 
        Wbgisl
        % Width dependence of Egisl (smorr) 
        Wegisl
        % Width dependence of Cgisl (smorr) 
        Wcgisl
        % Cross-term dependence of Agisl (smorr) 
        Pagisl
        % Cross-term dependence of Bgisl (smorr) 
        Pbgisl
        % Cross-term dependence of Egisl (smorr) 
        Pegisl
        % Cross-term dependence of Cgisl (smorr) 
        Pcgisl
        % Parameter for Igs (smorr) 
        Aigs
        % Parameter for Igs (smorr) 
        Bigs
        % Parameter for Igs (smorr) 
        Cigs
        % Length dependence of Aigs (smorr) 
        Laigs
        % Length dependence of Bigs (smorr) 
        Lbigs
        % Length dependence of Cigs (smorr) 
        Lcigs
        % Width dependence of Aigs (smorr) 
        Waigs
        % Width dependence of Bigs (smorr) 
        Wbigs
        % Width dependence of Cigs (smorr) 
        Wcigs
        % Cross-term dependence of Aigs (smorr) 
        Paigs
        % Cross-term dependence of Bigs (smorr) 
        Pbigs
        % Cross-term dependence of Cigs (smorr) 
        Pcigs
        % Parameter for Igd (smorr) 
        Aigd
        % Parameter for Igd (smorr) 
        Bigd
        % Parameter for Igd (smorr) 
        Cigd
        % Length dependence of Aigd (smorr) 
        Laigd
        % Length dependence of Bigd (smorr) 
        Lbigd
        % Length dependence of Cigd (smorr) 
        Lcigd
        % Width dependence of Aigd (smorr) 
        Waigd
        % Width dependence of Bigd (smorr) 
        Wbigd
        % Width dependence of Cigd (smorr) 
        Wcigd
        % Cross-term dependence of Aigd (smorr) 
        Paigd
        % Cross-term dependence of Bigd (smorr) 
        Pbigd
        % Cross-term dependence of Cigd (smorr) 
        Pcigd
        % Delta L for Ig model drain side (smorr) Unit: m
        Dlcigd
        % Non-ideality factor for bottom junction drain side (smorr) 
        Njtsd
        % Non-ideality factor for STI sidewall junction drain side (smorr) 
        Njtsswd
        % Non-ideality factor for gate-edge sidewall junction drain side (smorr) 
        Njtsswgd
        % Temperature coefficient for Njtsd (smorr) 
        Tnjtsd
        % Temperature coefficient for Njtsswd (smorr) 
        Tnjtsswd
        % Temperature coefficient for Njtsswgd (smorr) 
        Tnjtsswgd
        % Temperature for extraction of EOT (smorr) Unit: K
        Tempeot
        % Effective length for extraction of EOT (smorr) Unit: um
        Leffeot
        % Effective width for extraction of EOT (smorr) Unit: um
        Weffeot
        % Colombic scattering exponent (smorr) 
        Ucs
        % Temperature coefficient of colombic mobility (smorr) 
        Ucste
        % TAT current width dependance (smorr) 
        Jtweff
        % Length dependence of ucste (smorr) 
        Lucste
        % Length dependence of ucs (smorr) 
        Lucs
        % Width dependence of ucste (smorr) 
        Wucste
        % Width dependence of ucs (smorr) 
        Wucs
        % Cross-term dependence of ucste (smorr) 
        Pucste
        % Cross-term dependence of ucs (smorr) 
        Pucs
        % New Material Mod backward compatibility selector (sm-ri) 
        Mtrlcompatmod
        % parameter for GIDL selector (sm-ri) 
        Gidlmod
        % 3rd parameter for Vth shift due to pocket (smorr) 
        Dvtp2
        % 4th parameter for Vth shift due to pocket (smorr) 
        Dvtp3
        % 5th parameter for Vth shift due to pocket (smorr) 
        Dvtp4
        % 6th parameter for Vth shift due to pocket (smorr) 
        Dvtp5
        % Temperature parameter for nfactor (smorr) 
        Tnfactor
        % Temperature parameter for eta0 (smorr) 
        Teta0
        % Temperature parameter for tvoffcv (smorr) 
        Tvoffcv
        % GIDL vb parameter (smorr) 
        Fgidl
        % GIDL vg parameter (smorr) 
        Rgidl
        % GIDL vb parameter (smorr) 
        Kgidl
        % GISL vb parameter (smorr) 
        Fgisl
        % GIsL vg parameter (smorr) 
        Rgisl
        % GISL vb parameter (smorr) 
        Kgisl
        % Thermal noise parameter (smorr) 
        Tnoic
        % Thermal noise coefficient (smorr) 
        Rnoic
        % Length dependence of dvtp2 (smorr) 
        Ldvtp2
        % Length dependence of dvtp3 (smorr) 
        Ldvtp3
        % Length dependence of dvtp4 (smorr) 
        Ldvtp4
        % Length dependence of dvtp5 (smorr) 
        Ldvtp5
        % Width dependence of dvtp2 (smorr) 
        Wdvtp2
        % Width dependence of dvtp3 (smorr) 
        Wdvtp3
        % Width dependence of dvtp4 (smorr) 
        Wdvtp4
        % Width dependence of dvtp5 (smorr) 
        Wdvtp5
        % Cross-term dependence of dvtp2 (smorr) 
        Pdvtp2
        % Cross-term dependence of dvtp3 (smorr) 
        Pdvtp3
        % Cross-term dependence of dvtp4 (smorr) 
        Pdvtp4
        % Cross-term dependence of dvtp5 (smorr) 
        Pdvtp5
        % Length dependence of tnfactor (smorr) 
        Ltnfactor
        % Width dependence of tnfactor (smorr) 
        Wtnfactor
        % Cross-term dependence of tnfactor (smorr) 
        Ptnfactor
        % Length dependence of teta0 (smorr) 
        Lteta0
        % Width dependence of teta0 (smorr) 
        Wteta0
        % Cross-term dependence of teta0 (smorr) 
        Pteta0
        % Length dependence of tvoffcv (smorr) 
        Ltvoffcv
        % Width dependence of tvoffcv (smorr) 
        Wtvoffcv
        % Cross-term dependence of tvoffcv (smorr) 
        Ptvoffcv
        % Length dependence of fgidl (smorr) 
        Lfgidl
        % Width dependence of fgidl (smorr) 
        Wfgidl
        % Cross-term dependence of fgidl (smorr) 
        Pfgidl
        % Length dependence of rgidl (smorr) 
        Lrgidl
        % Width dependence of rgidl (smorr) 
        Wrgidl
        % Cross-term dependence of rgidl (smorr) 
        Prgidl
        % Length dependence of kgidl (smorr) 
        Lkgidl
        % Width dependence of kgidl (smorr) 
        Wkgidl
        % Cross-term dependence of kgidl (smorr) 
        Pkgidl
        % Length dependence of fgisl (smorr) 
        Lfgisl
        % Width dependence of fgisl (smorr) 
        Wfgisl
        % Cross-term dependence of fgisl (smorr) 
        Pfgisl
        % Length dependence of rgisl (smorr) 
        Lrgisl
        % Width dependence of rgisl (smorr) 
        Wrgisl
        % Cross-term dependence of rgisl (smorr) 
        Prgisl
        % Length dependence of kgisl (smorr) 
        Lkgisl
        % Width dependence of kgisl (smorr) 
        Wkgisl
        % Cross-term dependence of kgisl (smorr) 
        Pkgisl
        %  (sm-ri) 
        Updatelevel
        % Gate width shrink factor (smorr) 
        Wmlt
        % Gate length shrink factor (smorr) 
        Lmlt
        % Maximum gate to source voltage (TSMC SOA warning) (smorr) Unit: V
        Vgs_max
        % Maximum gate to drain voltage (TSMC SOA warning) (smorr) Unit: V
        Vgd_max
        % Maximum drain to source voltage (TSMC SOA warning) (smorr) Unit: V
        Vds_max
        % Maximum bulk to drain voltage (TSMC SOA warning) (smorr) Unit: V
        Vbd_max
        % Maximum bulk to substrate voltage (TSMC SOA warning) (smorr) Unit: V
        Vbs_max
        % Minimum resistance (smorr) 
        Minr
        % Drain contact resistance (smorr) 
        Rdc
        % Source contact resistance (smorr) 
        Rsc
    end
    methods
        function obj = set.NMOS(obj,val)
            obj = setParameter(obj,'NMOS',val,0,'boolean');
        end
        function res = get.NMOS(obj)
            res = getParameter(obj,'NMOS');
        end
        function obj = set.PMOS(obj,val)
            obj = setParameter(obj,'PMOS',val,0,'boolean');
        end
        function res = get.PMOS(obj)
            res = getParameter(obj,'PMOS');
        end
        function obj = set.Capmod(obj,val)
            obj = setParameter(obj,'Capmod',val,0,'integer');
        end
        function res = get.Capmod(obj)
            res = getParameter(obj,'Capmod');
        end
        function obj = set.Diomod(obj,val)
            obj = setParameter(obj,'Diomod',val,0,'integer');
        end
        function res = get.Diomod(obj)
            res = getParameter(obj,'Diomod');
        end
        function obj = set.Rdsmod(obj,val)
            obj = setParameter(obj,'Rdsmod',val,0,'integer');
        end
        function res = get.Rdsmod(obj)
            res = getParameter(obj,'Rdsmod');
        end
        function obj = set.Trnqsmod(obj,val)
            obj = setParameter(obj,'Trnqsmod',val,0,'integer');
        end
        function res = get.Trnqsmod(obj)
            res = getParameter(obj,'Trnqsmod');
        end
        function obj = set.Acnqsmod(obj,val)
            obj = setParameter(obj,'Acnqsmod',val,0,'integer');
        end
        function res = get.Acnqsmod(obj)
            res = getParameter(obj,'Acnqsmod');
        end
        function obj = set.Mobmod(obj,val)
            obj = setParameter(obj,'Mobmod',val,0,'integer');
        end
        function res = get.Mobmod(obj)
            res = getParameter(obj,'Mobmod');
        end
        function obj = set.Rbodymod(obj,val)
            obj = setParameter(obj,'Rbodymod',val,0,'integer');
        end
        function res = get.Rbodymod(obj)
            res = getParameter(obj,'Rbodymod');
        end
        function obj = set.Rgatemod(obj,val)
            obj = setParameter(obj,'Rgatemod',val,0,'integer');
        end
        function res = get.Rgatemod(obj)
            res = getParameter(obj,'Rgatemod');
        end
        function obj = set.Rgeomod(obj,val)
            obj = setParameter(obj,'Rgeomod',val,0,'integer');
        end
        function res = get.Rgeomod(obj)
            res = getParameter(obj,'Rgeomod');
        end
        function obj = set.Nf(obj,val)
            obj = setParameter(obj,'Nf',val,0,'real');
        end
        function res = get.Nf(obj)
            res = getParameter(obj,'Nf');
        end
        function obj = set.Permod(obj,val)
            obj = setParameter(obj,'Permod',val,0,'integer');
        end
        function res = get.Permod(obj)
            res = getParameter(obj,'Permod');
        end
        function obj = set.Geomod(obj,val)
            obj = setParameter(obj,'Geomod',val,0,'integer');
        end
        function res = get.Geomod(obj)
            res = getParameter(obj,'Geomod');
        end
        function obj = set.Fnoimod(obj,val)
            obj = setParameter(obj,'Fnoimod',val,0,'integer');
        end
        function res = get.Fnoimod(obj)
            res = getParameter(obj,'Fnoimod');
        end
        function obj = set.Tnoimod(obj,val)
            obj = setParameter(obj,'Tnoimod',val,0,'integer');
        end
        function res = get.Tnoimod(obj)
            res = getParameter(obj,'Tnoimod');
        end
        function obj = set.Igcmod(obj,val)
            obj = setParameter(obj,'Igcmod',val,0,'integer');
        end
        function res = get.Igcmod(obj)
            res = getParameter(obj,'Igcmod');
        end
        function obj = set.Igbmod(obj,val)
            obj = setParameter(obj,'Igbmod',val,0,'integer');
        end
        function res = get.Igbmod(obj)
            res = getParameter(obj,'Igbmod');
        end
        function obj = set.Paramchk(obj,val)
            obj = setParameter(obj,'Paramchk',val,0,'integer');
        end
        function res = get.Paramchk(obj)
            res = getParameter(obj,'Paramchk');
        end
        function obj = set.Binunit(obj,val)
            obj = setParameter(obj,'Binunit',val,0,'integer');
        end
        function res = get.Binunit(obj)
            res = getParameter(obj,'Binunit');
        end
        function obj = set.Version(obj,val)
            obj = setParameter(obj,'Version',val,0,'real');
        end
        function res = get.Version(obj)
            res = getParameter(obj,'Version');
        end
        function obj = set.Toxe(obj,val)
            obj = setParameter(obj,'Toxe',val,0,'real');
        end
        function res = get.Toxe(obj)
            res = getParameter(obj,'Toxe');
        end
        function obj = set.Toxp(obj,val)
            obj = setParameter(obj,'Toxp',val,0,'real');
        end
        function res = get.Toxp(obj)
            res = getParameter(obj,'Toxp');
        end
        function obj = set.Toxm(obj,val)
            obj = setParameter(obj,'Toxm',val,0,'real');
        end
        function res = get.Toxm(obj)
            res = getParameter(obj,'Toxm');
        end
        function obj = set.Toxref(obj,val)
            obj = setParameter(obj,'Toxref',val,0,'real');
        end
        function res = get.Toxref(obj)
            res = getParameter(obj,'Toxref');
        end
        function obj = set.Dtox(obj,val)
            obj = setParameter(obj,'Dtox',val,0,'real');
        end
        function res = get.Dtox(obj)
            res = getParameter(obj,'Dtox');
        end
        function obj = set.Epsrox(obj,val)
            obj = setParameter(obj,'Epsrox',val,0,'real');
        end
        function res = get.Epsrox(obj)
            res = getParameter(obj,'Epsrox');
        end
        function obj = set.Cdsc(obj,val)
            obj = setParameter(obj,'Cdsc',val,0,'real');
        end
        function res = get.Cdsc(obj)
            res = getParameter(obj,'Cdsc');
        end
        function obj = set.Cdscb(obj,val)
            obj = setParameter(obj,'Cdscb',val,0,'real');
        end
        function res = get.Cdscb(obj)
            res = getParameter(obj,'Cdscb');
        end
        function obj = set.Cdscd(obj,val)
            obj = setParameter(obj,'Cdscd',val,0,'real');
        end
        function res = get.Cdscd(obj)
            res = getParameter(obj,'Cdscd');
        end
        function obj = set.Cit(obj,val)
            obj = setParameter(obj,'Cit',val,0,'real');
        end
        function res = get.Cit(obj)
            res = getParameter(obj,'Cit');
        end
        function obj = set.Nfactor(obj,val)
            obj = setParameter(obj,'Nfactor',val,0,'real');
        end
        function res = get.Nfactor(obj)
            res = getParameter(obj,'Nfactor');
        end
        function obj = set.Xj(obj,val)
            obj = setParameter(obj,'Xj',val,0,'real');
        end
        function res = get.Xj(obj)
            res = getParameter(obj,'Xj');
        end
        function obj = set.Vsat(obj,val)
            obj = setParameter(obj,'Vsat',val,0,'real');
        end
        function res = get.Vsat(obj)
            res = getParameter(obj,'Vsat');
        end
        function obj = set.At(obj,val)
            obj = setParameter(obj,'At',val,0,'real');
        end
        function res = get.At(obj)
            res = getParameter(obj,'At');
        end
        function obj = set.A0(obj,val)
            obj = setParameter(obj,'A0',val,0,'real');
        end
        function res = get.A0(obj)
            res = getParameter(obj,'A0');
        end
        function obj = set.Ags(obj,val)
            obj = setParameter(obj,'Ags',val,0,'real');
        end
        function res = get.Ags(obj)
            res = getParameter(obj,'Ags');
        end
        function obj = set.A1(obj,val)
            obj = setParameter(obj,'A1',val,0,'real');
        end
        function res = get.A1(obj)
            res = getParameter(obj,'A1');
        end
        function obj = set.A2(obj,val)
            obj = setParameter(obj,'A2',val,0,'real');
        end
        function res = get.A2(obj)
            res = getParameter(obj,'A2');
        end
        function obj = set.Keta(obj,val)
            obj = setParameter(obj,'Keta',val,0,'real');
        end
        function res = get.Keta(obj)
            res = getParameter(obj,'Keta');
        end
        function obj = set.Nsub(obj,val)
            obj = setParameter(obj,'Nsub',val,0,'real');
        end
        function res = get.Nsub(obj)
            res = getParameter(obj,'Nsub');
        end
        function obj = set.Ndep(obj,val)
            obj = setParameter(obj,'Ndep',val,0,'real');
        end
        function res = get.Ndep(obj)
            res = getParameter(obj,'Ndep');
        end
        function obj = set.Nsd(obj,val)
            obj = setParameter(obj,'Nsd',val,0,'real');
        end
        function res = get.Nsd(obj)
            res = getParameter(obj,'Nsd');
        end
        function obj = set.Phin(obj,val)
            obj = setParameter(obj,'Phin',val,0,'real');
        end
        function res = get.Phin(obj)
            res = getParameter(obj,'Phin');
        end
        function obj = set.Ngate(obj,val)
            obj = setParameter(obj,'Ngate',val,0,'real');
        end
        function res = get.Ngate(obj)
            res = getParameter(obj,'Ngate');
        end
        function obj = set.Gamma1(obj,val)
            obj = setParameter(obj,'Gamma1',val,0,'real');
        end
        function res = get.Gamma1(obj)
            res = getParameter(obj,'Gamma1');
        end
        function obj = set.Gamma2(obj,val)
            obj = setParameter(obj,'Gamma2',val,0,'real');
        end
        function res = get.Gamma2(obj)
            res = getParameter(obj,'Gamma2');
        end
        function obj = set.Vbx(obj,val)
            obj = setParameter(obj,'Vbx',val,0,'real');
        end
        function res = get.Vbx(obj)
            res = getParameter(obj,'Vbx');
        end
        function obj = set.Vbm(obj,val)
            obj = setParameter(obj,'Vbm',val,0,'real');
        end
        function res = get.Vbm(obj)
            res = getParameter(obj,'Vbm');
        end
        function obj = set.Xt(obj,val)
            obj = setParameter(obj,'Xt',val,0,'real');
        end
        function res = get.Xt(obj)
            res = getParameter(obj,'Xt');
        end
        function obj = set.K1(obj,val)
            obj = setParameter(obj,'K1',val,0,'real');
        end
        function res = get.K1(obj)
            res = getParameter(obj,'K1');
        end
        function obj = set.Kt1(obj,val)
            obj = setParameter(obj,'Kt1',val,0,'real');
        end
        function res = get.Kt1(obj)
            res = getParameter(obj,'Kt1');
        end
        function obj = set.Kt1l(obj,val)
            obj = setParameter(obj,'Kt1l',val,0,'real');
        end
        function res = get.Kt1l(obj)
            res = getParameter(obj,'Kt1l');
        end
        function obj = set.Kt2(obj,val)
            obj = setParameter(obj,'Kt2',val,0,'real');
        end
        function res = get.Kt2(obj)
            res = getParameter(obj,'Kt2');
        end
        function obj = set.K2(obj,val)
            obj = setParameter(obj,'K2',val,0,'real');
        end
        function res = get.K2(obj)
            res = getParameter(obj,'K2');
        end
        function obj = set.K3(obj,val)
            obj = setParameter(obj,'K3',val,0,'real');
        end
        function res = get.K3(obj)
            res = getParameter(obj,'K3');
        end
        function obj = set.K3b(obj,val)
            obj = setParameter(obj,'K3b',val,0,'real');
        end
        function res = get.K3b(obj)
            res = getParameter(obj,'K3b');
        end
        function obj = set.W0(obj,val)
            obj = setParameter(obj,'W0',val,0,'real');
        end
        function res = get.W0(obj)
            res = getParameter(obj,'W0');
        end
        function obj = set.Dvtp0(obj,val)
            obj = setParameter(obj,'Dvtp0',val,0,'real');
        end
        function res = get.Dvtp0(obj)
            res = getParameter(obj,'Dvtp0');
        end
        function obj = set.Dvtp1(obj,val)
            obj = setParameter(obj,'Dvtp1',val,0,'real');
        end
        function res = get.Dvtp1(obj)
            res = getParameter(obj,'Dvtp1');
        end
        function obj = set.Lpe0(obj,val)
            obj = setParameter(obj,'Lpe0',val,0,'real');
        end
        function res = get.Lpe0(obj)
            res = getParameter(obj,'Lpe0');
        end
        function obj = set.Lpeb(obj,val)
            obj = setParameter(obj,'Lpeb',val,0,'real');
        end
        function res = get.Lpeb(obj)
            res = getParameter(obj,'Lpeb');
        end
        function obj = set.Dvt0(obj,val)
            obj = setParameter(obj,'Dvt0',val,0,'real');
        end
        function res = get.Dvt0(obj)
            res = getParameter(obj,'Dvt0');
        end
        function obj = set.Dvt1(obj,val)
            obj = setParameter(obj,'Dvt1',val,0,'real');
        end
        function res = get.Dvt1(obj)
            res = getParameter(obj,'Dvt1');
        end
        function obj = set.Dvt2(obj,val)
            obj = setParameter(obj,'Dvt2',val,0,'real');
        end
        function res = get.Dvt2(obj)
            res = getParameter(obj,'Dvt2');
        end
        function obj = set.Dvt0w(obj,val)
            obj = setParameter(obj,'Dvt0w',val,0,'real');
        end
        function res = get.Dvt0w(obj)
            res = getParameter(obj,'Dvt0w');
        end
        function obj = set.Dvt1w(obj,val)
            obj = setParameter(obj,'Dvt1w',val,0,'real');
        end
        function res = get.Dvt1w(obj)
            res = getParameter(obj,'Dvt1w');
        end
        function obj = set.Dvt2w(obj,val)
            obj = setParameter(obj,'Dvt2w',val,0,'real');
        end
        function res = get.Dvt2w(obj)
            res = getParameter(obj,'Dvt2w');
        end
        function obj = set.Drout(obj,val)
            obj = setParameter(obj,'Drout',val,0,'real');
        end
        function res = get.Drout(obj)
            res = getParameter(obj,'Drout');
        end
        function obj = set.Dsub(obj,val)
            obj = setParameter(obj,'Dsub',val,0,'real');
        end
        function res = get.Dsub(obj)
            res = getParameter(obj,'Dsub');
        end
        function obj = set.Vth0(obj,val)
            obj = setParameter(obj,'Vth0',val,0,'real');
        end
        function res = get.Vth0(obj)
            res = getParameter(obj,'Vth0');
        end
        function obj = set.Ua(obj,val)
            obj = setParameter(obj,'Ua',val,0,'real');
        end
        function res = get.Ua(obj)
            res = getParameter(obj,'Ua');
        end
        function obj = set.Ua1(obj,val)
            obj = setParameter(obj,'Ua1',val,0,'real');
        end
        function res = get.Ua1(obj)
            res = getParameter(obj,'Ua1');
        end
        function obj = set.Ub(obj,val)
            obj = setParameter(obj,'Ub',val,0,'real');
        end
        function res = get.Ub(obj)
            res = getParameter(obj,'Ub');
        end
        function obj = set.Ub1(obj,val)
            obj = setParameter(obj,'Ub1',val,0,'real');
        end
        function res = get.Ub1(obj)
            res = getParameter(obj,'Ub1');
        end
        function obj = set.Uc(obj,val)
            obj = setParameter(obj,'Uc',val,0,'real');
        end
        function res = get.Uc(obj)
            res = getParameter(obj,'Uc');
        end
        function obj = set.Uc1(obj,val)
            obj = setParameter(obj,'Uc1',val,0,'real');
        end
        function res = get.Uc1(obj)
            res = getParameter(obj,'Uc1');
        end
        function obj = set.U0(obj,val)
            obj = setParameter(obj,'U0',val,0,'real');
        end
        function res = get.U0(obj)
            res = getParameter(obj,'U0');
        end
        function obj = set.Eu(obj,val)
            obj = setParameter(obj,'Eu',val,0,'real');
        end
        function res = get.Eu(obj)
            res = getParameter(obj,'Eu');
        end
        function obj = set.Ute(obj,val)
            obj = setParameter(obj,'Ute',val,0,'real');
        end
        function res = get.Ute(obj)
            res = getParameter(obj,'Ute');
        end
        function obj = set.Voff(obj,val)
            obj = setParameter(obj,'Voff',val,0,'real');
        end
        function res = get.Voff(obj)
            res = getParameter(obj,'Voff');
        end
        function obj = set.Minv(obj,val)
            obj = setParameter(obj,'Minv',val,0,'real');
        end
        function res = get.Minv(obj)
            res = getParameter(obj,'Minv');
        end
        function obj = set.Voffl(obj,val)
            obj = setParameter(obj,'Voffl',val,0,'real');
        end
        function res = get.Voffl(obj)
            res = getParameter(obj,'Voffl');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Cgso(obj,val)
            obj = setParameter(obj,'Cgso',val,0,'real');
        end
        function res = get.Cgso(obj)
            res = getParameter(obj,'Cgso');
        end
        function obj = set.Cgdo(obj,val)
            obj = setParameter(obj,'Cgdo',val,0,'real');
        end
        function res = get.Cgdo(obj)
            res = getParameter(obj,'Cgdo');
        end
        function obj = set.Cgbo(obj,val)
            obj = setParameter(obj,'Cgbo',val,0,'real');
        end
        function res = get.Cgbo(obj)
            res = getParameter(obj,'Cgbo');
        end
        function obj = set.Xpart(obj,val)
            obj = setParameter(obj,'Xpart',val,0,'real');
        end
        function res = get.Xpart(obj)
            res = getParameter(obj,'Xpart');
        end
        function obj = set.Delta(obj,val)
            obj = setParameter(obj,'Delta',val,0,'real');
        end
        function res = get.Delta(obj)
            res = getParameter(obj,'Delta');
        end
        function obj = set.Rsh(obj,val)
            obj = setParameter(obj,'Rsh',val,0,'real');
        end
        function res = get.Rsh(obj)
            res = getParameter(obj,'Rsh');
        end
        function obj = set.Rdsw(obj,val)
            obj = setParameter(obj,'Rdsw',val,0,'real');
        end
        function res = get.Rdsw(obj)
            res = getParameter(obj,'Rdsw');
        end
        function obj = set.Rdswmin(obj,val)
            obj = setParameter(obj,'Rdswmin',val,0,'real');
        end
        function res = get.Rdswmin(obj)
            res = getParameter(obj,'Rdswmin');
        end
        function obj = set.Rsw(obj,val)
            obj = setParameter(obj,'Rsw',val,0,'real');
        end
        function res = get.Rsw(obj)
            res = getParameter(obj,'Rsw');
        end
        function obj = set.Rdw(obj,val)
            obj = setParameter(obj,'Rdw',val,0,'real');
        end
        function res = get.Rdw(obj)
            res = getParameter(obj,'Rdw');
        end
        function obj = set.Rdwmin(obj,val)
            obj = setParameter(obj,'Rdwmin',val,0,'real');
        end
        function res = get.Rdwmin(obj)
            res = getParameter(obj,'Rdwmin');
        end
        function obj = set.Rswmin(obj,val)
            obj = setParameter(obj,'Rswmin',val,0,'real');
        end
        function res = get.Rswmin(obj)
            res = getParameter(obj,'Rswmin');
        end
        function obj = set.Prwg(obj,val)
            obj = setParameter(obj,'Prwg',val,0,'real');
        end
        function res = get.Prwg(obj)
            res = getParameter(obj,'Prwg');
        end
        function obj = set.Prwb(obj,val)
            obj = setParameter(obj,'Prwb',val,0,'real');
        end
        function res = get.Prwb(obj)
            res = getParameter(obj,'Prwb');
        end
        function obj = set.Prt(obj,val)
            obj = setParameter(obj,'Prt',val,0,'real');
        end
        function res = get.Prt(obj)
            res = getParameter(obj,'Prt');
        end
        function obj = set.Eta0(obj,val)
            obj = setParameter(obj,'Eta0',val,0,'real');
        end
        function res = get.Eta0(obj)
            res = getParameter(obj,'Eta0');
        end
        function obj = set.Etab(obj,val)
            obj = setParameter(obj,'Etab',val,0,'real');
        end
        function res = get.Etab(obj)
            res = getParameter(obj,'Etab');
        end
        function obj = set.Pclm(obj,val)
            obj = setParameter(obj,'Pclm',val,0,'real');
        end
        function res = get.Pclm(obj)
            res = getParameter(obj,'Pclm');
        end
        function obj = set.Pdiblc1(obj,val)
            obj = setParameter(obj,'Pdiblc1',val,0,'real');
        end
        function res = get.Pdiblc1(obj)
            res = getParameter(obj,'Pdiblc1');
        end
        function obj = set.Pdiblc2(obj,val)
            obj = setParameter(obj,'Pdiblc2',val,0,'real');
        end
        function res = get.Pdiblc2(obj)
            res = getParameter(obj,'Pdiblc2');
        end
        function obj = set.Pdiblcb(obj,val)
            obj = setParameter(obj,'Pdiblcb',val,0,'real');
        end
        function res = get.Pdiblcb(obj)
            res = getParameter(obj,'Pdiblcb');
        end
        function obj = set.Fprout(obj,val)
            obj = setParameter(obj,'Fprout',val,0,'real');
        end
        function res = get.Fprout(obj)
            res = getParameter(obj,'Fprout');
        end
        function obj = set.Pdits(obj,val)
            obj = setParameter(obj,'Pdits',val,0,'real');
        end
        function res = get.Pdits(obj)
            res = getParameter(obj,'Pdits');
        end
        function obj = set.Pditsl(obj,val)
            obj = setParameter(obj,'Pditsl',val,0,'real');
        end
        function res = get.Pditsl(obj)
            res = getParameter(obj,'Pditsl');
        end
        function obj = set.Pditsd(obj,val)
            obj = setParameter(obj,'Pditsd',val,0,'real');
        end
        function res = get.Pditsd(obj)
            res = getParameter(obj,'Pditsd');
        end
        function obj = set.Pscbe1(obj,val)
            obj = setParameter(obj,'Pscbe1',val,0,'real');
        end
        function res = get.Pscbe1(obj)
            res = getParameter(obj,'Pscbe1');
        end
        function obj = set.Pscbe2(obj,val)
            obj = setParameter(obj,'Pscbe2',val,0,'real');
        end
        function res = get.Pscbe2(obj)
            res = getParameter(obj,'Pscbe2');
        end
        function obj = set.Pvag(obj,val)
            obj = setParameter(obj,'Pvag',val,0,'real');
        end
        function res = get.Pvag(obj)
            res = getParameter(obj,'Pvag');
        end
        function obj = set.Jss(obj,val)
            obj = setParameter(obj,'Jss',val,0,'real');
        end
        function res = get.Jss(obj)
            res = getParameter(obj,'Jss');
        end
        function obj = set.Jsws(obj,val)
            obj = setParameter(obj,'Jsws',val,0,'real');
        end
        function res = get.Jsws(obj)
            res = getParameter(obj,'Jsws');
        end
        function obj = set.Jswgs(obj,val)
            obj = setParameter(obj,'Jswgs',val,0,'real');
        end
        function res = get.Jswgs(obj)
            res = getParameter(obj,'Jswgs');
        end
        function obj = set.Pbs(obj,val)
            obj = setParameter(obj,'Pbs',val,0,'real');
        end
        function res = get.Pbs(obj)
            res = getParameter(obj,'Pbs');
        end
        function obj = set.Njs(obj,val)
            obj = setParameter(obj,'Njs',val,0,'real');
        end
        function res = get.Njs(obj)
            res = getParameter(obj,'Njs');
        end
        function obj = set.Xtis(obj,val)
            obj = setParameter(obj,'Xtis',val,0,'real');
        end
        function res = get.Xtis(obj)
            res = getParameter(obj,'Xtis');
        end
        function obj = set.Mjs(obj,val)
            obj = setParameter(obj,'Mjs',val,0,'real');
        end
        function res = get.Mjs(obj)
            res = getParameter(obj,'Mjs');
        end
        function obj = set.Pbsws(obj,val)
            obj = setParameter(obj,'Pbsws',val,0,'real');
        end
        function res = get.Pbsws(obj)
            res = getParameter(obj,'Pbsws');
        end
        function obj = set.Mjsws(obj,val)
            obj = setParameter(obj,'Mjsws',val,0,'real');
        end
        function res = get.Mjsws(obj)
            res = getParameter(obj,'Mjsws');
        end
        function obj = set.Pbswgs(obj,val)
            obj = setParameter(obj,'Pbswgs',val,0,'real');
        end
        function res = get.Pbswgs(obj)
            res = getParameter(obj,'Pbswgs');
        end
        function obj = set.Mjswgs(obj,val)
            obj = setParameter(obj,'Mjswgs',val,0,'real');
        end
        function res = get.Mjswgs(obj)
            res = getParameter(obj,'Mjswgs');
        end
        function obj = set.Cjs(obj,val)
            obj = setParameter(obj,'Cjs',val,0,'real');
        end
        function res = get.Cjs(obj)
            res = getParameter(obj,'Cjs');
        end
        function obj = set.Cjsws(obj,val)
            obj = setParameter(obj,'Cjsws',val,0,'real');
        end
        function res = get.Cjsws(obj)
            res = getParameter(obj,'Cjsws');
        end
        function obj = set.Cjswgs(obj,val)
            obj = setParameter(obj,'Cjswgs',val,0,'real');
        end
        function res = get.Cjswgs(obj)
            res = getParameter(obj,'Cjswgs');
        end
        function obj = set.Jsd(obj,val)
            obj = setParameter(obj,'Jsd',val,0,'real');
        end
        function res = get.Jsd(obj)
            res = getParameter(obj,'Jsd');
        end
        function obj = set.Jswd(obj,val)
            obj = setParameter(obj,'Jswd',val,0,'real');
        end
        function res = get.Jswd(obj)
            res = getParameter(obj,'Jswd');
        end
        function obj = set.Jswgd(obj,val)
            obj = setParameter(obj,'Jswgd',val,0,'real');
        end
        function res = get.Jswgd(obj)
            res = getParameter(obj,'Jswgd');
        end
        function obj = set.Pbd(obj,val)
            obj = setParameter(obj,'Pbd',val,0,'real');
        end
        function res = get.Pbd(obj)
            res = getParameter(obj,'Pbd');
        end
        function obj = set.Njd(obj,val)
            obj = setParameter(obj,'Njd',val,0,'real');
        end
        function res = get.Njd(obj)
            res = getParameter(obj,'Njd');
        end
        function obj = set.Xtid(obj,val)
            obj = setParameter(obj,'Xtid',val,0,'real');
        end
        function res = get.Xtid(obj)
            res = getParameter(obj,'Xtid');
        end
        function obj = set.Mjd(obj,val)
            obj = setParameter(obj,'Mjd',val,0,'real');
        end
        function res = get.Mjd(obj)
            res = getParameter(obj,'Mjd');
        end
        function obj = set.Pbswd(obj,val)
            obj = setParameter(obj,'Pbswd',val,0,'real');
        end
        function res = get.Pbswd(obj)
            res = getParameter(obj,'Pbswd');
        end
        function obj = set.Mjswd(obj,val)
            obj = setParameter(obj,'Mjswd',val,0,'real');
        end
        function res = get.Mjswd(obj)
            res = getParameter(obj,'Mjswd');
        end
        function obj = set.Pbswgd(obj,val)
            obj = setParameter(obj,'Pbswgd',val,0,'real');
        end
        function res = get.Pbswgd(obj)
            res = getParameter(obj,'Pbswgd');
        end
        function obj = set.Mjswgd(obj,val)
            obj = setParameter(obj,'Mjswgd',val,0,'real');
        end
        function res = get.Mjswgd(obj)
            res = getParameter(obj,'Mjswgd');
        end
        function obj = set.Cjd(obj,val)
            obj = setParameter(obj,'Cjd',val,0,'real');
        end
        function res = get.Cjd(obj)
            res = getParameter(obj,'Cjd');
        end
        function obj = set.Cjswd(obj,val)
            obj = setParameter(obj,'Cjswd',val,0,'real');
        end
        function res = get.Cjswd(obj)
            res = getParameter(obj,'Cjswd');
        end
        function obj = set.Cjswgd(obj,val)
            obj = setParameter(obj,'Cjswgd',val,0,'real');
        end
        function res = get.Cjswgd(obj)
            res = getParameter(obj,'Cjswgd');
        end
        function obj = set.Vfbcv(obj,val)
            obj = setParameter(obj,'Vfbcv',val,0,'real');
        end
        function res = get.Vfbcv(obj)
            res = getParameter(obj,'Vfbcv');
        end
        function obj = set.Vfb(obj,val)
            obj = setParameter(obj,'Vfb',val,0,'real');
        end
        function res = get.Vfb(obj)
            res = getParameter(obj,'Vfb');
        end
        function obj = set.Tpb(obj,val)
            obj = setParameter(obj,'Tpb',val,0,'real');
        end
        function res = get.Tpb(obj)
            res = getParameter(obj,'Tpb');
        end
        function obj = set.Tcj(obj,val)
            obj = setParameter(obj,'Tcj',val,0,'real');
        end
        function res = get.Tcj(obj)
            res = getParameter(obj,'Tcj');
        end
        function obj = set.Tpbsw(obj,val)
            obj = setParameter(obj,'Tpbsw',val,0,'real');
        end
        function res = get.Tpbsw(obj)
            res = getParameter(obj,'Tpbsw');
        end
        function obj = set.Tcjsw(obj,val)
            obj = setParameter(obj,'Tcjsw',val,0,'real');
        end
        function res = get.Tcjsw(obj)
            res = getParameter(obj,'Tcjsw');
        end
        function obj = set.Tpbswg(obj,val)
            obj = setParameter(obj,'Tpbswg',val,0,'real');
        end
        function res = get.Tpbswg(obj)
            res = getParameter(obj,'Tpbswg');
        end
        function obj = set.Tcjswg(obj,val)
            obj = setParameter(obj,'Tcjswg',val,0,'real');
        end
        function res = get.Tcjswg(obj)
            res = getParameter(obj,'Tcjswg');
        end
        function obj = set.Acde(obj,val)
            obj = setParameter(obj,'Acde',val,0,'real');
        end
        function res = get.Acde(obj)
            res = getParameter(obj,'Acde');
        end
        function obj = set.Moin(obj,val)
            obj = setParameter(obj,'Moin',val,0,'real');
        end
        function res = get.Moin(obj)
            res = getParameter(obj,'Moin');
        end
        function obj = set.Noff(obj,val)
            obj = setParameter(obj,'Noff',val,0,'real');
        end
        function res = get.Noff(obj)
            res = getParameter(obj,'Noff');
        end
        function obj = set.Voffcv(obj,val)
            obj = setParameter(obj,'Voffcv',val,0,'real');
        end
        function res = get.Voffcv(obj)
            res = getParameter(obj,'Voffcv');
        end
        function obj = set.Dmcg(obj,val)
            obj = setParameter(obj,'Dmcg',val,0,'real');
        end
        function res = get.Dmcg(obj)
            res = getParameter(obj,'Dmcg');
        end
        function obj = set.Dmci(obj,val)
            obj = setParameter(obj,'Dmci',val,0,'real');
        end
        function res = get.Dmci(obj)
            res = getParameter(obj,'Dmci');
        end
        function obj = set.Dmdg(obj,val)
            obj = setParameter(obj,'Dmdg',val,0,'real');
        end
        function res = get.Dmdg(obj)
            res = getParameter(obj,'Dmdg');
        end
        function obj = set.Dmcgt(obj,val)
            obj = setParameter(obj,'Dmcgt',val,0,'real');
        end
        function res = get.Dmcgt(obj)
            res = getParameter(obj,'Dmcgt');
        end
        function obj = set.Xgw(obj,val)
            obj = setParameter(obj,'Xgw',val,0,'real');
        end
        function res = get.Xgw(obj)
            res = getParameter(obj,'Xgw');
        end
        function obj = set.Xgl(obj,val)
            obj = setParameter(obj,'Xgl',val,0,'real');
        end
        function res = get.Xgl(obj)
            res = getParameter(obj,'Xgl');
        end
        function obj = set.Rshg(obj,val)
            obj = setParameter(obj,'Rshg',val,0,'real');
        end
        function res = get.Rshg(obj)
            res = getParameter(obj,'Rshg');
        end
        function obj = set.Ngcon(obj,val)
            obj = setParameter(obj,'Ngcon',val,0,'real');
        end
        function res = get.Ngcon(obj)
            res = getParameter(obj,'Ngcon');
        end
        function obj = set.Xrcrg1(obj,val)
            obj = setParameter(obj,'Xrcrg1',val,0,'real');
        end
        function res = get.Xrcrg1(obj)
            res = getParameter(obj,'Xrcrg1');
        end
        function obj = set.Xrcrg2(obj,val)
            obj = setParameter(obj,'Xrcrg2',val,0,'real');
        end
        function res = get.Xrcrg2(obj)
            res = getParameter(obj,'Xrcrg2');
        end
        function obj = set.Lint(obj,val)
            obj = setParameter(obj,'Lint',val,0,'real');
        end
        function res = get.Lint(obj)
            res = getParameter(obj,'Lint');
        end
        function obj = set.Ll(obj,val)
            obj = setParameter(obj,'Ll',val,0,'real');
        end
        function res = get.Ll(obj)
            res = getParameter(obj,'Ll');
        end
        function obj = set.Llc(obj,val)
            obj = setParameter(obj,'Llc',val,0,'real');
        end
        function res = get.Llc(obj)
            res = getParameter(obj,'Llc');
        end
        function obj = set.Lln(obj,val)
            obj = setParameter(obj,'Lln',val,0,'real');
        end
        function res = get.Lln(obj)
            res = getParameter(obj,'Lln');
        end
        function obj = set.Lw(obj,val)
            obj = setParameter(obj,'Lw',val,0,'real');
        end
        function res = get.Lw(obj)
            res = getParameter(obj,'Lw');
        end
        function obj = set.Lwc(obj,val)
            obj = setParameter(obj,'Lwc',val,0,'real');
        end
        function res = get.Lwc(obj)
            res = getParameter(obj,'Lwc');
        end
        function obj = set.Lwn(obj,val)
            obj = setParameter(obj,'Lwn',val,0,'real');
        end
        function res = get.Lwn(obj)
            res = getParameter(obj,'Lwn');
        end
        function obj = set.Lwl(obj,val)
            obj = setParameter(obj,'Lwl',val,0,'real');
        end
        function res = get.Lwl(obj)
            res = getParameter(obj,'Lwl');
        end
        function obj = set.Lwlc(obj,val)
            obj = setParameter(obj,'Lwlc',val,0,'real');
        end
        function res = get.Lwlc(obj)
            res = getParameter(obj,'Lwlc');
        end
        function obj = set.Lmin(obj,val)
            obj = setParameter(obj,'Lmin',val,0,'real');
        end
        function res = get.Lmin(obj)
            res = getParameter(obj,'Lmin');
        end
        function obj = set.Lmax(obj,val)
            obj = setParameter(obj,'Lmax',val,0,'real');
        end
        function res = get.Lmax(obj)
            res = getParameter(obj,'Lmax');
        end
        function obj = set.Wr(obj,val)
            obj = setParameter(obj,'Wr',val,0,'real');
        end
        function res = get.Wr(obj)
            res = getParameter(obj,'Wr');
        end
        function obj = set.Wint(obj,val)
            obj = setParameter(obj,'Wint',val,0,'real');
        end
        function res = get.Wint(obj)
            res = getParameter(obj,'Wint');
        end
        function obj = set.Dwg(obj,val)
            obj = setParameter(obj,'Dwg',val,0,'real');
        end
        function res = get.Dwg(obj)
            res = getParameter(obj,'Dwg');
        end
        function obj = set.Dwb(obj,val)
            obj = setParameter(obj,'Dwb',val,0,'real');
        end
        function res = get.Dwb(obj)
            res = getParameter(obj,'Dwb');
        end
        function obj = set.Wl(obj,val)
            obj = setParameter(obj,'Wl',val,0,'real');
        end
        function res = get.Wl(obj)
            res = getParameter(obj,'Wl');
        end
        function obj = set.Wlc(obj,val)
            obj = setParameter(obj,'Wlc',val,0,'real');
        end
        function res = get.Wlc(obj)
            res = getParameter(obj,'Wlc');
        end
        function obj = set.Wln(obj,val)
            obj = setParameter(obj,'Wln',val,0,'real');
        end
        function res = get.Wln(obj)
            res = getParameter(obj,'Wln');
        end
        function obj = set.Ww(obj,val)
            obj = setParameter(obj,'Ww',val,0,'real');
        end
        function res = get.Ww(obj)
            res = getParameter(obj,'Ww');
        end
        function obj = set.Wwc(obj,val)
            obj = setParameter(obj,'Wwc',val,0,'real');
        end
        function res = get.Wwc(obj)
            res = getParameter(obj,'Wwc');
        end
        function obj = set.Wwn(obj,val)
            obj = setParameter(obj,'Wwn',val,0,'real');
        end
        function res = get.Wwn(obj)
            res = getParameter(obj,'Wwn');
        end
        function obj = set.Wwl(obj,val)
            obj = setParameter(obj,'Wwl',val,0,'real');
        end
        function res = get.Wwl(obj)
            res = getParameter(obj,'Wwl');
        end
        function obj = set.Wwlc(obj,val)
            obj = setParameter(obj,'Wwlc',val,0,'real');
        end
        function res = get.Wwlc(obj)
            res = getParameter(obj,'Wwlc');
        end
        function obj = set.Wmin(obj,val)
            obj = setParameter(obj,'Wmin',val,0,'real');
        end
        function res = get.Wmin(obj)
            res = getParameter(obj,'Wmin');
        end
        function obj = set.Wmax(obj,val)
            obj = setParameter(obj,'Wmax',val,0,'real');
        end
        function res = get.Wmax(obj)
            res = getParameter(obj,'Wmax');
        end
        function obj = set.B0(obj,val)
            obj = setParameter(obj,'B0',val,0,'real');
        end
        function res = get.B0(obj)
            res = getParameter(obj,'B0');
        end
        function obj = set.B1(obj,val)
            obj = setParameter(obj,'B1',val,0,'real');
        end
        function res = get.B1(obj)
            res = getParameter(obj,'B1');
        end
        function obj = set.Cgsl(obj,val)
            obj = setParameter(obj,'Cgsl',val,0,'real');
        end
        function res = get.Cgsl(obj)
            res = getParameter(obj,'Cgsl');
        end
        function obj = set.Cgdl(obj,val)
            obj = setParameter(obj,'Cgdl',val,0,'real');
        end
        function res = get.Cgdl(obj)
            res = getParameter(obj,'Cgdl');
        end
        function obj = set.Ckappas(obj,val)
            obj = setParameter(obj,'Ckappas',val,0,'real');
        end
        function res = get.Ckappas(obj)
            res = getParameter(obj,'Ckappas');
        end
        function obj = set.Ckappad(obj,val)
            obj = setParameter(obj,'Ckappad',val,0,'real');
        end
        function res = get.Ckappad(obj)
            res = getParameter(obj,'Ckappad');
        end
        function obj = set.Cf(obj,val)
            obj = setParameter(obj,'Cf',val,0,'real');
        end
        function res = get.Cf(obj)
            res = getParameter(obj,'Cf');
        end
        function obj = set.Clc(obj,val)
            obj = setParameter(obj,'Clc',val,0,'real');
        end
        function res = get.Clc(obj)
            res = getParameter(obj,'Clc');
        end
        function obj = set.Cle(obj,val)
            obj = setParameter(obj,'Cle',val,0,'real');
        end
        function res = get.Cle(obj)
            res = getParameter(obj,'Cle');
        end
        function obj = set.Dwc(obj,val)
            obj = setParameter(obj,'Dwc',val,0,'real');
        end
        function res = get.Dwc(obj)
            res = getParameter(obj,'Dwc');
        end
        function obj = set.Dlc(obj,val)
            obj = setParameter(obj,'Dlc',val,0,'real');
        end
        function res = get.Dlc(obj)
            res = getParameter(obj,'Dlc');
        end
        function obj = set.Dlcig(obj,val)
            obj = setParameter(obj,'Dlcig',val,0,'real');
        end
        function res = get.Dlcig(obj)
            res = getParameter(obj,'Dlcig');
        end
        function obj = set.Dwj(obj,val)
            obj = setParameter(obj,'Dwj',val,0,'real');
        end
        function res = get.Dwj(obj)
            res = getParameter(obj,'Dwj');
        end
        function obj = set.Alpha0(obj,val)
            obj = setParameter(obj,'Alpha0',val,0,'real');
        end
        function res = get.Alpha0(obj)
            res = getParameter(obj,'Alpha0');
        end
        function obj = set.Alpha1(obj,val)
            obj = setParameter(obj,'Alpha1',val,0,'real');
        end
        function res = get.Alpha1(obj)
            res = getParameter(obj,'Alpha1');
        end
        function obj = set.Beta0(obj,val)
            obj = setParameter(obj,'Beta0',val,0,'real');
        end
        function res = get.Beta0(obj)
            res = getParameter(obj,'Beta0');
        end
        function obj = set.Agidl(obj,val)
            obj = setParameter(obj,'Agidl',val,0,'real');
        end
        function res = get.Agidl(obj)
            res = getParameter(obj,'Agidl');
        end
        function obj = set.Bgidl(obj,val)
            obj = setParameter(obj,'Bgidl',val,0,'real');
        end
        function res = get.Bgidl(obj)
            res = getParameter(obj,'Bgidl');
        end
        function obj = set.Cgidl(obj,val)
            obj = setParameter(obj,'Cgidl',val,0,'real');
        end
        function res = get.Cgidl(obj)
            res = getParameter(obj,'Cgidl');
        end
        function obj = set.Egidl(obj,val)
            obj = setParameter(obj,'Egidl',val,0,'real');
        end
        function res = get.Egidl(obj)
            res = getParameter(obj,'Egidl');
        end
        function obj = set.Aigc(obj,val)
            obj = setParameter(obj,'Aigc',val,0,'real');
        end
        function res = get.Aigc(obj)
            res = getParameter(obj,'Aigc');
        end
        function obj = set.Bigc(obj,val)
            obj = setParameter(obj,'Bigc',val,0,'real');
        end
        function res = get.Bigc(obj)
            res = getParameter(obj,'Bigc');
        end
        function obj = set.Cigc(obj,val)
            obj = setParameter(obj,'Cigc',val,0,'real');
        end
        function res = get.Cigc(obj)
            res = getParameter(obj,'Cigc');
        end
        function obj = set.Aigsd(obj,val)
            obj = setParameter(obj,'Aigsd',val,0,'real');
        end
        function res = get.Aigsd(obj)
            res = getParameter(obj,'Aigsd');
        end
        function obj = set.Bigsd(obj,val)
            obj = setParameter(obj,'Bigsd',val,0,'real');
        end
        function res = get.Bigsd(obj)
            res = getParameter(obj,'Bigsd');
        end
        function obj = set.Cigsd(obj,val)
            obj = setParameter(obj,'Cigsd',val,0,'real');
        end
        function res = get.Cigsd(obj)
            res = getParameter(obj,'Cigsd');
        end
        function obj = set.Aigbacc(obj,val)
            obj = setParameter(obj,'Aigbacc',val,0,'real');
        end
        function res = get.Aigbacc(obj)
            res = getParameter(obj,'Aigbacc');
        end
        function obj = set.Bigbacc(obj,val)
            obj = setParameter(obj,'Bigbacc',val,0,'real');
        end
        function res = get.Bigbacc(obj)
            res = getParameter(obj,'Bigbacc');
        end
        function obj = set.Cigbacc(obj,val)
            obj = setParameter(obj,'Cigbacc',val,0,'real');
        end
        function res = get.Cigbacc(obj)
            res = getParameter(obj,'Cigbacc');
        end
        function obj = set.Aigbinv(obj,val)
            obj = setParameter(obj,'Aigbinv',val,0,'real');
        end
        function res = get.Aigbinv(obj)
            res = getParameter(obj,'Aigbinv');
        end
        function obj = set.Bigbinv(obj,val)
            obj = setParameter(obj,'Bigbinv',val,0,'real');
        end
        function res = get.Bigbinv(obj)
            res = getParameter(obj,'Bigbinv');
        end
        function obj = set.Cigbinv(obj,val)
            obj = setParameter(obj,'Cigbinv',val,0,'real');
        end
        function res = get.Cigbinv(obj)
            res = getParameter(obj,'Cigbinv');
        end
        function obj = set.Nigc(obj,val)
            obj = setParameter(obj,'Nigc',val,0,'real');
        end
        function res = get.Nigc(obj)
            res = getParameter(obj,'Nigc');
        end
        function obj = set.Nigbinv(obj,val)
            obj = setParameter(obj,'Nigbinv',val,0,'real');
        end
        function res = get.Nigbinv(obj)
            res = getParameter(obj,'Nigbinv');
        end
        function obj = set.Nigbacc(obj,val)
            obj = setParameter(obj,'Nigbacc',val,0,'real');
        end
        function res = get.Nigbacc(obj)
            res = getParameter(obj,'Nigbacc');
        end
        function obj = set.Ntox(obj,val)
            obj = setParameter(obj,'Ntox',val,0,'real');
        end
        function res = get.Ntox(obj)
            res = getParameter(obj,'Ntox');
        end
        function obj = set.Eigbinv(obj,val)
            obj = setParameter(obj,'Eigbinv',val,0,'real');
        end
        function res = get.Eigbinv(obj)
            res = getParameter(obj,'Eigbinv');
        end
        function obj = set.Pigcd(obj,val)
            obj = setParameter(obj,'Pigcd',val,0,'real');
        end
        function res = get.Pigcd(obj)
            res = getParameter(obj,'Pigcd');
        end
        function obj = set.Poxedge(obj,val)
            obj = setParameter(obj,'Poxedge',val,0,'real');
        end
        function res = get.Poxedge(obj)
            res = getParameter(obj,'Poxedge');
        end
        function obj = set.Ijthdfwd(obj,val)
            obj = setParameter(obj,'Ijthdfwd',val,0,'real');
        end
        function res = get.Ijthdfwd(obj)
            res = getParameter(obj,'Ijthdfwd');
        end
        function obj = set.Ijthsfwd(obj,val)
            obj = setParameter(obj,'Ijthsfwd',val,0,'real');
        end
        function res = get.Ijthsfwd(obj)
            res = getParameter(obj,'Ijthsfwd');
        end
        function obj = set.Ijthdrev(obj,val)
            obj = setParameter(obj,'Ijthdrev',val,0,'real');
        end
        function res = get.Ijthdrev(obj)
            res = getParameter(obj,'Ijthdrev');
        end
        function obj = set.Ijthsrev(obj,val)
            obj = setParameter(obj,'Ijthsrev',val,0,'real');
        end
        function res = get.Ijthsrev(obj)
            res = getParameter(obj,'Ijthsrev');
        end
        function obj = set.Xjbvd(obj,val)
            obj = setParameter(obj,'Xjbvd',val,0,'real');
        end
        function res = get.Xjbvd(obj)
            res = getParameter(obj,'Xjbvd');
        end
        function obj = set.Xjbvs(obj,val)
            obj = setParameter(obj,'Xjbvs',val,0,'real');
        end
        function res = get.Xjbvs(obj)
            res = getParameter(obj,'Xjbvs');
        end
        function obj = set.Bvd(obj,val)
            obj = setParameter(obj,'Bvd',val,0,'real');
        end
        function res = get.Bvd(obj)
            res = getParameter(obj,'Bvd');
        end
        function obj = set.Bvs(obj,val)
            obj = setParameter(obj,'Bvs',val,0,'real');
        end
        function res = get.Bvs(obj)
            res = getParameter(obj,'Bvs');
        end
        function obj = set.Gbmin(obj,val)
            obj = setParameter(obj,'Gbmin',val,0,'real');
        end
        function res = get.Gbmin(obj)
            res = getParameter(obj,'Gbmin');
        end
        function obj = set.Rbdb(obj,val)
            obj = setParameter(obj,'Rbdb',val,0,'real');
        end
        function res = get.Rbdb(obj)
            res = getParameter(obj,'Rbdb');
        end
        function obj = set.Rbpb(obj,val)
            obj = setParameter(obj,'Rbpb',val,0,'real');
        end
        function res = get.Rbpb(obj)
            res = getParameter(obj,'Rbpb');
        end
        function obj = set.Rbsb(obj,val)
            obj = setParameter(obj,'Rbsb',val,0,'real');
        end
        function res = get.Rbsb(obj)
            res = getParameter(obj,'Rbsb');
        end
        function obj = set.Rbps(obj,val)
            obj = setParameter(obj,'Rbps',val,0,'real');
        end
        function res = get.Rbps(obj)
            res = getParameter(obj,'Rbps');
        end
        function obj = set.Rbpd(obj,val)
            obj = setParameter(obj,'Rbpd',val,0,'real');
        end
        function res = get.Rbpd(obj)
            res = getParameter(obj,'Rbpd');
        end
        function obj = set.Lcdsc(obj,val)
            obj = setParameter(obj,'Lcdsc',val,0,'real');
        end
        function res = get.Lcdsc(obj)
            res = getParameter(obj,'Lcdsc');
        end
        function obj = set.Lcdscb(obj,val)
            obj = setParameter(obj,'Lcdscb',val,0,'real');
        end
        function res = get.Lcdscb(obj)
            res = getParameter(obj,'Lcdscb');
        end
        function obj = set.Lcdscd(obj,val)
            obj = setParameter(obj,'Lcdscd',val,0,'real');
        end
        function res = get.Lcdscd(obj)
            res = getParameter(obj,'Lcdscd');
        end
        function obj = set.Lcit(obj,val)
            obj = setParameter(obj,'Lcit',val,0,'real');
        end
        function res = get.Lcit(obj)
            res = getParameter(obj,'Lcit');
        end
        function obj = set.Lnfactor(obj,val)
            obj = setParameter(obj,'Lnfactor',val,0,'real');
        end
        function res = get.Lnfactor(obj)
            res = getParameter(obj,'Lnfactor');
        end
        function obj = set.Lxj(obj,val)
            obj = setParameter(obj,'Lxj',val,0,'real');
        end
        function res = get.Lxj(obj)
            res = getParameter(obj,'Lxj');
        end
        function obj = set.Lvsat(obj,val)
            obj = setParameter(obj,'Lvsat',val,0,'real');
        end
        function res = get.Lvsat(obj)
            res = getParameter(obj,'Lvsat');
        end
        function obj = set.Lat(obj,val)
            obj = setParameter(obj,'Lat',val,0,'real');
        end
        function res = get.Lat(obj)
            res = getParameter(obj,'Lat');
        end
        function obj = set.La0(obj,val)
            obj = setParameter(obj,'La0',val,0,'real');
        end
        function res = get.La0(obj)
            res = getParameter(obj,'La0');
        end
        function obj = set.Lags(obj,val)
            obj = setParameter(obj,'Lags',val,0,'real');
        end
        function res = get.Lags(obj)
            res = getParameter(obj,'Lags');
        end
        function obj = set.La1(obj,val)
            obj = setParameter(obj,'La1',val,0,'real');
        end
        function res = get.La1(obj)
            res = getParameter(obj,'La1');
        end
        function obj = set.La2(obj,val)
            obj = setParameter(obj,'La2',val,0,'real');
        end
        function res = get.La2(obj)
            res = getParameter(obj,'La2');
        end
        function obj = set.Lketa(obj,val)
            obj = setParameter(obj,'Lketa',val,0,'real');
        end
        function res = get.Lketa(obj)
            res = getParameter(obj,'Lketa');
        end
        function obj = set.Lnsub(obj,val)
            obj = setParameter(obj,'Lnsub',val,0,'real');
        end
        function res = get.Lnsub(obj)
            res = getParameter(obj,'Lnsub');
        end
        function obj = set.Lndep(obj,val)
            obj = setParameter(obj,'Lndep',val,0,'real');
        end
        function res = get.Lndep(obj)
            res = getParameter(obj,'Lndep');
        end
        function obj = set.Lnsd(obj,val)
            obj = setParameter(obj,'Lnsd',val,0,'real');
        end
        function res = get.Lnsd(obj)
            res = getParameter(obj,'Lnsd');
        end
        function obj = set.Lphin(obj,val)
            obj = setParameter(obj,'Lphin',val,0,'real');
        end
        function res = get.Lphin(obj)
            res = getParameter(obj,'Lphin');
        end
        function obj = set.Lngate(obj,val)
            obj = setParameter(obj,'Lngate',val,0,'real');
        end
        function res = get.Lngate(obj)
            res = getParameter(obj,'Lngate');
        end
        function obj = set.Lgamma1(obj,val)
            obj = setParameter(obj,'Lgamma1',val,0,'real');
        end
        function res = get.Lgamma1(obj)
            res = getParameter(obj,'Lgamma1');
        end
        function obj = set.Lgamma2(obj,val)
            obj = setParameter(obj,'Lgamma2',val,0,'real');
        end
        function res = get.Lgamma2(obj)
            res = getParameter(obj,'Lgamma2');
        end
        function obj = set.Lvbx(obj,val)
            obj = setParameter(obj,'Lvbx',val,0,'real');
        end
        function res = get.Lvbx(obj)
            res = getParameter(obj,'Lvbx');
        end
        function obj = set.Lvbm(obj,val)
            obj = setParameter(obj,'Lvbm',val,0,'real');
        end
        function res = get.Lvbm(obj)
            res = getParameter(obj,'Lvbm');
        end
        function obj = set.Lxt(obj,val)
            obj = setParameter(obj,'Lxt',val,0,'real');
        end
        function res = get.Lxt(obj)
            res = getParameter(obj,'Lxt');
        end
        function obj = set.Lk1(obj,val)
            obj = setParameter(obj,'Lk1',val,0,'real');
        end
        function res = get.Lk1(obj)
            res = getParameter(obj,'Lk1');
        end
        function obj = set.Lkt1(obj,val)
            obj = setParameter(obj,'Lkt1',val,0,'real');
        end
        function res = get.Lkt1(obj)
            res = getParameter(obj,'Lkt1');
        end
        function obj = set.Lkt1l(obj,val)
            obj = setParameter(obj,'Lkt1l',val,0,'real');
        end
        function res = get.Lkt1l(obj)
            res = getParameter(obj,'Lkt1l');
        end
        function obj = set.Lkt2(obj,val)
            obj = setParameter(obj,'Lkt2',val,0,'real');
        end
        function res = get.Lkt2(obj)
            res = getParameter(obj,'Lkt2');
        end
        function obj = set.Lk2(obj,val)
            obj = setParameter(obj,'Lk2',val,0,'real');
        end
        function res = get.Lk2(obj)
            res = getParameter(obj,'Lk2');
        end
        function obj = set.Lk3(obj,val)
            obj = setParameter(obj,'Lk3',val,0,'real');
        end
        function res = get.Lk3(obj)
            res = getParameter(obj,'Lk3');
        end
        function obj = set.Lk3b(obj,val)
            obj = setParameter(obj,'Lk3b',val,0,'real');
        end
        function res = get.Lk3b(obj)
            res = getParameter(obj,'Lk3b');
        end
        function obj = set.Lw0(obj,val)
            obj = setParameter(obj,'Lw0',val,0,'real');
        end
        function res = get.Lw0(obj)
            res = getParameter(obj,'Lw0');
        end
        function obj = set.Ldvtp0(obj,val)
            obj = setParameter(obj,'Ldvtp0',val,0,'real');
        end
        function res = get.Ldvtp0(obj)
            res = getParameter(obj,'Ldvtp0');
        end
        function obj = set.Ldvtp1(obj,val)
            obj = setParameter(obj,'Ldvtp1',val,0,'real');
        end
        function res = get.Ldvtp1(obj)
            res = getParameter(obj,'Ldvtp1');
        end
        function obj = set.Llpe0(obj,val)
            obj = setParameter(obj,'Llpe0',val,0,'real');
        end
        function res = get.Llpe0(obj)
            res = getParameter(obj,'Llpe0');
        end
        function obj = set.Llpeb(obj,val)
            obj = setParameter(obj,'Llpeb',val,0,'real');
        end
        function res = get.Llpeb(obj)
            res = getParameter(obj,'Llpeb');
        end
        function obj = set.Ldvt0(obj,val)
            obj = setParameter(obj,'Ldvt0',val,0,'real');
        end
        function res = get.Ldvt0(obj)
            res = getParameter(obj,'Ldvt0');
        end
        function obj = set.Ldvt1(obj,val)
            obj = setParameter(obj,'Ldvt1',val,0,'real');
        end
        function res = get.Ldvt1(obj)
            res = getParameter(obj,'Ldvt1');
        end
        function obj = set.Ldvt2(obj,val)
            obj = setParameter(obj,'Ldvt2',val,0,'real');
        end
        function res = get.Ldvt2(obj)
            res = getParameter(obj,'Ldvt2');
        end
        function obj = set.Ldvt0w(obj,val)
            obj = setParameter(obj,'Ldvt0w',val,0,'real');
        end
        function res = get.Ldvt0w(obj)
            res = getParameter(obj,'Ldvt0w');
        end
        function obj = set.Ldvt1w(obj,val)
            obj = setParameter(obj,'Ldvt1w',val,0,'real');
        end
        function res = get.Ldvt1w(obj)
            res = getParameter(obj,'Ldvt1w');
        end
        function obj = set.Ldvt2w(obj,val)
            obj = setParameter(obj,'Ldvt2w',val,0,'real');
        end
        function res = get.Ldvt2w(obj)
            res = getParameter(obj,'Ldvt2w');
        end
        function obj = set.Ldrout(obj,val)
            obj = setParameter(obj,'Ldrout',val,0,'real');
        end
        function res = get.Ldrout(obj)
            res = getParameter(obj,'Ldrout');
        end
        function obj = set.Ldsub(obj,val)
            obj = setParameter(obj,'Ldsub',val,0,'real');
        end
        function res = get.Ldsub(obj)
            res = getParameter(obj,'Ldsub');
        end
        function obj = set.Lvth0(obj,val)
            obj = setParameter(obj,'Lvth0',val,0,'real');
        end
        function res = get.Lvth0(obj)
            res = getParameter(obj,'Lvth0');
        end
        function obj = set.Lvtho(obj,val)
            obj = setParameter(obj,'Lvtho',val,0,'real');
        end
        function res = get.Lvtho(obj)
            res = getParameter(obj,'Lvtho');
        end
        function obj = set.Lua(obj,val)
            obj = setParameter(obj,'Lua',val,0,'real');
        end
        function res = get.Lua(obj)
            res = getParameter(obj,'Lua');
        end
        function obj = set.Lua1(obj,val)
            obj = setParameter(obj,'Lua1',val,0,'real');
        end
        function res = get.Lua1(obj)
            res = getParameter(obj,'Lua1');
        end
        function obj = set.Lub(obj,val)
            obj = setParameter(obj,'Lub',val,0,'real');
        end
        function res = get.Lub(obj)
            res = getParameter(obj,'Lub');
        end
        function obj = set.Lub1(obj,val)
            obj = setParameter(obj,'Lub1',val,0,'real');
        end
        function res = get.Lub1(obj)
            res = getParameter(obj,'Lub1');
        end
        function obj = set.Luc(obj,val)
            obj = setParameter(obj,'Luc',val,0,'real');
        end
        function res = get.Luc(obj)
            res = getParameter(obj,'Luc');
        end
        function obj = set.Luc1(obj,val)
            obj = setParameter(obj,'Luc1',val,0,'real');
        end
        function res = get.Luc1(obj)
            res = getParameter(obj,'Luc1');
        end
        function obj = set.Lu0(obj,val)
            obj = setParameter(obj,'Lu0',val,0,'real');
        end
        function res = get.Lu0(obj)
            res = getParameter(obj,'Lu0');
        end
        function obj = set.Lute(obj,val)
            obj = setParameter(obj,'Lute',val,0,'real');
        end
        function res = get.Lute(obj)
            res = getParameter(obj,'Lute');
        end
        function obj = set.Lvoff(obj,val)
            obj = setParameter(obj,'Lvoff',val,0,'real');
        end
        function res = get.Lvoff(obj)
            res = getParameter(obj,'Lvoff');
        end
        function obj = set.Lminv(obj,val)
            obj = setParameter(obj,'Lminv',val,0,'real');
        end
        function res = get.Lminv(obj)
            res = getParameter(obj,'Lminv');
        end
        function obj = set.Ldelta(obj,val)
            obj = setParameter(obj,'Ldelta',val,0,'real');
        end
        function res = get.Ldelta(obj)
            res = getParameter(obj,'Ldelta');
        end
        function obj = set.Lrdsw(obj,val)
            obj = setParameter(obj,'Lrdsw',val,0,'real');
        end
        function res = get.Lrdsw(obj)
            res = getParameter(obj,'Lrdsw');
        end
        function obj = set.Lrsw(obj,val)
            obj = setParameter(obj,'Lrsw',val,0,'real');
        end
        function res = get.Lrsw(obj)
            res = getParameter(obj,'Lrsw');
        end
        function obj = set.Lrdw(obj,val)
            obj = setParameter(obj,'Lrdw',val,0,'real');
        end
        function res = get.Lrdw(obj)
            res = getParameter(obj,'Lrdw');
        end
        function obj = set.Lprwg(obj,val)
            obj = setParameter(obj,'Lprwg',val,0,'real');
        end
        function res = get.Lprwg(obj)
            res = getParameter(obj,'Lprwg');
        end
        function obj = set.Lprwb(obj,val)
            obj = setParameter(obj,'Lprwb',val,0,'real');
        end
        function res = get.Lprwb(obj)
            res = getParameter(obj,'Lprwb');
        end
        function obj = set.Lprt(obj,val)
            obj = setParameter(obj,'Lprt',val,0,'real');
        end
        function res = get.Lprt(obj)
            res = getParameter(obj,'Lprt');
        end
        function obj = set.Leta0(obj,val)
            obj = setParameter(obj,'Leta0',val,0,'real');
        end
        function res = get.Leta0(obj)
            res = getParameter(obj,'Leta0');
        end
        function obj = set.Letab(obj,val)
            obj = setParameter(obj,'Letab',val,0,'real');
        end
        function res = get.Letab(obj)
            res = getParameter(obj,'Letab');
        end
        function obj = set.Lpclm(obj,val)
            obj = setParameter(obj,'Lpclm',val,0,'real');
        end
        function res = get.Lpclm(obj)
            res = getParameter(obj,'Lpclm');
        end
        function obj = set.Lpdiblc1(obj,val)
            obj = setParameter(obj,'Lpdiblc1',val,0,'real');
        end
        function res = get.Lpdiblc1(obj)
            res = getParameter(obj,'Lpdiblc1');
        end
        function obj = set.Lpdiblc2(obj,val)
            obj = setParameter(obj,'Lpdiblc2',val,0,'real');
        end
        function res = get.Lpdiblc2(obj)
            res = getParameter(obj,'Lpdiblc2');
        end
        function obj = set.Lpdiblcb(obj,val)
            obj = setParameter(obj,'Lpdiblcb',val,0,'real');
        end
        function res = get.Lpdiblcb(obj)
            res = getParameter(obj,'Lpdiblcb');
        end
        function obj = set.Lfprout(obj,val)
            obj = setParameter(obj,'Lfprout',val,0,'real');
        end
        function res = get.Lfprout(obj)
            res = getParameter(obj,'Lfprout');
        end
        function obj = set.Lpdits(obj,val)
            obj = setParameter(obj,'Lpdits',val,0,'real');
        end
        function res = get.Lpdits(obj)
            res = getParameter(obj,'Lpdits');
        end
        function obj = set.Lpditsd(obj,val)
            obj = setParameter(obj,'Lpditsd',val,0,'real');
        end
        function res = get.Lpditsd(obj)
            res = getParameter(obj,'Lpditsd');
        end
        function obj = set.Lpscbe1(obj,val)
            obj = setParameter(obj,'Lpscbe1',val,0,'real');
        end
        function res = get.Lpscbe1(obj)
            res = getParameter(obj,'Lpscbe1');
        end
        function obj = set.Lpscbe2(obj,val)
            obj = setParameter(obj,'Lpscbe2',val,0,'real');
        end
        function res = get.Lpscbe2(obj)
            res = getParameter(obj,'Lpscbe2');
        end
        function obj = set.Lpvag(obj,val)
            obj = setParameter(obj,'Lpvag',val,0,'real');
        end
        function res = get.Lpvag(obj)
            res = getParameter(obj,'Lpvag');
        end
        function obj = set.Lwr(obj,val)
            obj = setParameter(obj,'Lwr',val,0,'real');
        end
        function res = get.Lwr(obj)
            res = getParameter(obj,'Lwr');
        end
        function obj = set.Ldwg(obj,val)
            obj = setParameter(obj,'Ldwg',val,0,'real');
        end
        function res = get.Ldwg(obj)
            res = getParameter(obj,'Ldwg');
        end
        function obj = set.Ldwb(obj,val)
            obj = setParameter(obj,'Ldwb',val,0,'real');
        end
        function res = get.Ldwb(obj)
            res = getParameter(obj,'Ldwb');
        end
        function obj = set.Lb0(obj,val)
            obj = setParameter(obj,'Lb0',val,0,'real');
        end
        function res = get.Lb0(obj)
            res = getParameter(obj,'Lb0');
        end
        function obj = set.Lb1(obj,val)
            obj = setParameter(obj,'Lb1',val,0,'real');
        end
        function res = get.Lb1(obj)
            res = getParameter(obj,'Lb1');
        end
        function obj = set.Lcgsl(obj,val)
            obj = setParameter(obj,'Lcgsl',val,0,'real');
        end
        function res = get.Lcgsl(obj)
            res = getParameter(obj,'Lcgsl');
        end
        function obj = set.Lcgdl(obj,val)
            obj = setParameter(obj,'Lcgdl',val,0,'real');
        end
        function res = get.Lcgdl(obj)
            res = getParameter(obj,'Lcgdl');
        end
        function obj = set.Lckappas(obj,val)
            obj = setParameter(obj,'Lckappas',val,0,'real');
        end
        function res = get.Lckappas(obj)
            res = getParameter(obj,'Lckappas');
        end
        function obj = set.Lckappad(obj,val)
            obj = setParameter(obj,'Lckappad',val,0,'real');
        end
        function res = get.Lckappad(obj)
            res = getParameter(obj,'Lckappad');
        end
        function obj = set.Lcf(obj,val)
            obj = setParameter(obj,'Lcf',val,0,'real');
        end
        function res = get.Lcf(obj)
            res = getParameter(obj,'Lcf');
        end
        function obj = set.Lclc(obj,val)
            obj = setParameter(obj,'Lclc',val,0,'real');
        end
        function res = get.Lclc(obj)
            res = getParameter(obj,'Lclc');
        end
        function obj = set.Lcle(obj,val)
            obj = setParameter(obj,'Lcle',val,0,'real');
        end
        function res = get.Lcle(obj)
            res = getParameter(obj,'Lcle');
        end
        function obj = set.Lalpha0(obj,val)
            obj = setParameter(obj,'Lalpha0',val,0,'real');
        end
        function res = get.Lalpha0(obj)
            res = getParameter(obj,'Lalpha0');
        end
        function obj = set.Lalpha1(obj,val)
            obj = setParameter(obj,'Lalpha1',val,0,'real');
        end
        function res = get.Lalpha1(obj)
            res = getParameter(obj,'Lalpha1');
        end
        function obj = set.Lbeta0(obj,val)
            obj = setParameter(obj,'Lbeta0',val,0,'real');
        end
        function res = get.Lbeta0(obj)
            res = getParameter(obj,'Lbeta0');
        end
        function obj = set.Lagidl(obj,val)
            obj = setParameter(obj,'Lagidl',val,0,'real');
        end
        function res = get.Lagidl(obj)
            res = getParameter(obj,'Lagidl');
        end
        function obj = set.Lbgidl(obj,val)
            obj = setParameter(obj,'Lbgidl',val,0,'real');
        end
        function res = get.Lbgidl(obj)
            res = getParameter(obj,'Lbgidl');
        end
        function obj = set.Lcgidl(obj,val)
            obj = setParameter(obj,'Lcgidl',val,0,'real');
        end
        function res = get.Lcgidl(obj)
            res = getParameter(obj,'Lcgidl');
        end
        function obj = set.Legidl(obj,val)
            obj = setParameter(obj,'Legidl',val,0,'real');
        end
        function res = get.Legidl(obj)
            res = getParameter(obj,'Legidl');
        end
        function obj = set.Laigc(obj,val)
            obj = setParameter(obj,'Laigc',val,0,'real');
        end
        function res = get.Laigc(obj)
            res = getParameter(obj,'Laigc');
        end
        function obj = set.Lbigc(obj,val)
            obj = setParameter(obj,'Lbigc',val,0,'real');
        end
        function res = get.Lbigc(obj)
            res = getParameter(obj,'Lbigc');
        end
        function obj = set.Lcigc(obj,val)
            obj = setParameter(obj,'Lcigc',val,0,'real');
        end
        function res = get.Lcigc(obj)
            res = getParameter(obj,'Lcigc');
        end
        function obj = set.Laigsd(obj,val)
            obj = setParameter(obj,'Laigsd',val,0,'real');
        end
        function res = get.Laigsd(obj)
            res = getParameter(obj,'Laigsd');
        end
        function obj = set.Lbigsd(obj,val)
            obj = setParameter(obj,'Lbigsd',val,0,'real');
        end
        function res = get.Lbigsd(obj)
            res = getParameter(obj,'Lbigsd');
        end
        function obj = set.Lcigsd(obj,val)
            obj = setParameter(obj,'Lcigsd',val,0,'real');
        end
        function res = get.Lcigsd(obj)
            res = getParameter(obj,'Lcigsd');
        end
        function obj = set.Laigbacc(obj,val)
            obj = setParameter(obj,'Laigbacc',val,0,'real');
        end
        function res = get.Laigbacc(obj)
            res = getParameter(obj,'Laigbacc');
        end
        function obj = set.Lbigbacc(obj,val)
            obj = setParameter(obj,'Lbigbacc',val,0,'real');
        end
        function res = get.Lbigbacc(obj)
            res = getParameter(obj,'Lbigbacc');
        end
        function obj = set.Lcigbacc(obj,val)
            obj = setParameter(obj,'Lcigbacc',val,0,'real');
        end
        function res = get.Lcigbacc(obj)
            res = getParameter(obj,'Lcigbacc');
        end
        function obj = set.Laigbinv(obj,val)
            obj = setParameter(obj,'Laigbinv',val,0,'real');
        end
        function res = get.Laigbinv(obj)
            res = getParameter(obj,'Laigbinv');
        end
        function obj = set.Lbigbinv(obj,val)
            obj = setParameter(obj,'Lbigbinv',val,0,'real');
        end
        function res = get.Lbigbinv(obj)
            res = getParameter(obj,'Lbigbinv');
        end
        function obj = set.Lcigbinv(obj,val)
            obj = setParameter(obj,'Lcigbinv',val,0,'real');
        end
        function res = get.Lcigbinv(obj)
            res = getParameter(obj,'Lcigbinv');
        end
        function obj = set.Lnigc(obj,val)
            obj = setParameter(obj,'Lnigc',val,0,'real');
        end
        function res = get.Lnigc(obj)
            res = getParameter(obj,'Lnigc');
        end
        function obj = set.Lnigbinv(obj,val)
            obj = setParameter(obj,'Lnigbinv',val,0,'real');
        end
        function res = get.Lnigbinv(obj)
            res = getParameter(obj,'Lnigbinv');
        end
        function obj = set.Lnigbacc(obj,val)
            obj = setParameter(obj,'Lnigbacc',val,0,'real');
        end
        function res = get.Lnigbacc(obj)
            res = getParameter(obj,'Lnigbacc');
        end
        function obj = set.Lntox(obj,val)
            obj = setParameter(obj,'Lntox',val,0,'real');
        end
        function res = get.Lntox(obj)
            res = getParameter(obj,'Lntox');
        end
        function obj = set.Leigbinv(obj,val)
            obj = setParameter(obj,'Leigbinv',val,0,'real');
        end
        function res = get.Leigbinv(obj)
            res = getParameter(obj,'Leigbinv');
        end
        function obj = set.Lpigcd(obj,val)
            obj = setParameter(obj,'Lpigcd',val,0,'real');
        end
        function res = get.Lpigcd(obj)
            res = getParameter(obj,'Lpigcd');
        end
        function obj = set.Lpoxedge(obj,val)
            obj = setParameter(obj,'Lpoxedge',val,0,'real');
        end
        function res = get.Lpoxedge(obj)
            res = getParameter(obj,'Lpoxedge');
        end
        function obj = set.Lvfbcv(obj,val)
            obj = setParameter(obj,'Lvfbcv',val,0,'real');
        end
        function res = get.Lvfbcv(obj)
            res = getParameter(obj,'Lvfbcv');
        end
        function obj = set.Lvfb(obj,val)
            obj = setParameter(obj,'Lvfb',val,0,'real');
        end
        function res = get.Lvfb(obj)
            res = getParameter(obj,'Lvfb');
        end
        function obj = set.Lacde(obj,val)
            obj = setParameter(obj,'Lacde',val,0,'real');
        end
        function res = get.Lacde(obj)
            res = getParameter(obj,'Lacde');
        end
        function obj = set.Lmoin(obj,val)
            obj = setParameter(obj,'Lmoin',val,0,'real');
        end
        function res = get.Lmoin(obj)
            res = getParameter(obj,'Lmoin');
        end
        function obj = set.Lnoff(obj,val)
            obj = setParameter(obj,'Lnoff',val,0,'real');
        end
        function res = get.Lnoff(obj)
            res = getParameter(obj,'Lnoff');
        end
        function obj = set.Lvoffcv(obj,val)
            obj = setParameter(obj,'Lvoffcv',val,0,'real');
        end
        function res = get.Lvoffcv(obj)
            res = getParameter(obj,'Lvoffcv');
        end
        function obj = set.Lxrcrg1(obj,val)
            obj = setParameter(obj,'Lxrcrg1',val,0,'real');
        end
        function res = get.Lxrcrg1(obj)
            res = getParameter(obj,'Lxrcrg1');
        end
        function obj = set.Lxrcrg2(obj,val)
            obj = setParameter(obj,'Lxrcrg2',val,0,'real');
        end
        function res = get.Lxrcrg2(obj)
            res = getParameter(obj,'Lxrcrg2');
        end
        function obj = set.Leu(obj,val)
            obj = setParameter(obj,'Leu',val,0,'real');
        end
        function res = get.Leu(obj)
            res = getParameter(obj,'Leu');
        end
        function obj = set.Wcdsc(obj,val)
            obj = setParameter(obj,'Wcdsc',val,0,'real');
        end
        function res = get.Wcdsc(obj)
            res = getParameter(obj,'Wcdsc');
        end
        function obj = set.Wcdscb(obj,val)
            obj = setParameter(obj,'Wcdscb',val,0,'real');
        end
        function res = get.Wcdscb(obj)
            res = getParameter(obj,'Wcdscb');
        end
        function obj = set.Wcdscd(obj,val)
            obj = setParameter(obj,'Wcdscd',val,0,'real');
        end
        function res = get.Wcdscd(obj)
            res = getParameter(obj,'Wcdscd');
        end
        function obj = set.Wcit(obj,val)
            obj = setParameter(obj,'Wcit',val,0,'real');
        end
        function res = get.Wcit(obj)
            res = getParameter(obj,'Wcit');
        end
        function obj = set.Wnfactor(obj,val)
            obj = setParameter(obj,'Wnfactor',val,0,'real');
        end
        function res = get.Wnfactor(obj)
            res = getParameter(obj,'Wnfactor');
        end
        function obj = set.Wxj(obj,val)
            obj = setParameter(obj,'Wxj',val,0,'real');
        end
        function res = get.Wxj(obj)
            res = getParameter(obj,'Wxj');
        end
        function obj = set.Wvsat(obj,val)
            obj = setParameter(obj,'Wvsat',val,0,'real');
        end
        function res = get.Wvsat(obj)
            res = getParameter(obj,'Wvsat');
        end
        function obj = set.Wat(obj,val)
            obj = setParameter(obj,'Wat',val,0,'real');
        end
        function res = get.Wat(obj)
            res = getParameter(obj,'Wat');
        end
        function obj = set.Wa0(obj,val)
            obj = setParameter(obj,'Wa0',val,0,'real');
        end
        function res = get.Wa0(obj)
            res = getParameter(obj,'Wa0');
        end
        function obj = set.Wags(obj,val)
            obj = setParameter(obj,'Wags',val,0,'real');
        end
        function res = get.Wags(obj)
            res = getParameter(obj,'Wags');
        end
        function obj = set.Wa1(obj,val)
            obj = setParameter(obj,'Wa1',val,0,'real');
        end
        function res = get.Wa1(obj)
            res = getParameter(obj,'Wa1');
        end
        function obj = set.Wa2(obj,val)
            obj = setParameter(obj,'Wa2',val,0,'real');
        end
        function res = get.Wa2(obj)
            res = getParameter(obj,'Wa2');
        end
        function obj = set.Wketa(obj,val)
            obj = setParameter(obj,'Wketa',val,0,'real');
        end
        function res = get.Wketa(obj)
            res = getParameter(obj,'Wketa');
        end
        function obj = set.Wnsub(obj,val)
            obj = setParameter(obj,'Wnsub',val,0,'real');
        end
        function res = get.Wnsub(obj)
            res = getParameter(obj,'Wnsub');
        end
        function obj = set.Wndep(obj,val)
            obj = setParameter(obj,'Wndep',val,0,'real');
        end
        function res = get.Wndep(obj)
            res = getParameter(obj,'Wndep');
        end
        function obj = set.Wnsd(obj,val)
            obj = setParameter(obj,'Wnsd',val,0,'real');
        end
        function res = get.Wnsd(obj)
            res = getParameter(obj,'Wnsd');
        end
        function obj = set.Wphin(obj,val)
            obj = setParameter(obj,'Wphin',val,0,'real');
        end
        function res = get.Wphin(obj)
            res = getParameter(obj,'Wphin');
        end
        function obj = set.Wngate(obj,val)
            obj = setParameter(obj,'Wngate',val,0,'real');
        end
        function res = get.Wngate(obj)
            res = getParameter(obj,'Wngate');
        end
        function obj = set.Wgamma1(obj,val)
            obj = setParameter(obj,'Wgamma1',val,0,'real');
        end
        function res = get.Wgamma1(obj)
            res = getParameter(obj,'Wgamma1');
        end
        function obj = set.Wgamma2(obj,val)
            obj = setParameter(obj,'Wgamma2',val,0,'real');
        end
        function res = get.Wgamma2(obj)
            res = getParameter(obj,'Wgamma2');
        end
        function obj = set.Wvbx(obj,val)
            obj = setParameter(obj,'Wvbx',val,0,'real');
        end
        function res = get.Wvbx(obj)
            res = getParameter(obj,'Wvbx');
        end
        function obj = set.Wvbm(obj,val)
            obj = setParameter(obj,'Wvbm',val,0,'real');
        end
        function res = get.Wvbm(obj)
            res = getParameter(obj,'Wvbm');
        end
        function obj = set.Wxt(obj,val)
            obj = setParameter(obj,'Wxt',val,0,'real');
        end
        function res = get.Wxt(obj)
            res = getParameter(obj,'Wxt');
        end
        function obj = set.Wk1(obj,val)
            obj = setParameter(obj,'Wk1',val,0,'real');
        end
        function res = get.Wk1(obj)
            res = getParameter(obj,'Wk1');
        end
        function obj = set.Wkt1(obj,val)
            obj = setParameter(obj,'Wkt1',val,0,'real');
        end
        function res = get.Wkt1(obj)
            res = getParameter(obj,'Wkt1');
        end
        function obj = set.Wkt1l(obj,val)
            obj = setParameter(obj,'Wkt1l',val,0,'real');
        end
        function res = get.Wkt1l(obj)
            res = getParameter(obj,'Wkt1l');
        end
        function obj = set.Wkt2(obj,val)
            obj = setParameter(obj,'Wkt2',val,0,'real');
        end
        function res = get.Wkt2(obj)
            res = getParameter(obj,'Wkt2');
        end
        function obj = set.Wk2(obj,val)
            obj = setParameter(obj,'Wk2',val,0,'real');
        end
        function res = get.Wk2(obj)
            res = getParameter(obj,'Wk2');
        end
        function obj = set.Wk3(obj,val)
            obj = setParameter(obj,'Wk3',val,0,'real');
        end
        function res = get.Wk3(obj)
            res = getParameter(obj,'Wk3');
        end
        function obj = set.Wk3b(obj,val)
            obj = setParameter(obj,'Wk3b',val,0,'real');
        end
        function res = get.Wk3b(obj)
            res = getParameter(obj,'Wk3b');
        end
        function obj = set.Ww0(obj,val)
            obj = setParameter(obj,'Ww0',val,0,'real');
        end
        function res = get.Ww0(obj)
            res = getParameter(obj,'Ww0');
        end
        function obj = set.Wdvtp0(obj,val)
            obj = setParameter(obj,'Wdvtp0',val,0,'real');
        end
        function res = get.Wdvtp0(obj)
            res = getParameter(obj,'Wdvtp0');
        end
        function obj = set.Wdvtp1(obj,val)
            obj = setParameter(obj,'Wdvtp1',val,0,'real');
        end
        function res = get.Wdvtp1(obj)
            res = getParameter(obj,'Wdvtp1');
        end
        function obj = set.Wlpe0(obj,val)
            obj = setParameter(obj,'Wlpe0',val,0,'real');
        end
        function res = get.Wlpe0(obj)
            res = getParameter(obj,'Wlpe0');
        end
        function obj = set.Wlpeb(obj,val)
            obj = setParameter(obj,'Wlpeb',val,0,'real');
        end
        function res = get.Wlpeb(obj)
            res = getParameter(obj,'Wlpeb');
        end
        function obj = set.Wdvt0(obj,val)
            obj = setParameter(obj,'Wdvt0',val,0,'real');
        end
        function res = get.Wdvt0(obj)
            res = getParameter(obj,'Wdvt0');
        end
        function obj = set.Wdvt1(obj,val)
            obj = setParameter(obj,'Wdvt1',val,0,'real');
        end
        function res = get.Wdvt1(obj)
            res = getParameter(obj,'Wdvt1');
        end
        function obj = set.Wdvt2(obj,val)
            obj = setParameter(obj,'Wdvt2',val,0,'real');
        end
        function res = get.Wdvt2(obj)
            res = getParameter(obj,'Wdvt2');
        end
        function obj = set.Wdvt0w(obj,val)
            obj = setParameter(obj,'Wdvt0w',val,0,'real');
        end
        function res = get.Wdvt0w(obj)
            res = getParameter(obj,'Wdvt0w');
        end
        function obj = set.Wdvt1w(obj,val)
            obj = setParameter(obj,'Wdvt1w',val,0,'real');
        end
        function res = get.Wdvt1w(obj)
            res = getParameter(obj,'Wdvt1w');
        end
        function obj = set.Wdvt2w(obj,val)
            obj = setParameter(obj,'Wdvt2w',val,0,'real');
        end
        function res = get.Wdvt2w(obj)
            res = getParameter(obj,'Wdvt2w');
        end
        function obj = set.Wdrout(obj,val)
            obj = setParameter(obj,'Wdrout',val,0,'real');
        end
        function res = get.Wdrout(obj)
            res = getParameter(obj,'Wdrout');
        end
        function obj = set.Wdsub(obj,val)
            obj = setParameter(obj,'Wdsub',val,0,'real');
        end
        function res = get.Wdsub(obj)
            res = getParameter(obj,'Wdsub');
        end
        function obj = set.Wvth0(obj,val)
            obj = setParameter(obj,'Wvth0',val,0,'real');
        end
        function res = get.Wvth0(obj)
            res = getParameter(obj,'Wvth0');
        end
        function obj = set.Wvtho(obj,val)
            obj = setParameter(obj,'Wvtho',val,0,'real');
        end
        function res = get.Wvtho(obj)
            res = getParameter(obj,'Wvtho');
        end
        function obj = set.Wua(obj,val)
            obj = setParameter(obj,'Wua',val,0,'real');
        end
        function res = get.Wua(obj)
            res = getParameter(obj,'Wua');
        end
        function obj = set.Wua1(obj,val)
            obj = setParameter(obj,'Wua1',val,0,'real');
        end
        function res = get.Wua1(obj)
            res = getParameter(obj,'Wua1');
        end
        function obj = set.Wub(obj,val)
            obj = setParameter(obj,'Wub',val,0,'real');
        end
        function res = get.Wub(obj)
            res = getParameter(obj,'Wub');
        end
        function obj = set.Wub1(obj,val)
            obj = setParameter(obj,'Wub1',val,0,'real');
        end
        function res = get.Wub1(obj)
            res = getParameter(obj,'Wub1');
        end
        function obj = set.Wuc(obj,val)
            obj = setParameter(obj,'Wuc',val,0,'real');
        end
        function res = get.Wuc(obj)
            res = getParameter(obj,'Wuc');
        end
        function obj = set.Wuc1(obj,val)
            obj = setParameter(obj,'Wuc1',val,0,'real');
        end
        function res = get.Wuc1(obj)
            res = getParameter(obj,'Wuc1');
        end
        function obj = set.Wu0(obj,val)
            obj = setParameter(obj,'Wu0',val,0,'real');
        end
        function res = get.Wu0(obj)
            res = getParameter(obj,'Wu0');
        end
        function obj = set.Wute(obj,val)
            obj = setParameter(obj,'Wute',val,0,'real');
        end
        function res = get.Wute(obj)
            res = getParameter(obj,'Wute');
        end
        function obj = set.Wvoff(obj,val)
            obj = setParameter(obj,'Wvoff',val,0,'real');
        end
        function res = get.Wvoff(obj)
            res = getParameter(obj,'Wvoff');
        end
        function obj = set.Wminv(obj,val)
            obj = setParameter(obj,'Wminv',val,0,'real');
        end
        function res = get.Wminv(obj)
            res = getParameter(obj,'Wminv');
        end
        function obj = set.Wdelta(obj,val)
            obj = setParameter(obj,'Wdelta',val,0,'real');
        end
        function res = get.Wdelta(obj)
            res = getParameter(obj,'Wdelta');
        end
        function obj = set.Wrdsw(obj,val)
            obj = setParameter(obj,'Wrdsw',val,0,'real');
        end
        function res = get.Wrdsw(obj)
            res = getParameter(obj,'Wrdsw');
        end
        function obj = set.Wrsw(obj,val)
            obj = setParameter(obj,'Wrsw',val,0,'real');
        end
        function res = get.Wrsw(obj)
            res = getParameter(obj,'Wrsw');
        end
        function obj = set.Wrdw(obj,val)
            obj = setParameter(obj,'Wrdw',val,0,'real');
        end
        function res = get.Wrdw(obj)
            res = getParameter(obj,'Wrdw');
        end
        function obj = set.Wprwg(obj,val)
            obj = setParameter(obj,'Wprwg',val,0,'real');
        end
        function res = get.Wprwg(obj)
            res = getParameter(obj,'Wprwg');
        end
        function obj = set.Wprwb(obj,val)
            obj = setParameter(obj,'Wprwb',val,0,'real');
        end
        function res = get.Wprwb(obj)
            res = getParameter(obj,'Wprwb');
        end
        function obj = set.Wprt(obj,val)
            obj = setParameter(obj,'Wprt',val,0,'real');
        end
        function res = get.Wprt(obj)
            res = getParameter(obj,'Wprt');
        end
        function obj = set.Weta0(obj,val)
            obj = setParameter(obj,'Weta0',val,0,'real');
        end
        function res = get.Weta0(obj)
            res = getParameter(obj,'Weta0');
        end
        function obj = set.Wetab(obj,val)
            obj = setParameter(obj,'Wetab',val,0,'real');
        end
        function res = get.Wetab(obj)
            res = getParameter(obj,'Wetab');
        end
        function obj = set.Wpclm(obj,val)
            obj = setParameter(obj,'Wpclm',val,0,'real');
        end
        function res = get.Wpclm(obj)
            res = getParameter(obj,'Wpclm');
        end
        function obj = set.Wpdiblc1(obj,val)
            obj = setParameter(obj,'Wpdiblc1',val,0,'real');
        end
        function res = get.Wpdiblc1(obj)
            res = getParameter(obj,'Wpdiblc1');
        end
        function obj = set.Wpdiblc2(obj,val)
            obj = setParameter(obj,'Wpdiblc2',val,0,'real');
        end
        function res = get.Wpdiblc2(obj)
            res = getParameter(obj,'Wpdiblc2');
        end
        function obj = set.Wpdiblcb(obj,val)
            obj = setParameter(obj,'Wpdiblcb',val,0,'real');
        end
        function res = get.Wpdiblcb(obj)
            res = getParameter(obj,'Wpdiblcb');
        end
        function obj = set.Wfprout(obj,val)
            obj = setParameter(obj,'Wfprout',val,0,'real');
        end
        function res = get.Wfprout(obj)
            res = getParameter(obj,'Wfprout');
        end
        function obj = set.Wpdits(obj,val)
            obj = setParameter(obj,'Wpdits',val,0,'real');
        end
        function res = get.Wpdits(obj)
            res = getParameter(obj,'Wpdits');
        end
        function obj = set.Wpditsd(obj,val)
            obj = setParameter(obj,'Wpditsd',val,0,'real');
        end
        function res = get.Wpditsd(obj)
            res = getParameter(obj,'Wpditsd');
        end
        function obj = set.Wpscbe1(obj,val)
            obj = setParameter(obj,'Wpscbe1',val,0,'real');
        end
        function res = get.Wpscbe1(obj)
            res = getParameter(obj,'Wpscbe1');
        end
        function obj = set.Wpscbe2(obj,val)
            obj = setParameter(obj,'Wpscbe2',val,0,'real');
        end
        function res = get.Wpscbe2(obj)
            res = getParameter(obj,'Wpscbe2');
        end
        function obj = set.Wpvag(obj,val)
            obj = setParameter(obj,'Wpvag',val,0,'real');
        end
        function res = get.Wpvag(obj)
            res = getParameter(obj,'Wpvag');
        end
        function obj = set.Wwr(obj,val)
            obj = setParameter(obj,'Wwr',val,0,'real');
        end
        function res = get.Wwr(obj)
            res = getParameter(obj,'Wwr');
        end
        function obj = set.Wdwg(obj,val)
            obj = setParameter(obj,'Wdwg',val,0,'real');
        end
        function res = get.Wdwg(obj)
            res = getParameter(obj,'Wdwg');
        end
        function obj = set.Wdwb(obj,val)
            obj = setParameter(obj,'Wdwb',val,0,'real');
        end
        function res = get.Wdwb(obj)
            res = getParameter(obj,'Wdwb');
        end
        function obj = set.Wb0(obj,val)
            obj = setParameter(obj,'Wb0',val,0,'real');
        end
        function res = get.Wb0(obj)
            res = getParameter(obj,'Wb0');
        end
        function obj = set.Wb1(obj,val)
            obj = setParameter(obj,'Wb1',val,0,'real');
        end
        function res = get.Wb1(obj)
            res = getParameter(obj,'Wb1');
        end
        function obj = set.Wcgsl(obj,val)
            obj = setParameter(obj,'Wcgsl',val,0,'real');
        end
        function res = get.Wcgsl(obj)
            res = getParameter(obj,'Wcgsl');
        end
        function obj = set.Wcgdl(obj,val)
            obj = setParameter(obj,'Wcgdl',val,0,'real');
        end
        function res = get.Wcgdl(obj)
            res = getParameter(obj,'Wcgdl');
        end
        function obj = set.Wckappas(obj,val)
            obj = setParameter(obj,'Wckappas',val,0,'real');
        end
        function res = get.Wckappas(obj)
            res = getParameter(obj,'Wckappas');
        end
        function obj = set.Wckappad(obj,val)
            obj = setParameter(obj,'Wckappad',val,0,'real');
        end
        function res = get.Wckappad(obj)
            res = getParameter(obj,'Wckappad');
        end
        function obj = set.Wcf(obj,val)
            obj = setParameter(obj,'Wcf',val,0,'real');
        end
        function res = get.Wcf(obj)
            res = getParameter(obj,'Wcf');
        end
        function obj = set.Wclc(obj,val)
            obj = setParameter(obj,'Wclc',val,0,'real');
        end
        function res = get.Wclc(obj)
            res = getParameter(obj,'Wclc');
        end
        function obj = set.Wcle(obj,val)
            obj = setParameter(obj,'Wcle',val,0,'real');
        end
        function res = get.Wcle(obj)
            res = getParameter(obj,'Wcle');
        end
        function obj = set.Walpha0(obj,val)
            obj = setParameter(obj,'Walpha0',val,0,'real');
        end
        function res = get.Walpha0(obj)
            res = getParameter(obj,'Walpha0');
        end
        function obj = set.Walpha1(obj,val)
            obj = setParameter(obj,'Walpha1',val,0,'real');
        end
        function res = get.Walpha1(obj)
            res = getParameter(obj,'Walpha1');
        end
        function obj = set.Wbeta0(obj,val)
            obj = setParameter(obj,'Wbeta0',val,0,'real');
        end
        function res = get.Wbeta0(obj)
            res = getParameter(obj,'Wbeta0');
        end
        function obj = set.Wagidl(obj,val)
            obj = setParameter(obj,'Wagidl',val,0,'real');
        end
        function res = get.Wagidl(obj)
            res = getParameter(obj,'Wagidl');
        end
        function obj = set.Wbgidl(obj,val)
            obj = setParameter(obj,'Wbgidl',val,0,'real');
        end
        function res = get.Wbgidl(obj)
            res = getParameter(obj,'Wbgidl');
        end
        function obj = set.Wcgidl(obj,val)
            obj = setParameter(obj,'Wcgidl',val,0,'real');
        end
        function res = get.Wcgidl(obj)
            res = getParameter(obj,'Wcgidl');
        end
        function obj = set.Wegidl(obj,val)
            obj = setParameter(obj,'Wegidl',val,0,'real');
        end
        function res = get.Wegidl(obj)
            res = getParameter(obj,'Wegidl');
        end
        function obj = set.Waigc(obj,val)
            obj = setParameter(obj,'Waigc',val,0,'real');
        end
        function res = get.Waigc(obj)
            res = getParameter(obj,'Waigc');
        end
        function obj = set.Wbigc(obj,val)
            obj = setParameter(obj,'Wbigc',val,0,'real');
        end
        function res = get.Wbigc(obj)
            res = getParameter(obj,'Wbigc');
        end
        function obj = set.Wcigc(obj,val)
            obj = setParameter(obj,'Wcigc',val,0,'real');
        end
        function res = get.Wcigc(obj)
            res = getParameter(obj,'Wcigc');
        end
        function obj = set.Waigsd(obj,val)
            obj = setParameter(obj,'Waigsd',val,0,'real');
        end
        function res = get.Waigsd(obj)
            res = getParameter(obj,'Waigsd');
        end
        function obj = set.Wbigsd(obj,val)
            obj = setParameter(obj,'Wbigsd',val,0,'real');
        end
        function res = get.Wbigsd(obj)
            res = getParameter(obj,'Wbigsd');
        end
        function obj = set.Wcigsd(obj,val)
            obj = setParameter(obj,'Wcigsd',val,0,'real');
        end
        function res = get.Wcigsd(obj)
            res = getParameter(obj,'Wcigsd');
        end
        function obj = set.Waigbacc(obj,val)
            obj = setParameter(obj,'Waigbacc',val,0,'real');
        end
        function res = get.Waigbacc(obj)
            res = getParameter(obj,'Waigbacc');
        end
        function obj = set.Wbigbacc(obj,val)
            obj = setParameter(obj,'Wbigbacc',val,0,'real');
        end
        function res = get.Wbigbacc(obj)
            res = getParameter(obj,'Wbigbacc');
        end
        function obj = set.Wcigbacc(obj,val)
            obj = setParameter(obj,'Wcigbacc',val,0,'real');
        end
        function res = get.Wcigbacc(obj)
            res = getParameter(obj,'Wcigbacc');
        end
        function obj = set.Waigbinv(obj,val)
            obj = setParameter(obj,'Waigbinv',val,0,'real');
        end
        function res = get.Waigbinv(obj)
            res = getParameter(obj,'Waigbinv');
        end
        function obj = set.Wbigbinv(obj,val)
            obj = setParameter(obj,'Wbigbinv',val,0,'real');
        end
        function res = get.Wbigbinv(obj)
            res = getParameter(obj,'Wbigbinv');
        end
        function obj = set.Wcigbinv(obj,val)
            obj = setParameter(obj,'Wcigbinv',val,0,'real');
        end
        function res = get.Wcigbinv(obj)
            res = getParameter(obj,'Wcigbinv');
        end
        function obj = set.Wnigc(obj,val)
            obj = setParameter(obj,'Wnigc',val,0,'real');
        end
        function res = get.Wnigc(obj)
            res = getParameter(obj,'Wnigc');
        end
        function obj = set.Wnigbinv(obj,val)
            obj = setParameter(obj,'Wnigbinv',val,0,'real');
        end
        function res = get.Wnigbinv(obj)
            res = getParameter(obj,'Wnigbinv');
        end
        function obj = set.Wnigbacc(obj,val)
            obj = setParameter(obj,'Wnigbacc',val,0,'real');
        end
        function res = get.Wnigbacc(obj)
            res = getParameter(obj,'Wnigbacc');
        end
        function obj = set.Wntox(obj,val)
            obj = setParameter(obj,'Wntox',val,0,'real');
        end
        function res = get.Wntox(obj)
            res = getParameter(obj,'Wntox');
        end
        function obj = set.Weigbinv(obj,val)
            obj = setParameter(obj,'Weigbinv',val,0,'real');
        end
        function res = get.Weigbinv(obj)
            res = getParameter(obj,'Weigbinv');
        end
        function obj = set.Wpigcd(obj,val)
            obj = setParameter(obj,'Wpigcd',val,0,'real');
        end
        function res = get.Wpigcd(obj)
            res = getParameter(obj,'Wpigcd');
        end
        function obj = set.Wpoxedge(obj,val)
            obj = setParameter(obj,'Wpoxedge',val,0,'real');
        end
        function res = get.Wpoxedge(obj)
            res = getParameter(obj,'Wpoxedge');
        end
        function obj = set.Wvfbcv(obj,val)
            obj = setParameter(obj,'Wvfbcv',val,0,'real');
        end
        function res = get.Wvfbcv(obj)
            res = getParameter(obj,'Wvfbcv');
        end
        function obj = set.Wvfb(obj,val)
            obj = setParameter(obj,'Wvfb',val,0,'real');
        end
        function res = get.Wvfb(obj)
            res = getParameter(obj,'Wvfb');
        end
        function obj = set.Wacde(obj,val)
            obj = setParameter(obj,'Wacde',val,0,'real');
        end
        function res = get.Wacde(obj)
            res = getParameter(obj,'Wacde');
        end
        function obj = set.Wmoin(obj,val)
            obj = setParameter(obj,'Wmoin',val,0,'real');
        end
        function res = get.Wmoin(obj)
            res = getParameter(obj,'Wmoin');
        end
        function obj = set.Wnoff(obj,val)
            obj = setParameter(obj,'Wnoff',val,0,'real');
        end
        function res = get.Wnoff(obj)
            res = getParameter(obj,'Wnoff');
        end
        function obj = set.Wvoffcv(obj,val)
            obj = setParameter(obj,'Wvoffcv',val,0,'real');
        end
        function res = get.Wvoffcv(obj)
            res = getParameter(obj,'Wvoffcv');
        end
        function obj = set.Wxrcrg1(obj,val)
            obj = setParameter(obj,'Wxrcrg1',val,0,'real');
        end
        function res = get.Wxrcrg1(obj)
            res = getParameter(obj,'Wxrcrg1');
        end
        function obj = set.Wxrcrg2(obj,val)
            obj = setParameter(obj,'Wxrcrg2',val,0,'real');
        end
        function res = get.Wxrcrg2(obj)
            res = getParameter(obj,'Wxrcrg2');
        end
        function obj = set.Weu(obj,val)
            obj = setParameter(obj,'Weu',val,0,'real');
        end
        function res = get.Weu(obj)
            res = getParameter(obj,'Weu');
        end
        function obj = set.Pcdsc(obj,val)
            obj = setParameter(obj,'Pcdsc',val,0,'real');
        end
        function res = get.Pcdsc(obj)
            res = getParameter(obj,'Pcdsc');
        end
        function obj = set.Pcdscb(obj,val)
            obj = setParameter(obj,'Pcdscb',val,0,'real');
        end
        function res = get.Pcdscb(obj)
            res = getParameter(obj,'Pcdscb');
        end
        function obj = set.Pcdscd(obj,val)
            obj = setParameter(obj,'Pcdscd',val,0,'real');
        end
        function res = get.Pcdscd(obj)
            res = getParameter(obj,'Pcdscd');
        end
        function obj = set.Pcit(obj,val)
            obj = setParameter(obj,'Pcit',val,0,'real');
        end
        function res = get.Pcit(obj)
            res = getParameter(obj,'Pcit');
        end
        function obj = set.Pnfactor(obj,val)
            obj = setParameter(obj,'Pnfactor',val,0,'real');
        end
        function res = get.Pnfactor(obj)
            res = getParameter(obj,'Pnfactor');
        end
        function obj = set.Pxj(obj,val)
            obj = setParameter(obj,'Pxj',val,0,'real');
        end
        function res = get.Pxj(obj)
            res = getParameter(obj,'Pxj');
        end
        function obj = set.Pvsat(obj,val)
            obj = setParameter(obj,'Pvsat',val,0,'real');
        end
        function res = get.Pvsat(obj)
            res = getParameter(obj,'Pvsat');
        end
        function obj = set.Pat(obj,val)
            obj = setParameter(obj,'Pat',val,0,'real');
        end
        function res = get.Pat(obj)
            res = getParameter(obj,'Pat');
        end
        function obj = set.Pa0(obj,val)
            obj = setParameter(obj,'Pa0',val,0,'real');
        end
        function res = get.Pa0(obj)
            res = getParameter(obj,'Pa0');
        end
        function obj = set.Pags(obj,val)
            obj = setParameter(obj,'Pags',val,0,'real');
        end
        function res = get.Pags(obj)
            res = getParameter(obj,'Pags');
        end
        function obj = set.Pa1(obj,val)
            obj = setParameter(obj,'Pa1',val,0,'real');
        end
        function res = get.Pa1(obj)
            res = getParameter(obj,'Pa1');
        end
        function obj = set.Pa2(obj,val)
            obj = setParameter(obj,'Pa2',val,0,'real');
        end
        function res = get.Pa2(obj)
            res = getParameter(obj,'Pa2');
        end
        function obj = set.Pketa(obj,val)
            obj = setParameter(obj,'Pketa',val,0,'real');
        end
        function res = get.Pketa(obj)
            res = getParameter(obj,'Pketa');
        end
        function obj = set.Pnsub(obj,val)
            obj = setParameter(obj,'Pnsub',val,0,'real');
        end
        function res = get.Pnsub(obj)
            res = getParameter(obj,'Pnsub');
        end
        function obj = set.Pndep(obj,val)
            obj = setParameter(obj,'Pndep',val,0,'real');
        end
        function res = get.Pndep(obj)
            res = getParameter(obj,'Pndep');
        end
        function obj = set.Pnsd(obj,val)
            obj = setParameter(obj,'Pnsd',val,0,'real');
        end
        function res = get.Pnsd(obj)
            res = getParameter(obj,'Pnsd');
        end
        function obj = set.Pphin(obj,val)
            obj = setParameter(obj,'Pphin',val,0,'real');
        end
        function res = get.Pphin(obj)
            res = getParameter(obj,'Pphin');
        end
        function obj = set.Pngate(obj,val)
            obj = setParameter(obj,'Pngate',val,0,'real');
        end
        function res = get.Pngate(obj)
            res = getParameter(obj,'Pngate');
        end
        function obj = set.Pgamma1(obj,val)
            obj = setParameter(obj,'Pgamma1',val,0,'real');
        end
        function res = get.Pgamma1(obj)
            res = getParameter(obj,'Pgamma1');
        end
        function obj = set.Pgamma2(obj,val)
            obj = setParameter(obj,'Pgamma2',val,0,'real');
        end
        function res = get.Pgamma2(obj)
            res = getParameter(obj,'Pgamma2');
        end
        function obj = set.Pvbx(obj,val)
            obj = setParameter(obj,'Pvbx',val,0,'real');
        end
        function res = get.Pvbx(obj)
            res = getParameter(obj,'Pvbx');
        end
        function obj = set.Pvbm(obj,val)
            obj = setParameter(obj,'Pvbm',val,0,'real');
        end
        function res = get.Pvbm(obj)
            res = getParameter(obj,'Pvbm');
        end
        function obj = set.Pxt(obj,val)
            obj = setParameter(obj,'Pxt',val,0,'real');
        end
        function res = get.Pxt(obj)
            res = getParameter(obj,'Pxt');
        end
        function obj = set.Pk1(obj,val)
            obj = setParameter(obj,'Pk1',val,0,'real');
        end
        function res = get.Pk1(obj)
            res = getParameter(obj,'Pk1');
        end
        function obj = set.Pkt1(obj,val)
            obj = setParameter(obj,'Pkt1',val,0,'real');
        end
        function res = get.Pkt1(obj)
            res = getParameter(obj,'Pkt1');
        end
        function obj = set.Pkt1l(obj,val)
            obj = setParameter(obj,'Pkt1l',val,0,'real');
        end
        function res = get.Pkt1l(obj)
            res = getParameter(obj,'Pkt1l');
        end
        function obj = set.Pkt2(obj,val)
            obj = setParameter(obj,'Pkt2',val,0,'real');
        end
        function res = get.Pkt2(obj)
            res = getParameter(obj,'Pkt2');
        end
        function obj = set.Pk2(obj,val)
            obj = setParameter(obj,'Pk2',val,0,'real');
        end
        function res = get.Pk2(obj)
            res = getParameter(obj,'Pk2');
        end
        function obj = set.Pk3(obj,val)
            obj = setParameter(obj,'Pk3',val,0,'real');
        end
        function res = get.Pk3(obj)
            res = getParameter(obj,'Pk3');
        end
        function obj = set.Pk3b(obj,val)
            obj = setParameter(obj,'Pk3b',val,0,'real');
        end
        function res = get.Pk3b(obj)
            res = getParameter(obj,'Pk3b');
        end
        function obj = set.Pw0(obj,val)
            obj = setParameter(obj,'Pw0',val,0,'real');
        end
        function res = get.Pw0(obj)
            res = getParameter(obj,'Pw0');
        end
        function obj = set.Pdvtp0(obj,val)
            obj = setParameter(obj,'Pdvtp0',val,0,'real');
        end
        function res = get.Pdvtp0(obj)
            res = getParameter(obj,'Pdvtp0');
        end
        function obj = set.Pdvtp1(obj,val)
            obj = setParameter(obj,'Pdvtp1',val,0,'real');
        end
        function res = get.Pdvtp1(obj)
            res = getParameter(obj,'Pdvtp1');
        end
        function obj = set.Plpe0(obj,val)
            obj = setParameter(obj,'Plpe0',val,0,'real');
        end
        function res = get.Plpe0(obj)
            res = getParameter(obj,'Plpe0');
        end
        function obj = set.Plpeb(obj,val)
            obj = setParameter(obj,'Plpeb',val,0,'real');
        end
        function res = get.Plpeb(obj)
            res = getParameter(obj,'Plpeb');
        end
        function obj = set.Pdvt0(obj,val)
            obj = setParameter(obj,'Pdvt0',val,0,'real');
        end
        function res = get.Pdvt0(obj)
            res = getParameter(obj,'Pdvt0');
        end
        function obj = set.Pdvt1(obj,val)
            obj = setParameter(obj,'Pdvt1',val,0,'real');
        end
        function res = get.Pdvt1(obj)
            res = getParameter(obj,'Pdvt1');
        end
        function obj = set.Pdvt2(obj,val)
            obj = setParameter(obj,'Pdvt2',val,0,'real');
        end
        function res = get.Pdvt2(obj)
            res = getParameter(obj,'Pdvt2');
        end
        function obj = set.Pdvt0w(obj,val)
            obj = setParameter(obj,'Pdvt0w',val,0,'real');
        end
        function res = get.Pdvt0w(obj)
            res = getParameter(obj,'Pdvt0w');
        end
        function obj = set.Pdvt1w(obj,val)
            obj = setParameter(obj,'Pdvt1w',val,0,'real');
        end
        function res = get.Pdvt1w(obj)
            res = getParameter(obj,'Pdvt1w');
        end
        function obj = set.Pdvt2w(obj,val)
            obj = setParameter(obj,'Pdvt2w',val,0,'real');
        end
        function res = get.Pdvt2w(obj)
            res = getParameter(obj,'Pdvt2w');
        end
        function obj = set.Pdrout(obj,val)
            obj = setParameter(obj,'Pdrout',val,0,'real');
        end
        function res = get.Pdrout(obj)
            res = getParameter(obj,'Pdrout');
        end
        function obj = set.Pdsub(obj,val)
            obj = setParameter(obj,'Pdsub',val,0,'real');
        end
        function res = get.Pdsub(obj)
            res = getParameter(obj,'Pdsub');
        end
        function obj = set.Pvth0(obj,val)
            obj = setParameter(obj,'Pvth0',val,0,'real');
        end
        function res = get.Pvth0(obj)
            res = getParameter(obj,'Pvth0');
        end
        function obj = set.Pvtho(obj,val)
            obj = setParameter(obj,'Pvtho',val,0,'real');
        end
        function res = get.Pvtho(obj)
            res = getParameter(obj,'Pvtho');
        end
        function obj = set.Pua(obj,val)
            obj = setParameter(obj,'Pua',val,0,'real');
        end
        function res = get.Pua(obj)
            res = getParameter(obj,'Pua');
        end
        function obj = set.Pua1(obj,val)
            obj = setParameter(obj,'Pua1',val,0,'real');
        end
        function res = get.Pua1(obj)
            res = getParameter(obj,'Pua1');
        end
        function obj = set.Pub(obj,val)
            obj = setParameter(obj,'Pub',val,0,'real');
        end
        function res = get.Pub(obj)
            res = getParameter(obj,'Pub');
        end
        function obj = set.Pub1(obj,val)
            obj = setParameter(obj,'Pub1',val,0,'real');
        end
        function res = get.Pub1(obj)
            res = getParameter(obj,'Pub1');
        end
        function obj = set.Puc(obj,val)
            obj = setParameter(obj,'Puc',val,0,'real');
        end
        function res = get.Puc(obj)
            res = getParameter(obj,'Puc');
        end
        function obj = set.Puc1(obj,val)
            obj = setParameter(obj,'Puc1',val,0,'real');
        end
        function res = get.Puc1(obj)
            res = getParameter(obj,'Puc1');
        end
        function obj = set.Pu0(obj,val)
            obj = setParameter(obj,'Pu0',val,0,'real');
        end
        function res = get.Pu0(obj)
            res = getParameter(obj,'Pu0');
        end
        function obj = set.Pute(obj,val)
            obj = setParameter(obj,'Pute',val,0,'real');
        end
        function res = get.Pute(obj)
            res = getParameter(obj,'Pute');
        end
        function obj = set.Pvoff(obj,val)
            obj = setParameter(obj,'Pvoff',val,0,'real');
        end
        function res = get.Pvoff(obj)
            res = getParameter(obj,'Pvoff');
        end
        function obj = set.Pminv(obj,val)
            obj = setParameter(obj,'Pminv',val,0,'real');
        end
        function res = get.Pminv(obj)
            res = getParameter(obj,'Pminv');
        end
        function obj = set.Pdelta(obj,val)
            obj = setParameter(obj,'Pdelta',val,0,'real');
        end
        function res = get.Pdelta(obj)
            res = getParameter(obj,'Pdelta');
        end
        function obj = set.Prdsw(obj,val)
            obj = setParameter(obj,'Prdsw',val,0,'real');
        end
        function res = get.Prdsw(obj)
            res = getParameter(obj,'Prdsw');
        end
        function obj = set.Prsw(obj,val)
            obj = setParameter(obj,'Prsw',val,0,'real');
        end
        function res = get.Prsw(obj)
            res = getParameter(obj,'Prsw');
        end
        function obj = set.Prdw(obj,val)
            obj = setParameter(obj,'Prdw',val,0,'real');
        end
        function res = get.Prdw(obj)
            res = getParameter(obj,'Prdw');
        end
        function obj = set.Pprwg(obj,val)
            obj = setParameter(obj,'Pprwg',val,0,'real');
        end
        function res = get.Pprwg(obj)
            res = getParameter(obj,'Pprwg');
        end
        function obj = set.Pprwb(obj,val)
            obj = setParameter(obj,'Pprwb',val,0,'real');
        end
        function res = get.Pprwb(obj)
            res = getParameter(obj,'Pprwb');
        end
        function obj = set.Pprt(obj,val)
            obj = setParameter(obj,'Pprt',val,0,'real');
        end
        function res = get.Pprt(obj)
            res = getParameter(obj,'Pprt');
        end
        function obj = set.Peta0(obj,val)
            obj = setParameter(obj,'Peta0',val,0,'real');
        end
        function res = get.Peta0(obj)
            res = getParameter(obj,'Peta0');
        end
        function obj = set.Petab(obj,val)
            obj = setParameter(obj,'Petab',val,0,'real');
        end
        function res = get.Petab(obj)
            res = getParameter(obj,'Petab');
        end
        function obj = set.Ppclm(obj,val)
            obj = setParameter(obj,'Ppclm',val,0,'real');
        end
        function res = get.Ppclm(obj)
            res = getParameter(obj,'Ppclm');
        end
        function obj = set.Ppdiblc1(obj,val)
            obj = setParameter(obj,'Ppdiblc1',val,0,'real');
        end
        function res = get.Ppdiblc1(obj)
            res = getParameter(obj,'Ppdiblc1');
        end
        function obj = set.Ppdiblc2(obj,val)
            obj = setParameter(obj,'Ppdiblc2',val,0,'real');
        end
        function res = get.Ppdiblc2(obj)
            res = getParameter(obj,'Ppdiblc2');
        end
        function obj = set.Ppdiblcb(obj,val)
            obj = setParameter(obj,'Ppdiblcb',val,0,'real');
        end
        function res = get.Ppdiblcb(obj)
            res = getParameter(obj,'Ppdiblcb');
        end
        function obj = set.Pfprout(obj,val)
            obj = setParameter(obj,'Pfprout',val,0,'real');
        end
        function res = get.Pfprout(obj)
            res = getParameter(obj,'Pfprout');
        end
        function obj = set.Ppdits(obj,val)
            obj = setParameter(obj,'Ppdits',val,0,'real');
        end
        function res = get.Ppdits(obj)
            res = getParameter(obj,'Ppdits');
        end
        function obj = set.Ppditsd(obj,val)
            obj = setParameter(obj,'Ppditsd',val,0,'real');
        end
        function res = get.Ppditsd(obj)
            res = getParameter(obj,'Ppditsd');
        end
        function obj = set.Ppscbe1(obj,val)
            obj = setParameter(obj,'Ppscbe1',val,0,'real');
        end
        function res = get.Ppscbe1(obj)
            res = getParameter(obj,'Ppscbe1');
        end
        function obj = set.Ppscbe2(obj,val)
            obj = setParameter(obj,'Ppscbe2',val,0,'real');
        end
        function res = get.Ppscbe2(obj)
            res = getParameter(obj,'Ppscbe2');
        end
        function obj = set.Ppvag(obj,val)
            obj = setParameter(obj,'Ppvag',val,0,'real');
        end
        function res = get.Ppvag(obj)
            res = getParameter(obj,'Ppvag');
        end
        function obj = set.Pwr(obj,val)
            obj = setParameter(obj,'Pwr',val,0,'real');
        end
        function res = get.Pwr(obj)
            res = getParameter(obj,'Pwr');
        end
        function obj = set.Pdwg(obj,val)
            obj = setParameter(obj,'Pdwg',val,0,'real');
        end
        function res = get.Pdwg(obj)
            res = getParameter(obj,'Pdwg');
        end
        function obj = set.Pdwb(obj,val)
            obj = setParameter(obj,'Pdwb',val,0,'real');
        end
        function res = get.Pdwb(obj)
            res = getParameter(obj,'Pdwb');
        end
        function obj = set.Pb0(obj,val)
            obj = setParameter(obj,'Pb0',val,0,'real');
        end
        function res = get.Pb0(obj)
            res = getParameter(obj,'Pb0');
        end
        function obj = set.Pb1(obj,val)
            obj = setParameter(obj,'Pb1',val,0,'real');
        end
        function res = get.Pb1(obj)
            res = getParameter(obj,'Pb1');
        end
        function obj = set.Pcgsl(obj,val)
            obj = setParameter(obj,'Pcgsl',val,0,'real');
        end
        function res = get.Pcgsl(obj)
            res = getParameter(obj,'Pcgsl');
        end
        function obj = set.Pcgdl(obj,val)
            obj = setParameter(obj,'Pcgdl',val,0,'real');
        end
        function res = get.Pcgdl(obj)
            res = getParameter(obj,'Pcgdl');
        end
        function obj = set.Pckappas(obj,val)
            obj = setParameter(obj,'Pckappas',val,0,'real');
        end
        function res = get.Pckappas(obj)
            res = getParameter(obj,'Pckappas');
        end
        function obj = set.Pckappad(obj,val)
            obj = setParameter(obj,'Pckappad',val,0,'real');
        end
        function res = get.Pckappad(obj)
            res = getParameter(obj,'Pckappad');
        end
        function obj = set.Pcf(obj,val)
            obj = setParameter(obj,'Pcf',val,0,'real');
        end
        function res = get.Pcf(obj)
            res = getParameter(obj,'Pcf');
        end
        function obj = set.Pclc(obj,val)
            obj = setParameter(obj,'Pclc',val,0,'real');
        end
        function res = get.Pclc(obj)
            res = getParameter(obj,'Pclc');
        end
        function obj = set.Pcle(obj,val)
            obj = setParameter(obj,'Pcle',val,0,'real');
        end
        function res = get.Pcle(obj)
            res = getParameter(obj,'Pcle');
        end
        function obj = set.Palpha0(obj,val)
            obj = setParameter(obj,'Palpha0',val,0,'real');
        end
        function res = get.Palpha0(obj)
            res = getParameter(obj,'Palpha0');
        end
        function obj = set.Palpha1(obj,val)
            obj = setParameter(obj,'Palpha1',val,0,'real');
        end
        function res = get.Palpha1(obj)
            res = getParameter(obj,'Palpha1');
        end
        function obj = set.Pbeta0(obj,val)
            obj = setParameter(obj,'Pbeta0',val,0,'real');
        end
        function res = get.Pbeta0(obj)
            res = getParameter(obj,'Pbeta0');
        end
        function obj = set.Pagidl(obj,val)
            obj = setParameter(obj,'Pagidl',val,0,'real');
        end
        function res = get.Pagidl(obj)
            res = getParameter(obj,'Pagidl');
        end
        function obj = set.Pbgidl(obj,val)
            obj = setParameter(obj,'Pbgidl',val,0,'real');
        end
        function res = get.Pbgidl(obj)
            res = getParameter(obj,'Pbgidl');
        end
        function obj = set.Pcgidl(obj,val)
            obj = setParameter(obj,'Pcgidl',val,0,'real');
        end
        function res = get.Pcgidl(obj)
            res = getParameter(obj,'Pcgidl');
        end
        function obj = set.Pegidl(obj,val)
            obj = setParameter(obj,'Pegidl',val,0,'real');
        end
        function res = get.Pegidl(obj)
            res = getParameter(obj,'Pegidl');
        end
        function obj = set.Paigc(obj,val)
            obj = setParameter(obj,'Paigc',val,0,'real');
        end
        function res = get.Paigc(obj)
            res = getParameter(obj,'Paigc');
        end
        function obj = set.Pbigc(obj,val)
            obj = setParameter(obj,'Pbigc',val,0,'real');
        end
        function res = get.Pbigc(obj)
            res = getParameter(obj,'Pbigc');
        end
        function obj = set.Pcigc(obj,val)
            obj = setParameter(obj,'Pcigc',val,0,'real');
        end
        function res = get.Pcigc(obj)
            res = getParameter(obj,'Pcigc');
        end
        function obj = set.Paigsd(obj,val)
            obj = setParameter(obj,'Paigsd',val,0,'real');
        end
        function res = get.Paigsd(obj)
            res = getParameter(obj,'Paigsd');
        end
        function obj = set.Pbigsd(obj,val)
            obj = setParameter(obj,'Pbigsd',val,0,'real');
        end
        function res = get.Pbigsd(obj)
            res = getParameter(obj,'Pbigsd');
        end
        function obj = set.Pcigsd(obj,val)
            obj = setParameter(obj,'Pcigsd',val,0,'real');
        end
        function res = get.Pcigsd(obj)
            res = getParameter(obj,'Pcigsd');
        end
        function obj = set.Paigbacc(obj,val)
            obj = setParameter(obj,'Paigbacc',val,0,'real');
        end
        function res = get.Paigbacc(obj)
            res = getParameter(obj,'Paigbacc');
        end
        function obj = set.Pbigbacc(obj,val)
            obj = setParameter(obj,'Pbigbacc',val,0,'real');
        end
        function res = get.Pbigbacc(obj)
            res = getParameter(obj,'Pbigbacc');
        end
        function obj = set.Pcigbacc(obj,val)
            obj = setParameter(obj,'Pcigbacc',val,0,'real');
        end
        function res = get.Pcigbacc(obj)
            res = getParameter(obj,'Pcigbacc');
        end
        function obj = set.Paigbinv(obj,val)
            obj = setParameter(obj,'Paigbinv',val,0,'real');
        end
        function res = get.Paigbinv(obj)
            res = getParameter(obj,'Paigbinv');
        end
        function obj = set.Pbigbinv(obj,val)
            obj = setParameter(obj,'Pbigbinv',val,0,'real');
        end
        function res = get.Pbigbinv(obj)
            res = getParameter(obj,'Pbigbinv');
        end
        function obj = set.Pcigbinv(obj,val)
            obj = setParameter(obj,'Pcigbinv',val,0,'real');
        end
        function res = get.Pcigbinv(obj)
            res = getParameter(obj,'Pcigbinv');
        end
        function obj = set.Pnigc(obj,val)
            obj = setParameter(obj,'Pnigc',val,0,'real');
        end
        function res = get.Pnigc(obj)
            res = getParameter(obj,'Pnigc');
        end
        function obj = set.Pnigbinv(obj,val)
            obj = setParameter(obj,'Pnigbinv',val,0,'real');
        end
        function res = get.Pnigbinv(obj)
            res = getParameter(obj,'Pnigbinv');
        end
        function obj = set.Pnigbacc(obj,val)
            obj = setParameter(obj,'Pnigbacc',val,0,'real');
        end
        function res = get.Pnigbacc(obj)
            res = getParameter(obj,'Pnigbacc');
        end
        function obj = set.Pntox(obj,val)
            obj = setParameter(obj,'Pntox',val,0,'real');
        end
        function res = get.Pntox(obj)
            res = getParameter(obj,'Pntox');
        end
        function obj = set.Peigbinv(obj,val)
            obj = setParameter(obj,'Peigbinv',val,0,'real');
        end
        function res = get.Peigbinv(obj)
            res = getParameter(obj,'Peigbinv');
        end
        function obj = set.Ppigcd(obj,val)
            obj = setParameter(obj,'Ppigcd',val,0,'real');
        end
        function res = get.Ppigcd(obj)
            res = getParameter(obj,'Ppigcd');
        end
        function obj = set.Ppoxedge(obj,val)
            obj = setParameter(obj,'Ppoxedge',val,0,'real');
        end
        function res = get.Ppoxedge(obj)
            res = getParameter(obj,'Ppoxedge');
        end
        function obj = set.Pvfbcv(obj,val)
            obj = setParameter(obj,'Pvfbcv',val,0,'real');
        end
        function res = get.Pvfbcv(obj)
            res = getParameter(obj,'Pvfbcv');
        end
        function obj = set.Pvfb(obj,val)
            obj = setParameter(obj,'Pvfb',val,0,'real');
        end
        function res = get.Pvfb(obj)
            res = getParameter(obj,'Pvfb');
        end
        function obj = set.Pacde(obj,val)
            obj = setParameter(obj,'Pacde',val,0,'real');
        end
        function res = get.Pacde(obj)
            res = getParameter(obj,'Pacde');
        end
        function obj = set.Pmoin(obj,val)
            obj = setParameter(obj,'Pmoin',val,0,'real');
        end
        function res = get.Pmoin(obj)
            res = getParameter(obj,'Pmoin');
        end
        function obj = set.Pnoff(obj,val)
            obj = setParameter(obj,'Pnoff',val,0,'real');
        end
        function res = get.Pnoff(obj)
            res = getParameter(obj,'Pnoff');
        end
        function obj = set.Pvoffcv(obj,val)
            obj = setParameter(obj,'Pvoffcv',val,0,'real');
        end
        function res = get.Pvoffcv(obj)
            res = getParameter(obj,'Pvoffcv');
        end
        function obj = set.Pxrcrg1(obj,val)
            obj = setParameter(obj,'Pxrcrg1',val,0,'real');
        end
        function res = get.Pxrcrg1(obj)
            res = getParameter(obj,'Pxrcrg1');
        end
        function obj = set.Pxrcrg2(obj,val)
            obj = setParameter(obj,'Pxrcrg2',val,0,'real');
        end
        function res = get.Pxrcrg2(obj)
            res = getParameter(obj,'Pxrcrg2');
        end
        function obj = set.Peu(obj,val)
            obj = setParameter(obj,'Peu',val,0,'real');
        end
        function res = get.Peu(obj)
            res = getParameter(obj,'Peu');
        end
        function obj = set.Noia(obj,val)
            obj = setParameter(obj,'Noia',val,0,'real');
        end
        function res = get.Noia(obj)
            res = getParameter(obj,'Noia');
        end
        function obj = set.Noib(obj,val)
            obj = setParameter(obj,'Noib',val,0,'real');
        end
        function res = get.Noib(obj)
            res = getParameter(obj,'Noib');
        end
        function obj = set.Noic(obj,val)
            obj = setParameter(obj,'Noic',val,0,'real');
        end
        function res = get.Noic(obj)
            res = getParameter(obj,'Noic');
        end
        function obj = set.Tnoia(obj,val)
            obj = setParameter(obj,'Tnoia',val,0,'real');
        end
        function res = get.Tnoia(obj)
            res = getParameter(obj,'Tnoia');
        end
        function obj = set.Tnoib(obj,val)
            obj = setParameter(obj,'Tnoib',val,0,'real');
        end
        function res = get.Tnoib(obj)
            res = getParameter(obj,'Tnoib');
        end
        function obj = set.Ntnoi(obj,val)
            obj = setParameter(obj,'Ntnoi',val,0,'real');
        end
        function res = get.Ntnoi(obj)
            res = getParameter(obj,'Ntnoi');
        end
        function obj = set.Em(obj,val)
            obj = setParameter(obj,'Em',val,0,'real');
        end
        function res = get.Em(obj)
            res = getParameter(obj,'Em');
        end
        function obj = set.Ef(obj,val)
            obj = setParameter(obj,'Ef',val,0,'real');
        end
        function res = get.Ef(obj)
            res = getParameter(obj,'Ef');
        end
        function obj = set.Af(obj,val)
            obj = setParameter(obj,'Af',val,0,'real');
        end
        function res = get.Af(obj)
            res = getParameter(obj,'Af');
        end
        function obj = set.Kf(obj,val)
            obj = setParameter(obj,'Kf',val,0,'real');
        end
        function res = get.Kf(obj)
            res = getParameter(obj,'Kf');
        end
        function obj = set.Xw(obj,val)
            obj = setParameter(obj,'Xw',val,0,'real');
        end
        function res = get.Xw(obj)
            res = getParameter(obj,'Xw');
        end
        function obj = set.Xl(obj,val)
            obj = setParameter(obj,'Xl',val,0,'real');
        end
        function res = get.Xl(obj)
            res = getParameter(obj,'Xl');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Ad(obj,val)
            obj = setParameter(obj,'Ad',val,0,'real');
        end
        function res = get.Ad(obj)
            res = getParameter(obj,'Ad');
        end
        function obj = set.As(obj,val)
            obj = setParameter(obj,'As',val,0,'real');
        end
        function res = get.As(obj)
            res = getParameter(obj,'As');
        end
        function obj = set.Pd(obj,val)
            obj = setParameter(obj,'Pd',val,0,'real');
        end
        function res = get.Pd(obj)
            res = getParameter(obj,'Pd');
        end
        function obj = set.Ps(obj,val)
            obj = setParameter(obj,'Ps',val,0,'real');
        end
        function res = get.Ps(obj)
            res = getParameter(obj,'Ps');
        end
        function obj = set.Nrd(obj,val)
            obj = setParameter(obj,'Nrd',val,0,'real');
        end
        function res = get.Nrd(obj)
            res = getParameter(obj,'Nrd');
        end
        function obj = set.Nrs(obj,val)
            obj = setParameter(obj,'Nrs',val,0,'real');
        end
        function res = get.Nrs(obj)
            res = getParameter(obj,'Nrs');
        end
        function obj = set.Imelt(obj,val)
            obj = setParameter(obj,'Imelt',val,0,'real');
        end
        function res = get.Imelt(obj)
            res = getParameter(obj,'Imelt');
        end
        function obj = set.Tlev(obj,val)
            obj = setParameter(obj,'Tlev',val,0,'integer');
        end
        function res = get.Tlev(obj)
            res = getParameter(obj,'Tlev');
        end
        function obj = set.Tlevc(obj,val)
            obj = setParameter(obj,'Tlevc',val,0,'integer');
        end
        function res = get.Tlevc(obj)
            res = getParameter(obj,'Tlevc');
        end
        function obj = set.Eg(obj,val)
            obj = setParameter(obj,'Eg',val,0,'real');
        end
        function res = get.Eg(obj)
            res = getParameter(obj,'Eg');
        end
        function obj = set.Gap1(obj,val)
            obj = setParameter(obj,'Gap1',val,0,'real');
        end
        function res = get.Gap1(obj)
            res = getParameter(obj,'Gap1');
        end
        function obj = set.Gap2(obj,val)
            obj = setParameter(obj,'Gap2',val,0,'real');
        end
        function res = get.Gap2(obj)
            res = getParameter(obj,'Gap2');
        end
        function obj = set.wVsubfwd(obj,val)
            obj = setParameter(obj,'wVsubfwd',val,0,'real');
        end
        function res = get.wVsubfwd(obj)
            res = getParameter(obj,'wVsubfwd');
        end
        function obj = set.wBvsub(obj,val)
            obj = setParameter(obj,'wBvsub',val,0,'real');
        end
        function res = get.wBvsub(obj)
            res = getParameter(obj,'wBvsub');
        end
        function obj = set.wBvg(obj,val)
            obj = setParameter(obj,'wBvg',val,0,'real');
        end
        function res = get.wBvg(obj)
            res = getParameter(obj,'wBvg');
        end
        function obj = set.wBvds(obj,val)
            obj = setParameter(obj,'wBvds',val,0,'real');
        end
        function res = get.wBvds(obj)
            res = getParameter(obj,'wBvds');
        end
        function obj = set.wIdsmax(obj,val)
            obj = setParameter(obj,'wIdsmax',val,0,'real');
        end
        function res = get.wIdsmax(obj)
            res = getParameter(obj,'wIdsmax');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.Tempmod(obj,val)
            obj = setParameter(obj,'Tempmod',val,0,'integer');
        end
        function res = get.Tempmod(obj)
            res = getParameter(obj,'Tempmod');
        end
        function obj = set.Lambda(obj,val)
            obj = setParameter(obj,'Lambda',val,0,'real');
        end
        function res = get.Lambda(obj)
            res = getParameter(obj,'Lambda');
        end
        function obj = set.Vtl(obj,val)
            obj = setParameter(obj,'Vtl',val,0,'real');
        end
        function res = get.Vtl(obj)
            res = getParameter(obj,'Vtl');
        end
        function obj = set.Lc(obj,val)
            obj = setParameter(obj,'Lc',val,0,'real');
        end
        function res = get.Lc(obj)
            res = getParameter(obj,'Lc');
        end
        function obj = set.Xn(obj,val)
            obj = setParameter(obj,'Xn',val,0,'real');
        end
        function res = get.Xn(obj)
            res = getParameter(obj,'Xn');
        end
        function obj = set.Llambda(obj,val)
            obj = setParameter(obj,'Llambda',val,0,'real');
        end
        function res = get.Llambda(obj)
            res = getParameter(obj,'Llambda');
        end
        function obj = set.Lvtl(obj,val)
            obj = setParameter(obj,'Lvtl',val,0,'real');
        end
        function res = get.Lvtl(obj)
            res = getParameter(obj,'Lvtl');
        end
        function obj = set.Lxn(obj,val)
            obj = setParameter(obj,'Lxn',val,0,'real');
        end
        function res = get.Lxn(obj)
            res = getParameter(obj,'Lxn');
        end
        function obj = set.Wlambda(obj,val)
            obj = setParameter(obj,'Wlambda',val,0,'real');
        end
        function res = get.Wlambda(obj)
            res = getParameter(obj,'Wlambda');
        end
        function obj = set.Wvtl(obj,val)
            obj = setParameter(obj,'Wvtl',val,0,'real');
        end
        function res = get.Wvtl(obj)
            res = getParameter(obj,'Wvtl');
        end
        function obj = set.Wxn(obj,val)
            obj = setParameter(obj,'Wxn',val,0,'real');
        end
        function res = get.Wxn(obj)
            res = getParameter(obj,'Wxn');
        end
        function obj = set.Plambda(obj,val)
            obj = setParameter(obj,'Plambda',val,0,'real');
        end
        function res = get.Plambda(obj)
            res = getParameter(obj,'Plambda');
        end
        function obj = set.Pvtl(obj,val)
            obj = setParameter(obj,'Pvtl',val,0,'real');
        end
        function res = get.Pvtl(obj)
            res = getParameter(obj,'Pvtl');
        end
        function obj = set.Pxn(obj,val)
            obj = setParameter(obj,'Pxn',val,0,'real');
        end
        function res = get.Pxn(obj)
            res = getParameter(obj,'Pxn');
        end
        function obj = set.Saref(obj,val)
            obj = setParameter(obj,'Saref',val,0,'real');
        end
        function res = get.Saref(obj)
            res = getParameter(obj,'Saref');
        end
        function obj = set.Sbref(obj,val)
            obj = setParameter(obj,'Sbref',val,0,'real');
        end
        function res = get.Sbref(obj)
            res = getParameter(obj,'Sbref');
        end
        function obj = set.Wlod(obj,val)
            obj = setParameter(obj,'Wlod',val,0,'real');
        end
        function res = get.Wlod(obj)
            res = getParameter(obj,'Wlod');
        end
        function obj = set.Ku0(obj,val)
            obj = setParameter(obj,'Ku0',val,0,'real');
        end
        function res = get.Ku0(obj)
            res = getParameter(obj,'Ku0');
        end
        function obj = set.Kvsat(obj,val)
            obj = setParameter(obj,'Kvsat',val,0,'real');
        end
        function res = get.Kvsat(obj)
            res = getParameter(obj,'Kvsat');
        end
        function obj = set.Kvth0(obj,val)
            obj = setParameter(obj,'Kvth0',val,0,'real');
        end
        function res = get.Kvth0(obj)
            res = getParameter(obj,'Kvth0');
        end
        function obj = set.Tku0(obj,val)
            obj = setParameter(obj,'Tku0',val,0,'real');
        end
        function res = get.Tku0(obj)
            res = getParameter(obj,'Tku0');
        end
        function obj = set.Llodku0(obj,val)
            obj = setParameter(obj,'Llodku0',val,0,'real');
        end
        function res = get.Llodku0(obj)
            res = getParameter(obj,'Llodku0');
        end
        function obj = set.Wlodku0(obj,val)
            obj = setParameter(obj,'Wlodku0',val,0,'real');
        end
        function res = get.Wlodku0(obj)
            res = getParameter(obj,'Wlodku0');
        end
        function obj = set.Llodvth(obj,val)
            obj = setParameter(obj,'Llodvth',val,0,'real');
        end
        function res = get.Llodvth(obj)
            res = getParameter(obj,'Llodvth');
        end
        function obj = set.Wlodvth(obj,val)
            obj = setParameter(obj,'Wlodvth',val,0,'real');
        end
        function res = get.Wlodvth(obj)
            res = getParameter(obj,'Wlodvth');
        end
        function obj = set.Lku0(obj,val)
            obj = setParameter(obj,'Lku0',val,0,'real');
        end
        function res = get.Lku0(obj)
            res = getParameter(obj,'Lku0');
        end
        function obj = set.Wku0(obj,val)
            obj = setParameter(obj,'Wku0',val,0,'real');
        end
        function res = get.Wku0(obj)
            res = getParameter(obj,'Wku0');
        end
        function obj = set.Pku0(obj,val)
            obj = setParameter(obj,'Pku0',val,0,'real');
        end
        function res = get.Pku0(obj)
            res = getParameter(obj,'Pku0');
        end
        function obj = set.Lkvth0(obj,val)
            obj = setParameter(obj,'Lkvth0',val,0,'real');
        end
        function res = get.Lkvth0(obj)
            res = getParameter(obj,'Lkvth0');
        end
        function obj = set.Wkvth0(obj,val)
            obj = setParameter(obj,'Wkvth0',val,0,'real');
        end
        function res = get.Wkvth0(obj)
            res = getParameter(obj,'Wkvth0');
        end
        function obj = set.Pkvth0(obj,val)
            obj = setParameter(obj,'Pkvth0',val,0,'real');
        end
        function res = get.Pkvth0(obj)
            res = getParameter(obj,'Pkvth0');
        end
        function obj = set.Stk2(obj,val)
            obj = setParameter(obj,'Stk2',val,0,'real');
        end
        function res = get.Stk2(obj)
            res = getParameter(obj,'Stk2');
        end
        function obj = set.Lodk2(obj,val)
            obj = setParameter(obj,'Lodk2',val,0,'real');
        end
        function res = get.Lodk2(obj)
            res = getParameter(obj,'Lodk2');
        end
        function obj = set.Steta0(obj,val)
            obj = setParameter(obj,'Steta0',val,0,'real');
        end
        function res = get.Steta0(obj)
            res = getParameter(obj,'Steta0');
        end
        function obj = set.Lodeta0(obj,val)
            obj = setParameter(obj,'Lodeta0',val,0,'real');
        end
        function res = get.Lodeta0(obj)
            res = getParameter(obj,'Lodeta0');
        end
        function obj = set.Rnoia(obj,val)
            obj = setParameter(obj,'Rnoia',val,0,'real');
        end
        function res = get.Rnoia(obj)
            res = getParameter(obj,'Rnoia');
        end
        function obj = set.Rnoib(obj,val)
            obj = setParameter(obj,'Rnoib',val,0,'real');
        end
        function res = get.Rnoib(obj)
            res = getParameter(obj,'Rnoib');
        end
        function obj = set.Vfbsdoff(obj,val)
            obj = setParameter(obj,'Vfbsdoff',val,0,'real');
        end
        function res = get.Vfbsdoff(obj)
            res = getParameter(obj,'Vfbsdoff');
        end
        function obj = set.Lintnoi(obj,val)
            obj = setParameter(obj,'Lintnoi',val,0,'real');
        end
        function res = get.Lintnoi(obj)
            res = getParameter(obj,'Lintnoi');
        end
        function obj = set.Jtss(obj,val)
            obj = setParameter(obj,'Jtss',val,0,'real');
        end
        function res = get.Jtss(obj)
            res = getParameter(obj,'Jtss');
        end
        function obj = set.Jtsd(obj,val)
            obj = setParameter(obj,'Jtsd',val,0,'real');
        end
        function res = get.Jtsd(obj)
            res = getParameter(obj,'Jtsd');
        end
        function obj = set.Jtssws(obj,val)
            obj = setParameter(obj,'Jtssws',val,0,'real');
        end
        function res = get.Jtssws(obj)
            res = getParameter(obj,'Jtssws');
        end
        function obj = set.Jtsswd(obj,val)
            obj = setParameter(obj,'Jtsswd',val,0,'real');
        end
        function res = get.Jtsswd(obj)
            res = getParameter(obj,'Jtsswd');
        end
        function obj = set.Jtsswgs(obj,val)
            obj = setParameter(obj,'Jtsswgs',val,0,'real');
        end
        function res = get.Jtsswgs(obj)
            res = getParameter(obj,'Jtsswgs');
        end
        function obj = set.Jtsswgd(obj,val)
            obj = setParameter(obj,'Jtsswgd',val,0,'real');
        end
        function res = get.Jtsswgd(obj)
            res = getParameter(obj,'Jtsswgd');
        end
        function obj = set.Njts(obj,val)
            obj = setParameter(obj,'Njts',val,0,'real');
        end
        function res = get.Njts(obj)
            res = getParameter(obj,'Njts');
        end
        function obj = set.Njtssw(obj,val)
            obj = setParameter(obj,'Njtssw',val,0,'real');
        end
        function res = get.Njtssw(obj)
            res = getParameter(obj,'Njtssw');
        end
        function obj = set.Njtsswg(obj,val)
            obj = setParameter(obj,'Njtsswg',val,0,'real');
        end
        function res = get.Njtsswg(obj)
            res = getParameter(obj,'Njtsswg');
        end
        function obj = set.Xtss(obj,val)
            obj = setParameter(obj,'Xtss',val,0,'real');
        end
        function res = get.Xtss(obj)
            res = getParameter(obj,'Xtss');
        end
        function obj = set.Xtsd(obj,val)
            obj = setParameter(obj,'Xtsd',val,0,'real');
        end
        function res = get.Xtsd(obj)
            res = getParameter(obj,'Xtsd');
        end
        function obj = set.Xtssws(obj,val)
            obj = setParameter(obj,'Xtssws',val,0,'real');
        end
        function res = get.Xtssws(obj)
            res = getParameter(obj,'Xtssws');
        end
        function obj = set.Xtsswd(obj,val)
            obj = setParameter(obj,'Xtsswd',val,0,'real');
        end
        function res = get.Xtsswd(obj)
            res = getParameter(obj,'Xtsswd');
        end
        function obj = set.Xtsswgs(obj,val)
            obj = setParameter(obj,'Xtsswgs',val,0,'real');
        end
        function res = get.Xtsswgs(obj)
            res = getParameter(obj,'Xtsswgs');
        end
        function obj = set.Xtsswgd(obj,val)
            obj = setParameter(obj,'Xtsswgd',val,0,'real');
        end
        function res = get.Xtsswgd(obj)
            res = getParameter(obj,'Xtsswgd');
        end
        function obj = set.Tnjts(obj,val)
            obj = setParameter(obj,'Tnjts',val,0,'real');
        end
        function res = get.Tnjts(obj)
            res = getParameter(obj,'Tnjts');
        end
        function obj = set.Tnjtssw(obj,val)
            obj = setParameter(obj,'Tnjtssw',val,0,'real');
        end
        function res = get.Tnjtssw(obj)
            res = getParameter(obj,'Tnjtssw');
        end
        function obj = set.Tnjtsswg(obj,val)
            obj = setParameter(obj,'Tnjtsswg',val,0,'real');
        end
        function res = get.Tnjtsswg(obj)
            res = getParameter(obj,'Tnjtsswg');
        end
        function obj = set.Vtss(obj,val)
            obj = setParameter(obj,'Vtss',val,0,'real');
        end
        function res = get.Vtss(obj)
            res = getParameter(obj,'Vtss');
        end
        function obj = set.Vtsd(obj,val)
            obj = setParameter(obj,'Vtsd',val,0,'real');
        end
        function res = get.Vtsd(obj)
            res = getParameter(obj,'Vtsd');
        end
        function obj = set.Vtssws(obj,val)
            obj = setParameter(obj,'Vtssws',val,0,'real');
        end
        function res = get.Vtssws(obj)
            res = getParameter(obj,'Vtssws');
        end
        function obj = set.Vtsswd(obj,val)
            obj = setParameter(obj,'Vtsswd',val,0,'real');
        end
        function res = get.Vtsswd(obj)
            res = getParameter(obj,'Vtsswd');
        end
        function obj = set.Vtsswgs(obj,val)
            obj = setParameter(obj,'Vtsswgs',val,0,'real');
        end
        function res = get.Vtsswgs(obj)
            res = getParameter(obj,'Vtsswgs');
        end
        function obj = set.Vtsswgd(obj,val)
            obj = setParameter(obj,'Vtsswgd',val,0,'real');
        end
        function res = get.Vtsswgd(obj)
            res = getParameter(obj,'Vtsswgd');
        end
        function obj = set.Lvfbsdoff(obj,val)
            obj = setParameter(obj,'Lvfbsdoff',val,0,'real');
        end
        function res = get.Lvfbsdoff(obj)
            res = getParameter(obj,'Lvfbsdoff');
        end
        function obj = set.Wvfbsdoff(obj,val)
            obj = setParameter(obj,'Wvfbsdoff',val,0,'real');
        end
        function res = get.Wvfbsdoff(obj)
            res = getParameter(obj,'Wvfbsdoff');
        end
        function obj = set.Pvfbsdoff(obj,val)
            obj = setParameter(obj,'Pvfbsdoff',val,0,'real');
        end
        function res = get.Pvfbsdoff(obj)
            res = getParameter(obj,'Pvfbsdoff');
        end
        function obj = set.Ud(obj,val)
            obj = setParameter(obj,'Ud',val,0,'real');
        end
        function res = get.Ud(obj)
            res = getParameter(obj,'Ud');
        end
        function obj = set.Ud1(obj,val)
            obj = setParameter(obj,'Ud1',val,0,'real');
        end
        function res = get.Ud1(obj)
            res = getParameter(obj,'Ud1');
        end
        function obj = set.Up(obj,val)
            obj = setParameter(obj,'Up',val,0,'real');
        end
        function res = get.Up(obj)
            res = getParameter(obj,'Up');
        end
        function obj = set.Lp(obj,val)
            obj = setParameter(obj,'Lp',val,0,'real');
        end
        function res = get.Lp(obj)
            res = getParameter(obj,'Lp');
        end
        function obj = set.Tvfbsdoff(obj,val)
            obj = setParameter(obj,'Tvfbsdoff',val,0,'real');
        end
        function res = get.Tvfbsdoff(obj)
            res = getParameter(obj,'Tvfbsdoff');
        end
        function obj = set.Tvoff(obj,val)
            obj = setParameter(obj,'Tvoff',val,0,'real');
        end
        function res = get.Tvoff(obj)
            res = getParameter(obj,'Tvoff');
        end
        function obj = set.Rbps0(obj,val)
            obj = setParameter(obj,'Rbps0',val,0,'real');
        end
        function res = get.Rbps0(obj)
            res = getParameter(obj,'Rbps0');
        end
        function obj = set.Rbpsl(obj,val)
            obj = setParameter(obj,'Rbpsl',val,0,'real');
        end
        function res = get.Rbpsl(obj)
            res = getParameter(obj,'Rbpsl');
        end
        function obj = set.Rbpsw(obj,val)
            obj = setParameter(obj,'Rbpsw',val,0,'real');
        end
        function res = get.Rbpsw(obj)
            res = getParameter(obj,'Rbpsw');
        end
        function obj = set.Rbpsnf(obj,val)
            obj = setParameter(obj,'Rbpsnf',val,0,'real');
        end
        function res = get.Rbpsnf(obj)
            res = getParameter(obj,'Rbpsnf');
        end
        function obj = set.Rbpd0(obj,val)
            obj = setParameter(obj,'Rbpd0',val,0,'real');
        end
        function res = get.Rbpd0(obj)
            res = getParameter(obj,'Rbpd0');
        end
        function obj = set.Rbpdl(obj,val)
            obj = setParameter(obj,'Rbpdl',val,0,'real');
        end
        function res = get.Rbpdl(obj)
            res = getParameter(obj,'Rbpdl');
        end
        function obj = set.Rbpdw(obj,val)
            obj = setParameter(obj,'Rbpdw',val,0,'real');
        end
        function res = get.Rbpdw(obj)
            res = getParameter(obj,'Rbpdw');
        end
        function obj = set.Rbpdnf(obj,val)
            obj = setParameter(obj,'Rbpdnf',val,0,'real');
        end
        function res = get.Rbpdnf(obj)
            res = getParameter(obj,'Rbpdnf');
        end
        function obj = set.Rbpbx0(obj,val)
            obj = setParameter(obj,'Rbpbx0',val,0,'real');
        end
        function res = get.Rbpbx0(obj)
            res = getParameter(obj,'Rbpbx0');
        end
        function obj = set.Rbpbxl(obj,val)
            obj = setParameter(obj,'Rbpbxl',val,0,'real');
        end
        function res = get.Rbpbxl(obj)
            res = getParameter(obj,'Rbpbxl');
        end
        function obj = set.Rbpbxw(obj,val)
            obj = setParameter(obj,'Rbpbxw',val,0,'real');
        end
        function res = get.Rbpbxw(obj)
            res = getParameter(obj,'Rbpbxw');
        end
        function obj = set.Rbpbxnf(obj,val)
            obj = setParameter(obj,'Rbpbxnf',val,0,'real');
        end
        function res = get.Rbpbxnf(obj)
            res = getParameter(obj,'Rbpbxnf');
        end
        function obj = set.Rbpby0(obj,val)
            obj = setParameter(obj,'Rbpby0',val,0,'real');
        end
        function res = get.Rbpby0(obj)
            res = getParameter(obj,'Rbpby0');
        end
        function obj = set.Rbpbyl(obj,val)
            obj = setParameter(obj,'Rbpbyl',val,0,'real');
        end
        function res = get.Rbpbyl(obj)
            res = getParameter(obj,'Rbpbyl');
        end
        function obj = set.Rbpbyw(obj,val)
            obj = setParameter(obj,'Rbpbyw',val,0,'real');
        end
        function res = get.Rbpbyw(obj)
            res = getParameter(obj,'Rbpbyw');
        end
        function obj = set.Rbpbynf(obj,val)
            obj = setParameter(obj,'Rbpbynf',val,0,'real');
        end
        function res = get.Rbpbynf(obj)
            res = getParameter(obj,'Rbpbynf');
        end
        function obj = set.Rbsbx0(obj,val)
            obj = setParameter(obj,'Rbsbx0',val,0,'real');
        end
        function res = get.Rbsbx0(obj)
            res = getParameter(obj,'Rbsbx0');
        end
        function obj = set.Rbsby0(obj,val)
            obj = setParameter(obj,'Rbsby0',val,0,'real');
        end
        function res = get.Rbsby0(obj)
            res = getParameter(obj,'Rbsby0');
        end
        function obj = set.Rbdbx0(obj,val)
            obj = setParameter(obj,'Rbdbx0',val,0,'real');
        end
        function res = get.Rbdbx0(obj)
            res = getParameter(obj,'Rbdbx0');
        end
        function obj = set.Rbdby0(obj,val)
            obj = setParameter(obj,'Rbdby0',val,0,'real');
        end
        function res = get.Rbdby0(obj)
            res = getParameter(obj,'Rbdby0');
        end
        function obj = set.Rbsdbxl(obj,val)
            obj = setParameter(obj,'Rbsdbxl',val,0,'real');
        end
        function res = get.Rbsdbxl(obj)
            res = getParameter(obj,'Rbsdbxl');
        end
        function obj = set.Rbsdbxw(obj,val)
            obj = setParameter(obj,'Rbsdbxw',val,0,'real');
        end
        function res = get.Rbsdbxw(obj)
            res = getParameter(obj,'Rbsdbxw');
        end
        function obj = set.Rbsdbxnf(obj,val)
            obj = setParameter(obj,'Rbsdbxnf',val,0,'real');
        end
        function res = get.Rbsdbxnf(obj)
            res = getParameter(obj,'Rbsdbxnf');
        end
        function obj = set.Rbsdbyl(obj,val)
            obj = setParameter(obj,'Rbsdbyl',val,0,'real');
        end
        function res = get.Rbsdbyl(obj)
            res = getParameter(obj,'Rbsdbyl');
        end
        function obj = set.Rbsdbyw(obj,val)
            obj = setParameter(obj,'Rbsdbyw',val,0,'real');
        end
        function res = get.Rbsdbyw(obj)
            res = getParameter(obj,'Rbsdbyw');
        end
        function obj = set.Rbsdbynf(obj,val)
            obj = setParameter(obj,'Rbsdbynf',val,0,'real');
        end
        function res = get.Rbsdbynf(obj)
            res = getParameter(obj,'Rbsdbynf');
        end
        function obj = set.Lud(obj,val)
            obj = setParameter(obj,'Lud',val,0,'real');
        end
        function res = get.Lud(obj)
            res = getParameter(obj,'Lud');
        end
        function obj = set.Lud1(obj,val)
            obj = setParameter(obj,'Lud1',val,0,'real');
        end
        function res = get.Lud1(obj)
            res = getParameter(obj,'Lud1');
        end
        function obj = set.Lup(obj,val)
            obj = setParameter(obj,'Lup',val,0,'real');
        end
        function res = get.Lup(obj)
            res = getParameter(obj,'Lup');
        end
        function obj = set.Llp(obj,val)
            obj = setParameter(obj,'Llp',val,0,'real');
        end
        function res = get.Llp(obj)
            res = getParameter(obj,'Llp');
        end
        function obj = set.Ltvfbsdoff(obj,val)
            obj = setParameter(obj,'Ltvfbsdoff',val,0,'real');
        end
        function res = get.Ltvfbsdoff(obj)
            res = getParameter(obj,'Ltvfbsdoff');
        end
        function obj = set.Ltvoff(obj,val)
            obj = setParameter(obj,'Ltvoff',val,0,'real');
        end
        function res = get.Ltvoff(obj)
            res = getParameter(obj,'Ltvoff');
        end
        function obj = set.Wud(obj,val)
            obj = setParameter(obj,'Wud',val,0,'real');
        end
        function res = get.Wud(obj)
            res = getParameter(obj,'Wud');
        end
        function obj = set.Wud1(obj,val)
            obj = setParameter(obj,'Wud1',val,0,'real');
        end
        function res = get.Wud1(obj)
            res = getParameter(obj,'Wud1');
        end
        function obj = set.Wup(obj,val)
            obj = setParameter(obj,'Wup',val,0,'real');
        end
        function res = get.Wup(obj)
            res = getParameter(obj,'Wup');
        end
        function obj = set.Wlp(obj,val)
            obj = setParameter(obj,'Wlp',val,0,'real');
        end
        function res = get.Wlp(obj)
            res = getParameter(obj,'Wlp');
        end
        function obj = set.Wtvfbsdoff(obj,val)
            obj = setParameter(obj,'Wtvfbsdoff',val,0,'real');
        end
        function res = get.Wtvfbsdoff(obj)
            res = getParameter(obj,'Wtvfbsdoff');
        end
        function obj = set.Wtvoff(obj,val)
            obj = setParameter(obj,'Wtvoff',val,0,'real');
        end
        function res = get.Wtvoff(obj)
            res = getParameter(obj,'Wtvoff');
        end
        function obj = set.Pud(obj,val)
            obj = setParameter(obj,'Pud',val,0,'real');
        end
        function res = get.Pud(obj)
            res = getParameter(obj,'Pud');
        end
        function obj = set.Pud1(obj,val)
            obj = setParameter(obj,'Pud1',val,0,'real');
        end
        function res = get.Pud1(obj)
            res = getParameter(obj,'Pud1');
        end
        function obj = set.Pup(obj,val)
            obj = setParameter(obj,'Pup',val,0,'real');
        end
        function res = get.Pup(obj)
            res = getParameter(obj,'Pup');
        end
        function obj = set.Plp(obj,val)
            obj = setParameter(obj,'Plp',val,0,'real');
        end
        function res = get.Plp(obj)
            res = getParameter(obj,'Plp');
        end
        function obj = set.Ptvfbsdoff(obj,val)
            obj = setParameter(obj,'Ptvfbsdoff',val,0,'real');
        end
        function res = get.Ptvfbsdoff(obj)
            res = getParameter(obj,'Ptvfbsdoff');
        end
        function obj = set.Ptvoff(obj,val)
            obj = setParameter(obj,'Ptvoff',val,0,'real');
        end
        function res = get.Ptvoff(obj)
            res = getParameter(obj,'Ptvoff');
        end
        function obj = set.Web(obj,val)
            obj = setParameter(obj,'Web',val,0,'real');
        end
        function res = get.Web(obj)
            res = getParameter(obj,'Web');
        end
        function obj = set.Wec(obj,val)
            obj = setParameter(obj,'Wec',val,0,'real');
        end
        function res = get.Wec(obj)
            res = getParameter(obj,'Wec');
        end
        function obj = set.Kvth0we(obj,val)
            obj = setParameter(obj,'Kvth0we',val,0,'real');
        end
        function res = get.Kvth0we(obj)
            res = getParameter(obj,'Kvth0we');
        end
        function obj = set.K2we(obj,val)
            obj = setParameter(obj,'K2we',val,0,'real');
        end
        function res = get.K2we(obj)
            res = getParameter(obj,'K2we');
        end
        function obj = set.Ku0we(obj,val)
            obj = setParameter(obj,'Ku0we',val,0,'real');
        end
        function res = get.Ku0we(obj)
            res = getParameter(obj,'Ku0we');
        end
        function obj = set.Scref(obj,val)
            obj = setParameter(obj,'Scref',val,0,'real');
        end
        function res = get.Scref(obj)
            res = getParameter(obj,'Scref');
        end
        function obj = set.Wpemod(obj,val)
            obj = setParameter(obj,'Wpemod',val,0,'integer');
        end
        function res = get.Wpemod(obj)
            res = getParameter(obj,'Wpemod');
        end
        function obj = set.Lkvth0we(obj,val)
            obj = setParameter(obj,'Lkvth0we',val,0,'real');
        end
        function res = get.Lkvth0we(obj)
            res = getParameter(obj,'Lkvth0we');
        end
        function obj = set.Lk2we(obj,val)
            obj = setParameter(obj,'Lk2we',val,0,'real');
        end
        function res = get.Lk2we(obj)
            res = getParameter(obj,'Lk2we');
        end
        function obj = set.Lku0we(obj,val)
            obj = setParameter(obj,'Lku0we',val,0,'real');
        end
        function res = get.Lku0we(obj)
            res = getParameter(obj,'Lku0we');
        end
        function obj = set.Wkvth0we(obj,val)
            obj = setParameter(obj,'Wkvth0we',val,0,'real');
        end
        function res = get.Wkvth0we(obj)
            res = getParameter(obj,'Wkvth0we');
        end
        function obj = set.Wk2we(obj,val)
            obj = setParameter(obj,'Wk2we',val,0,'real');
        end
        function res = get.Wk2we(obj)
            res = getParameter(obj,'Wk2we');
        end
        function obj = set.Wku0we(obj,val)
            obj = setParameter(obj,'Wku0we',val,0,'real');
        end
        function res = get.Wku0we(obj)
            res = getParameter(obj,'Wku0we');
        end
        function obj = set.Pkvth0we(obj,val)
            obj = setParameter(obj,'Pkvth0we',val,0,'real');
        end
        function res = get.Pkvth0we(obj)
            res = getParameter(obj,'Pkvth0we');
        end
        function obj = set.Pk2we(obj,val)
            obj = setParameter(obj,'Pk2we',val,0,'real');
        end
        function res = get.Pk2we(obj)
            res = getParameter(obj,'Pk2we');
        end
        function obj = set.Pku0we(obj,val)
            obj = setParameter(obj,'Pku0we',val,0,'real');
        end
        function res = get.Pku0we(obj)
            res = getParameter(obj,'Pku0we');
        end
        function obj = set.Cvchargemod(obj,val)
            obj = setParameter(obj,'Cvchargemod',val,0,'integer');
        end
        function res = get.Cvchargemod(obj)
            res = getParameter(obj,'Cvchargemod');
        end
        function obj = set.Ados(obj,val)
            obj = setParameter(obj,'Ados',val,0,'real');
        end
        function res = get.Ados(obj)
            res = getParameter(obj,'Ados');
        end
        function obj = set.Bdos(obj,val)
            obj = setParameter(obj,'Bdos',val,0,'real');
        end
        function res = get.Bdos(obj)
            res = getParameter(obj,'Bdos');
        end
        function obj = set.Mtrlmod(obj,val)
            obj = setParameter(obj,'Mtrlmod',val,0,'integer');
        end
        function res = get.Mtrlmod(obj)
            res = getParameter(obj,'Mtrlmod');
        end
        function obj = set.Eot(obj,val)
            obj = setParameter(obj,'Eot',val,0,'real');
        end
        function res = get.Eot(obj)
            res = getParameter(obj,'Eot');
        end
        function obj = set.Vddeot(obj,val)
            obj = setParameter(obj,'Vddeot',val,0,'real');
        end
        function res = get.Vddeot(obj)
            res = getParameter(obj,'Vddeot');
        end
        function obj = set.Phig(obj,val)
            obj = setParameter(obj,'Phig',val,0,'real');
        end
        function res = get.Phig(obj)
            res = getParameter(obj,'Phig');
        end
        function obj = set.Epsrgate(obj,val)
            obj = setParameter(obj,'Epsrgate',val,0,'real');
        end
        function res = get.Epsrgate(obj)
            res = getParameter(obj,'Epsrgate');
        end
        function obj = set.Easub(obj,val)
            obj = setParameter(obj,'Easub',val,0,'real');
        end
        function res = get.Easub(obj)
            res = getParameter(obj,'Easub');
        end
        function obj = set.Epsrsub(obj,val)
            obj = setParameter(obj,'Epsrsub',val,0,'real');
        end
        function res = get.Epsrsub(obj)
            res = getParameter(obj,'Epsrsub');
        end
        function obj = set.Ni0sub(obj,val)
            obj = setParameter(obj,'Ni0sub',val,0,'real');
        end
        function res = get.Ni0sub(obj)
            res = getParameter(obj,'Ni0sub');
        end
        function obj = set.Bg0sub(obj,val)
            obj = setParameter(obj,'Bg0sub',val,0,'real');
        end
        function res = get.Bg0sub(obj)
            res = getParameter(obj,'Bg0sub');
        end
        function obj = set.Tbgasub(obj,val)
            obj = setParameter(obj,'Tbgasub',val,0,'real');
        end
        function res = get.Tbgasub(obj)
            res = getParameter(obj,'Tbgasub');
        end
        function obj = set.Tbgbsub(obj,val)
            obj = setParameter(obj,'Tbgbsub',val,0,'real');
        end
        function res = get.Tbgbsub(obj)
            res = getParameter(obj,'Tbgbsub');
        end
        function obj = set.Minvcv(obj,val)
            obj = setParameter(obj,'Minvcv',val,0,'real');
        end
        function res = get.Minvcv(obj)
            res = getParameter(obj,'Minvcv');
        end
        function obj = set.Voffcvl(obj,val)
            obj = setParameter(obj,'Voffcvl',val,0,'real');
        end
        function res = get.Voffcvl(obj)
            res = getParameter(obj,'Voffcvl');
        end
        function obj = set.Lminvcv(obj,val)
            obj = setParameter(obj,'Lminvcv',val,0,'real');
        end
        function res = get.Lminvcv(obj)
            res = getParameter(obj,'Lminvcv');
        end
        function obj = set.Wminvcv(obj,val)
            obj = setParameter(obj,'Wminvcv',val,0,'real');
        end
        function res = get.Wminvcv(obj)
            res = getParameter(obj,'Wminvcv');
        end
        function obj = set.Pminvcv(obj,val)
            obj = setParameter(obj,'Pminvcv',val,0,'real');
        end
        function res = get.Pminvcv(obj)
            res = getParameter(obj,'Pminvcv');
        end
        function obj = set.Agisl(obj,val)
            obj = setParameter(obj,'Agisl',val,0,'real');
        end
        function res = get.Agisl(obj)
            res = getParameter(obj,'Agisl');
        end
        function obj = set.Bgisl(obj,val)
            obj = setParameter(obj,'Bgisl',val,0,'real');
        end
        function res = get.Bgisl(obj)
            res = getParameter(obj,'Bgisl');
        end
        function obj = set.Egisl(obj,val)
            obj = setParameter(obj,'Egisl',val,0,'real');
        end
        function res = get.Egisl(obj)
            res = getParameter(obj,'Egisl');
        end
        function obj = set.Cgisl(obj,val)
            obj = setParameter(obj,'Cgisl',val,0,'real');
        end
        function res = get.Cgisl(obj)
            res = getParameter(obj,'Cgisl');
        end
        function obj = set.Lagisl(obj,val)
            obj = setParameter(obj,'Lagisl',val,0,'real');
        end
        function res = get.Lagisl(obj)
            res = getParameter(obj,'Lagisl');
        end
        function obj = set.Lbgisl(obj,val)
            obj = setParameter(obj,'Lbgisl',val,0,'real');
        end
        function res = get.Lbgisl(obj)
            res = getParameter(obj,'Lbgisl');
        end
        function obj = set.Legisl(obj,val)
            obj = setParameter(obj,'Legisl',val,0,'real');
        end
        function res = get.Legisl(obj)
            res = getParameter(obj,'Legisl');
        end
        function obj = set.Lcgisl(obj,val)
            obj = setParameter(obj,'Lcgisl',val,0,'real');
        end
        function res = get.Lcgisl(obj)
            res = getParameter(obj,'Lcgisl');
        end
        function obj = set.Wagisl(obj,val)
            obj = setParameter(obj,'Wagisl',val,0,'real');
        end
        function res = get.Wagisl(obj)
            res = getParameter(obj,'Wagisl');
        end
        function obj = set.Wbgisl(obj,val)
            obj = setParameter(obj,'Wbgisl',val,0,'real');
        end
        function res = get.Wbgisl(obj)
            res = getParameter(obj,'Wbgisl');
        end
        function obj = set.Wegisl(obj,val)
            obj = setParameter(obj,'Wegisl',val,0,'real');
        end
        function res = get.Wegisl(obj)
            res = getParameter(obj,'Wegisl');
        end
        function obj = set.Wcgisl(obj,val)
            obj = setParameter(obj,'Wcgisl',val,0,'real');
        end
        function res = get.Wcgisl(obj)
            res = getParameter(obj,'Wcgisl');
        end
        function obj = set.Pagisl(obj,val)
            obj = setParameter(obj,'Pagisl',val,0,'real');
        end
        function res = get.Pagisl(obj)
            res = getParameter(obj,'Pagisl');
        end
        function obj = set.Pbgisl(obj,val)
            obj = setParameter(obj,'Pbgisl',val,0,'real');
        end
        function res = get.Pbgisl(obj)
            res = getParameter(obj,'Pbgisl');
        end
        function obj = set.Pegisl(obj,val)
            obj = setParameter(obj,'Pegisl',val,0,'real');
        end
        function res = get.Pegisl(obj)
            res = getParameter(obj,'Pegisl');
        end
        function obj = set.Pcgisl(obj,val)
            obj = setParameter(obj,'Pcgisl',val,0,'real');
        end
        function res = get.Pcgisl(obj)
            res = getParameter(obj,'Pcgisl');
        end
        function obj = set.Aigs(obj,val)
            obj = setParameter(obj,'Aigs',val,0,'real');
        end
        function res = get.Aigs(obj)
            res = getParameter(obj,'Aigs');
        end
        function obj = set.Bigs(obj,val)
            obj = setParameter(obj,'Bigs',val,0,'real');
        end
        function res = get.Bigs(obj)
            res = getParameter(obj,'Bigs');
        end
        function obj = set.Cigs(obj,val)
            obj = setParameter(obj,'Cigs',val,0,'real');
        end
        function res = get.Cigs(obj)
            res = getParameter(obj,'Cigs');
        end
        function obj = set.Laigs(obj,val)
            obj = setParameter(obj,'Laigs',val,0,'real');
        end
        function res = get.Laigs(obj)
            res = getParameter(obj,'Laigs');
        end
        function obj = set.Lbigs(obj,val)
            obj = setParameter(obj,'Lbigs',val,0,'real');
        end
        function res = get.Lbigs(obj)
            res = getParameter(obj,'Lbigs');
        end
        function obj = set.Lcigs(obj,val)
            obj = setParameter(obj,'Lcigs',val,0,'real');
        end
        function res = get.Lcigs(obj)
            res = getParameter(obj,'Lcigs');
        end
        function obj = set.Waigs(obj,val)
            obj = setParameter(obj,'Waigs',val,0,'real');
        end
        function res = get.Waigs(obj)
            res = getParameter(obj,'Waigs');
        end
        function obj = set.Wbigs(obj,val)
            obj = setParameter(obj,'Wbigs',val,0,'real');
        end
        function res = get.Wbigs(obj)
            res = getParameter(obj,'Wbigs');
        end
        function obj = set.Wcigs(obj,val)
            obj = setParameter(obj,'Wcigs',val,0,'real');
        end
        function res = get.Wcigs(obj)
            res = getParameter(obj,'Wcigs');
        end
        function obj = set.Paigs(obj,val)
            obj = setParameter(obj,'Paigs',val,0,'real');
        end
        function res = get.Paigs(obj)
            res = getParameter(obj,'Paigs');
        end
        function obj = set.Pbigs(obj,val)
            obj = setParameter(obj,'Pbigs',val,0,'real');
        end
        function res = get.Pbigs(obj)
            res = getParameter(obj,'Pbigs');
        end
        function obj = set.Pcigs(obj,val)
            obj = setParameter(obj,'Pcigs',val,0,'real');
        end
        function res = get.Pcigs(obj)
            res = getParameter(obj,'Pcigs');
        end
        function obj = set.Aigd(obj,val)
            obj = setParameter(obj,'Aigd',val,0,'real');
        end
        function res = get.Aigd(obj)
            res = getParameter(obj,'Aigd');
        end
        function obj = set.Bigd(obj,val)
            obj = setParameter(obj,'Bigd',val,0,'real');
        end
        function res = get.Bigd(obj)
            res = getParameter(obj,'Bigd');
        end
        function obj = set.Cigd(obj,val)
            obj = setParameter(obj,'Cigd',val,0,'real');
        end
        function res = get.Cigd(obj)
            res = getParameter(obj,'Cigd');
        end
        function obj = set.Laigd(obj,val)
            obj = setParameter(obj,'Laigd',val,0,'real');
        end
        function res = get.Laigd(obj)
            res = getParameter(obj,'Laigd');
        end
        function obj = set.Lbigd(obj,val)
            obj = setParameter(obj,'Lbigd',val,0,'real');
        end
        function res = get.Lbigd(obj)
            res = getParameter(obj,'Lbigd');
        end
        function obj = set.Lcigd(obj,val)
            obj = setParameter(obj,'Lcigd',val,0,'real');
        end
        function res = get.Lcigd(obj)
            res = getParameter(obj,'Lcigd');
        end
        function obj = set.Waigd(obj,val)
            obj = setParameter(obj,'Waigd',val,0,'real');
        end
        function res = get.Waigd(obj)
            res = getParameter(obj,'Waigd');
        end
        function obj = set.Wbigd(obj,val)
            obj = setParameter(obj,'Wbigd',val,0,'real');
        end
        function res = get.Wbigd(obj)
            res = getParameter(obj,'Wbigd');
        end
        function obj = set.Wcigd(obj,val)
            obj = setParameter(obj,'Wcigd',val,0,'real');
        end
        function res = get.Wcigd(obj)
            res = getParameter(obj,'Wcigd');
        end
        function obj = set.Paigd(obj,val)
            obj = setParameter(obj,'Paigd',val,0,'real');
        end
        function res = get.Paigd(obj)
            res = getParameter(obj,'Paigd');
        end
        function obj = set.Pbigd(obj,val)
            obj = setParameter(obj,'Pbigd',val,0,'real');
        end
        function res = get.Pbigd(obj)
            res = getParameter(obj,'Pbigd');
        end
        function obj = set.Pcigd(obj,val)
            obj = setParameter(obj,'Pcigd',val,0,'real');
        end
        function res = get.Pcigd(obj)
            res = getParameter(obj,'Pcigd');
        end
        function obj = set.Dlcigd(obj,val)
            obj = setParameter(obj,'Dlcigd',val,0,'real');
        end
        function res = get.Dlcigd(obj)
            res = getParameter(obj,'Dlcigd');
        end
        function obj = set.Njtsd(obj,val)
            obj = setParameter(obj,'Njtsd',val,0,'real');
        end
        function res = get.Njtsd(obj)
            res = getParameter(obj,'Njtsd');
        end
        function obj = set.Njtsswd(obj,val)
            obj = setParameter(obj,'Njtsswd',val,0,'real');
        end
        function res = get.Njtsswd(obj)
            res = getParameter(obj,'Njtsswd');
        end
        function obj = set.Njtsswgd(obj,val)
            obj = setParameter(obj,'Njtsswgd',val,0,'real');
        end
        function res = get.Njtsswgd(obj)
            res = getParameter(obj,'Njtsswgd');
        end
        function obj = set.Tnjtsd(obj,val)
            obj = setParameter(obj,'Tnjtsd',val,0,'real');
        end
        function res = get.Tnjtsd(obj)
            res = getParameter(obj,'Tnjtsd');
        end
        function obj = set.Tnjtsswd(obj,val)
            obj = setParameter(obj,'Tnjtsswd',val,0,'real');
        end
        function res = get.Tnjtsswd(obj)
            res = getParameter(obj,'Tnjtsswd');
        end
        function obj = set.Tnjtsswgd(obj,val)
            obj = setParameter(obj,'Tnjtsswgd',val,0,'real');
        end
        function res = get.Tnjtsswgd(obj)
            res = getParameter(obj,'Tnjtsswgd');
        end
        function obj = set.Tempeot(obj,val)
            obj = setParameter(obj,'Tempeot',val,0,'real');
        end
        function res = get.Tempeot(obj)
            res = getParameter(obj,'Tempeot');
        end
        function obj = set.Leffeot(obj,val)
            obj = setParameter(obj,'Leffeot',val,0,'real');
        end
        function res = get.Leffeot(obj)
            res = getParameter(obj,'Leffeot');
        end
        function obj = set.Weffeot(obj,val)
            obj = setParameter(obj,'Weffeot',val,0,'real');
        end
        function res = get.Weffeot(obj)
            res = getParameter(obj,'Weffeot');
        end
        function obj = set.Ucs(obj,val)
            obj = setParameter(obj,'Ucs',val,0,'real');
        end
        function res = get.Ucs(obj)
            res = getParameter(obj,'Ucs');
        end
        function obj = set.Ucste(obj,val)
            obj = setParameter(obj,'Ucste',val,0,'real');
        end
        function res = get.Ucste(obj)
            res = getParameter(obj,'Ucste');
        end
        function obj = set.Jtweff(obj,val)
            obj = setParameter(obj,'Jtweff',val,0,'real');
        end
        function res = get.Jtweff(obj)
            res = getParameter(obj,'Jtweff');
        end
        function obj = set.Lucste(obj,val)
            obj = setParameter(obj,'Lucste',val,0,'real');
        end
        function res = get.Lucste(obj)
            res = getParameter(obj,'Lucste');
        end
        function obj = set.Lucs(obj,val)
            obj = setParameter(obj,'Lucs',val,0,'real');
        end
        function res = get.Lucs(obj)
            res = getParameter(obj,'Lucs');
        end
        function obj = set.Wucste(obj,val)
            obj = setParameter(obj,'Wucste',val,0,'real');
        end
        function res = get.Wucste(obj)
            res = getParameter(obj,'Wucste');
        end
        function obj = set.Wucs(obj,val)
            obj = setParameter(obj,'Wucs',val,0,'real');
        end
        function res = get.Wucs(obj)
            res = getParameter(obj,'Wucs');
        end
        function obj = set.Pucste(obj,val)
            obj = setParameter(obj,'Pucste',val,0,'real');
        end
        function res = get.Pucste(obj)
            res = getParameter(obj,'Pucste');
        end
        function obj = set.Pucs(obj,val)
            obj = setParameter(obj,'Pucs',val,0,'real');
        end
        function res = get.Pucs(obj)
            res = getParameter(obj,'Pucs');
        end
        function obj = set.Mtrlcompatmod(obj,val)
            obj = setParameter(obj,'Mtrlcompatmod',val,0,'integer');
        end
        function res = get.Mtrlcompatmod(obj)
            res = getParameter(obj,'Mtrlcompatmod');
        end
        function obj = set.Gidlmod(obj,val)
            obj = setParameter(obj,'Gidlmod',val,0,'integer');
        end
        function res = get.Gidlmod(obj)
            res = getParameter(obj,'Gidlmod');
        end
        function obj = set.Dvtp2(obj,val)
            obj = setParameter(obj,'Dvtp2',val,0,'real');
        end
        function res = get.Dvtp2(obj)
            res = getParameter(obj,'Dvtp2');
        end
        function obj = set.Dvtp3(obj,val)
            obj = setParameter(obj,'Dvtp3',val,0,'real');
        end
        function res = get.Dvtp3(obj)
            res = getParameter(obj,'Dvtp3');
        end
        function obj = set.Dvtp4(obj,val)
            obj = setParameter(obj,'Dvtp4',val,0,'real');
        end
        function res = get.Dvtp4(obj)
            res = getParameter(obj,'Dvtp4');
        end
        function obj = set.Dvtp5(obj,val)
            obj = setParameter(obj,'Dvtp5',val,0,'real');
        end
        function res = get.Dvtp5(obj)
            res = getParameter(obj,'Dvtp5');
        end
        function obj = set.Tnfactor(obj,val)
            obj = setParameter(obj,'Tnfactor',val,0,'real');
        end
        function res = get.Tnfactor(obj)
            res = getParameter(obj,'Tnfactor');
        end
        function obj = set.Teta0(obj,val)
            obj = setParameter(obj,'Teta0',val,0,'real');
        end
        function res = get.Teta0(obj)
            res = getParameter(obj,'Teta0');
        end
        function obj = set.Tvoffcv(obj,val)
            obj = setParameter(obj,'Tvoffcv',val,0,'real');
        end
        function res = get.Tvoffcv(obj)
            res = getParameter(obj,'Tvoffcv');
        end
        function obj = set.Fgidl(obj,val)
            obj = setParameter(obj,'Fgidl',val,0,'real');
        end
        function res = get.Fgidl(obj)
            res = getParameter(obj,'Fgidl');
        end
        function obj = set.Rgidl(obj,val)
            obj = setParameter(obj,'Rgidl',val,0,'real');
        end
        function res = get.Rgidl(obj)
            res = getParameter(obj,'Rgidl');
        end
        function obj = set.Kgidl(obj,val)
            obj = setParameter(obj,'Kgidl',val,0,'real');
        end
        function res = get.Kgidl(obj)
            res = getParameter(obj,'Kgidl');
        end
        function obj = set.Fgisl(obj,val)
            obj = setParameter(obj,'Fgisl',val,0,'real');
        end
        function res = get.Fgisl(obj)
            res = getParameter(obj,'Fgisl');
        end
        function obj = set.Rgisl(obj,val)
            obj = setParameter(obj,'Rgisl',val,0,'real');
        end
        function res = get.Rgisl(obj)
            res = getParameter(obj,'Rgisl');
        end
        function obj = set.Kgisl(obj,val)
            obj = setParameter(obj,'Kgisl',val,0,'real');
        end
        function res = get.Kgisl(obj)
            res = getParameter(obj,'Kgisl');
        end
        function obj = set.Tnoic(obj,val)
            obj = setParameter(obj,'Tnoic',val,0,'real');
        end
        function res = get.Tnoic(obj)
            res = getParameter(obj,'Tnoic');
        end
        function obj = set.Rnoic(obj,val)
            obj = setParameter(obj,'Rnoic',val,0,'real');
        end
        function res = get.Rnoic(obj)
            res = getParameter(obj,'Rnoic');
        end
        function obj = set.Ldvtp2(obj,val)
            obj = setParameter(obj,'Ldvtp2',val,0,'real');
        end
        function res = get.Ldvtp2(obj)
            res = getParameter(obj,'Ldvtp2');
        end
        function obj = set.Ldvtp3(obj,val)
            obj = setParameter(obj,'Ldvtp3',val,0,'real');
        end
        function res = get.Ldvtp3(obj)
            res = getParameter(obj,'Ldvtp3');
        end
        function obj = set.Ldvtp4(obj,val)
            obj = setParameter(obj,'Ldvtp4',val,0,'real');
        end
        function res = get.Ldvtp4(obj)
            res = getParameter(obj,'Ldvtp4');
        end
        function obj = set.Ldvtp5(obj,val)
            obj = setParameter(obj,'Ldvtp5',val,0,'real');
        end
        function res = get.Ldvtp5(obj)
            res = getParameter(obj,'Ldvtp5');
        end
        function obj = set.Wdvtp2(obj,val)
            obj = setParameter(obj,'Wdvtp2',val,0,'real');
        end
        function res = get.Wdvtp2(obj)
            res = getParameter(obj,'Wdvtp2');
        end
        function obj = set.Wdvtp3(obj,val)
            obj = setParameter(obj,'Wdvtp3',val,0,'real');
        end
        function res = get.Wdvtp3(obj)
            res = getParameter(obj,'Wdvtp3');
        end
        function obj = set.Wdvtp4(obj,val)
            obj = setParameter(obj,'Wdvtp4',val,0,'real');
        end
        function res = get.Wdvtp4(obj)
            res = getParameter(obj,'Wdvtp4');
        end
        function obj = set.Wdvtp5(obj,val)
            obj = setParameter(obj,'Wdvtp5',val,0,'real');
        end
        function res = get.Wdvtp5(obj)
            res = getParameter(obj,'Wdvtp5');
        end
        function obj = set.Pdvtp2(obj,val)
            obj = setParameter(obj,'Pdvtp2',val,0,'real');
        end
        function res = get.Pdvtp2(obj)
            res = getParameter(obj,'Pdvtp2');
        end
        function obj = set.Pdvtp3(obj,val)
            obj = setParameter(obj,'Pdvtp3',val,0,'real');
        end
        function res = get.Pdvtp3(obj)
            res = getParameter(obj,'Pdvtp3');
        end
        function obj = set.Pdvtp4(obj,val)
            obj = setParameter(obj,'Pdvtp4',val,0,'real');
        end
        function res = get.Pdvtp4(obj)
            res = getParameter(obj,'Pdvtp4');
        end
        function obj = set.Pdvtp5(obj,val)
            obj = setParameter(obj,'Pdvtp5',val,0,'real');
        end
        function res = get.Pdvtp5(obj)
            res = getParameter(obj,'Pdvtp5');
        end
        function obj = set.Ltnfactor(obj,val)
            obj = setParameter(obj,'Ltnfactor',val,0,'real');
        end
        function res = get.Ltnfactor(obj)
            res = getParameter(obj,'Ltnfactor');
        end
        function obj = set.Wtnfactor(obj,val)
            obj = setParameter(obj,'Wtnfactor',val,0,'real');
        end
        function res = get.Wtnfactor(obj)
            res = getParameter(obj,'Wtnfactor');
        end
        function obj = set.Ptnfactor(obj,val)
            obj = setParameter(obj,'Ptnfactor',val,0,'real');
        end
        function res = get.Ptnfactor(obj)
            res = getParameter(obj,'Ptnfactor');
        end
        function obj = set.Lteta0(obj,val)
            obj = setParameter(obj,'Lteta0',val,0,'real');
        end
        function res = get.Lteta0(obj)
            res = getParameter(obj,'Lteta0');
        end
        function obj = set.Wteta0(obj,val)
            obj = setParameter(obj,'Wteta0',val,0,'real');
        end
        function res = get.Wteta0(obj)
            res = getParameter(obj,'Wteta0');
        end
        function obj = set.Pteta0(obj,val)
            obj = setParameter(obj,'Pteta0',val,0,'real');
        end
        function res = get.Pteta0(obj)
            res = getParameter(obj,'Pteta0');
        end
        function obj = set.Ltvoffcv(obj,val)
            obj = setParameter(obj,'Ltvoffcv',val,0,'real');
        end
        function res = get.Ltvoffcv(obj)
            res = getParameter(obj,'Ltvoffcv');
        end
        function obj = set.Wtvoffcv(obj,val)
            obj = setParameter(obj,'Wtvoffcv',val,0,'real');
        end
        function res = get.Wtvoffcv(obj)
            res = getParameter(obj,'Wtvoffcv');
        end
        function obj = set.Ptvoffcv(obj,val)
            obj = setParameter(obj,'Ptvoffcv',val,0,'real');
        end
        function res = get.Ptvoffcv(obj)
            res = getParameter(obj,'Ptvoffcv');
        end
        function obj = set.Lfgidl(obj,val)
            obj = setParameter(obj,'Lfgidl',val,0,'real');
        end
        function res = get.Lfgidl(obj)
            res = getParameter(obj,'Lfgidl');
        end
        function obj = set.Wfgidl(obj,val)
            obj = setParameter(obj,'Wfgidl',val,0,'real');
        end
        function res = get.Wfgidl(obj)
            res = getParameter(obj,'Wfgidl');
        end
        function obj = set.Pfgidl(obj,val)
            obj = setParameter(obj,'Pfgidl',val,0,'real');
        end
        function res = get.Pfgidl(obj)
            res = getParameter(obj,'Pfgidl');
        end
        function obj = set.Lrgidl(obj,val)
            obj = setParameter(obj,'Lrgidl',val,0,'real');
        end
        function res = get.Lrgidl(obj)
            res = getParameter(obj,'Lrgidl');
        end
        function obj = set.Wrgidl(obj,val)
            obj = setParameter(obj,'Wrgidl',val,0,'real');
        end
        function res = get.Wrgidl(obj)
            res = getParameter(obj,'Wrgidl');
        end
        function obj = set.Prgidl(obj,val)
            obj = setParameter(obj,'Prgidl',val,0,'real');
        end
        function res = get.Prgidl(obj)
            res = getParameter(obj,'Prgidl');
        end
        function obj = set.Lkgidl(obj,val)
            obj = setParameter(obj,'Lkgidl',val,0,'real');
        end
        function res = get.Lkgidl(obj)
            res = getParameter(obj,'Lkgidl');
        end
        function obj = set.Wkgidl(obj,val)
            obj = setParameter(obj,'Wkgidl',val,0,'real');
        end
        function res = get.Wkgidl(obj)
            res = getParameter(obj,'Wkgidl');
        end
        function obj = set.Pkgidl(obj,val)
            obj = setParameter(obj,'Pkgidl',val,0,'real');
        end
        function res = get.Pkgidl(obj)
            res = getParameter(obj,'Pkgidl');
        end
        function obj = set.Lfgisl(obj,val)
            obj = setParameter(obj,'Lfgisl',val,0,'real');
        end
        function res = get.Lfgisl(obj)
            res = getParameter(obj,'Lfgisl');
        end
        function obj = set.Wfgisl(obj,val)
            obj = setParameter(obj,'Wfgisl',val,0,'real');
        end
        function res = get.Wfgisl(obj)
            res = getParameter(obj,'Wfgisl');
        end
        function obj = set.Pfgisl(obj,val)
            obj = setParameter(obj,'Pfgisl',val,0,'real');
        end
        function res = get.Pfgisl(obj)
            res = getParameter(obj,'Pfgisl');
        end
        function obj = set.Lrgisl(obj,val)
            obj = setParameter(obj,'Lrgisl',val,0,'real');
        end
        function res = get.Lrgisl(obj)
            res = getParameter(obj,'Lrgisl');
        end
        function obj = set.Wrgisl(obj,val)
            obj = setParameter(obj,'Wrgisl',val,0,'real');
        end
        function res = get.Wrgisl(obj)
            res = getParameter(obj,'Wrgisl');
        end
        function obj = set.Prgisl(obj,val)
            obj = setParameter(obj,'Prgisl',val,0,'real');
        end
        function res = get.Prgisl(obj)
            res = getParameter(obj,'Prgisl');
        end
        function obj = set.Lkgisl(obj,val)
            obj = setParameter(obj,'Lkgisl',val,0,'real');
        end
        function res = get.Lkgisl(obj)
            res = getParameter(obj,'Lkgisl');
        end
        function obj = set.Wkgisl(obj,val)
            obj = setParameter(obj,'Wkgisl',val,0,'real');
        end
        function res = get.Wkgisl(obj)
            res = getParameter(obj,'Wkgisl');
        end
        function obj = set.Pkgisl(obj,val)
            obj = setParameter(obj,'Pkgisl',val,0,'real');
        end
        function res = get.Pkgisl(obj)
            res = getParameter(obj,'Pkgisl');
        end
        function obj = set.Updatelevel(obj,val)
            obj = setParameter(obj,'Updatelevel',val,0,'integer');
        end
        function res = get.Updatelevel(obj)
            res = getParameter(obj,'Updatelevel');
        end
        function obj = set.Wmlt(obj,val)
            obj = setParameter(obj,'Wmlt',val,0,'real');
        end
        function res = get.Wmlt(obj)
            res = getParameter(obj,'Wmlt');
        end
        function obj = set.Lmlt(obj,val)
            obj = setParameter(obj,'Lmlt',val,0,'real');
        end
        function res = get.Lmlt(obj)
            res = getParameter(obj,'Lmlt');
        end
        function obj = set.Vgs_max(obj,val)
            obj = setParameter(obj,'Vgs_max',val,0,'real');
        end
        function res = get.Vgs_max(obj)
            res = getParameter(obj,'Vgs_max');
        end
        function obj = set.Vgd_max(obj,val)
            obj = setParameter(obj,'Vgd_max',val,0,'real');
        end
        function res = get.Vgd_max(obj)
            res = getParameter(obj,'Vgd_max');
        end
        function obj = set.Vds_max(obj,val)
            obj = setParameter(obj,'Vds_max',val,0,'real');
        end
        function res = get.Vds_max(obj)
            res = getParameter(obj,'Vds_max');
        end
        function obj = set.Vbd_max(obj,val)
            obj = setParameter(obj,'Vbd_max',val,0,'real');
        end
        function res = get.Vbd_max(obj)
            res = getParameter(obj,'Vbd_max');
        end
        function obj = set.Vbs_max(obj,val)
            obj = setParameter(obj,'Vbs_max',val,0,'real');
        end
        function res = get.Vbs_max(obj)
            res = getParameter(obj,'Vbs_max');
        end
        function obj = set.Minr(obj,val)
            obj = setParameter(obj,'Minr',val,0,'real');
        end
        function res = get.Minr(obj)
            res = getParameter(obj,'Minr');
        end
        function obj = set.Rdc(obj,val)
            obj = setParameter(obj,'Rdc',val,0,'real');
        end
        function res = get.Rdc(obj)
            res = getParameter(obj,'Rdc');
        end
        function obj = set.Rsc(obj,val)
            obj = setParameter(obj,'Rsc',val,0,'real');
        end
        function res = get.Rsc(obj)
            res = getParameter(obj,'Rsc');
        end
    end
end

classdef ADS_PC_OpenStub < ADScomponent
    % ADS_PC_OpenStub matlab representation for the ADS PC_OpenStub component
    % User-defined model
    % PC_OpenStub [:Name] n1
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % User device parameter (Smorr) Unit: unknown
        W
        % User device parameter (Smorr) Unit: unknown
        Length
        % User device parameter (Sm-ri) Unit: unknown
        Layer
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'integer');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

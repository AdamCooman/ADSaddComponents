classdef ADS_Mixer < ADScomponent
    % ADS_Mixer matlab representation for the ADS Mixer component
    % System simulator mixer
    % Mixer [:Name] rf if lo
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Mixer sideband: -1 = LSB, 0 = DSB, 1 = USB (s--ri) 
        SideBand
        % Conversion Gain term (smorr) 
        ConvGain
        % Input or output power in dBm at `GainComp' dB gain compression (smorr) Unit: dBm
        GainCompPower
        % Conversion gain compression at `GainCompPower' (smorr) 
        GainComp
        % Input or output second order intercept point in dBm (smorr) Unit: dBm
        SOI
        % Input or output third order intercept point in dBm (smorr) Unit: dBm
        TOI
        % Power Saturation Point in dBm (smorr) Unit: dBm
        Psat
        % Gain compression at `Psat' in dB (smorr) Unit: dB
        GainCompSat
        % Forward Reflection Coefficient (smorc) 
        S11
        % Reverse Reflection Coefficient (smorc) 
        S22
        % Input to Output Port Rejection in dB (smorc) 
        RF_Rej
        % LO reflection Coefficient (smorc) 
        S33
        % LO to Output Port Rejection in dB (smorc) 
        LO_Rej2
        % LO to Input Port Rejection in dB (smorc) 
        LO_Rej1
        % Minimum LO power before power starvation (dBm) (smorr) Unit: dBm
        PminLO
        % Refer intercept, am to pm, and compession to input (s--ri) 
        ReferToInput
        % Image Rejection in dB at Output Port (smorr) Unit: dB
        ImageRej
        % Reference Impedance for all S-parameters (smorc) Unit: Ohms
        Zref
        % Noise Figure in dB (smorr) 
        NF
        % Reference frequency for gain compression (smorr) Unit: Hz
        GainCompFreq
        % LO noise to IF noise conversion gain (smorr) Unit: dB
        LOtoIF_ConvGain
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
        % Nonlinear Gain Compression Type: 0=polynomial, 1=file based (s---i) 
        GainCompType
        % Nonlinear Gain Compression File (s---d) 
        GainCompFile
        % IMT order for input port (sm-ri) 
        IMTm
        % IMT order for LO port (sm-ri) 
        IMTn
        % IMT value type (sm-ri) 
        IMTvalueType
        % IMT File (s---d) 
        IMT_File
        % Minimum Noise Figure at Sopt in dB (smorr) 
        NFmin
        % Equivalent Noise Resistance (smorr) Unit: Ohms
        Rn
        % Optimum Source Reflection for Minimum Noise Figure (smorc) 
        Sopt
        % Reference Impedance for Port1 (smorc) Unit: Ohms
        Z1
        % Reference Impedance for Port2 (smorc) Unit: Ohms
        Z2
        % Reference Impedance for Port3 (smorc) Unit: Ohms
        Z3
    end
    methods
        function obj = set.SideBand(obj,val)
            obj = setParameter(obj,'SideBand',val,0,'integer');
        end
        function res = get.SideBand(obj)
            res = getParameter(obj,'SideBand');
        end
        function obj = set.ConvGain(obj,val)
            obj = setParameter(obj,'ConvGain',val,0,'real');
        end
        function res = get.ConvGain(obj)
            res = getParameter(obj,'ConvGain');
        end
        function obj = set.GainCompPower(obj,val)
            obj = setParameter(obj,'GainCompPower',val,0,'real');
        end
        function res = get.GainCompPower(obj)
            res = getParameter(obj,'GainCompPower');
        end
        function obj = set.GainComp(obj,val)
            obj = setParameter(obj,'GainComp',val,0,'real');
        end
        function res = get.GainComp(obj)
            res = getParameter(obj,'GainComp');
        end
        function obj = set.SOI(obj,val)
            obj = setParameter(obj,'SOI',val,0,'real');
        end
        function res = get.SOI(obj)
            res = getParameter(obj,'SOI');
        end
        function obj = set.TOI(obj,val)
            obj = setParameter(obj,'TOI',val,0,'real');
        end
        function res = get.TOI(obj)
            res = getParameter(obj,'TOI');
        end
        function obj = set.Psat(obj,val)
            obj = setParameter(obj,'Psat',val,0,'real');
        end
        function res = get.Psat(obj)
            res = getParameter(obj,'Psat');
        end
        function obj = set.GainCompSat(obj,val)
            obj = setParameter(obj,'GainCompSat',val,0,'real');
        end
        function res = get.GainCompSat(obj)
            res = getParameter(obj,'GainCompSat');
        end
        function obj = set.S11(obj,val)
            obj = setParameter(obj,'S11',val,0,'complex');
        end
        function res = get.S11(obj)
            res = getParameter(obj,'S11');
        end
        function obj = set.S22(obj,val)
            obj = setParameter(obj,'S22',val,0,'complex');
        end
        function res = get.S22(obj)
            res = getParameter(obj,'S22');
        end
        function obj = set.RF_Rej(obj,val)
            obj = setParameter(obj,'RF_Rej',val,0,'complex');
        end
        function res = get.RF_Rej(obj)
            res = getParameter(obj,'RF_Rej');
        end
        function obj = set.S33(obj,val)
            obj = setParameter(obj,'S33',val,0,'complex');
        end
        function res = get.S33(obj)
            res = getParameter(obj,'S33');
        end
        function obj = set.LO_Rej2(obj,val)
            obj = setParameter(obj,'LO_Rej2',val,0,'complex');
        end
        function res = get.LO_Rej2(obj)
            res = getParameter(obj,'LO_Rej2');
        end
        function obj = set.LO_Rej1(obj,val)
            obj = setParameter(obj,'LO_Rej1',val,0,'complex');
        end
        function res = get.LO_Rej1(obj)
            res = getParameter(obj,'LO_Rej1');
        end
        function obj = set.PminLO(obj,val)
            obj = setParameter(obj,'PminLO',val,0,'real');
        end
        function res = get.PminLO(obj)
            res = getParameter(obj,'PminLO');
        end
        function obj = set.ReferToInput(obj,val)
            obj = setParameter(obj,'ReferToInput',val,0,'integer');
        end
        function res = get.ReferToInput(obj)
            res = getParameter(obj,'ReferToInput');
        end
        function obj = set.ImageRej(obj,val)
            obj = setParameter(obj,'ImageRej',val,0,'real');
        end
        function res = get.ImageRej(obj)
            res = getParameter(obj,'ImageRej');
        end
        function obj = set.Zref(obj,val)
            obj = setParameter(obj,'Zref',val,0,'complex');
        end
        function res = get.Zref(obj)
            res = getParameter(obj,'Zref');
        end
        function obj = set.NF(obj,val)
            obj = setParameter(obj,'NF',val,0,'real');
        end
        function res = get.NF(obj)
            res = getParameter(obj,'NF');
        end
        function obj = set.GainCompFreq(obj,val)
            obj = setParameter(obj,'GainCompFreq',val,0,'real');
        end
        function res = get.GainCompFreq(obj)
            res = getParameter(obj,'GainCompFreq');
        end
        function obj = set.LOtoIF_ConvGain(obj,val)
            obj = setParameter(obj,'LOtoIF_ConvGain',val,0,'real');
        end
        function res = get.LOtoIF_ConvGain(obj)
            res = getParameter(obj,'LOtoIF_ConvGain');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
        function obj = set.GainCompType(obj,val)
            obj = setParameter(obj,'GainCompType',val,0,'integer');
        end
        function res = get.GainCompType(obj)
            res = getParameter(obj,'GainCompType');
        end
        function obj = set.GainCompFile(obj,val)
            obj = setParameter(obj,'GainCompFile',val,0,'string');
        end
        function res = get.GainCompFile(obj)
            res = getParameter(obj,'GainCompFile');
        end
        function obj = set.IMTm(obj,val)
            obj = setParameter(obj,'IMTm',val,0,'integer');
        end
        function res = get.IMTm(obj)
            res = getParameter(obj,'IMTm');
        end
        function obj = set.IMTn(obj,val)
            obj = setParameter(obj,'IMTn',val,0,'integer');
        end
        function res = get.IMTn(obj)
            res = getParameter(obj,'IMTn');
        end
        function obj = set.IMTvalueType(obj,val)
            obj = setParameter(obj,'IMTvalueType',val,0,'integer');
        end
        function res = get.IMTvalueType(obj)
            res = getParameter(obj,'IMTvalueType');
        end
        function obj = set.IMT_File(obj,val)
            obj = setParameter(obj,'IMT_File',val,0,'string');
        end
        function res = get.IMT_File(obj)
            res = getParameter(obj,'IMT_File');
        end
        function obj = set.NFmin(obj,val)
            obj = setParameter(obj,'NFmin',val,0,'real');
        end
        function res = get.NFmin(obj)
            res = getParameter(obj,'NFmin');
        end
        function obj = set.Rn(obj,val)
            obj = setParameter(obj,'Rn',val,0,'real');
        end
        function res = get.Rn(obj)
            res = getParameter(obj,'Rn');
        end
        function obj = set.Sopt(obj,val)
            obj = setParameter(obj,'Sopt',val,0,'complex');
        end
        function res = get.Sopt(obj)
            res = getParameter(obj,'Sopt');
        end
        function obj = set.Z1(obj,val)
            obj = setParameter(obj,'Z1',val,0,'complex');
        end
        function res = get.Z1(obj)
            res = getParameter(obj,'Z1');
        end
        function obj = set.Z2(obj,val)
            obj = setParameter(obj,'Z2',val,0,'complex');
        end
        function res = get.Z2(obj)
            res = getParameter(obj,'Z2');
        end
        function obj = set.Z3(obj,val)
            obj = setParameter(obj,'Z3',val,0,'complex');
        end
        function res = get.Z3(obj)
            res = getParameter(obj,'Z3');
        end
    end
end

classdef ADS_PowerGroundPlane < ADScomponent
    % ADS_PowerGroundPlane matlab representation for the ADS PowerGroundPlane component
    % Power Supply and Ground Plane Bouncing Model
    % PowerGroundPlane [:Name] n1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Power supply plane Layer Number (Sm-ri) Unit: 1
        PowerLayer
        % Ground plane Layer Number (Sm-ri) Unit: 1
        GroundLayer
        % Number of polygon vertices (Sm-ri) Unit: 1
        Nv
        % Number of Power Supply Plane S parameter ports (Sm-ri) Unit: 1
        Nps
        % Number of Ground Plane S parameter ports (Sm-ri) Unit: 1
        Ngs
        % Number of array, maximum between nv, nps, and ngs (S--ri) Unit: 1
        Nt
        % X_coordinates of polygon vertices (Smorr) Unit: m
        Xv
        % Y_coordinates of polygon vertices (Smorr) Unit: m
        Yv
        % X_coordinates of Power Supply Plane S parameter ports (Smorr) Unit: m
        Xps
        % Y_coordinates of Power Supply Plane S parameter ports (Smorr) Unit: m
        Yps
        % X_coordinates of Ground Plane S parameter ports (Smorr) Unit: m
        Xgs
        % Y_coordinates of Ground Plane S parameter ports (Smorr) Unit: m
        Ygs
        % Height of Upper Ground Plane---optional (Smorr) Unit: m
        HeightUpperGround
        % Height of Bottom Ground Plane---optional (Smorr) Unit: m
        HeightLowerGround
        % Estimated Clock Frequency of high speed digital signal (Smorr) Unit: Hz
        ClockFreq
        % Estimated Rise Time of high speed digital signal (Smorr) Unit: sec
        RiseTime
        % Length of TLM cell---optional (Smorr) Unit: m
        CellLength
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.PowerLayer(obj,val)
            obj = setParameter(obj,'PowerLayer',val,0,'integer');
        end
        function res = get.PowerLayer(obj)
            res = getParameter(obj,'PowerLayer');
        end
        function obj = set.GroundLayer(obj,val)
            obj = setParameter(obj,'GroundLayer',val,0,'integer');
        end
        function res = get.GroundLayer(obj)
            res = getParameter(obj,'GroundLayer');
        end
        function obj = set.Nv(obj,val)
            obj = setParameter(obj,'Nv',val,0,'integer');
        end
        function res = get.Nv(obj)
            res = getParameter(obj,'Nv');
        end
        function obj = set.Nps(obj,val)
            obj = setParameter(obj,'Nps',val,0,'integer');
        end
        function res = get.Nps(obj)
            res = getParameter(obj,'Nps');
        end
        function obj = set.Ngs(obj,val)
            obj = setParameter(obj,'Ngs',val,0,'integer');
        end
        function res = get.Ngs(obj)
            res = getParameter(obj,'Ngs');
        end
        function obj = set.Nt(obj,val)
            obj = setParameter(obj,'Nt',val,0,'integer');
        end
        function res = get.Nt(obj)
            res = getParameter(obj,'Nt');
        end
        function obj = set.Xv(obj,val)
            obj = setParameter(obj,'Xv',val,0,'real');
        end
        function res = get.Xv(obj)
            res = getParameter(obj,'Xv');
        end
        function obj = set.Yv(obj,val)
            obj = setParameter(obj,'Yv',val,0,'real');
        end
        function res = get.Yv(obj)
            res = getParameter(obj,'Yv');
        end
        function obj = set.Xps(obj,val)
            obj = setParameter(obj,'Xps',val,0,'real');
        end
        function res = get.Xps(obj)
            res = getParameter(obj,'Xps');
        end
        function obj = set.Yps(obj,val)
            obj = setParameter(obj,'Yps',val,0,'real');
        end
        function res = get.Yps(obj)
            res = getParameter(obj,'Yps');
        end
        function obj = set.Xgs(obj,val)
            obj = setParameter(obj,'Xgs',val,0,'real');
        end
        function res = get.Xgs(obj)
            res = getParameter(obj,'Xgs');
        end
        function obj = set.Ygs(obj,val)
            obj = setParameter(obj,'Ygs',val,0,'real');
        end
        function res = get.Ygs(obj)
            res = getParameter(obj,'Ygs');
        end
        function obj = set.HeightUpperGround(obj,val)
            obj = setParameter(obj,'HeightUpperGround',val,0,'real');
        end
        function res = get.HeightUpperGround(obj)
            res = getParameter(obj,'HeightUpperGround');
        end
        function obj = set.HeightLowerGround(obj,val)
            obj = setParameter(obj,'HeightLowerGround',val,0,'real');
        end
        function res = get.HeightLowerGround(obj)
            res = getParameter(obj,'HeightLowerGround');
        end
        function obj = set.ClockFreq(obj,val)
            obj = setParameter(obj,'ClockFreq',val,0,'real');
        end
        function res = get.ClockFreq(obj)
            res = getParameter(obj,'ClockFreq');
        end
        function obj = set.RiseTime(obj,val)
            obj = setParameter(obj,'RiseTime',val,0,'real');
        end
        function res = get.RiseTime(obj)
            res = getParameter(obj,'RiseTime');
        end
        function obj = set.CellLength(obj,val)
            obj = setParameter(obj,'CellLength',val,0,'real');
        end
        function res = get.CellLength(obj)
            res = getParameter(obj,'CellLength');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

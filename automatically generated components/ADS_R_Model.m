classdef ADS_R_Model < ADScomponent
    % ADS_R_Model matlab representation for the ADS R_Model component
    % Linear Two Terminal Resistor with Model
    % ModelName [:Name] p n
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Resistance (smorr) Unit: Ohms
        R
        % Temperature of resistor in degrees Celsius (smorr) Unit: deg C
        Temp
        % Temperature rise over ambient (smorr) Unit: deg C
        Trise
        % Nominal temperature of resistor in degrees Celsius (smorr) Unit: deg C
        Tnom
        % Temperature coefficient; per degree Celsius (smorr) Unit: 1/deg C
        TC1
        % Temperature coefficient; per degree Celsius squared (smorr) Unit: 1/(deg C)^2
        TC2
        % Flag to enable/disable resistor thermal noise (sm-rb) 
        Noise
        % Width (smorr) Unit: m
        Width
        % Length (smorr) Unit: m
        Length
        % Maximum power dissipation (warning) (s--rr) Unit: W
        wPmax
        % Maximum current (warning) (s--rr) Unit: A/m
        wImax
        % Number of resistors in parallel (smorr) 
        M
        % Secured Devel parameters (s---b) 
        Secured
        % Capacitance (smorr) Unit: F
        C
        % Scale factor (smorr) 
        Scale
        % Maximum operating voltage (TSMC SOA warning) (smorr) Unit: V
        Bv_max
    end
    methods
        function obj = set.R(obj,val)
            obj = setParameter(obj,'R',val,0,'real');
        end
        function res = get.R(obj)
            res = getParameter(obj,'R');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Trise(obj,val)
            obj = setParameter(obj,'Trise',val,0,'real');
        end
        function res = get.Trise(obj)
            res = getParameter(obj,'Trise');
        end
        function obj = set.Tnom(obj,val)
            obj = setParameter(obj,'Tnom',val,0,'real');
        end
        function res = get.Tnom(obj)
            res = getParameter(obj,'Tnom');
        end
        function obj = set.TC1(obj,val)
            obj = setParameter(obj,'TC1',val,0,'real');
        end
        function res = get.TC1(obj)
            res = getParameter(obj,'TC1');
        end
        function obj = set.TC2(obj,val)
            obj = setParameter(obj,'TC2',val,0,'real');
        end
        function res = get.TC2(obj)
            res = getParameter(obj,'TC2');
        end
        function obj = set.Noise(obj,val)
            obj = setParameter(obj,'Noise',val,0,'boolean');
        end
        function res = get.Noise(obj)
            res = getParameter(obj,'Noise');
        end
        function obj = set.Width(obj,val)
            obj = setParameter(obj,'Width',val,0,'real');
        end
        function res = get.Width(obj)
            res = getParameter(obj,'Width');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.wPmax(obj,val)
            obj = setParameter(obj,'wPmax',val,0,'real');
        end
        function res = get.wPmax(obj)
            res = getParameter(obj,'wPmax');
        end
        function obj = set.wImax(obj,val)
            obj = setParameter(obj,'wImax',val,0,'real');
        end
        function res = get.wImax(obj)
            res = getParameter(obj,'wImax');
        end
        function obj = set.M(obj,val)
            obj = setParameter(obj,'M',val,0,'real');
        end
        function res = get.M(obj)
            res = getParameter(obj,'M');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'boolean');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Scale(obj,val)
            obj = setParameter(obj,'Scale',val,0,'real');
        end
        function res = get.Scale(obj)
            res = getParameter(obj,'Scale');
        end
        function obj = set.Bv_max(obj,val)
            obj = setParameter(obj,'Bv_max',val,0,'real');
        end
        function res = get.Bv_max(obj)
            res = getParameter(obj,'Bv_max');
        end
    end
end

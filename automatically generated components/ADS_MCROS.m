classdef ADS_MCROS < ADScomponent
    % ADS_MCROS matlab representation for the ADS MCROS component
    % MDS Microstrip Cross Junction
    % MCROS [:Name] n1 n2 n3 n4
    properties (Access=protected)
        NumberOfNodes = 4
    end
    properties (Dependent)
        % Width of microstrip line 1 (Smorr) Unit: m
        W1
        % Width of microstrip line 2 (Smorr) Unit: m
        W2
        % Width of microstrip line 3 (Smorr) Unit: m
        W3
        % Width of microstrip line 4 (Smorr) Unit: m
        W4
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.W4(obj,val)
            obj = setParameter(obj,'W4',val,0,'real');
        end
        function res = get.W4(obj)
            res = getParameter(obj,'W4');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

classdef ADS_MonteCarlo < ADSnodeless
    % ADS_MonteCarlo matlab representation for the ADS MonteCarlo component
    % Monte Carlo analysis (using OptPak)
    % MonteCarlo [:Name] ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Analysis instance/path name to control (repeatable) (s---s) 
        SimInstanceName
        % Stat{..} Variations (s---b) 
        StatVariations
        % Process Variations (s---b) 
        ProcessVariations
        % Mismatch Variations (s---b) 
        MismatchVariations
        % The Start Trial Number of Monte Carlo Trial (sm--i) 
        StartTrial
        % The Stop Trial Number of Monte Carlo Trial (sm--i) 
        StopTrial
        % Number of Yield Optimization iterations (sm--i) 
        NumIters
        % Maximum number of Monte Carlo trials (sm--i) 
        MaxTrials
        % Type of shadow model to use: none, mfp, or hpsm (s---s) 
        ShadowModelType
        % Post Production Tunning Mode: none, ppt, or maxppt (s---s) 
        PPT_Mode
        % Seed for random number generator (s---i) 
        Seed
        % Flag to send analysis solutions to dataset (s---b) 
        SaveSolns
        % Send random variables to dataset (s---b) 
        SaveRandVars
        % Send mismatch variables to dataset (s---b) 
        SaveMismatchVars
        % Send yieldspec values to dataset (s---b) 
        SaveSpecs
        % Update dataset on each iteration (s---b) 
        UpdateDataset
        % Save data for all iterations (s---b) 
        SaveAllIterations
        % Flag to indicate use of all Specs (s---b) 
        UseAllSpecs
        % Name of yield_spec to use for yield estimate (repeatable) (s---s) 
        YieldSpecName
        % Analysis to call before evaluating yield_specs (repeatable) (s---s) 
        PresimInstanceName
        % Abort analysis sweep when specs are not satisfied (s---b) 
        UseShortcut
        % Degree of annotation (s---i) 
        StatusLevel
        % Flag to enable controllor (sm--b) 
        Enable
        % Restores nominal values if controlling optimization (s---b) 
        RestoreNomValues
        % Name of variable(s) to use in optimization (repeatable) (s---s) 
        OptimVar
        % Perform nominal simulation (s---b) 
        PerformNomSim
        % Recover Error Limit (s---i) 
        RecoverErrorLimit
        % Debug level (s---i) 
        DebugLevel
        % sort variables (s---b) 
        SortVars
    end
    methods
        function obj = set.SimInstanceName(obj,val)
            obj = setParameter(obj,'SimInstanceName',val,1,'string');
        end
        function res = get.SimInstanceName(obj)
            res = getParameter(obj,'SimInstanceName');
        end
        function obj = set.StatVariations(obj,val)
            obj = setParameter(obj,'StatVariations',val,0,'boolean');
        end
        function res = get.StatVariations(obj)
            res = getParameter(obj,'StatVariations');
        end
        function obj = set.ProcessVariations(obj,val)
            obj = setParameter(obj,'ProcessVariations',val,0,'boolean');
        end
        function res = get.ProcessVariations(obj)
            res = getParameter(obj,'ProcessVariations');
        end
        function obj = set.MismatchVariations(obj,val)
            obj = setParameter(obj,'MismatchVariations',val,0,'boolean');
        end
        function res = get.MismatchVariations(obj)
            res = getParameter(obj,'MismatchVariations');
        end
        function obj = set.StartTrial(obj,val)
            obj = setParameter(obj,'StartTrial',val,0,'integer');
        end
        function res = get.StartTrial(obj)
            res = getParameter(obj,'StartTrial');
        end
        function obj = set.StopTrial(obj,val)
            obj = setParameter(obj,'StopTrial',val,0,'integer');
        end
        function res = get.StopTrial(obj)
            res = getParameter(obj,'StopTrial');
        end
        function obj = set.NumIters(obj,val)
            obj = setParameter(obj,'NumIters',val,0,'integer');
        end
        function res = get.NumIters(obj)
            res = getParameter(obj,'NumIters');
        end
        function obj = set.MaxTrials(obj,val)
            obj = setParameter(obj,'MaxTrials',val,0,'integer');
        end
        function res = get.MaxTrials(obj)
            res = getParameter(obj,'MaxTrials');
        end
        function obj = set.ShadowModelType(obj,val)
            obj = setParameter(obj,'ShadowModelType',val,0,'string');
        end
        function res = get.ShadowModelType(obj)
            res = getParameter(obj,'ShadowModelType');
        end
        function obj = set.PPT_Mode(obj,val)
            obj = setParameter(obj,'PPT_Mode',val,0,'string');
        end
        function res = get.PPT_Mode(obj)
            res = getParameter(obj,'PPT_Mode');
        end
        function obj = set.Seed(obj,val)
            obj = setParameter(obj,'Seed',val,0,'integer');
        end
        function res = get.Seed(obj)
            res = getParameter(obj,'Seed');
        end
        function obj = set.SaveSolns(obj,val)
            obj = setParameter(obj,'SaveSolns',val,0,'boolean');
        end
        function res = get.SaveSolns(obj)
            res = getParameter(obj,'SaveSolns');
        end
        function obj = set.SaveRandVars(obj,val)
            obj = setParameter(obj,'SaveRandVars',val,0,'boolean');
        end
        function res = get.SaveRandVars(obj)
            res = getParameter(obj,'SaveRandVars');
        end
        function obj = set.SaveMismatchVars(obj,val)
            obj = setParameter(obj,'SaveMismatchVars',val,0,'boolean');
        end
        function res = get.SaveMismatchVars(obj)
            res = getParameter(obj,'SaveMismatchVars');
        end
        function obj = set.SaveSpecs(obj,val)
            obj = setParameter(obj,'SaveSpecs',val,0,'boolean');
        end
        function res = get.SaveSpecs(obj)
            res = getParameter(obj,'SaveSpecs');
        end
        function obj = set.UpdateDataset(obj,val)
            obj = setParameter(obj,'UpdateDataset',val,0,'boolean');
        end
        function res = get.UpdateDataset(obj)
            res = getParameter(obj,'UpdateDataset');
        end
        function obj = set.SaveAllIterations(obj,val)
            obj = setParameter(obj,'SaveAllIterations',val,0,'boolean');
        end
        function res = get.SaveAllIterations(obj)
            res = getParameter(obj,'SaveAllIterations');
        end
        function obj = set.UseAllSpecs(obj,val)
            obj = setParameter(obj,'UseAllSpecs',val,0,'boolean');
        end
        function res = get.UseAllSpecs(obj)
            res = getParameter(obj,'UseAllSpecs');
        end
        function obj = set.YieldSpecName(obj,val)
            obj = setParameter(obj,'YieldSpecName',val,0,'string');
        end
        function res = get.YieldSpecName(obj)
            res = getParameter(obj,'YieldSpecName');
        end
        function obj = set.PresimInstanceName(obj,val)
            obj = setParameter(obj,'PresimInstanceName',val,1,'string');
        end
        function res = get.PresimInstanceName(obj)
            res = getParameter(obj,'PresimInstanceName');
        end
        function obj = set.UseShortcut(obj,val)
            obj = setParameter(obj,'UseShortcut',val,0,'boolean');
        end
        function res = get.UseShortcut(obj)
            res = getParameter(obj,'UseShortcut');
        end
        function obj = set.StatusLevel(obj,val)
            obj = setParameter(obj,'StatusLevel',val,0,'integer');
        end
        function res = get.StatusLevel(obj)
            res = getParameter(obj,'StatusLevel');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.RestoreNomValues(obj,val)
            obj = setParameter(obj,'RestoreNomValues',val,0,'boolean');
        end
        function res = get.RestoreNomValues(obj)
            res = getParameter(obj,'RestoreNomValues');
        end
        function obj = set.OptimVar(obj,val)
            obj = setParameter(obj,'OptimVar',val,1,'string');
        end
        function res = get.OptimVar(obj)
            res = getParameter(obj,'OptimVar');
        end
        function obj = set.PerformNomSim(obj,val)
            obj = setParameter(obj,'PerformNomSim',val,0,'boolean');
        end
        function res = get.PerformNomSim(obj)
            res = getParameter(obj,'PerformNomSim');
        end
        function obj = set.RecoverErrorLimit(obj,val)
            obj = setParameter(obj,'RecoverErrorLimit',val,0,'integer');
        end
        function res = get.RecoverErrorLimit(obj)
            res = getParameter(obj,'RecoverErrorLimit');
        end
        function obj = set.DebugLevel(obj,val)
            obj = setParameter(obj,'DebugLevel',val,0,'integer');
        end
        function res = get.DebugLevel(obj)
            res = getParameter(obj,'DebugLevel');
        end
        function obj = set.SortVars(obj,val)
            obj = setParameter(obj,'SortVars',val,0,'boolean');
        end
        function res = get.SortVars(obj)
            res = getParameter(obj,'SortVars');
        end
    end
end

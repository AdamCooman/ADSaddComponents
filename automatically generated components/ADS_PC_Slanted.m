classdef ADS_PC_Slanted < ADScomponent
    % ADS_PC_Slanted matlab representation for the ADS PC_Slanted component
    % User-defined model
    % PC_Slanted [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        W
        % User device parameter (Smorr) Unit: unknown
        Layer
        % User device parameter (Smorr) Unit: unknown
        S
        % User device parameter (Smorr) Unit: unknown
        X_Offset
        % User device parameter (Smorr) Unit: unknown
        Y_Offset
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'real');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.X_Offset(obj,val)
            obj = setParameter(obj,'X_Offset',val,0,'real');
        end
        function res = get.X_Offset(obj)
            res = getParameter(obj,'X_Offset');
        end
        function obj = set.Y_Offset(obj,val)
            obj = setParameter(obj,'Y_Offset',val,0,'real');
        end
        function res = get.Y_Offset(obj)
            res = getParameter(obj,'Y_Offset');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

classdef ADS_IC_Line < ADScomponent
    % ADS_IC_Line matlab representation for the ADS IC_Line component
    % User-defined model
    % IC_Line [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (Smorr) Unit: unknown
        W
        % User device parameter (Smorr) Unit: unknown
        Layer
        % User device parameter (S--rr) Unit: unknown
        Type
        % User device parameter (Smorr) Unit: unknown
        S
        % User device parameter (Smorr) Unit: unknown
        Length
        % User device parameter (Sm-rs) Unit: unknown
        Subst
        % RLGC File Name (sm-rs) Unit: unknown
        RLGC_File
        % Flag to reuse RLGC matricies (sm-ri) Unit: unknown
        ReuseRLGC
        % User device parameter (s--ri) Unit: unknown
        Na
        % User device parameter (s--rr) Unit: unknown
        Href
        % Flag to write geofile to ems2.geo (s--ri) Unit: unknown
        WriteGeo
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.W(obj,val)
            obj = setParameter(obj,'W',val,0,'real');
        end
        function res = get.W(obj)
            res = getParameter(obj,'W');
        end
        function obj = set.Layer(obj,val)
            obj = setParameter(obj,'Layer',val,0,'real');
        end
        function res = get.Layer(obj)
            res = getParameter(obj,'Layer');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'real');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.S(obj,val)
            obj = setParameter(obj,'S',val,0,'real');
        end
        function res = get.S(obj)
            res = getParameter(obj,'S');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.RLGC_File(obj,val)
            obj = setParameter(obj,'RLGC_File',val,0,'string');
        end
        function res = get.RLGC_File(obj)
            res = getParameter(obj,'RLGC_File');
        end
        function obj = set.ReuseRLGC(obj,val)
            obj = setParameter(obj,'ReuseRLGC',val,0,'integer');
        end
        function res = get.ReuseRLGC(obj)
            res = getParameter(obj,'ReuseRLGC');
        end
        function obj = set.Na(obj,val)
            obj = setParameter(obj,'Na',val,0,'integer');
        end
        function res = get.Na(obj)
            res = getParameter(obj,'Na');
        end
        function obj = set.Href(obj,val)
            obj = setParameter(obj,'Href',val,0,'real');
        end
        function res = get.Href(obj)
            res = getParameter(obj,'Href');
        end
        function obj = set.WriteGeo(obj,val)
            obj = setParameter(obj,'WriteGeo',val,0,'integer');
        end
        function res = get.WriteGeo(obj)
            res = getParameter(obj,'WriteGeo');
        end
    end
end

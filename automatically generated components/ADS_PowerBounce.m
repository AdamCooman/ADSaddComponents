classdef ADS_PowerBounce < ADScomponent
    % ADS_PowerBounce matlab representation for the ADS PowerBounce component
    % Power Supply Plane Bouncing Model
    % PowerBounce [:Name] n1 ...
    properties (Access=protected)
        NumberOfNodes = 1
    end
    properties (Dependent)
        % Power supply plane Layer Number (Sm-ri) Unit: 1
        PowerLayer
        % Ground plane Layer Number (Sm-ri) Unit: 1
        GroundLayer
        % Number of polygon vertices (Sm-ri) Unit: 1
        Nv
        % Number of S parameter ports (Sm-ri) Unit: 1
        Ns
        % Number of array, maximum between nv and ns (S--ri) Unit: 1
        Nt
        % X_coordinates of polygon vertices (Smorr) Unit: m
        Xv
        % Y_coordinates of polygon vertices (Smorr) Unit: m
        Yv
        % X_coordinates of S parameter ports (Smorr) Unit: m
        Xs
        % Y_coordinates of S parameter ports (Smorr) Unit: m
        Ys
        % Estimated Clock Frequency of high speed digital signal (Smorr) Unit: Hz
        ClockFreq
        % Estimated Rise Time of high speed digital signal (Smorr) Unit: sec
        RiseTime
        % Length of TLM cell---optional (Smorr) Unit: m
        CellLength
        % User device parameter (Sm-rs) Unit: unknown
        Subst
    end
    methods
        function obj = set.PowerLayer(obj,val)
            obj = setParameter(obj,'PowerLayer',val,0,'integer');
        end
        function res = get.PowerLayer(obj)
            res = getParameter(obj,'PowerLayer');
        end
        function obj = set.GroundLayer(obj,val)
            obj = setParameter(obj,'GroundLayer',val,0,'integer');
        end
        function res = get.GroundLayer(obj)
            res = getParameter(obj,'GroundLayer');
        end
        function obj = set.Nv(obj,val)
            obj = setParameter(obj,'Nv',val,0,'integer');
        end
        function res = get.Nv(obj)
            res = getParameter(obj,'Nv');
        end
        function obj = set.Ns(obj,val)
            obj = setParameter(obj,'Ns',val,0,'integer');
        end
        function res = get.Ns(obj)
            res = getParameter(obj,'Ns');
        end
        function obj = set.Nt(obj,val)
            obj = setParameter(obj,'Nt',val,0,'integer');
        end
        function res = get.Nt(obj)
            res = getParameter(obj,'Nt');
        end
        function obj = set.Xv(obj,val)
            obj = setParameter(obj,'Xv',val,0,'real');
        end
        function res = get.Xv(obj)
            res = getParameter(obj,'Xv');
        end
        function obj = set.Yv(obj,val)
            obj = setParameter(obj,'Yv',val,0,'real');
        end
        function res = get.Yv(obj)
            res = getParameter(obj,'Yv');
        end
        function obj = set.Xs(obj,val)
            obj = setParameter(obj,'Xs',val,0,'real');
        end
        function res = get.Xs(obj)
            res = getParameter(obj,'Xs');
        end
        function obj = set.Ys(obj,val)
            obj = setParameter(obj,'Ys',val,0,'real');
        end
        function res = get.Ys(obj)
            res = getParameter(obj,'Ys');
        end
        function obj = set.ClockFreq(obj,val)
            obj = setParameter(obj,'ClockFreq',val,0,'real');
        end
        function res = get.ClockFreq(obj)
            res = getParameter(obj,'ClockFreq');
        end
        function obj = set.RiseTime(obj,val)
            obj = setParameter(obj,'RiseTime',val,0,'real');
        end
        function res = get.RiseTime(obj)
            res = getParameter(obj,'RiseTime');
        end
        function obj = set.CellLength(obj,val)
            obj = setParameter(obj,'CellLength',val,0,'real');
        end
        function res = get.CellLength(obj)
            res = getParameter(obj,'CellLength');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
    end
end

classdef ADS_OscProbe < ADSnodeless
    % ADS_OscProbe matlab representation for the ADS OscProbe component
    % Oscillator Probe
    % OscProbe [:Name]
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Node at which signal is injected (s---i) 
        Node
        % Initial guess at fundamental voltage (smorr) Unit: V
        V
        % Gain adjustment (smo-c) Unit: Ohms
        Polish
        % Gain adjustment (smo-c) Unit: V
        SS_Voltage
        % Number of octaves to search (smo-r) 
        NumOctaves
        % Number of steps per search octave (smo-r) 
        Steps
        % Fundmental number for oscillator (s---i) 
        FundIndex
        % Harmonic number for oscillator fundmental (sm--i) 
        Harm
        % Maximum arc-length step during loop gain search (s---r) 
        MaxLoopGainStep
        % Phase of oscport fundamental (smo-r) 
        phase
    end
    methods
        function obj = set.Node(obj,val)
            obj = setParameter(obj,'Node',val,1,'integer');
        end
        function res = get.Node(obj)
            res = getParameter(obj,'Node');
        end
        function obj = set.V(obj,val)
            obj = setParameter(obj,'V',val,0,'real');
        end
        function res = get.V(obj)
            res = getParameter(obj,'V');
        end
        function obj = set.Polish(obj,val)
            obj = setParameter(obj,'Polish',val,0,'complex');
        end
        function res = get.Polish(obj)
            res = getParameter(obj,'Polish');
        end
        function obj = set.SS_Voltage(obj,val)
            obj = setParameter(obj,'SS_Voltage',val,0,'complex');
        end
        function res = get.SS_Voltage(obj)
            res = getParameter(obj,'SS_Voltage');
        end
        function obj = set.NumOctaves(obj,val)
            obj = setParameter(obj,'NumOctaves',val,0,'real');
        end
        function res = get.NumOctaves(obj)
            res = getParameter(obj,'NumOctaves');
        end
        function obj = set.Steps(obj,val)
            obj = setParameter(obj,'Steps',val,0,'real');
        end
        function res = get.Steps(obj)
            res = getParameter(obj,'Steps');
        end
        function obj = set.FundIndex(obj,val)
            obj = setParameter(obj,'FundIndex',val,0,'integer');
        end
        function res = get.FundIndex(obj)
            res = getParameter(obj,'FundIndex');
        end
        function obj = set.Harm(obj,val)
            obj = setParameter(obj,'Harm',val,0,'integer');
        end
        function res = get.Harm(obj)
            res = getParameter(obj,'Harm');
        end
        function obj = set.MaxLoopGainStep(obj,val)
            obj = setParameter(obj,'MaxLoopGainStep',val,0,'real');
        end
        function res = get.MaxLoopGainStep(obj)
            res = getParameter(obj,'MaxLoopGainStep');
        end
        function obj = set.phase(obj,val)
            obj = setParameter(obj,'phase',val,0,'real');
        end
        function res = get.phase(obj)
            res = getParameter(obj,'phase');
        end
    end
end

classdef ADS_Noisy2Port < ADScomponent
    % ADS_Noisy2Port matlab representation for the ADS Noisy2Port component
    % Linear Noisy Two Port Network
    % Noisy2Port [:Name] p n g
    properties (Access=protected)
        NumberOfNodes = 3
    end
    properties (Dependent)
        % Minimum Noise Figure (Smorr) 
        NFmin
        % Noise resistance (Smorr) Unit: Ohms
        Rn
        % Input reflection coefficient for NFmin (Smorc) 
        Sopt
        % Reference resistance for Sopt (smorr) Unit: Ohms
        Rref
        % SYM array pointer to all noisy two-port parameters (s---c) 
        All
    end
    methods
        function obj = set.NFmin(obj,val)
            obj = setParameter(obj,'NFmin',val,0,'real');
        end
        function res = get.NFmin(obj)
            res = getParameter(obj,'NFmin');
        end
        function obj = set.Rn(obj,val)
            obj = setParameter(obj,'Rn',val,0,'real');
        end
        function res = get.Rn(obj)
            res = getParameter(obj,'Rn');
        end
        function obj = set.Sopt(obj,val)
            obj = setParameter(obj,'Sopt',val,0,'complex');
        end
        function res = get.Sopt(obj)
            res = getParameter(obj,'Sopt');
        end
        function obj = set.Rref(obj,val)
            obj = setParameter(obj,'Rref',val,0,'real');
        end
        function res = get.Rref(obj)
            res = getParameter(obj,'Rref');
        end
        function obj = set.All(obj,val)
            obj = setParameter(obj,'All',val,0,'complex');
        end
        function res = get.All(obj)
            res = getParameter(obj,'All');
        end
    end
end

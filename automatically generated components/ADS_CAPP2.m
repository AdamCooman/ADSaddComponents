classdef ADS_CAPP2 < ADScomponent
    % ADS_CAPP2 matlab representation for the ADS CAPP2 component
    % Chip Capacitor
    % CAPP2 [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % capacitance (Smorr) Unit: f
        C
        % dielectric loss tangent (Smorr) Unit: 1
        TanD
        % quality factor (Smorr) Unit: 1
        Q
        % reference frequency for q (Smorr) Unit: Hz
        FreqQ
        % resonance frequency (Smorr) Unit: Hz
        FreqRes
        % exponent for frequency dependance of q (Smorr) Unit: 1
        Exp
        % Physical Temperature (smorr) Unit: C
        Temp
        % Non-causal function impulse response order (s--ri) Unit: (unknown units)
        Secured
    end
    methods
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,0,'real');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.TanD(obj,val)
            obj = setParameter(obj,'TanD',val,0,'real');
        end
        function res = get.TanD(obj)
            res = getParameter(obj,'TanD');
        end
        function obj = set.Q(obj,val)
            obj = setParameter(obj,'Q',val,0,'real');
        end
        function res = get.Q(obj)
            res = getParameter(obj,'Q');
        end
        function obj = set.FreqQ(obj,val)
            obj = setParameter(obj,'FreqQ',val,0,'real');
        end
        function res = get.FreqQ(obj)
            res = getParameter(obj,'FreqQ');
        end
        function obj = set.FreqRes(obj,val)
            obj = setParameter(obj,'FreqRes',val,0,'real');
        end
        function res = get.FreqRes(obj)
            res = getParameter(obj,'FreqRes');
        end
        function obj = set.Exp(obj,val)
            obj = setParameter(obj,'Exp',val,0,'real');
        end
        function res = get.Exp(obj)
            res = getParameter(obj,'Exp');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Secured(obj,val)
            obj = setParameter(obj,'Secured',val,0,'integer');
        end
        function res = get.Secured(obj)
            res = getParameter(obj,'Secured');
        end
    end
end

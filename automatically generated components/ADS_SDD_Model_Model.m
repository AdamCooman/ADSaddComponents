classdef ADS_SDD_Model_Model < ADSmodel
    % ADS_SDD_Model_Model matlab representation for the ADS SDD_Model_Model component
    % Symbolically Defined Device with Model model
    % model ModelName SDD_Model ...
    properties (Access=protected)
        NumberOfNodes = 0
    end
    properties (Dependent)
        % Number of ports (s---i) 
        Ports
        % Explicit current, I[port,heqn]=f(_v1,_i1,_v2,_i2...) (smo-r) Unit: A
        I
        % Implicit eqn, F[port,heqn]=f(_v1,_i1,_v2,_i2...)=0 (smo-r) 
        F
        % H[heqn] frequency weighting function; heqn > 1 (s---c) 
        H
        % Obsolete (s---b) 
        UseFiniteDiff
        % Explicit specified type (s---s) 
        Type
        % Enable SDD equations (sm--b) 
        Enable
        % Port noise current squared, In[port,heqn]=f(_v1,_i1,_v2,_i2...) (smo-r) Unit: A
        In
        % Port noise current correlation, Nc[PortA,PortB]=f(_v1,_i1,_v2,_i2...) (smo-c) 
        Nc
        % Controlling current (s---d) 
        C
        % Controlling current port (s---i) 
        Cport
        % Expression evaluated to detect strange DC behavior (s---r) 
        StrangeDC
        % Disable the frequency dependency error checking (s---b) 
        DisableFreqError
        % Non-causal function impulse response order (sm--i) 
        ImpNoncausalLength
        % Convolution mode (sm--i) 
        ImpMode
        % Maximum Frequency to which device is evaluated (smo-r) Unit: Hz
        ImpMaxFreq
        % Sample spacing in frequency (smo-r) Unit: Hz
        ImpDeltaFreq
        % maximum allowed impulse response order (sm--i) 
        ImpMaxOrder
        % Smoothing Window (sm--i) 
        ImpWindow
        % Relative impulse Response truncation factor (smo-r) 
        ImpRelTol
        % Absolute impulse Response truncation factor (smo-r) 
        ImpAbsTol
    end
    methods
        function obj = set.Ports(obj,val)
            obj = setParameter(obj,'Ports',val,0,'integer');
        end
        function res = get.Ports(obj)
            res = getParameter(obj,'Ports');
        end
        function obj = set.I(obj,val)
            obj = setParameter(obj,'I',val,2,'real');
        end
        function res = get.I(obj)
            res = getParameter(obj,'I');
        end
        function obj = set.F(obj,val)
            obj = setParameter(obj,'F',val,2,'real');
        end
        function res = get.F(obj)
            res = getParameter(obj,'F');
        end
        function obj = set.H(obj,val)
            obj = setParameter(obj,'H',val,1,'complex');
        end
        function res = get.H(obj)
            res = getParameter(obj,'H');
        end
        function obj = set.UseFiniteDiff(obj,val)
            obj = setParameter(obj,'UseFiniteDiff',val,0,'boolean');
        end
        function res = get.UseFiniteDiff(obj)
            res = getParameter(obj,'UseFiniteDiff');
        end
        function obj = set.Type(obj,val)
            obj = setParameter(obj,'Type',val,0,'string');
        end
        function res = get.Type(obj)
            res = getParameter(obj,'Type');
        end
        function obj = set.Enable(obj,val)
            obj = setParameter(obj,'Enable',val,0,'boolean');
        end
        function res = get.Enable(obj)
            res = getParameter(obj,'Enable');
        end
        function obj = set.In(obj,val)
            obj = setParameter(obj,'In',val,2,'real');
        end
        function res = get.In(obj)
            res = getParameter(obj,'In');
        end
        function obj = set.Nc(obj,val)
            obj = setParameter(obj,'Nc',val,2,'complex');
        end
        function res = get.Nc(obj)
            res = getParameter(obj,'Nc');
        end
        function obj = set.C(obj,val)
            obj = setParameter(obj,'C',val,1,'string');
        end
        function res = get.C(obj)
            res = getParameter(obj,'C');
        end
        function obj = set.Cport(obj,val)
            obj = setParameter(obj,'Cport',val,1,'integer');
        end
        function res = get.Cport(obj)
            res = getParameter(obj,'Cport');
        end
        function obj = set.StrangeDC(obj,val)
            obj = setParameter(obj,'StrangeDC',val,0,'real');
        end
        function res = get.StrangeDC(obj)
            res = getParameter(obj,'StrangeDC');
        end
        function obj = set.DisableFreqError(obj,val)
            obj = setParameter(obj,'DisableFreqError',val,0,'boolean');
        end
        function res = get.DisableFreqError(obj)
            res = getParameter(obj,'DisableFreqError');
        end
        function obj = set.ImpNoncausalLength(obj,val)
            obj = setParameter(obj,'ImpNoncausalLength',val,0,'integer');
        end
        function res = get.ImpNoncausalLength(obj)
            res = getParameter(obj,'ImpNoncausalLength');
        end
        function obj = set.ImpMode(obj,val)
            obj = setParameter(obj,'ImpMode',val,0,'integer');
        end
        function res = get.ImpMode(obj)
            res = getParameter(obj,'ImpMode');
        end
        function obj = set.ImpMaxFreq(obj,val)
            obj = setParameter(obj,'ImpMaxFreq',val,0,'real');
        end
        function res = get.ImpMaxFreq(obj)
            res = getParameter(obj,'ImpMaxFreq');
        end
        function obj = set.ImpDeltaFreq(obj,val)
            obj = setParameter(obj,'ImpDeltaFreq',val,0,'real');
        end
        function res = get.ImpDeltaFreq(obj)
            res = getParameter(obj,'ImpDeltaFreq');
        end
        function obj = set.ImpMaxOrder(obj,val)
            obj = setParameter(obj,'ImpMaxOrder',val,0,'integer');
        end
        function res = get.ImpMaxOrder(obj)
            res = getParameter(obj,'ImpMaxOrder');
        end
        function obj = set.ImpWindow(obj,val)
            obj = setParameter(obj,'ImpWindow',val,0,'integer');
        end
        function res = get.ImpWindow(obj)
            res = getParameter(obj,'ImpWindow');
        end
        function obj = set.ImpRelTol(obj,val)
            obj = setParameter(obj,'ImpRelTol',val,0,'real');
        end
        function res = get.ImpRelTol(obj)
            res = getParameter(obj,'ImpRelTol');
        end
        function obj = set.ImpAbsTol(obj,val)
            obj = setParameter(obj,'ImpAbsTol',val,0,'real');
        end
        function res = get.ImpAbsTol(obj)
            res = getParameter(obj,'ImpAbsTol');
        end
    end
end

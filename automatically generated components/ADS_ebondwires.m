classdef ADS_ebondwires < ADScomponent
    % ADS_ebondwires matlab representation for the ADS ebondwires component
    % User-defined model
    % ebondwires [:Name] n1 n2 ...
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % User device parameter (S--ri) Unit: unknown
        N
        % User device parameter (S--ri) Unit: unknown
        Offsets_Accumulate
        % User device parameter (Smorr) Unit: unknown
        SepX
        % User device parameter (Smorr) Unit: unknown
        SepY
        % User device parameter (Smorr) Unit: unknown
        SepAngle
        % User device parameter (Sm-rs) Unit: unknown
        Shape
        % User device parameter (Smorr) Unit: unknown
        Length
        % User device parameter (Smorr) Unit: unknown
        Wire_Angle
        % User device parameter (Smorr) Unit: unknown
        Xoffset
        % User device parameter (Smorr) Unit: unknown
        Yoffset
        % User device parameter (smorr) Unit: unknown
        x
        % User device parameter (smorr) Unit: unknown
        y
        % User device parameter (smorr) Unit: unknown
        angle
        % User device parameter (sm-ri) Unit: unknown
        xMirror
        % User device parameter (sm-ri) Unit: unknown
        yMirror
    end
    methods
        function obj = set.N(obj,val)
            obj = setParameter(obj,'N',val,0,'integer');
        end
        function res = get.N(obj)
            res = getParameter(obj,'N');
        end
        function obj = set.Offsets_Accumulate(obj,val)
            obj = setParameter(obj,'Offsets_Accumulate',val,0,'integer');
        end
        function res = get.Offsets_Accumulate(obj)
            res = getParameter(obj,'Offsets_Accumulate');
        end
        function obj = set.SepX(obj,val)
            obj = setParameter(obj,'SepX',val,0,'real');
        end
        function res = get.SepX(obj)
            res = getParameter(obj,'SepX');
        end
        function obj = set.SepY(obj,val)
            obj = setParameter(obj,'SepY',val,0,'real');
        end
        function res = get.SepY(obj)
            res = getParameter(obj,'SepY');
        end
        function obj = set.SepAngle(obj,val)
            obj = setParameter(obj,'SepAngle',val,0,'real');
        end
        function res = get.SepAngle(obj)
            res = getParameter(obj,'SepAngle');
        end
        function obj = set.Shape(obj,val)
            obj = setParameter(obj,'Shape',val,0,'string');
        end
        function res = get.Shape(obj)
            res = getParameter(obj,'Shape');
        end
        function obj = set.Length(obj,val)
            obj = setParameter(obj,'Length',val,0,'real');
        end
        function res = get.Length(obj)
            res = getParameter(obj,'Length');
        end
        function obj = set.Wire_Angle(obj,val)
            obj = setParameter(obj,'Wire_Angle',val,0,'real');
        end
        function res = get.Wire_Angle(obj)
            res = getParameter(obj,'Wire_Angle');
        end
        function obj = set.Xoffset(obj,val)
            obj = setParameter(obj,'Xoffset',val,0,'real');
        end
        function res = get.Xoffset(obj)
            res = getParameter(obj,'Xoffset');
        end
        function obj = set.Yoffset(obj,val)
            obj = setParameter(obj,'Yoffset',val,0,'real');
        end
        function res = get.Yoffset(obj)
            res = getParameter(obj,'Yoffset');
        end
        function obj = set.x(obj,val)
            obj = setParameter(obj,'x',val,0,'real');
        end
        function res = get.x(obj)
            res = getParameter(obj,'x');
        end
        function obj = set.y(obj,val)
            obj = setParameter(obj,'y',val,0,'real');
        end
        function res = get.y(obj)
            res = getParameter(obj,'y');
        end
        function obj = set.angle(obj,val)
            obj = setParameter(obj,'angle',val,0,'real');
        end
        function res = get.angle(obj)
            res = getParameter(obj,'angle');
        end
        function obj = set.xMirror(obj,val)
            obj = setParameter(obj,'xMirror',val,0,'integer');
        end
        function res = get.xMirror(obj)
            res = getParameter(obj,'xMirror');
        end
        function obj = set.yMirror(obj,val)
            obj = setParameter(obj,'yMirror',val,0,'integer');
        end
        function res = get.yMirror(obj)
            res = getParameter(obj,'yMirror');
        end
    end
end

classdef ADS_PCLIN6 < ADScomponent
    % ADS_PCLIN6 matlab representation for the ADS PCLIN6 component
    % 6 Printed Circuit Coupled Lines
    % PCLIN6 [:Name] n1 n2 n3 n4 n5 n6 n7 n8 n9 n10 n11 n12
    properties (Access=protected)
        NumberOfNodes = 12
    end
    properties (Dependent)
        % Width of line #1 (Smorr) Unit: m
        W1
        % Distance from line #1 to left wall (Smorr) Unit: m
        S1
        % Conductor layer number - line #1 (Sm-ri) Unit: unknown
        CLayer1
        % Width of line #2 (Smorr) Unit: m
        W2
        % Distance from line #2 to left wall (Smorr) Unit: m
        S2
        % Conductor layer number - line #2 (Sm-ri) Unit: unknown
        CLayer2
        % Width of line #3 (Smorr) Unit: m
        W3
        % Distance from line #3 to left wall (Smorr) Unit: m
        S3
        % Conductor layer number - line #3 (Sm-ri) Unit: unknown
        CLayer3
        % Width of line #4 (Smorr) Unit: m
        W4
        % Distance from line #4 to left wall (Smorr) Unit: m
        S4
        % Conductor layer number - line #4 (Sm-ri) Unit: unknown
        CLayer4
        % Width of line #5 (Smorr) Unit: m
        W5
        % Distance from line #5 to left wall (Smorr) Unit: m
        S5
        % Conductor layer number - line #5 (Sm-ri) Unit: unknown
        CLayer5
        % Width of line #6 (Smorr) Unit: m
        W6
        % Distance from line #6 to left wall (Smorr) Unit: m
        S6
        % Conductor layer number - line #6 (Sm-ri) Unit: unknown
        CLayer6
        % Length of the lines (Smorr) Unit: m
        L
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
        % Factor to refine the background grid (Sm-ri) Unit: unknown
        Refine_grid_factor
    end
    methods
        function obj = set.W1(obj,val)
            obj = setParameter(obj,'W1',val,0,'real');
        end
        function res = get.W1(obj)
            res = getParameter(obj,'W1');
        end
        function obj = set.S1(obj,val)
            obj = setParameter(obj,'S1',val,0,'real');
        end
        function res = get.S1(obj)
            res = getParameter(obj,'S1');
        end
        function obj = set.CLayer1(obj,val)
            obj = setParameter(obj,'CLayer1',val,0,'integer');
        end
        function res = get.CLayer1(obj)
            res = getParameter(obj,'CLayer1');
        end
        function obj = set.W2(obj,val)
            obj = setParameter(obj,'W2',val,0,'real');
        end
        function res = get.W2(obj)
            res = getParameter(obj,'W2');
        end
        function obj = set.S2(obj,val)
            obj = setParameter(obj,'S2',val,0,'real');
        end
        function res = get.S2(obj)
            res = getParameter(obj,'S2');
        end
        function obj = set.CLayer2(obj,val)
            obj = setParameter(obj,'CLayer2',val,0,'integer');
        end
        function res = get.CLayer2(obj)
            res = getParameter(obj,'CLayer2');
        end
        function obj = set.W3(obj,val)
            obj = setParameter(obj,'W3',val,0,'real');
        end
        function res = get.W3(obj)
            res = getParameter(obj,'W3');
        end
        function obj = set.S3(obj,val)
            obj = setParameter(obj,'S3',val,0,'real');
        end
        function res = get.S3(obj)
            res = getParameter(obj,'S3');
        end
        function obj = set.CLayer3(obj,val)
            obj = setParameter(obj,'CLayer3',val,0,'integer');
        end
        function res = get.CLayer3(obj)
            res = getParameter(obj,'CLayer3');
        end
        function obj = set.W4(obj,val)
            obj = setParameter(obj,'W4',val,0,'real');
        end
        function res = get.W4(obj)
            res = getParameter(obj,'W4');
        end
        function obj = set.S4(obj,val)
            obj = setParameter(obj,'S4',val,0,'real');
        end
        function res = get.S4(obj)
            res = getParameter(obj,'S4');
        end
        function obj = set.CLayer4(obj,val)
            obj = setParameter(obj,'CLayer4',val,0,'integer');
        end
        function res = get.CLayer4(obj)
            res = getParameter(obj,'CLayer4');
        end
        function obj = set.W5(obj,val)
            obj = setParameter(obj,'W5',val,0,'real');
        end
        function res = get.W5(obj)
            res = getParameter(obj,'W5');
        end
        function obj = set.S5(obj,val)
            obj = setParameter(obj,'S5',val,0,'real');
        end
        function res = get.S5(obj)
            res = getParameter(obj,'S5');
        end
        function obj = set.CLayer5(obj,val)
            obj = setParameter(obj,'CLayer5',val,0,'integer');
        end
        function res = get.CLayer5(obj)
            res = getParameter(obj,'CLayer5');
        end
        function obj = set.W6(obj,val)
            obj = setParameter(obj,'W6',val,0,'real');
        end
        function res = get.W6(obj)
            res = getParameter(obj,'W6');
        end
        function obj = set.S6(obj,val)
            obj = setParameter(obj,'S6',val,0,'real');
        end
        function res = get.S6(obj)
            res = getParameter(obj,'S6');
        end
        function obj = set.CLayer6(obj,val)
            obj = setParameter(obj,'CLayer6',val,0,'integer');
        end
        function res = get.CLayer6(obj)
            res = getParameter(obj,'CLayer6');
        end
        function obj = set.L(obj,val)
            obj = setParameter(obj,'L',val,0,'real');
        end
        function res = get.L(obj)
            res = getParameter(obj,'L');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
        function obj = set.Refine_grid_factor(obj,val)
            obj = setParameter(obj,'Refine_grid_factor',val,0,'integer');
        end
        function res = get.Refine_grid_factor(obj)
            res = getParameter(obj,'Refine_grid_factor');
        end
    end
end

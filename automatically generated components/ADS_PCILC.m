classdef ADS_PCILC < ADScomponent
    % ADS_PCILC matlab representation for the ADS PCILC component
    % Printed Circuit Inter-Layer Connection
    % PCILC [:Name] n1 n2
    properties (Access=protected)
        NumberOfNodes = 2
    end
    properties (Dependent)
        % Diameter of via hole (Smorr) Unit: m
        D
        % Conductor layer number at pin 1 (Sm-ri) Unit: unknown
        CLayer1
        % Conductor layer number at pin 2 (Sm-ri) Unit: unknown
        CLayer2
        % Printed circuit substrate (Sm-rs) Unit: unknown
        Subst
        % Physical temperature (smorr) Unit: C
        Temp
    end
    methods
        function obj = set.D(obj,val)
            obj = setParameter(obj,'D',val,0,'real');
        end
        function res = get.D(obj)
            res = getParameter(obj,'D');
        end
        function obj = set.CLayer1(obj,val)
            obj = setParameter(obj,'CLayer1',val,0,'integer');
        end
        function res = get.CLayer1(obj)
            res = getParameter(obj,'CLayer1');
        end
        function obj = set.CLayer2(obj,val)
            obj = setParameter(obj,'CLayer2',val,0,'integer');
        end
        function res = get.CLayer2(obj)
            res = getParameter(obj,'CLayer2');
        end
        function obj = set.Subst(obj,val)
            obj = setParameter(obj,'Subst',val,0,'string');
        end
        function res = get.Subst(obj)
            res = getParameter(obj,'Subst');
        end
        function obj = set.Temp(obj,val)
            obj = setParameter(obj,'Temp',val,0,'real');
        end
        function res = get.Temp(obj)
            res = getParameter(obj,'Temp');
        end
    end
end

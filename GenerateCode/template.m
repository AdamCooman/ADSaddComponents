classdef ADS_$NAME$ < $SUPERCLASS$
    % ADS_$NAME$ matlab representation for the ADS $NAME$ component
    % $DESCRIPTION$
    % $SYNTAXSTRING$
    properties (Access=protected)
        NumberOfNodes = $NUMBEROFNODES$
    end
    properties (Dependent)
$PROPERTIES$
    end
    methods
$GETTERSANDSETTERS$
    end
end
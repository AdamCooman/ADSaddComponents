function generateParamCells(fid,parameters)

% sort the parameters according to required and optional by looking at
% their SMORR string

requiredparams = {};
optionalparams = {};

if length(parameters)==1
    
    SMORR = removeSpaces(parameters.SMORR.Text);
    if strcmp(SMORR(1),'S');
        % if the first character in the SMORR string is S, the parameter is required
        requiredparams = {requiredparams{:} parameters};
    elseif strcmp(SMORR(1),'s')
        % if the first character in the SMORR string is s, the parameter is optional
        optionalparams = {optionalparams{:} parameters};
    end

else
for ii=1:length(parameters)
    SMORR = removeSpaces(parameters{ii}.SMORR.Text);
    if strcmp(SMORR(1),'S');
        % if the first character in the SMORR string is S, the parameter is required
        requiredparams = {requiredparams{:} parameters{ii}};
    elseif strcmp(SMORR(1),'s')
        % if the first character in the SMORR string is s, the parameter is optional
        optionalparams = {optionalparams{:} parameters{ii}};
    end
end
end

% process the required parameters
if ~isempty(requiredparams)
    fprintf(fid,'requiredparams={\r\n');
    for ii=1:length(requiredparams)
        fprintf(fid,paramWithCheck(requiredparams{ii}));
    end
    fprintf(fid,'    };\r\n');
    fprintf(fid,'\r\n');
else
    fprintf(fid,'requiredparams={};\r\n');
    fprintf(fid,'\r\n');
end

% process the optional parameters
if ~isempty(optionalparams)
    fprintf(fid,'optionalparams={\r\n');
    for ii=1:length(optionalparams)
        fprintf(fid,paramWithCheck(optionalparams{ii}));
    end
    fprintf(fid,'    };\r\n');
    fprintf(fid,'\r\n');
else
    fprintf(fid,'optionalparams={};\r\n');
    fprintf(fid,'\r\n');
end


end

function str = paramWithCheck(par)
% makes the string for the cell array
%'StartTime',@check_positive
%'name',@check_function
name = removeSpaces(par.NAME.Text);
smorr = removeSpaces(par.SMORR.Text);

% if the name ends in "[]", a vector can be expected
% the name can also end in [,] or in [,,]
if length(name)>2
    if ~isempty(regexp(name,'\[\]$','once'))
        name = name(1:end-2);
        VECTOR = 1;
    else
        if ~isempty(regexp(name,'\[\,\]$','once'))
            name = name(1:end-3);
            VECTOR = 2;
        else
            if ~isempty(regexp(name,'\[\,\,\]$','once'))
                name = name(1:end-4);
                VECTOR = 3;
            else
                VECTOR = 0;
            end
        end
    end
else
    VECTOR=0;
end

% build the string
str = ['''' name ''',@check_'];
if VECTOR
    str=[str 'vector_'];
end

switch smorr(end)
    case 'b' % ee_bool
        str=[str 'yesno'];
    case 'i' % integer
        str=[str 'integer'];
    case 'r' % real number
        str=[str 'real'];
    case 'c' % complex number
        str=[str 'complex'];
    case 'd' % devide instance
        str=[str 'string'];
    case 's' % character string
        str=[str 'string'];
    case 'n'
        str=[str 'node'];
    otherwise
        str=[str 'string'];
end

str=[str ',' num2str(VECTOR) '\r\n'];

end
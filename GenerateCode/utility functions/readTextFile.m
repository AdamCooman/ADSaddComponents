function res = readTextFile(filename)
fid = fopen(filename);
Netlist = fread(fid,Inf,'*char').';
fclose(fid);
% Split the netlist into a cell array of statements
if ispc
    res = strsplit(Netlist,sprintf('\r\n')).';
else
    res = strsplit(Netlist,newline).';
end
end
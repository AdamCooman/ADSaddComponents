function res = removeSpaces(str)

ind = strfind(str,' ');
tokeep=setdiff(1:length(str),ind);
res = str(tokeep);


end
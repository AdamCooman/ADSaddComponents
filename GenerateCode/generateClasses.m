clear variables
close all
clc

codefolder = 'generated code';
if ~isfolder(codefolder)
    mkdir(codefolder);
end
addpath(codefolder);

load('ParsedXMLs.mat')

for dd=1:length(data)
    fprintf('Processing %s ...\n',data(dd).NAME);    
    % sometimes, there is a space in the filename, I want to get rig of those
    data(dd).NAME = regexprep(data(dd).NAME,' ','_');
    % generate the code
    res = generateClassCode(data(dd));
    % write the code to a file
    fid = fopen(fullfile(codefolder,['ADS_' data(dd).NAME '.m']),'w');
    fprintf(fid,'%s\n',res);
    fclose(fid);
    % try generating the class to see whether it doesn't contain any syntax errors
    try
        eval(['ADS_' data(dd).NAME ';']);
    catch err
        fprintf('Construction of %s is failing:\n',data(dd).NAME);
        rethrow(err);
    end
end

function res = generateClassCode(data)
% load the template
res = string(readTextFile('template.m'));
% determine the superclass and the amount of nodes
[superclass,numberOfNodes] = determineSuperClass(data);
% replace the superclass string
res = regexprep(res,'\$SUPERCLASS\$',superclass);
% replace the numberOfNodes string
res = regexprep(res,'\$NUMBEROFNODES\$',num2str(numberOfNodes));
% replace the name
res = regexprep(res,'\$NAME\$',data.NAME);
% replace the description
res = regexprep(res,'\$DESCRIPTION\$',data.DESCRIPTION);
% replace the syntax string
res = regexprep(res,'\$SYNTAXSTRING\$',data.SYNTAX);
% replace the properties
ind = find(~cellfun('isempty',regexp(res,'\$PROPERTIES\$')));
res = [res(1:ind-1);generatePublicProperties(data.PARAMETER);res(ind+1:end)];
% add the getters and setters
ind = find(~cellfun('isempty',regexp(res,'\$GETTERSANDSETTERS\$')));
res = [res(1:ind-1);generateGettersAndSetters(data.PARAMETER);res(ind+1:end)];
end

function ress = generatePublicProperties(props)
template = [...
    "        % $DESCRIPTION$ ($SMORR$) $UNIT$";...
    "        $NAME$"
    ];
ress = string.empty;
for pp=1:length(props)
    prop = props(pp);
    res = template;
    res = regexprep(res,'\$NAME\$',cleanVectorName(prop.NAME));
    res = regexprep(res,'\$SMORR\$',prop.SMORR);
    res = regexprep(res,'\$DESCRIPTION\$',prop.DESCRIPTION);
    if ~isempty(prop.UNIT)
        res = regexprep(res,'\$UNIT\$',['Unit: ' prop.UNIT]);
    else
        res = regexprep(res,'\$UNIT\$','');
    end
    ress = [ress;res];
end
end

function ress=generateGettersAndSetters(props)
% get the template going
template = [...
        "        function obj = set.$NAME$(obj,val)";...
        "            obj = setParameter(obj,'$NAME$',val,$DIMS$,'$TYPE$');";...
        "        end";...
        "        function res = get.$NAME$(obj)";...
        "            res = getParameter(obj,'$NAME$');";...
        "        end";...
    ];
ress = string.empty;
for pp=1:length(props)
    prop = props(pp);
    res = template;
    [prop.NAME,paramdims]=cleanVectorName(prop.NAME);    
    res = regexprep(res,'\$NAME\$',prop.NAME);
    res = regexprep(res,'\$DIMS\$',num2str(paramdims));
    switch prop.SMORR(end)
        case 'c'
           MTYPE = 'complex';
        case 'r'
            MTYPE = 'real';
        case 'i'
            MTYPE = 'integer';
        case 's'
            MTYPE = 'string';
        case 'b'
            MTYPE = 'boolean';
        case 'd'
            MTYPE = 'string';
        case '-'
            MTYPE = 'string';
        otherwise
            error('unknown type');
    end
    res = regexprep(res,'\$TYPE\$',MTYPE);
    ress = [ress;res];
end
end

function [superclass,numberOfNodes] = determineSuperClass(data)
% syntax is one of three cases
% TYPE [:Name] NODE1 NODE2 ...
% TYPE [:Name] ...
% model ModelName TYPE ...
% ModelName [:Name] NODE NODE ...
syntax = string(strsplit(data.SYNTAX));
% remove a possible '...' at the end
if syntax(end)=="..."
    syntax = syntax(1:end-1);
end
if syntax(1)=="model"
    superclass = "ADSmodel";
    numberOfNodes = 0;
else
    numberOfNodes = length(syntax)-2;
    % if it has nodes, it's a component
    if numberOfNodes>0
        superclass = "ADScomponent";
    else
        superclass = "ADSnodeless";
    end
end
end

function [name,numberofdims]=cleanVectorName(name)
numberofdims = 0;
if endsWith(name,']')
    name = char(name);
    name = name(1:end-1);
    while (name(end)=='[')||(name(end)==',')
        name = name(1:end-1);
        numberofdims = numberofdims+1;
    end
    name = string(name);
end
end

% script to extract the xml files from all the possible components from the hpeesofsim help
clear variables
close all
clc

% overwrite the runhpeesofsimcommand function
ADSobj = ADS();
ADSobj.version = "ADS2019";
ADSobj.displaySimulationOutput = false;

%% generate the list of components

disp('generating list of components')
% if you do hpeesofsim -h, then he tells you which components and analyses he has
[~,componentsText] = ADSobj.runhpeesofsimcommand('-h');
componentsText = strsplit(componentsText,'\n').';

% the first three lines are:
% {0�0 char                                          }
% {'Available devices and analyses:'                 }
% {'Devices:                Control:'                }
% I can ignore these
componentsText = componentsText(4:end);
% just split what remains on spaces and we get the list of components
components = strsplit([componentsText{:}]);


%% now get the xml file for each component
if isfolder('xml')
    rmdir('xml','s');
end
mkdir('xml')
cd xml
for ii = 1:length(components)
    disp(['generating xml for ' components{ii}]);
    % I run this in a try catch loop because hpeesof crashes when
    % generating the xml file for the OutSelector component
    try
        ADSobj.runhpeesofsimcommand(['-x ' components{ii}]);
    end
end
cd ..

%% generate the html files
if isfolder('html')
    rmdir('html','s');
end
mkdir('html')
cd html
for ii = 1:length(components)
    disp(['generating html for ' components{ii}]);
    ADSobj.runhpeesofsimcommand(['-H ' components{ii}]);
end
cd ..



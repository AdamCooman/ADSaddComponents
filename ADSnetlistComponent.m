classdef ADSnetlistComponent
    properties 
        NetlistComponentName (1,1) string = "";
    end
    properties (Dependent)
        NetlistStatement
    end
    properties (Access=protected)
        Parameters (1,1) struct = struct();
    end
    methods
        function res = get.NetlistStatement(obj)
            res = getStartOfNetlistStatement(obj);
            % append the parameters to the netlist statement
            res = strjoin([res structfun(@param2netlist,obj.Parameters).']);
        end
    end
    methods (Access=protected,Abstract)
       	res = getStartOfNetlistStatement(obj);
    end
    methods (Access=protected)
        function obj = setParameter(obj,name,val,dims,type)
            % checks the value of a parameter and assigns it to the
            % internal parameter struct
            switch dims
                case 0
                    dimschk = 'scalar';
                case 1
                    dimschk = 'vector';
                case 2
                    dimschk = '2d';
                case 3
                    dimschk = '3d';
            end
            
            switch type
                case 'complex'
                    if ~isstring(val)
                        validateattributes(val,{'numeric'},{dimschk},class(obj),name);
                    end
                case 'real'
                    if ~isstring(val)
                        validateattributes(val,{'numeric'},{'real',dimschk},class(obj),name);
                    end
                case 'integer'
                    if ~isstring(val)
                        validateattributes(val,{'numeric'},{'integer',dimschk},class(obj),name);
                    end
                case 'boolean'
                    if ~isstring(val)
                        validateattributes(val,{'logical'},{dimschk},class(obj),name);
                    end
                case 'string'
                    validateattributes(val,{'string'},{dimschk},class(obj),name);
            end
            % we passed the check, assign the data to the Parameters struct
            obj.Parameters.(name).value = val;
            obj.Parameters.(name).type = type;
            obj.Parameters.(name).dims = dims;
            obj.Parameters.(name).name = name;
        end
        function res = getParameter(obj,name)
            if ~isfield(obj.Parameters,name)
                error('Parameter: %s has not been set, \n you can find its default value in the ADS documentation\n',name);
            else
                res = obj.Parameters.(name).value;
            end
        end
    end
end

function res = param2netlist(par)
res = repmat("",size(par.value));
switch par.dims
    case 0
        res = sprintf("%s=%s",par.name,par2ADS(par.value,par.type));
    case 1
        for ii=1:length(par.value)
            res(ii) = sprintf("%s[%d]=%s",par.name,ii,par2ADS(par.value(ii),par.type));
        end
    case 2
        for ii=1:size(par.value,1)
            for jj=1:size(par.value,2)
                res(ii,jj) = sprintf("%s[%d,%d]=%s",par.name,ii,jj,par2ADS(par.value(ii,jj),par.type));
            end
        end
    case 3
        for ii=1:size(par.value,1)
            for jj=1:size(par.value,2)
                for ll=1:size(par.value,3)
                    res(ii,jj) = sprintf("%s[%d,%d,%d]=%s",par.name,ii,jj,ll,par2ADS(par.value(ii,jj,ll),par.type));
                end
            end
        end
    otherwise
        error('unknown dimensionality');
end
res = strjoin(res(:).');
end

function val = par2ADS(val,type)
    % if it is already a string, we have no work to do
    if ~isstring(val)
        switch type
            case 'complex'
                val = sprintf("complex(%1.18e,%1.18e)",real(val),imag(val));
            case 'real'
                val = sprintf("%1.18e",val);
            case 'integer'
                val = sprintf("%d",val);
            case 'boolean'
                if val
                    val="yes";
                else
                    val = "no";
                end
            otherwise
                error('unknown type');
        end
    end
end

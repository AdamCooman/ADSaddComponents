classdef ADSmodel < ADSnetlistComponent
methods (Access=protected)
    function res = getStartOfNetlistStatement(obj)
        res = class(obj);
        res = string(res(5:end));
        if strlength(obj.NetlistComponentName)~=0
            res = strjoin(["model",obj.NetlistComponentName,res]);
        else
            error('You need to assign a name');
        end
    end
end
end

